

if (!global.hasOwnProperty('db')) {

    var mysql = require('mysql2');
    const dbConfig = require("./app-config").getDbConfig()
    let CONNECTION_POOL = process.env.CONNECTION_POOL;

    const { host, database, username, password } = dbConfig
      
    

    console.log('Connection Pool Size:' + CONNECTION_POOL)
    global.db = global.db ? global.db : mysql.createPool({
      connectionLimit : CONNECTION_POOL,
      host            : host,
      user            : username,
      password        : password,
      database        : database,
      multipleStatements: true,
      dateStrings: [
          'DATE',
          'DATETIME'
      ]
    });      
    
}

module.exports = global.db
