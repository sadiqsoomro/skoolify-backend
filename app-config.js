require('dotenv').config();



/**
 * =====================================================
 *  App configurations
 * =====================================================
 */
//process.env.PORT = 3000 commented for heroku deplyment
//process.env.DEVELOPMENT = "false";
const APP_TIMEOUT = parseInt(process.env.APP_TIMEOUT); // 10 seconds

/**
 * =====================================================
 *  TWILIO CONFIGURATIONS +15022653943
 * =====================================================
 */

const ACCOUNT_SID = process.env.ACCOUNT_SID;
const AUTH_TOKEN = process.env.AUTH_TOKEN;
const FROM_TWILIO_NUMBER = process.env.FROM_TWILIO_NUMBER;

/**
 * =====================================================
 *  JWT OPTIONS
 * =====================================================
 */

const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const JWT_EXPIRY_TIME = process.env.JWT_EXPIRY_TIME;

/**
 * =====================================================
 *  Easy Paisa Payment Configuration
 * =====================================================
 */

const EASY_PAISA_URL = process.env.EASY_PAISA_URL;
const EASY_PAISA_CREDENTIALS = process.env.EASY_PAISA_CREDENTIALS;
const EASYPAISA_STORE_ID = process.env.EASYPAISA_STORE_ID;

/**
 * =====================================================
 *  Finja/SIM SIM Payment Configuration
 * =====================================================
 */

const FINJA_API = process.env.FINJA_API;
const FINJA_MERCHANT_ID = process.env.FINJA_MERCHANT_ID;


/**
 * =====================================================
 *  User Response Messages
 * =====================================================
 */

const GENERIC_ERROR_MSG = "Sorry unable to process. Please try again !";
const INVALID_PARAMETERS = "Required fields missing!";
const EMPTY_DATA_ERROR_MSG ="Currently there is no data available for desired record!";
const USER_NO_ROLE = "please assign a role before user login";
const INVALID_OTP_ERROR_MSG ="Invalid OTP!" ;
const INVALID_API_UUID_ERROR_MSG = "Invalid API key or UUID!";
const NO_USER_ERROR_MSG = "Incorrect User/Password";

/**
 * Encryption Key
 */

const ENCRYPTION_KEY = process.env.ENCRYPTION_KEY;


/**
 * =====================================================
 *  DB CONFIG PROPERTIES
 * =====================================================
 */

 function getDbConfig() { 
    return {
      host: process.env.DB_HOST,
      database:process.env.DB_DATABASE,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    };  
  }

  /**
 * =====================================================
 *  ZOHO PARAMETER
 * =====================================================
 */

const ZOHO_REFRESH_TOKEN = process.env.ZOHO_REFRESH_TOKEN;
const ZOHO_CLIENTID = process.env.ZOHO_CLIENTID;
const ZOHO_SECRET = process.env.ZOHO_SECRET;
const ZOHO_TOKEN_URL = process.env.ZOHO_TOKEN_URL;
const ZOHO_CONTACT_URL=process.env.ZOHO_CONTACT_URL
const ZOHO_ORGID = process.env.ZOHO_ORGID;
const ZOHO_INVOICE_URL = process.env.ZOHO_INVOICE_URL;

const ENABLE_ITS_SMS = process.env.ENABLE_ITS_SMS;




const pushNotificationsettings = {
  gcm: {
    id:  process.env.FCM_KEY,
    phonegap: false // phonegap compatibility mode, see below (defaults to false)
  },
  apn: {
    token: {
      key: "./api/utils/"+process.env.P8_FILE_NAME +".p8",
      keyId: process.env.KEY_ID,
      teamId: process.env.TEAM_ID
    },
    production: process.env.APNS_PRODUCTION // true for APN production environment, false for APN sandbox environment,
  },
  isAlwaysUseFCM: false
};

const messageSettings = function(title, messageText,_screenName,_parentID,_studentID,_classID,_branchID,_schoolID,_eventDateTime) {
  return {
    title: title, // title for android
    body: messageText, // body for android
    sound: "default",
    alert: {
      title: title, // ios notification title
      body: messageText // ios notification body
    },
    custom: {
      screenName: _screenName,
      parentID: _parentID,
      studentID: _studentID,
      classID: _classID,
      branchID: _branchID,
      schoolID: _schoolID,
      eventDateTime: _eventDateTime
    },
   
    topic: "pk.infinitystudio.skoolify", // REQUIRED for iOS (apn and gcm)
    /* The topic of the notification. When using token-based authentication, specify the bundle ID of the app.
     * When using certificate-based authentication, the topic is usually your app's bundle ID.
     * More details can be found under https://developer.apple.com/documentation/usernotifications/setting_up_a_remote_notification_server/sending_notification_requests_to_apns
     */

    priority: "high", // gcm, apn. Supported values are 'high' or 'normal' (gcm). Will be translated to 10 and 5 for apn. Defaults to 'high'
    collapseKey: "", // gcm for android, used as collapseId in apn
    contentAvailable: true, // gcm, apn. node-apn will translate true to 1 as required by apn.
    delayWhileIdle: true, // gcm for android
    restrictedPackageName: "", // gcm for android
    dryRun: false, // gcm for android

    clickAction: "", // gcm for android. In ios, category will be used if not supplied

    retries: 1, // gcm, apn
    encoding: "", // apn
    badge: 2, // gcm for ios, apn

    truncateAtWordEnd: true, // apn and gcm for ios
    mutableContent: 0, // apn
    threadId: "", // apn
    expiry: Math.floor(Date.now() / 1000) + 28 * 86400, // seconds
    timeToLive: 28 * 86400 // if both expiry and timeToLive are given, expiry will take precedency
  };
};
//push
module.exports = {
  getDbConfig,
  JWT_SECRET_KEY,
  GENERIC_ERROR_MSG,
  JWT_EXPIRY_TIME,
  APP_TIMEOUT,
  ACCOUNT_SID,
  AUTH_TOKEN,
  FROM_TWILIO_NUMBER,
  INVALID_PARAMETERS,
  EMPTY_DATA_ERROR_MSG,
  USER_NO_ROLE,
  NO_USER_ERROR_MSG,
  INVALID_OTP_ERROR_MSG,
  INVALID_API_UUID_ERROR_MSG,
  ENCRYPTION_KEY,
  messageSettings,
  pushNotificationsettings,
  EASY_PAISA_URL,
  EASY_PAISA_CREDENTIALS,
  EASYPAISA_STORE_ID,
  FINJA_MERCHANT_ID,
  FINJA_API,
  ZOHO_TOKEN_URL,
  ZOHO_CONTACT_URL,
  ZOHO_REFRESH_TOKEN,
  ZOHO_CLIENTID,
  ZOHO_SECRET,
  ZOHO_ORGID,
  ZOHO_INVOICE_URL,
  ENABLE_ITS_SMS,
};

// Invoice Prefix
// SFV- Student Fee Voucher
// SSC - School Service Charges
// MPB - Market Place Bill