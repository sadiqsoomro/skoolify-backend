module.exports = function (app) {
    const messageController = require('../controllers/controllers-message')

    app.post('/v1/getMessages', function (req, res) {
        messageController.getMessages(req, res)
    })

    app.post('/v1/getStudentMessages', function (req, res) {
        messageController.getStudentMessages(req, res)
    })
    app.post('/v1/getMessagesSummary', function (req, res) {
        messageController.getMessagesSummary(req, res)
    })

    app.post('/v1/deleteMessages', function (req, res) {
        messageController.deleteMessages(req, res)
    })

    

    app.post('/v1/setMessagesArray', function (req, res) {
        messageController.setMessagesArray(req, res)
    })


    app.post('/v1/setConsent', function (req, res) {
        messageController.setConsent(req, res)
    })
    
    app.post('/v1/getNotificationCount', function (req, res) {
        messageController.getNotificationCount(req, res)
    })

    app.post('/v1/getMessageDetails', function (req, res) {
        messageController.getMessageDetails(req, res)
    })

    app.post('/v1/setMessageIsDelivered', function (req, res) {
        messageController.setMessageIsDelivered(req, res)
    })

    app.post('/v1/setMessageIsRead', function (req, res) {
        messageController.setMessageIsRead(req, res)
    })
    app.post('/v1/getConsentReportExport', function (req, res) {
        messageController.getConsentReportExport(req, res)
    })
    

}