module.exports = function (app) {
    const subjectController = require('../controllers/controllers-subject')

    app.post('/v1/getSubjects', function (req, res) {
        subjectController.getSubjects(req, res)
    })

    app.post('/v1/getActiveSubjects', function (req, res) {
        subjectController.getActiveSubjects(req, res)
    })

    app.post('/v1/getExamSubjects', function (req, res) {
        subjectController.getExamSubjects(req, res)
    })


    app.post('/v1/getSubjectsTeachers', function (req, res) {
        subjectController.getSubjectsTeachers(req, res)
    })

    app.post('/v1/getUserAllSubjects', function (req, res) {
        subjectController.getUserAllSubjects(req, res)
    })

    app.post('/v1/getUserExamSubjects', function (req, res) {
        subjectController.getUserExamSubjects(req, res)
    })

    app.post('/v1/getNonSubjects', function (req, res) {
        subjectController.getNonSubjects(req, res)
    })

    app.post('/v1/getActiveNonSubjects', function (req, res) {
        subjectController.getActiveNonSubjects(req, res)
    })


    app.post('/v1/deleteNonSubject', function (req, res) {
        subjectController.deleteNonSubject(req, res)
    })

    app.post('/v1/getClassSubjects', function (req, res) {
        subjectController.getClassSubjects(req, res)
    })

    app.post('/v1/setSubjects', function (req, res) {
        subjectController.setSubjects(req, res)
    })

    app.post('/v1/setSubjectsArray', function (req, res) {
        subjectController.setSubjectsArray(req, res)
    })

    app.post('/v1/setNonSubjects', function (req, res) {
        subjectController.setNonSubjects(req, res)
    })

  
    app.post('/v1/getNonSubjectLegends', function (req, res) {
        subjectController.getNonSubjectLegends(req, res)
    })
    app.post('/v1/setNonSubjectLegends', function (req, res) {
        subjectController.setNonSubjectLegends(req, res)
    })
    app.post('/v1/setDelinkSubjectTeacher', function (req, res) {
        subjectController.setDelinkSubjectTeacher(req, res)
    })
    app.post('/v1/deleteSubject', function (req, res) {
        subjectController.deleteSubject(req, res)
    })

    app.post('/v1/getUserSubjects', function (req, res) {
        subjectController.getUserSubjects(req, res)
    })
    app.post('/v1/getUserSubjects2', function (req, res) {
        subjectController.getUserSubjects2(req, res)
    })

    app.post('/v1/getUserNonSubjects', function (req, res) {
        subjectController.getUserNonSubjects(req, res)
    })
    app.post('/v1/getUserNonSubjectsWithTeacher', function (req, res) {
        subjectController.getUserNonSubjectsWithTeacher(req, res)
    })

}
