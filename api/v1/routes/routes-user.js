module.exports = function (app) {
    const userController = require('../controllers/controllers-user')

    app.post('/v1/userValidation', function (req, res) {
        userController.userValidation(req, res)
    })

    app.post('/v1/userDetails', function (req, res) {
        userController.userDetails(req, res)
    })
    app.post('/v1/getUserModules', function (req, res) {
        userController.getUserModules(req, res)
    })
    app.post('/v1/getUserList', function (req, res) {
        userController.getUserList(req, res)
    })

    app.post('/v1/getUserRoleList', function (req, res) {
        userController.getUserRoleList(req, res)
    })

    
    app.post('/v1/setUser', function (req, res) {
        userController.setUser(req, res)
    })

    // app.post('/v1/setUsersArray', function (req, res) {
    //     userController.setUsersArray(req, res)
    // })
    app.post('/v1/getRoleLinkedUsers', function (req, res) {
        userController.getRoleLinkedUsers(req, res)
    })
    app.post('/v1/getActiveUserList', function (req, res) {
        userController.getActiveUserList(req, res)
    })
    
}