module.exports = function (app) {
    const examController = require('../controllers/controllers-exam')


    app.post('/v1/getClassTerms', function (req, res) {
        examController.getClassTerms(req, res)
    })

    app.post('/v1/getExamTerm', function (req, res) {
        examController.getExamTerm(req, res)
    })

    app.post('/v1/getActiveExamTerm', function (req, res) {
        examController.getActiveExamTerm(req, res)
    })


    app.post('/v1/deleteExamTerm', function (req, res) {
        examController.deleteExamTerm(req, res)
    })
    

    app.post('/v1/getExamResults', function (req, res) {
        examController.getExamResults(req, res)
    })
    app.post('/v1/getAssessmentResults', function (req, res) {
        examController.getAssessmentResults(req, res)
    })
    
    app.post('/v1/getQuizResults', function (req, res) {
        examController.getQuizResults(req, res)
    })

    app.post('/v1/getQuizGraphResults', function (req, res) {
        examController.getQuizGraphResults(req, res)
    })

    app.post('/v1/setQuizResults', function (req, res) {
        examController.setQuizResults(req, res)
    })

    app.post('/v1/setQuizResultsArray', function (req, res) {
        examController.setQuizResultsArray(req, res)
    })

    // app.post('/v1/getAssessment', function (req, res) {
    //     examController.getAssessment(req, res)
    // })

    // app.post('/v1/setAssessment', function (req, res) {
    //     examController.setAssessment(req, res)
    // })

    // app.post('/v1/setAssessmentDetails', function (req, res) {
    //     examController.setAssessmentDetails(req, res)
    // })

    app.post('/v1/setExamNonSubjectResults', function (req, res) {
        examController.setExamNonSubjectResults(req, res)
    })

    app.post('/v1/setExamSubjectResults', function (req, res) {
        examController.setExamSubjectResults(req, res)
    })
    app.post('/v1/setClassChange', function (req, res) {
        examController.setClassChange(req, res)
    })
    app.post('/v1/setClassChangeArray', function (req, res) {
        examController.setClassChangeArray(req, res)
    })

    
    app.post('/v1/setExamSubjectResultsArray', function (req, res) {
        examController.setExamSubjectResultsArray(req, res)
    })

    app.post('/v1/setExamNonSubjectResultsArray', function (req, res) {
        examController.setExamNonSubjectResultsArray(req, res)
    })
    app.post('/v1/setExamTermResults', function (req, res) {
        examController.setExamTermResults(req, res)
    })
    app.post('/v1/setExamTermResultsArray', function (req, res) {
        examController.setExamTermResultsArray(req, res)
    })

    app.post('/v1/setExamTerm', function (req, res) {
        examController.setExamTerm(req, res)
    })

    app.post('/v1/assignClassTerm', function (req, res) {
        examController.assignClassTerm(req, res)
    })

    app.post('/v1/getExamOverallResult', function (req, res) {
        examController.getExamOverallResult(req, res)
    })
    app.post('/v1/getExamSubjectResult', function (req, res) {
        examController.getExamSubjectResult(req, res)
    })
    app.post('/v1/getExamNSResult', function (req, res) {
        examController.getExamNSResult(req, res)
    })

    app.post('/v1/deleteFeeChallan', function (req, res) {
        examController.deleteFeeChallan(req, res)
    })

    app.post('/v1/setExamTermStatus', function (req, res) {
        examController.setExamTermStatus(req, res)
    })

    app.post('/v1/deleteExamSubjectResult', function (req, res) {
        examController.deleteExamSubjectResult(req, res)
    })

    app.post('/v1/deleteExamNonSubjectResult', function (req, res) {
        examController.deleteExamNonSubjectResult(req, res)
    })

    app.post('/v1/getExamResultReport', function (req, res) {
        examController.getExamResultReport(req, res)
    })
    app.post('/v1/deleteQuiz', function (req, res) {
        examController.deleteQuiz(req, res)
    })
    app.post('/v1/getExamResultsMobile', function (req, res) {
        examController.getExamResultsMobile(req, res)
    })
    
}   
