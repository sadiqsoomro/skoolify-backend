module.exports = function (app) {
    const adsController = require('../controllers/controllers-ads')

    app.post('/v1/getAllAds', function (req, res) {
        adsController.getAllAds(req, res)
    })

    app.post('/v1/getTopAds', function (req, res) {
        adsController.getTopAds(req, res)
    })
    
    app.get('/v1/getDateTest', function (req, res) {
        adsController.getDateTest(req, res)
    })
    
}