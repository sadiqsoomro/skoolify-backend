module.exports = function (app) {
    const diaryController = require('../controllers/controllers-diary')

    app.post('/v1/getDailyDiaryCount', function (req, res) {
        diaryController.getDailyDiaryCount(req, res)
    })

    // app.post('/v1/getDailyDiary', function (req, res) {
    //     diaryController.getDailyDiary(req, res)
    // })

    
    // app.post('/v1/setDailyDiary', function (req, res) {
    //     diaryController.setDailyDiary(req, res)
    // })

    app.post('/v1/setDailyDiaryArray', function (req, res) {
        diaryController.setDailyDiaryArray(req, res)
    })

    
    app.post('/v1/deleteDailyDiary', function (req, res) {
        diaryController.deleteDailyDiary(req, res)
    })

    app.post('/v1/getDailyDiarySummary', function (req, res) {
        diaryController.getDailyDiarySummary(req, res)
    })

    app.post('/v1/getStudentDailyDiary', function (req, res) {
        diaryController.getStudentDailyDiary(req, res)
    })
    app.post('/v1/getDailyDiaryListing', function (req, res) {
        diaryController.getDailyDiaryListing(req, res)
    })
}