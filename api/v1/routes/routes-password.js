module.exports = function (app) {
    const passwordController = require('../controllers/controllers-password')

    app.post('/v1/passwordValidation', function (req, res) {
        passwordController.passwordValidation(req, res)
    })

    app.post('/v1/changePassword', function (req, res) {
        passwordController.changePassword(req, res)
    })

    app.post('/v1/resetPassword', function (req, res) {
        passwordController.resetPassword(req, res)
    })
    
}