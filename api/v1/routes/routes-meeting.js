module.exports = function (app) {
    const meetingController = require('../controllers/controllers-meeting')

    app.post('/v1/setMeetingInvite', function (req, res) {
        meetingController.setMeetingInvite(req, res)
    })

    app.post('/v1/getMeetingInvite', function (req, res) {
        meetingController.getMeetingInvite(req, res)
    })
    
    app.post('/v1/getUserMeetingInvite', function (req, res) {
        meetingController.getUserMeetingInvite(req, res)
    })
    app.post('/v1/getMeetingDetails', function (req, res) {
        meetingController.getMeetingDetails(req, res)
    })
    app.post('/v1/deleteMeetingOccurence', function (req, res) {
        meetingController.deleteMeetingOccurence(req, res)
    })
    app.post('/v1/deleteMeetingInvite', function (req, res) {
        meetingController.deleteMeetingInvite(req, res)
    })
    
    app.post('/v1/getMeetingInvitees', function (req, res) {
        meetingController.getMeetingInvitees(req, res)
    })

    app.post('/v1/getMeetingJoin', function (req, res) {
        meetingController.getMeetingJoin(req, res)
    })
    app.post('/v1/setMeetingSession', function (req, res) {
        meetingController.setMeetingSession(req, res)
    })
    app.post('/v1/getMeetingRecurrence', function (req, res) {
        meetingController.getMeetingRecurrence(req, res)
    })
}