module.exports = function (app) {
    const tabletController = require('../controllers/controllers-tablet')

    app.post('/v1/getUserTablet', function (req, res) {
        tabletController.getUserTablet(req, res)
    })
    app.post('/v1/getUserPasswordTablet', function (req, res) {
        tabletController.getUserPasswordTablet(req, res)
    })
    app.post('/v1/getTimeTableTablet', function (req, res) {
        tabletController.getTimeTableTablet(req, res)
    })
    app.post('/v1/getClassesTablet', function (req, res) {
        tabletController.getClassesTablet(req, res)
    })
    app.post('/v1/getTeacherTablet', function (req, res) {
        tabletController.getTeacherTablet(req, res)
    })
    app.post('/v1/getRoomTablet', function (req, res) {
        tabletController.getRoomTablet(req, res)
    })
    app.post('/v1/getSubjectTablet', function (req, res) {
        tabletController.getSubjectTablet(req, res)
    })
    app.post('/v1/setStudentAttendanceTablet', function (req, res) {
        tabletController.setStudentAttendanceTablet(req, res)
    })
    app.post('/v1/getClassStudentsTablet', function (req, res) {
        tabletController.getClassStudentsTablet(req, res)
    })
    
}