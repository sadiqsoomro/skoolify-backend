module.exports = function (app) {
    const generalController = require('../controllers/controllers-general')

    app.post('/v1/getParent', function (req, res) {
        generalController.getParent(req, res)
    })

    app.post('/v1/getActiveParent', function (req, res) {
        generalController.getActiveParent(req, res)
    })

    app.post('/v1/getParentByStudentID', function (req, res) {
        generalController.getParentByStudentID(req, res)
    })

    app.post('/v1/setParents', function (req, res) {
        generalController.setParents(req, res)
    })

    app.post('/v1/getCountries', function (req, res) {
        generalController.getCountries(req, res)
    })
    
    app.post('/v1/getActiveCountries', function (req, res) {
        generalController.getActiveCountries(req, res)
    })

    app.post('/v1/setCountries', function (req, res) {
        generalController.setCountries(req, res)
    })

    app.post('/v1/getCities', function (req, res) {
        generalController.getCities(req, res)
    })

    app.post('/v1/getActiveCities', function (req, res) {
        generalController.getActiveCities(req, res)
    })

    app.post('/v1/setCities', function (req, res) {
        generalController.setCities(req, res)
    })

    app.post('/v1/getAreas', function (req, res) {
        generalController.getAreas(req, res)
    })    
    
    app.post('/v1/getActiveAreas', function (req, res) {
        generalController.getActiveAreas(req, res)
    }) 

    app.post('/v1/setAreas', function (req, res) {
        generalController.setAreas(req, res)
    })   
    
    app.post('/v1/setParentStudentLink', function (req, res) {
        generalController.setParentStudentLink(req, res)
    }) 

    app.post('/v1/setParentStudentDeLink', function (req, res) {
        generalController.setParentStudentDeLink(req, res)
    })  
    app.post('/v1/deleteParent', function (req, res) {
        generalController.deleteParent(req, res)
    })
    app.post('/v1/UploadParentStudentArray', function (req, res) {
        generalController.UploadParentStudentArray(req, res)
    })
    app.post('/v1/setTraceDump', function (req, res) {
        generalController.setTraceDump(req, res)
    })

    app.post('/v1/setEncryptData', function (req, res) {
        generalController.setEncryptData(req, res)
    })

    app.post('/v1/setDecryptData', function (req, res) {
        generalController.setDecryptData(req, res)
    })
    app.post('/v1/setDeviceToken', function (req, res) {
        generalController.setDeviceToken(req, res)
    })   
    app.post('/v1/getTimezoneList', function (req, res) {
        generalController.getTimezoneList(req, res)
    }) 
    app.post('/v1/getOTPDetails', function (req, res) {
        generalController.getOTPDetails(req, res)
    }) 
    app.post('/v1/setParentMobile', function (req, res) {
        generalController.setParentMobile(req, res)
    }) 
    app.post('/v1/setPaymentProviders', function (req, res) {
        generalController.setPaymentProviders(req, res)
    }) 
    app.post('/v1/getPaymentProviderList', function (req, res) {
        generalController.getPaymentProviderList(req, res)
    }) 
    app.post('/v1/getParentMobile', function (req, res) {
    generalController.getParentMobile(req, res)
    }) 
    
}

