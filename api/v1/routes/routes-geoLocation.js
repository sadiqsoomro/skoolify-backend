module.exports = function (app) {
    const geoLocationController = require('../controllers/controllers-geoLocation')


    app.post('/v1/getIP2Location', function (req, res) {
        geoLocationController.getIP2Location(req, res)
    })
}   
