const authController = require('../controllers/controllers-auth')

module.exports = function (app) {

    app.get('/', function(req, res) {
        //res.send('loaderio-ca5a9269f6f642e28887199e276dae37')
        res.redirect('https://www.skoolify.app')
    })

    // app.get('/loaderio-ca5a9269f6f642e28887199e276dae37', function(req, res) {
    //     res.send('loaderio-ca5a9269f6f642e28887199e276dae37')
    // })

    app.post('/v1/getToken', function (req, res) {
        authController.getToken(req, res)
    })

    app.use(function (req, res, next) {
        authController.validateRequestToken(req, res, next)
    })

    app.post('/v1/generateOTP', function (req, res) {
        authController.generateOTP(req, res)
    })


    app.post('/v1/verifyOTP', function (req, res) {
        authController.verifyOTP(req, res)
    })

    app.post('/v1/getTnC', function (req, res) {
        authController.getTnC(req, res)
    })
    app.post('/v1/setTnC', function (req, res) {
        authController.setTnC(req, res)
    })

    app.post('/v1/getAppTnC', function (req, res) {
        authController.getAppTnC(req, res)
    })

    app.post('/v1/setAppAcceptTnC', function (req, res) {
        authController.setAppAcceptTnC(req, res)
    })
    

    app.post('/v1/getContract', function (req, res) {
        authController.getContract(req, res)
    })
    app.post('/v1/setContract', function (req, res) {
        authController.setContract(req, res)
    })
    app.post('/v1/setTokenExpiry', function (req, res) {
        authController.setTokenExpiry(req, res)
    })
    app.post('/v1/setSMSSendArray', function (req, res) {
        authController.setSMSSendArray(req, res)
    })
    // app.post('/v1/getVideoToken', function (req, res) {
    //     authController.getVideoToken(req, res)
    // })

    app.post('/v1/setVerifyOTP', function (req, res) {
        authController.setVerifyOTP(req, res)
    })
    app.post('/v1/generateTextVoiceOTP', function (req, res) {
        authController.generateTextVoiceOTP(req, res)
    })
    
}
