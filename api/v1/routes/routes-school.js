module.exports = function (app) {
    const schoolController = require('../controllers/controllers-school')

    app.post('/v1/getcalendarDays', function (req, res) {
        schoolController.getcalendarDays(req, res)
    })

    app.post('/v1/setCalendarDaysArray', function (req, res) {
        schoolController.setCalendarDaysArray(req, res)
    })

    app.post('/v1/setCommsRequest', function (req, res) {
        schoolController.setCommsRequest(req, res)
    })

    app.post('/v1/getSchoolLogo', function (req, res) {
        schoolController.getSchoolLogo(req, res)
    })

    app.post('/v1/setSchools', function (req, res) {
        schoolController.setSchools(req, res)
    })
    
    app.post('/v1/deleteSchoolBranch', function (req, res) {
        schoolController.deleteSchoolBranch(req, res)
    })

    
    app.post('/v1/deleteSchool', function (req, res) {
        schoolController.deleteSchool(req, res)
    })
    
   
    app.post('/v1/getSchools', function (req, res) {
        schoolController.getSchools(req, res)
    })

    app.post('/v1/getSchoolBranches', function (req, res) {
        schoolController.getSchoolBranches(req, res)
    })

    app.post('/v1/getActiveBranches', function (req, res) {
        schoolController.getActiveBranches(req, res)
    })

    app.post('/v1/getFeeTypes', function (req, res) {
        schoolController.getFeeTypes(req, res)
    })

    app.post('/v1/getActiveFeeTypes', function (req, res) {
        schoolController.getActiveFeeTypes(req, res)
    })
    
    app.post('/v1/getClassLevels', function (req, res) {
        schoolController.getClassLevels(req, res)
    })

    app.post('/v1/getActiveClassLevels', function (req, res) {
        schoolController.getActiveClassLevels(req, res)
    })


    app.post('/v1/getAllClassStudents', function (req, res) {
        schoolController.getAllClassStudents(req, res)
    })    
    
    app.post('/v1/getClassStudents', function (req, res) {
        schoolController.getClassStudents(req, res)
    })

    app.post('/v1/getClassActiveStudents', function (req, res) {
        schoolController.getClassActiveStudents(req, res)
    })

    app.post('/v1/getClasses', function (req, res) {
        schoolController.getClasses(req, res)
    })

    app.post('/v1/getActiveClasses', function (req, res) {
        schoolController.getActiveClasses(req, res)
    })


    app.post('/v1/getUserClasses', function (req, res) {
        schoolController.getUserClasses(req, res)
    })

 

    app.post('/v1/getUserClassLevels', function (req, res) {
        schoolController.getUserClassLevels(req, res)
    })

    app.post('/v1/setClasses', function (req, res) {
        schoolController.setClasses(req, res)
    })

    app.post('/v1/getCommRequest', function (req, res) {
        schoolController.getCommRequest(req, res)
    })
    app.post('/v1/setCommsIsRead', function (req, res) {
        schoolController.setCommsIsRead(req, res)
    })
 
    app.post('/v1/getCommsCount', function (req, res) {
        schoolController.getCommsCount(req, res)
    })
      
    app.post('/v1/setSchoolBranches', function (req, res) {
        schoolController.setSchoolBranches(req, res)
    })

    // app.post('/v1/setSchoolBranchesArray', function (req, res) {
    //     schoolController.setSchoolBranchesArray(req, res)
    // })
    
    app.post('/v1/setClassLevels', function (req, res) {
        schoolController.setClassLevels(req, res)
    })

    app.post('/v1/getBillingPeriod', function (req, res) {
        schoolController.getBillingPeriod(req, res)
    })

    app.post('/v1/getActiveBillingPeriod', function (req, res) {
        schoolController.getActiveBillingPeriod(req, res)
    })

    app.post('/v1/setBillingPeriod', function (req, res) {
        schoolController.setBillingPeriod(req, res)
    })


    app.post('/v1/deleteBillingPeriod', function (req, res) {
        schoolController.deleteBillingPeriod(req, res)
    })


    app.post('/v1/getStudentLevelFee', function (req, res) {
        schoolController.getStudentLevelFee(req, res)
    })

    app.post('/v1/getReportRegistrations', function (req, res) {
        schoolController.getReportRegistrations(req, res)
    })
    app.post('/v1/getReportDatewiseRegistrations', function (req, res) {
        schoolController.getReportDatewiseRegistrations(req, res)
    })
    
    app.post('/v1/getCalendarEvents', function (req, res) {
        schoolController.getCalendarEvents(req, res)
    })

    app.post('/v1/getActiveSchools', function (req, res) {
        schoolController.getActiveSchools(req, res)
    })

    app.post('/v1/deleteCalendarDays', function (req, res) {
        schoolController.deleteCalendarDays(req, res)
    })
    app.post('/v1/getCalendarHolidays', function (req, res) {
        schoolController.getCalendarHolidays(req, res)
    })

    app.post('/v1/deleteClass', function (req, res) {
        schoolController.deleteClass(req, res)
    })

    app.post('/v1/deleteClassLevel', function (req, res) {
        schoolController.deleteClassLevel(req, res)
    })
    app.post('/v1/setClassRoom', function (req, res) {
        schoolController.setClassRoom(req, res)
    })

    app.post('/v1/getClassRoom', function (req, res) {
        schoolController.getClassRoom(req, res)
    })

    app.post('/v1/getActiveClassRoom', function (req, res) {
        schoolController.getActiveClassRoom(req, res)
    })
    app.post('/v1/deleteClassRoom', function (req, res) {
        schoolController.deleteClassRoom(req, res)
    })
    app.post('/v1/getSchoolContract', function (req, res) {
        schoolController.getSchoolContract(req, res)
    })
    app.post('/v1/setContractAcceptance', function (req, res) {
        schoolController.setContractAcceptance(req, res)
    })   
    app.post('/v1/deleteContract', function (req, res) {
        schoolController.deleteContract(req, res)
    })
     app.post('/v1/getUserCommunications', function (req, res) {
        schoolController.getUserCommunications(req, res)
    })
    
    app.post('/v1/getBranchClasses', function (req, res) {
        schoolController.getBranchClasses(req, res)
    })    
    app.post('/v1/getAcademicEvents', function (req, res) {
        schoolController.getAcademicEvents(req, res)
    })    
    app.post('/v1/deleteAcademicEvents', function (req, res) {
        schoolController.deleteAcademicEvents(req, res)
    }) 
    
    app.post('/v1/setAcademicEventsArray', function (req, res) {
        schoolController.setAcademicEventsArray(req, res)
    }) 
    app.post('/v1/getCalendarList', function (req, res) {
        schoolController.getCalendarList(req, res)
    })  
    // app.post('/v1/setContractChargesArray', function (req, res) {
    //     schoolController.setContractChargesArray(req, res)
    // })   
    // app.post('/v1/getContractCharges', function (req, res) {
    //     schoolController.getContractCharges(req, res)
    // }) 
    
    app.post('/v1/deleteSchoolContract', function (req, res) {
        schoolController.deleteSchoolContract(req, res)
    }) 
    app.post('/v1/deleteFeeType', function (req, res) {
        schoolController.deleteFeeType(req, res)
    })    
    app.post('/v1/getBranchGroups', function (req, res) {
        schoolController.getBranchGroups(req, res)
    })
    app.post('/v1/getSelfJoinSchools', function (req, res) {
        schoolController.getSelfJoinSchools(req, res)
    })  
    
    app.post('/v1/getClassesCombined', function (req, res) {
        schoolController.getClassesCombined(req, res)
    })  
    
    app.post('/v1/getContractGroupCharges', function (req, res) {
        schoolController.getContractGroupCharges(req, res)
    })     
    app.post('/v1/setContractGroupCharges', function (req, res) {
        schoolController.setContractGroupCharges(req, res)
    }) 
    app.post('/v1/getContractCollection', function (req, res) {
        schoolController.getContractCollection(req, res)
    })
    app.post('/v1/setContractCollectionArray', function (req, res) {
        schoolController.setContractCollectionArray(req, res)
    }) 
    app.post('/v1/getSettlementSchoolReport', function (req, res) {
        schoolController.getSettlementSchoolReport(req, res)
    }) 
    app.post('/v1/setSettlementVerification', function (req, res) {
        schoolController.setSettlementVerification(req, res)
    }) 
    
    
}
