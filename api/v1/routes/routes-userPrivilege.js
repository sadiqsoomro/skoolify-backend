module.exports = function (app) {
    const userPrivilegeController = require('../controllers/controllers-userPrivilege')

    app.post('/v1/getRoles', function (req, res) {
        userPrivilegeController.getRoles(req, res)
    })   
    app.post('/v1/deleteRole', function (req, res) {
        userPrivilegeController.deleteRole(req, res)
    })  

    app.post('/v1/getUserAssignedRoles', function (req, res) {
        userPrivilegeController.getUserAssignedRoles(req, res)
    })   

    app.post('/v1/getRoleAccessRights', function (req, res) {
        userPrivilegeController.getRoleAccessRights(req, res)
    })   

    app.post('/v1/setRole', function (req, res) {
        userPrivilegeController.setRole(req, res)
    })   

    app.post('/v1/setAssignRoles', function (req, res) {
        userPrivilegeController.setAssignRoles(req, res)
    })   
    app.post('/v1/setDeAssignRoles', function (req, res) {
        userPrivilegeController.setDeAssignRoles(req, res)
    })  
    
    app.post('/v1/setRoleAccessRights', function (req, res) {
        userPrivilegeController.setRoleAccessRights(req, res)
    })

    app.post('/v1/setRoleAccessRightsArray', function (req, res) {
        userPrivilegeController.setRoleAccessRightsArray(req, res)
    })

    app.post('/v1/setAssignRolesArray', function (req, res) {
        userPrivilegeController.setAssignRolesArray(req, res)
    })

}