module.exports = function (app) {
    const teacherController = require('../controllers/controllers-teacher')

    app.post('/v1/assignTeacher', function (req, res) {
        teacherController.assignTeacher(req, res)
    })

    app.post('/v1/deleteSubjectTeacher', function (req, res) {
        teacherController.deleteSubjectTeacher(req, res)
    })

    app.post('/v1/assignNSTeacher', function (req, res) {
        teacherController.assignNSTeacher(req, res)
    })

    app.post('/v1/deleteNSTeacher', function (req, res) {
        teacherController.deleteNSTeacher(req, res)
    })

}
