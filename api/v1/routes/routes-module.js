module.exports = function (app) {
    const moduleController = require('../controllers/controllers-module')

    app.post('/v1/getModules', function (req, res) {
        moduleController.getModules(req, res)
    })

    app.post('/v1/getActiveModules', function (req, res) {
        moduleController.getActiveModules(req, res)
    })

    app.post('/v1/setModule', function (req, res) {
        moduleController.setModules(req, res)
    })
    
}