module.exports = function (app) {
    const studentController = require('../controllers/controllers-student')

    app.post('/v1/getStudents', function (req, res) {
        studentController.getStudents(req, res)
    })
    app.post('/v1/setStudents', function (req, res) {
        studentController.setStudents(req, res)
    })
    app.post('/v1/setStudentsArray', function (req, res) {
        studentController.setStudentsArray(req, res)
    })

    app.post('/v1/getStudentFees', function (req, res) {
        studentController.getStudentFees(req, res)
    })


    app.post('/v1/getStudentOverallResults', function (req, res) {
        studentController.getStudentOverallResults(req, res)
    })

    app.post('/v1/setStudentFees', function (req, res) {
        studentController.setStudentFees(req, res)
    })

    app.post('/v1/setFeeVoucherGenerate', function (req, res) {
        studentController.setFeeVoucherGenerate(req, res)
    })
    app.post('/v1/setFeeVoucher', function (req, res) {
        studentController.setFeeVoucher(req, res)
    })
    

    app.post('/v1/deleteFeeChallanDetail', function (req, res) {
        studentController.deleteFeeChallanDetail(req, res)
    })
    app.post('/v1/setFeeTypes', function (req, res) {
        studentController.setFeeTypes(req, res)
    })

    app.post('/v1/setFeeStatus', function (req, res) {
        studentController.setFeeStatus(req, res)
    })

    // app.post('/v1/setFeeDetails', function (req, res) {
    //     studentController.setFeeDetails(req, res)
    // })

    app.post('/v1/getAttendance', function (req, res) {
        studentController.getAttendance(req, res)
    })

    app.post('/v1/getStudentAttendance', function (req, res) {
        studentController.getStudentAttendance(req, res)
    })

    // app.post('/v1/setAttendance', function (req, res) {
    //     studentController.setAttendance(req, res)
    // })

    app.post('/v1/setAttendanceArray', function (req, res) {
        studentController.setAttendanceArray(req, res)
    })

    app.post('/v1/setStudentImage', function (req, res) {
        studentController.setStudentImage(req, res)
    })
    
    app.post('/v1/getStudentsbyParentID', function (req, res) {
        studentController.getStudentsbyParentID(req, res)
    })

    app.post('/v1/getStudentSearch', function (req, res) {
        studentController.getStudentSearch(req, res)
    })


    app.post('/v1/setRecurringFeeArray', function (req, res) {
        studentController.setRecurringFeeArray(req, res)
    })
    app.post('/v1/getRecurringFee', function (req, res) {
        studentController.getRecurringFee(req, res)
    })

    app.post('/v1/getFeeVoucher', function (req, res) {
        studentController.getFeeVoucher(req, res)
    })

    app.post('/v1/getFeeVoucherDetail', function (req, res) {
        studentController.getFeeVoucherDetail(req, res)
    })

    app.post('/v1/setFeeVoucherDelete', function (req, res) {
        studentController.setFeeVoucherDelete(req, res)
    })
    app.post('/v1/getFeesVoucherPrint', function (req, res) {
        studentController.getFeesVoucherPrint(req, res)
    })

    app.post('/v1/getAttendanceReport', function (req, res) {
        studentController.getAttendanceReport(req, res)
    })

    app.post('/v1/setSubjectAttendanceArray', function (req, res) {
        studentController.setSubjectAttendanceArray(req, res)
    })

    // app.post('/v1/setSubjectAttendance', function (req, res) {
    //     studentController.setSubjectAttendance(req, res)
    // })
    app.post('/v1/getStudentSubjectAttendance', function (req, res) {
        studentController.getStudentSubjectAttendance(req, res)
    })
        app.post('/v1/assignSubjectToStudentArray', function (req, res) {
        studentController.assignSubjectToStudentArray(req, res)
    })
    app.post('/v1/getStudentSubjects', function (req, res) {
        studentController.getStudentSubjects(req, res)
    })

    
    app.post('/v1/deleteSubjectToStudentArray', function (req, res) {
        studentController.deleteSubjectToStudentArray(req, res)
    }) 
    app.post('/v1/setStudentTimeTableLinkArray', function (req, res) {
        studentController.setStudentTimeTableLinkArray(req, res)
    }) 
    app.post('/v1/setStudentTimeTableDelink', function (req, res) {
        studentController.setStudentTimeTableDelink(req, res)
    })  
    app.post('/v1/getStudentTimeTableLinked', function (req, res) {
        studentController.getStudentTimeTableLinked(req, res)
    })  
    app.post('/v1/getActiveSubjectsInTimetable', function (req, res) {
        studentController.getActiveSubjectsInTimetable(req, res)
    }) 
    app.post('/v1/getStudentsInTimetable', function (req, res) {
        studentController.getStudentsInTimetable(req, res)
    }) 
    app.post('/v1/getStudentOwnSubjects', function (req, res) {
        studentController.getStudentOwnSubjects(req, res)
    })
    app.post('/v1/getStudentDailyAttendance', function (req, res) {
        studentController.getStudentDailyAttendance(req, res)
    })
    
    app.post('/v1/setFeeVoucherStatus', function (req, res) {
        studentController.setFeeVoucherStatus(req, res)
    }) 
    app.post('/v1/setStudentNotesTag', function (req, res) {
        studentController.setStudentNotesTag(req, res)
    })
    app.post('/v1/setStudentNotes', function (req, res) {
        studentController.setStudentNotes(req, res)
    })
    app.post('/v1/getActiveStudentTags', function (req, res) {
        studentController.getActiveStudentTags(req, res)
    })
    app.post('/v1/getStudentTags', function (req, res) {
        studentController.getStudentTags(req, res)
    })
    app.post('/v1/getStudentNotes', function (req, res) {
        studentController.getStudentNotes(req, res)
    })
    app.post('/v1/deleteStudentNotes', function (req, res) {
        studentController.deleteStudentNotes(req, res)
    })
    app.post('/v1/deleteStudentNotesTag', function (req, res) {
        studentController.deleteStudentNotesTag(req, res)
    })
    app.post('/v1/getParentAndInsert', function (req, res) {
        studentController.getParentAndInsert(req, res)
    })
    app.post('/v1/setStudentSelfJoin', function (req, res) {
        studentController.setStudentSelfJoin(req, res)
    }) 

    app.post('/v1/deleteStudentSelfJoin', function (req, res) {
        studentController.deleteStudentSelfJoin(req, res)
    }) 
    app.post('/v1/getAttendanceMobile', function (req, res) {
        studentController.getAttendanceMobile(req, res)
    })
    app.post('/v1/getFeeVoucherExport', function (req, res) {
        studentController.getFeeVoucherExport(req, res)
    })
    // app.post('/v1/setParentLanguage', function (req, res) {
    //     studentController.setParentLanguage(req, res)
    // })
    
}


