module.exports = function (app) {
    const timeTableController = require('../controllers/controllers-timetable')

    // app.post('/v1/getTimeTableCount', function (req, res) {
    //     timeTableController.getTimeTableCount(req, res)
    // })

    app.post('/v1/getTimeTable', function (req, res) {
        timeTableController.getTimeTable(req, res)
    })

    app.post('/v1/setTimeTable', function (req, res) {
        timeTableController.setTimeTable(req, res)
    })

    app.post('/v1/setTimeTableArray', function (req, res) {
        timeTableController.setTimeTableArray(req, res)
    })
    

    app.post('/v1/deleteTimeTable', function (req, res) {
        timeTableController.deleteTimeTable(req, res)
    })


    app.post('/v1/setActivityArray', function (req, res) {
        timeTableController.setActivityArray(req, res)
    })
    

    app.post('/v1/getActivity', function (req, res) {
        timeTableController.getActivity(req, res)
    })

    app.post('/v1/getActiveActivity', function (req, res) {
        timeTableController.getActiveActivity(req, res)
    })



    app.post('/v1/deleteActivity', function (req, res) {
        timeTableController.deleteActivity(req, res)
    })

    
    app.post('/v1/getStudentActivities', function (req, res) {
        timeTableController.getStudentActivities(req, res)
    })
    
    app.post('/v1/setStudentActivitiesArray', function (req, res) {
        timeTableController.setStudentActivitiesArray(req, res)
    })

    app.post('/v1/deleteStudentActivities', function (req, res) {
        timeTableController.deleteStudentActivities(req, res)
    })


    app.post('/v1/getTimeTableTimings', function (req, res) {
        timeTableController.getTimeTableTimings(req, res)
    })
    app.post('/v1/deleteTimeTableTimings', function (req, res) {
        timeTableController.deleteTimeTableTimings(req, res)
    })
    
    app.post('/v1/getStudentTimeTable', function (req, res) {
        timeTableController.getStudentTimeTable(req, res)
    })
    app.post('/v1/setSubjectTimetable', function (req, res) {
        timeTableController.setSubjectTimetable(req, res)
    })
    app.post('/v1/getSubjectTimetable', function (req, res) {
        timeTableController.getSubjectTimetable(req, res)
    })
    app.post('/v1/deleteSubjectTimetable', function (req, res) {
        timeTableController.deleteSubjectTimetable(req, res)
    })
    
}
