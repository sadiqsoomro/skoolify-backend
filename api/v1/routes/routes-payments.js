module.exports = function (app) {
    const paymentController = require('../controllers/controllers-payments')    
    
    app.post('/v1/getPaymentProviderDetails', function (req, res) {
        paymentController.getPaymentProviderDetails(req, res)
    }) 
    app.post('/v1/setPaymentTransactionLogWeb', function (req, res) {
        paymentController.setPaymentTransactionLogWeb(req, res)
    })     
    app.post('/v1/getPaymentVoucherCharges', function (req, res) {
        paymentController.getPaymentVoucherCharges(req, res)
    }) 
       app.post('/v1/getBankPaymentVoucherCharges', function (req, res) {
        paymentController.getBankPaymentVoucherCharges(req, res)
    })     
    app.post('/v1/getPaymentProviderTypes', function (req, res) {
        paymentController.getPaymentProviderTypes(req, res)
    }) 
    app.post('/v1/getPaymentProviders', function (req, res) {
        paymentController.getPaymentProviders(req, res)
    }) 
    app.post('/v1/getPaymentLogs', function (req, res) {
        paymentController.getPaymentLogs(req, res)
    }) 

    app.post('/v1/setSchoolServiceChargesGenerate', function (req, res) {
        paymentController.setSchoolServiceChargesGenerate(req, res)
    }) 

    app.post('/v1/getSchoolServiceCharges', function (req, res) {
        paymentController.getSchoolServiceCharges(req, res)
    }) 
    app.post('/v1/getSchoolOutstandingList', function (req, res) {
        paymentController.getSchoolOutstandingList(req, res)
    }) 
    app.post('/v1/setCreateInvoicesInERP', function (req, res) {
        paymentController.setCreateInvoicesInERP(req, res)
    }) 
    app.post('/v1/getCustomerIDfromERP', function (req, res) {
        paymentController.getCustomerIDfromERP(req, res)
    }) 
    app.post('/v1/setInvoiceStatusUpdatefromERP', function (req, res) {
        paymentController.setInvoiceStatusUpdatefromERP(req, res)
    }) 
    app.post('/v1/getAvanzaValidateCustomer', function (req, res) {
        paymentController.getAvanzaValidateCustomer(req, res)
    }) 

    app.post('/v1/getAvanzaValidateOTP', function (req, res) {
        paymentController.getAvanzaValidateOTP(req, res)
    }) 

    app.post('/v1/getNIFTInitialize', function (req, res) {
        paymentController.getNIFTInitialize(req, res)
    }) 
    app.post('/v1/getNIFTCustomerValidation', function (req, res) {
        paymentController.getNIFTCustomerValidation(req, res)
    }) 
    app.post('/v1/setNIFTValidateOTP', function (req, res) {
        paymentController.setNIFTValidateOTP(req, res)
    })
    app.post('/v1/getNIFTPaymentStatus', function (req, res) {
        paymentController.getNIFTPaymentStatus(req, res)
    })
    app.post('/v1/setPaymentLogHBLCC', function (req, res) {
        paymentController.setPaymentLogHBLCC(req, res)
    }) 
    app.post('/v1/getProviderLogo', function (req, res) {
        paymentController.getProviderLogo(req, res)
    }) 

}


