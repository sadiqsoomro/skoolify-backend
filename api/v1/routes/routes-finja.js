module.exports = function (app) {
    const finjaController = require('../controllers/controllers-finja')


    app.post('/v1/getFinjaCustomerInfo', function (req, res) {
        finjaController.getFinjaCustomerInfo(req, res)
    })
    app.post('/v1/setFinjaPayment', function (req, res) {
        finjaController.setFinjaPayment(req, res)
    })
    
}   
