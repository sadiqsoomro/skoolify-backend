module.exports = function (app) {
    const easypaisaController = require('../controllers/controllers-easypaisa');
    //var timeout = require('connect-timeout');

    app.post('/v1/setEasyPaisaPayment', function (req, res) {
        req.setTimeout(parseInt(process.env.EASY_PAISA_TRANSACTION_TIMEOUT)) 
        easypaisaController.setEasyPaisaPayment(req, res)
    })
    app.post('/v1/getEasyPaisaStatus', function (req, res) {
        easypaisaController.getEasyPaisaStatus(req, res)
    })
    
}   
