module.exports = function (app) {
    const commController = require('../controllers/controllers-communication')


    app.post('/v1/setCommunicationTypes', function (req, res) {
        commController.setCommunicationTypes(req, res)
    })
    app.post('/v1/getCommunicationTypes', function (req, res) {
        commController.getCommunicationTypes(req, res)
    })
    app.post('/v1/getCommunicationTypesMobile', function (req, res) {
        commController.getCommunicationTypesMobile(req, res)
    })
    app.post('/v1/setCommunicationThreadMobile', function (req, res) {
        commController.setCommunicationThreadMobile(req, res)
    })
    app.post('/v1/setCommunicationThread', function (req, res) {
        commController.setCommunicationThread(req, res)
    })
    app.post('/v1/getCommunicationThreadMobile', function (req, res) {
        commController.getCommunicationThreadMobile(req, res)
    })
    app.post('/v1/getCommunicationGroupMobile', function (req, res) {
        commController.getCommunicationGroupMobile(req, res)
    })
    app.post('/v1/setCommunicationStatusChange', function (req, res) {
        commController.setCommunicationStatusChange(req, res)
    })
    app.post('/v1/setCommunicationArchive', function (req, res) {
        commController.setCommunicationArchive(req, res)
    })
    // app.post('/v1/getCommunicationSearch', function (req, res) {
    //     commController.getCommunicationSearch(req, res)
    // })
}