const Joi = require('joi')


module.exports.getAllAds = {
    countryID: Joi.string(),
    cityID: Joi.string(),
    areaID: Joi.string(),
    schoolID: Joi.string(),
    branchID: Joi.string(),
    classLevelID: Joi.string(),
    classID: Joi.string().required()
}

module.exports.getTopAds = {
    countryID: Joi.string(),
    cityID: Joi.string(),
    areaID: Joi.string(),
    schoolID: Joi.string(),
    branchID: Joi.string(),
    classLevelID: Joi.string(),
    classID: Joi.string().required()
}

module.exports.getDateTest = {
    myDate: Joi.string(),
    language: Joi.string(),
    dateFlag: Joi.string()
}



