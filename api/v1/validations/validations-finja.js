const Joi = require('joi')

module.exports.getFinjaCustomerInfo = {
    mobileNo: Joi.string().required()   
}

module.exports.setFinjaPayment = { 
    // SIM SIM paymentToMerchant
     txnHash: Joi.string().required(),
     txnOTP: Joi.string().required(),
     txnToken: Joi.string().required(),
     customerID:Joi.string().required(),
     customerName:Joi.string().required(),
     parentUUID:Joi.string(),
     userUUID:Joi.string(),
     walletAccountNo: Joi.string().required(),
     providerID: Joi.string().required(),
     ipAddress: Joi.string().required(),
     internalTxnRefNo: Joi.string(),
     paymentCurrency: Joi.string().required(),
     paymentAmount: Joi.string().required(),
     tranRequestCode: Joi.string().required(),
     emailAddress: Joi.string().required(),
     additionalData: Joi.string()
 
 }
