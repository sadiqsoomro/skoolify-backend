const Joi = require('joi')


module.exports.getUserTablet = Joi.object().keys({
    username: Joi.string().required(),
    branchID: Joi.string()
})

module.exports.getUserPasswordTablet = Joi.object().keys({
    
    userUUID: Joi.string().required(), 
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(), 
    password: Joi.string().required(), 
    deviceToken: Joi.string().required(),
    sourceUUID: Joi.string().required()
})

module.exports.getTimeTableTablet = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string().required(), 
    userUUID: Joi.string(), 
    subjectID: Joi.string(),
    roomID: Joi.string(),
    classID: Joi.string()
})

module.exports.getClassesTablet = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string().required()
})

module.exports.getTeacherTablet = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string().required()
})

module.exports.getRoomTablet = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string().required()
})

module.exports.getSubjectTablet = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string().required()
})


module.exports.setStudentAttendanceTablet = Joi.array().items(
    Joi.object({
        attendanceID: Joi.string(),
        attendanceDate: Joi.string().required(),
        batchNo: Joi.string().required(),
        timetableSubjectID:Joi.string().required(),
        classID:  Joi.string().required(),
        studentID: Joi.string().required(),
        isPresent: Joi.string().required(),
        notes: Joi.string().allow(""),
        userID: Joi.string().required()  
        })
)

module.exports.getClassStudentsTablet = Joi.object().keys({
    timetableSubjectID: Joi.string().required()
    
})