const Joi = require('joi')

module.exports.setEasyPaisaPayment = { 
   
    txnHash: Joi.string().required(),
    parentUUID:Joi.string(),
    userUUID:Joi.string(),
    walletAccountNo: Joi.string().required(),
    providerID: Joi.string().required(),
    ipAddress: Joi.string().required(),
    internalTxnRefNo: Joi.string(),
    paymentCurrency: Joi.string().required(),
    paymentAmount: Joi.string().required(),
    tranRequestCode: Joi.string().required(),
    emailAddress: Joi.string().required(),
    additionalData: Joi.string().allow("")

}

module.exports.getEasyPaisaStatus = { 
   
    sourceUUID: Joi.string().required(),
    walletAccountNo: Joi.string().required(),
    orderID: Joi.string().required()

}

