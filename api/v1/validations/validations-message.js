const Joi = require('joi')
const { join } = require('lodash')

module.exports.getMessages = Joi.object().keys({
    messageDateFrom: Joi.string().required(),
    messageDateTo: Joi.string().required(),
    messageGUID:Joi.string(),
    messageType: Joi.string().allow(""),
    schoolID: Joi.string(),
    branchID:Joi.string(),
    classID: Joi.string(),
    classLevelID: Joi.string(),
    cityId: Joi.string(),
    areaId: Joi.string(),
    studentID: Joi.string(),
    parentID: Joi.string(),
    parentUUID: Joi.string()
})


module.exports.getStudentMessages = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID:Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string().required(),
    parentID: Joi.string(),
    parentUUID: Joi.string(),
    recordSequenceNo:Joi.string(),
    requiredCount:Joi.string(),


})

module.exports.getMessagesSummary = Joi.object().keys({
    messageGUID:Joi.string(), 
    messageDateFrom: Joi.string().required(),
    messageDateTo: Joi.string().required(),
    schoolID: Joi.string(),
    branchID: Joi.string().required(),
    classLevelID: Joi.string(),
    classID: Joi.string(),
    messageType: Joi.string().allow("")
})

module.exports.deleteMessages = Joi.object().keys({
    messageGUID: Joi.string().required()
    
})

module.exports.setConsent = Joi.object().keys({
    messageID: Joi.string().required(),
    parentID: Joi.string(),
    parentUUID: Joi.string(),
    consentResponse: Joi.string(), // will be mandatory after app upgrade
    consentMessage: Joi.string().required()
})

 module.exports.setMessagesArray = Joi.array().items(
     Joi.object({
    messageText: Joi.string().required(),
    messageType: Joi.string().required(),
    countryID:Joi.string(),
    cityID: Joi.string(),
    areaID: Joi.string(),
    schoolID: Joi.string(),
    branchID: Joi.string(),
    classLevelID:  Joi.string(),
    classID: Joi.string(),
    studentID: Joi.string(),
    attachment:Joi.string(),
    userID: Joi.string().required(),
    notificationOnly: Joi.string().required()
 })
 )
 module.exports.getNotificationCount = Joi.object().keys({
    parentID: Joi.string(),
    parentUUID: Joi.string(),
    studentID: Joi.string().required(),
    lastDateTime: Joi.string().allow("")
})


module.exports.getMessageDetails = Joi.object().keys({
    messageGUID: Joi.string().required()
})


module.exports.setMessageIsRead = Joi.object().keys({
    messageGUID: Joi.string().required(),
    studentID: Joi.string().required()
})


module.exports.setMessageIsDelivered = Joi.object().keys({
    messageGUID: Joi.string().required(),
    studentID: Joi.string().required()
})

module.exports.getConsentReportExport = Joi.object().keys({
    messageGUID: Joi.string().required()
})

