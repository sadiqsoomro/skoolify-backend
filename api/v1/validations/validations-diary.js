const Joi = require('joi')

// module.exports.getDailyDiary = {
//     classID: Joi.string().required(),
//     diaryID: Joi.string(),
//     studentID: Joi.string().required(),
//     diaryGUID: Joi.string(),
//     subjectID: Joi.string()
// }


// module.exports.setDailyDiary = {
//     diaryID: Joi.string(),
//     diaryGUID:Joi.string().required(),
//     subjectFlag:Joi.string().required(),
//     studentID: Joi.string(),
//     classID : Joi.string().required(),
//     subjectID : Joi.string().required(),
//     description: Joi.string().required(),
//     diaryDate : Joi.string().required(),
//     userID: Joi.string().required(),
//     attachment: Joi.string(),
//     documentLink :Joi.string(),
//     dueDate:Joi.string()
// }


module.exports.setDailyDiaryArray = Joi.array().items(
    Joi.object({
        studentID:Joi.string().allow(""),
        diaryID: Joi.string(),
        diaryGUID:Joi.string(),
        subjectFlag:Joi.string().required(),
        classID : Joi.string().required(),
        subjectID : Joi.string().required(),
        description: Joi.string().required(),
        diaryDate : Joi.string().required(),
        userID: Joi.string().required(),
        attachment: Joi.string(),
        documentLink :Joi.string(),
        dueDate :Joi.string()
    })
)

module.exports.deleteDailyDiary = {
    diaryGUID: Joi.string().required(),
    studentID: Joi.string()
    
}

module.exports.getDailyDiarySummary = {
    classLevelID:Joi.string().required(),
    classID: Joi.string(),
    diaryGUID: Joi.string(),
    subjectID: Joi.string(),
    fromDate: Joi.string(),
    toDate : Joi.string()
}

module.exports.getStudentDailyDiary = {
    classID: Joi.string().required(),
    studentID: Joi.string().required(),
    recordSequenceNo: Joi.string(),
    requiredCount: Joi.string()
}

module.exports.getDailyDiaryListing = {
    classID: Joi.string().required(),
    diaryGUID: Joi.string().required(),
    subjectID: Joi.string().required()
}