const Joi = require('joi')

module.exports.getIP2Location = { 
    IPAddress: Joi.string().ip({
        version: [
          'ipv4',
          'ipv6'
        ]
      }).required()
}
