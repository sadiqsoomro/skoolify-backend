const Joi = require('joi')

module.exports.getQuizResults = { 
    classID: Joi.string().required(),
    studentID:  Joi.string(),
    subjectID: Joi.string(), 
    quizDate: Joi.string()

}

module.exports.getQuizGraphResults = { 
    classID: Joi.string().required(),
    subjectID: Joi.string(), 
    quizDate: Joi.string()

}

module.exports.setQuizResults = {
    quizID : Joi.string(),
    quizDate: Joi.string().required(),
    comments: Joi.string(),
    marksObtained: Joi.string().required(),
    marksTotal: Joi.string().required(),
    subjectID: Joi.string().required(),
    studentID: Joi.string().required(),
    classID: Joi.string().required(),
    showGraph:Joi.string()
}

module.exports.setQuizResultsArray = Joi.array().items(
    Joi.object({
    quizID : Joi.string(),
    quizDate: Joi.string().required(),
    comments: Joi.string(),
    marksObtained: Joi.string().required(),
    marksTotal: Joi.string().required(),
    subjectID: Joi.string().required(),
    studentID: Joi.string().required(),
    classID: Joi.string().required(),
    showGraph:Joi.string()
    })
)

module.exports.examResultsCount = { 
    studentId: Joi.string().required() 
}

module.exports.getExamTerm = {
    branchID: Joi.string(),
    schoolID: Joi.string().required()
}

module.exports.getActiveExamTerm = {
    branchID: Joi.string(),
    schoolID: Joi.string().required()
}

module.exports.deleteExamTerm = {
    examsTermID: Joi.string().required()
}

module.exports.getClassTerms = {
    
    studentID: Joi.string(),
    classID: Joi.string(),
    classLevelID: Joi.string()
}

module.exports.assignClassTerm = {
    examTermLinkID: Joi.string(),
    examsTermID: Joi.string().required(),
    classLevelID: Joi.string().required()
}

module.exports.setExamTerm = {
    examsTermID: Joi.string(),
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    termName: Joi.string().required(),
    termNote: Joi.string().required(),
    termStartDate: Joi.string().required(),
    termEndDate: Joi.string().required(),
    isPublished: Joi.string(),
    isActive: Joi.string()
}

module.exports.getExamResults = Joi.object().keys({
    classID: Joi.string().required(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    subjectID: Joi.string(),
    nonSubjectID: Joi.string()


}).with('classID', ['examsTermID','studentID'])

module.exports.getAssessmentResults = Joi.object().keys({
    classID: Joi.string().required(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required()

}).with('classID', ['examsTermID','studentID'])


module.exports.setAssessmentDetails = {
    notesNS: Joi.string().required(),
    gradeNS: Joi.string().required(),
    nonSubjectID: Joi.string().required(),
    assessmentID: Joi.string().required(),
    assessmentNSID: Joi.string()
}


module.exports.setExamSubjectResults = {
    examSubjectResultID: Joi.string(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    subjectID: Joi.string().required(),
    totalMarks: Joi.string().required(),
    isAbsent: Joi.string().allow(""),
    obtainedMarks: Joi.string().required(),
    showMarksOnApp: Joi.string(),
    grade: Joi.string().allow(""),
    notes: Joi.string().allow(""),
    userID: Joi.string().required()
}

module.exports.setClassChange = {
    
    studentID: Joi.string().required(),
    classID: Joi.string().required(),
    classYear: Joi.string().required()
    
}

module.exports.setClassChangeArray = Joi.array().items(
    Joi.object({
        studentID: Joi.string().required(),
        classID: Joi.string().required(),
        classYear: Joi.string()
    })
)


module.exports.setExamSubjectResultsArray = Joi.array().items(
    Joi.object({
    examSubjectResultID: Joi.string(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    subjectID: Joi.string().required(),
    totalMarks: Joi.string().allow(""),
    isAbsent: Joi.string().allow(""),
    obtainedMarks: Joi.string().allow(""),
    showMarksOnApp: Joi.string(),
    grade: Joi.string().allow(""),
    notes: Joi.string().allow(""),
    userID: Joi.string().required()
    })
)

module.exports.setExamNonSubjectResultsArray = Joi.array().items(
    Joi.object({
    examNSResultID: Joi.string(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    nonSubjectID: Joi.string().required(),
    nsLegendID: Joi.string().required(),
    gradeNS: Joi.string(),
    notesNS: Joi.string().allow(""),
    userID: Joi.string().required()
    })
)

module.exports.setExamTermResults = {
    examOverallID: Joi.string(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    overallNotes: Joi.string(),
    rank: Joi.string().allow(""),
    resultStatus: Joi.string(),
    holdFlag: Joi.string(),
    overallGrade: Joi.string().allow("")
}


module.exports.setExamTermResultsArray = Joi.array().items(
    Joi.object({
    examOverallID: Joi.string(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    classID: Joi.string().required(),
    overallNotes: Joi.string().allow(""),
    rank: Joi.string().allow(""),
    resultStatus: Joi.string().allow(""),
    holdFlag: Joi.string(),
    overallGrade: Joi.string().allow("")
    })
)

module.exports.getExamOverallResult = {
    classID: Joi.string().required(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string()
}

module.exports.getExamSubjectResult = {
    examsTermID: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string(),
    subjectID:Joi.string().allow("")
}



module.exports.getExamNSResult = {
    examsTermID: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string(),
    nonSubjectID: Joi.string().allow("")
}

module.exports.deleteFeeChallan = {
    challanID: Joi.string().required()  
}

module.exports.setExamTermStatus = {   
    examsTermID: Joi.string().required(),
    isPublished: Joi.string().required()
}


module.exports.deleteExamSubjectResult = {   
    examSubjectResultID: Joi.string().required()
}

module.exports.deleteExamNonSubjectResult = {   
    examNSResultID: Joi.string().required()
}

module.exports.getExamResultReport = Joi.object().keys({
    classID: Joi.string().required(),
    examsTermID: Joi.string().required(),
    studentID: Joi.string().required(),
    subjectID: Joi.string(),
    nonSubjectID: Joi.string()
}).with('classID', ['examsTermID','studentID'])

module.exports.deleteQuiz = {   
    quizID: Joi.string().required()
}

module.exports.getExamResultsMobile = {   
    examsTermID:Joi.string().required(), 
    classID:Joi.string().required(), 
    studentUUID:Joi.string().required(), 
    subjectID:Joi.string(),  
    nonsubjectID:Joi.string(),
    fromDate:Joi.string().required(),
    toDate:Joi.string().required()
}

