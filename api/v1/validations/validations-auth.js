const Joi = require('joi')

module.exports.getToken = Joi.object().keys({
    apiKey: Joi.string().required(),
    sourceUuid: Joi.string().required()
}).with('apiKey', 'sourceUuid')

module.exports.generateOTP = Joi.object().keys({
    mobileNumber: Joi.string().required(),
    sourceUuid: Joi.string().required()
}).with('mobileNumber', 'sourceUuid')





module.exports.verifyOTP = Joi.object().keys({
    mobileNumber: Joi.string().required(),
    otp: Joi.string().required(),
    uuid: Joi.string().required(),
    deviceToken: Joi.string(),
    deviceType: Joi.string()

})

module.exports.getTnC = Joi.object().keys({
    tncID: Joi.string(),
    userID: Joi.string()

})

module.exports.setTnC = Joi.object().keys({
    tncID: Joi.string(),
    tncText: Joi.string().required(),
    tncText_ar: Joi.string().required(),
    version: Joi.string().required(),
    userID: Joi.string().required(),
    isPublished: Joi.string().required()

})

module.exports.getAppTnC = Joi.object().keys({
    requiredLanguage:Joi.string().allow("")
})

module.exports.setAppAcceptTnC = Joi.object().keys({
    tncID: Joi.string().required(),
    sourceUUID: Joi.string().required()
})

module.exports.getContract = Joi.object().keys({
    contractID: Joi.string(),
    userID: Joi.string()
})

module.exports.setContract = Joi.object().keys({
    contractID: Joi.string(),
    contractText: Joi.string().required(),
    chargesText: Joi.string(),
    collectionText: Joi.string(),
    version: Joi.string().required(),
    userID: Joi.string().required(),
    isPublished: Joi.string().required()

})

module.exports.setTokenExpiry = Joi.object().keys({
    token: Joi.string().required()

})

module.exports.setSMSSendArray = Joi.array().items(
    Joi.object({
        mobileNumber: Joi.string().required(),
        smsType: Joi.string().required()
    })
)

// module.exports.getVideoToken = Joi.object().keys({
//     identity: Joi.string().required(),
//     roomName: Joi.string().required()

// })

module.exports.setVerifyOTP = Joi.object().keys({
    mobileNumber: Joi.string().required(),
    OTP: Joi.string().required(),
    sourceUUID: Joi.string().required(),
    deviceToken: Joi.string().allow(null),
    deviceType: Joi.string().allow(null)

})

module.exports.generateTextVoiceOTP = Joi.object().keys({
    mobileNumber: Joi.string().required(),
    sourceUUID: Joi.string().required(),
    OTPType: Joi.string().required(),
    SMSHash: Joi.string()

})


