const Joi = require('joi')

module.exports.userValidation = {
    mobileNo: Joi.string().required(),
    domain: Joi.string()
}

module.exports.userDetails = {
    userID: Joi.string().required(),
}

module.exports.getUserModules = {
    userID: Joi.string().required(),
    roleID: Joi.string().required(),
    branchID: Joi.string(),
    schoolID: Joi.string()
}

module.exports.getUserList = {
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    userID: Joi.string()
}

module.exports.getUserRoleList = {
    userID: Joi.string().required(),
    branchID: Joi.string().required()
}



module.exports.setUser = {
    userID:Joi.string(),
    password:Joi.string().allow(""),
    fullName: Joi.string().required(),
    mobileNo: Joi.string().required(),
    emailAddress: Joi.string(),
    schoolID: Joi.string().required(),
    createdByUserID: Joi.string().required(),
    isActive: Joi.string().required(),
    isSignatory: Joi.string(),
    feePaymentAllowed: Joi.string(),
    isAdmin: Joi.string()
}


// module.exports.setUsersArray =  Joi.array().items(
//     Joi.object({
//     userID:Joi.string(),
//     password:Joi.string().required().allow(""),
//     fullName: Joi.string().required(),
//     mobileNo: Joi.string().required(),
//     emailAddress: Joi.string(),
//     schoolID: Joi.string().required(),
//     createdByUserID: Joi.string().required(),
//     isActive: Joi.string().required()
//     })
// )

module.exports.updateUser = {
    userID: Joi.string().required(),
    fullName: Joi.string().required(),
    mobileNo: Joi.string().required(),
    emailAddress: Joi.string(),
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    isActive: Joi.string().required()

}

module.exports.getRoleLinkedUsers = {
    roleID: Joi.string().required(),
    branchID: Joi.string().required()
}

module.exports.getActiveUserList = {
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    userID: Joi.string()
}
