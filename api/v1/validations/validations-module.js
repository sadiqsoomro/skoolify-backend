const Joi = require('joi')

module.exports.setModules = {
    moduleID: Joi.string(),
    moduleName: Joi.string().required(),
    parentModuleID: Joi.string().required(),
    moduleURL: Joi.string().required(),
    isParent: Joi.string().required(),
    moduleDescription: Joi.string().required(),
    isActive: Joi.string().required()
}


module.exports.getModules = {
    moduleID: Joi.string().optional()
    
}

module.exports.getActiveModules = {
    moduleID: Joi.string().optional()
    
}