const Joi = require('joi')

module.exports.getParent = { 
    schoolID: Joi.string(),
    mobileNumber: Joi.string(), 
    bundleIdentifier: Joi.string(),
    parentName:Joi.string(),
    parentCNIC:Joi.string()
}

module.exports.getActiveParent = { 
    mobileNumber: Joi.string().required()
}

module.exports.getParentByStudentID = { 
    studentID: Joi.string().required()    
}

module.exports.setParents = { 
    parentID: Joi.string(),
    parentName: Joi.string().required(),
    parentLastName: Joi.string(),
    parentCNIC: Joi.string(),
    mobileNumber: Joi.string().required(),
    email: Joi.string(),
    parentType: Joi.string().required(),
    isTaxRegistered: Joi.string(),
    taxID: Joi.string(),
    isActiveTaxPayer: Joi.string(),
    addressLine1: Joi.string(),
    addressLine2: Joi.string(),
    addressState: Joi.string(),
    addressPostalCode: Joi.string(),
    cityID: Joi.string(),
    countryID: Joi.string(),
    isActive: Joi.string().required()
}


module.exports.getCities = {
    countryID: Joi.string().required(),
    cityID: Joi.string()
}

module.exports.getActiveCities = {
    countryID: Joi.string().required(),
    cityID: Joi.string()
}

module.exports.getCountries = {
    countryID : Joi.string()
}

module.exports.getActiveCountries = {
    
    countryID : Joi.string()
}

module.exports.getAreas = {
    cityID: Joi.string().required(),
    areaID: Joi.string()
}

module.exports.getActiveAreas = {
    cityID: Joi.string().required(),
    areaID: Joi.string()
}

module.exports.setCountries = {
    countryID: Joi.string(),
    countryName: Joi.string().required(),
    dialingCode: Joi.string().required(),
    currencyISOCode: Joi.string().required(),
    currencyAlphaCode: Joi.string().required(),
    currencyName: Joi.string().required(),
    currencyDecimalValue: Joi.string().required(),
    isActive: Joi.string().required()
}

module.exports.setCities = {
    cityID: Joi.string(),
    countryID:Joi.string().required(),
    cityName: Joi.string().required(),
    gmtOffset: Joi.string().required(),
    isActive: Joi.string().required()
}

module.exports.setAreas = {
    areaID: Joi.string(),
    cityID: Joi.string().required(),
    areaName: Joi.string().required(),
    isActive: Joi.string().required()
}

module.exports.setParentStudentLink = {
    parentID: Joi.string().required(),
    studentID: Joi.string().required()
}

module.exports.setParentStudentDeLink = {
    parentID: Joi.string().required(),
    studentID: Joi.string().required()
}

module.exports.deleteParent = { 
    parentID: Joi.string().required()
}

module.exports.UploadParentStudentArray = Joi.array().items(
    Joi.object({ 
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    studentName: Joi.string().required(),
    gender: Joi.string().allow(""),
    classLevelName: Joi.string().required(),
    classSection: Joi.string().required(),
    rollNo: Joi.string().required(),
    dob: Joi.string().required(),
    fatherName: Joi.string().allow(""),
    fatherCNIC: Joi.string().allow(""),
    fatherMobileNo: Joi.string().allow(""),
    fatherEmail: Joi.string().allow(""),
    motherName: Joi.string().allow(""),
    motherCNIC: Joi.string().allow(""),
    motherMobileNo: Joi.string().allow(""),
    motherEmail: Joi.string().allow("")
})
)


module.exports.setTraceDump = { 
    sourceUUID: Joi.string().required(),
    OSVersion: Joi.string().required(),
    buildVersion: Joi.string().required(),
    stackTrace: Joi.string(),
    navigationStates: Joi.string(),
    parameters:Joi.string(),
    stackData:Joi.string()
}


module.exports.setEncryptData = { 
    inputData: Joi.string().required()   
}

module.exports.setDecryptData = { 
    inputData: Joi.string().required()   
}

module.exports.setDeviceToken = { 
    sourceUUID: Joi.string().required(),
    parentID:Joi.string(),
    parentUUID:Joi.string(),
    deviceToken: Joi.string().required(),
    deviceType:Joi.string().required(),
    appVersion:Joi.string().required(),
    osVersion:Joi.string().required(),
    deviceModel:Joi.string(),
    preferredLanguage:Joi.string()
}

module.exports.getTimezoneList = { 
    
}

module.exports.getOTPDetails = { 
    mobileNumber: Joi.string().required(),
    schoolID: Joi.string().required(),
    branchID:Joi.string().required()
}

module.exports.setParentMobile = { 
    parentUUID: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    nationalID: Joi.string(),
    mobileNumber: Joi.string().required(),
    email: Joi.string(),
    parentType: Joi.string().required(),
    taxID: Joi.string(),
    addressLine1: Joi.string(),
    addressLine2: Joi.string(),
    addressState: Joi.string(),
    addressPostalCode: Joi.string(),
    cityID: Joi.string(),
    countryID: Joi.string()
}

module.exports.setPaymentProviders = { 
    providerID: Joi.string(),
    countryID: Joi.string().required(),
    transactionTypeID:Joi.string().required(),
    providerName:Joi.string().required(),
    instrumentFormat:Joi.string(),
    tncLink:Joi.string(),
    notes:Joi.string(),
    providerChargeType:Joi.string().required(),
    providerChargeRate:Joi.string().required(),
    accessTokenKey:Joi.string(),
    profileOrMerchantID:Joi.string(),
    secretSalt:Joi.string(),
    hostedURL:Joi.string().required(),
    providerURL:Joi.string().required(),
    providerSuccessURL:Joi.string(),
    providerFailureURL:Joi.string(), 
    brandIcon:Joi.string().required(),
    isActive:Joi.string().required()


}

module.exports.getPaymentProviderList = { 
    
}

module.exports.getParentMobile = { 
    parentUUID: Joi.string().required(),
    mobileNumber: Joi.string().required()
}