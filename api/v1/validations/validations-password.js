const Joi = require('joi')

module.exports.passwordValidation = {
    mobileNo: Joi.string().required(),
    password: Joi.string().required(),
    domain: Joi.string()
}

module.exports.changePassword = {
    userID: Joi.string().required(),
    newPassword: Joi.string().required()
}

module.exports.resetPassword= {
    userID: Joi.string().required(),
    newPassword: Joi.string().required(),
    updateUserID: Joi.string().required()
}