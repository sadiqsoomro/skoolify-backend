const Joi = require('joi')

module.exports.getcalendarDays = Joi.object().keys({
    schoolID: Joi.string().required(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required()
})

module.exports.setCalendarDaysArray = Joi.array().items(
    Joi.object({
    calendarID :Joi.string(),
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    holidayName: Joi.string().required(),
    isHoliday: Joi.string(),
    calendarDate: Joi.string().required()
    })
)

module.exports.setCommsRequest = Joi.object().keys({
    parentID: Joi.string(),
    parentUUID: Joi.string(),
    studentID: Joi.string().required(),
    requestType: Joi.string().required(),
    requestText: Joi.string().required()
})

module.exports.setCommsIsRead = Joi.object().keys({
    communicationListID: Joi.string().required(),
    status: Joi.string().required()
})

module.exports.getCommsCount = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required()
})

module.exports.getSchools = Joi.object().keys({
    schoolID: Joi.string(),
    domain: Joi.string()
})

module.exports.deleteSchool = Joi.object().keys({
    schoolID: Joi.string().required()
})

module.exports.deleteSchoolBranch = Joi.object().keys({
    branchID: Joi.string().required()
})
module.exports.setSchools = Joi.object().keys({
    schoolName: Joi.string().required(),
    schoolLogo: Joi.string(),
    domain: Joi.string().required(),
    websiteURL: Joi.string().required(),
    owner: Joi.string().required(),
    selfRegistered: Joi.string().required(),
    isActive: Joi.string().required(),
    schoolID: Joi.string(),
    legalEntityName: Joi.string().required(),
    legalEntityAddress: Joi.string().required(),
    legalEntityPerson: Joi.string().required(),
    legalEntityContactNo: Joi.string().required(),
    schoolParameters:  Joi.object({
  
        studentAttendanceGracePeriod: Joi.string(),
        staffAttendanceGracePeriod:Joi.string(),
        weekStartDay:Joi.string(),
        feePaymentAllowed:Joi.string(),
        showAccNoOnVoucher:Joi.string(),
        appHomework:Joi.string(),
        appTimetable:Joi.string(),
        appExamQuiz:Joi.string(),
        appfeeVoucher:Joi.string(),
        appAttendance:Joi.string(),
        appMessage:Joi.string(),
        appCommunicate:Joi.string(),
        appEvents:Joi.string()
        })        
    

})

module.exports.getSchoolBranches = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    areaID: Joi.string()
})

module.exports.getActiveBranches = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    areaID: Joi.string()
})


module.exports.getFeeTypes = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID:Joi.string(),
    feetypeID: Joi.string()
})

module.exports.getActiveFeeTypes = Joi.object().keys({
    schoolID: Joi.string().required(),
    feetypeID: Joi.string()
})


module.exports.getClassLevels = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    classLevelID: Joi.string()
})

module.exports.getActiveClassLevels = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    classLevelID: Joi.string()
})

module.exports.getClassStudents = Joi.object().keys({
    
    classID: Joi.string().required()
})

module.exports.getClassActiveStudents = Joi.object().keys({
    
    classID: Joi.string().required()
})

module.exports.getAllClassStudents = Joi.object().keys({
    
    classID: Joi.string().required()
})

module.exports.getClasses = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    classLevelID: Joi.string(),
    classTeacherID: Joi.string()
})

module.exports.getActiveClasses = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    classLevelID: Joi.string(),
    classTeacherID: Joi.string()
})

module.exports.getUserClasses = Joi.object().keys({
    userID: Joi.string().required(),
    classID: Joi.string(),
    classLevelID: Joi.string()

})



module.exports.getUserClassLevels = Joi.object().keys({
    branchID: Joi.string().required(),
    userID: Joi.string().required(),
    classID: Joi.string()

})

module.exports.setClasses = Joi.object().keys({
    isActive: Joi.string().required(),
    classTeacherID: Joi.string(),
    classShift: Joi.string().required(),
    classSection: Joi.string().required(),
    classLevelID: Joi.string().required(),
    branchID: Joi.string().required(),
    classID: Joi.string()
})

module.exports.getCommRequest = Joi.object().keys({
  
    schoolID:Joi.string().required(), 
    branchID:Joi.string(),
    classID:Joi.string(),
    classLevelID:Joi.string(),
    studentID:Joi.string()
})

module.exports.getSchoolLogo = Joi.object().keys({
  
    domain:Joi.string().required()
})

module.exports. setSchoolBranches = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    branchCode: Joi.string(),
    branchName: Joi.string().required(),
    address1: Joi.string().required(),
    address2: Joi.string(),
    address3: Joi.string(),
    principleName: Joi.string().required(),
    email: Joi.string().required(),
    contact1: Joi.string().required(),
    contact2: Joi.string(),
    contact3: Joi.string(),
    gpsCordinates: Joi.string().required(),
    bankName: Joi.string(),
    bankBranch:Joi.string(),
    bankAccNo: Joi.string(),
    areaID: Joi.string().required(),
    cityID: Joi.string().required(),
    countryID: Joi.string().required(),
    isActive: Joi.string().required()
})

// module.exports.setSchoolBranchesArray = Joi.array().items(
//     Joi.object({
//     schoolID: Joi.string().required(),
//     branchID: Joi.string(),
//     branchCode: Joi.string().required(),
//     branchName: Joi.string().required(),
//     address1: Joi.string().required(),
//     address2: Joi.string(),
//     address3: Joi.string(),
//     principleName: Joi.string().required(),
//     email: Joi.string().required(),
//     contact1: Joi.string().required(),
//     contact2: Joi.string(),
//     contact3: Joi.string(),
//     gpsCordinates: Joi.string().required(),
//     areaID: Joi.string().required(),
//     cityID: Joi.string().required(),
//     countryID: Joi.string().required(),
//     isActive: Joi.string().required() })
// )

module.exports.setClassLevels = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    classLevelID: Joi.string(),
    classLevelName: Joi.string().required(),
    classAlias:  Joi.string().required(),
    isSubjectLevelAttendance: Joi.string().required(),
    assessmentOnly: Joi.string().required(),
    isActive: Joi.string().required()

})

module.exports.getBillingPeriod = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    billingPeriodID: Joi.string()
})

module.exports.getActiveBillingPeriod = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    billingPeriodID: Joi.string()
})

module.exports.setBillingPeriod = Joi.object().keys({
    billingPeriodID: Joi.string(),
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    periodName: Joi.string().required(),
    isActive: Joi.string().required()
})

module.exports.deleteBillingPeriod = Joi.object().keys({
    billingPeriodID: Joi.string().required()
})

module.exports.getStudentLevelFee = Joi.object().keys({
    feeTypeID: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string()
})
module.exports.getReportRegistrations = Joi.object().keys({
    branchID: Joi.string().required(),
    mobileNumber: Joi.string(),
    recordSequenceNo: Joi.string(),
    requiredCount: Joi.string()
})
module.exports.getReportDatewiseRegistrations = Joi.object().keys({
    schoolID: Joi.string(),
    branchID: Joi.string().required(),
    fromDate: Joi.string().required(),
    toDate: Joi.string().required()

    
})

module.exports.getCalendarEvents = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    classLevelID: Joi.string(),
    classID: Joi.string(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required() 
})

module.exports.getActiveSchools = Joi.object().keys({
    schoolID: Joi.string()
})


module.exports.deleteCalendarDays = Joi.object().keys({
    calendarID: Joi.string().required()
})

module.exports.getCalendarHolidays = Joi.object().keys({
    schoolID: Joi.string().required(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required()
})

module.exports.deleteClass = Joi.object().keys({
    classID: Joi.string().required()
})

module.exports.deleteClassLevel = Joi.object().keys({
    classLevelID: Joi.string().required()
})
module.exports.setClassRoom = Joi.object().keys({
    roomID: Joi.string(),
    branchID: Joi.string().required(),
    roomName: Joi.string().required(),
    roomDesc: Joi.string(),
    isClassable: Joi.string().required(),
    deviceIdentifier: Joi.string(),
    isActive: Joi.string().required()
})

module.exports.getClassRoom = Joi.object().keys({
    
    branchID: Joi.string().required()
})

module.exports.getActiveClassRoom= Joi.object().keys({
    branchID: Joi.string().required()
})

module.exports.deleteClassRoom = Joi.object().keys({
    roomID: Joi.string().required()
})

module.exports.getSchoolContract = Joi.object().keys({
    userID: Joi.string().required()
})

module.exports.setContractAcceptance = Joi.object().keys({
    acceptanceID:Joi.string(), 
    schoolID: Joi.string().required(), 
    userID: Joi.string().required(), 
    ipAddress: Joi.string().required(), 
    contractID: Joi.string().required(), 
    contractText:Joi.string().required(), 
    chargesID: Joi.string().required(), 
    chargesText: Joi.string().required(), 
    collectionID: Joi.string(),
    collectionText:Joi.string()
})


module.exports.deleteContract = Joi.object().keys({
    contractID: Joi.string().required()
})

module.exports.getUserCommunications = Joi.object().keys({
    userID: Joi.string().required(),
    branchID: Joi.string().required(),
    classID: Joi.string(),
    classLevelID: Joi.string(),
    studentID: Joi.string()
})

module.exports.getBranchClasses = Joi.object().keys({
    branchID: Joi.string().required()
})

module.exports.getAcademicEvents = Joi.object().keys({
    branchID: Joi.string().required(),
    classLevelID: Joi.string(),
    classID: Joi.string(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required()
})

module.exports.setAcademicEventsArray = Joi.array().items(
    Joi.object().keys({
    eventID: Joi.string(),
    classLevelID: Joi.string().required(),
    classID: Joi.string(),
    eventDate: Joi.string().required(),
    eventName: Joi.string().required(),
    userID: Joi.string().required(),
}))

module.exports.getCalendarList = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required()
})

module.exports.deleteAcademicEvents = Joi.object().keys({
    eventID: Joi.string().required()
})

// module.exports.setContractChargesArray = Joi.array().items(
//     Joi.object().keys({
//     chargesID: Joi.string(),
//     collectionID: Joi.string(),
//     schoolID: Joi.string().required(),
//     currencyCode: Joi.string().required(),
//     unitPrice: Joi.string().required(),
//     taxRate: Joi.string().required(),
//     billingFrequency: Joi.string().required(),
//     chargesNotes: Joi.string().allow(""),
//     startDate: Joi.string().required(),
//     endDate: Joi.string(),
//     isActiveCharges: Joi.string().required(),
//     collectionType: Joi.string(),
//     collectionRate: Joi.string(),
//     userID: Joi.string().required(),
//     collectionNotes: Joi.string().allow(""),
//     collectionAcTitle: Joi.string(),
//     collectionAcNumber: Joi.string(),
//     collectionAcBranch: Joi.string(),
//     collectionAcBank: Joi.string(),
//     isActiveCollection: Joi.string()
    

// }))

// module.exports.getContractCharges = Joi.object().keys({
//     schoolID: Joi.string().required()
// })

module.exports.deleteSchoolContract = Joi.object().keys({
    schoolID: Joi.string().required()
})

module.exports.deleteFeeType = Joi.object().keys({
    feeTypeID: Joi.string().required()
})

module.exports.getBranchGroups = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string()
})
module.exports.getSelfJoinSchools = Joi.object().keys({

})

module.exports.getClassesCombined = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string()
})

module.exports.getContractGroupCharges = Joi.object().keys({
    schoolID: Joi.string().required()
})

module.exports.setContractGroupCharges = Joi.object().keys({
    
    chargesID: Joi.string(),
    branchGroupID:Joi.string().required(),
    currencyCode: Joi.string().required(),
    unitPrice: Joi.string().required(),
    taxRate: Joi.string().required(),
    billingFrequency: Joi.string().required(),
    chargesNotes: Joi.string().allow(""),
    startDate: Joi.string().required(),
    endDate: Joi.string(),
    userUUID: Joi.string().required(),
    isActive: Joi.string().required()

})

module.exports.getContractCollection = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string()
})

module.exports.setContractCollectionArray = Joi.array().items(
    Joi.object().keys({
    
    collectionID: Joi.string(),
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    collectionType: Joi.string().required(),
    collectionRate: Joi.string().required(),
    collectionNotes: Joi.string().allow(""),
    startDate: Joi.string().required(),
    userUUID: Joi.string().required(),
    isActive: Joi.string().required()
}))

module.exports.getSettlementSchoolReport = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    fromDate: Joi.string().required(),
    toDate: Joi.string().required(),
    isExport: Joi.string().required()
})

module.exports.setSettlementVerification = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    voucherUUID: Joi.string().required(),
    isSettled: Joi.string(),
    isVerified: Joi.string(),
    userUUID :Joi.string()
})