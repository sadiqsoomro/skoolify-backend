const Joi = require('joi')

module.exports.setCommunicationTypes = { 
    commTypeID: Joi.string(),
    schoolID:  Joi.string().required(),
    branchID: Joi.string().required(), 
    commTypeName: Joi.string().required(),
    isActive: Joi.string().required()

}

module.exports.getCommunicationTypes = { 
    schoolID:  Joi.string().required(),
    branchID: Joi.string().required()
}



module.exports.getCommunicationTypesMobile = { 
    schoolID:  Joi.string().required(),
    branchID: Joi.string().required()

}

module.exports.setCommunicationThreadMobile = { 
    parentUUID:  Joi.string().required(),
    studentUUID: Joi.string().required(),
    threadUUID: Joi.string(),
    threadSubject: Joi.string().required(),
    commTypeID: Joi.string().required(),
    commText:Joi.string().required()

}

module.exports.setCommunicationThread = { 
    parentUUID:  Joi.string().required(),
    studentUUID: Joi.string().required(),
    userUUID: Joi.string().required(),
    threadUUID: Joi.string().required(),
    threadSubject: Joi.string().required(),
    commTypeID: Joi.string().required(),
    commText:Joi.string().required()

}
module.exports.getCommunicationThreadMobile = { 
    threadUUID: Joi.string().required(),
    parentUUID:  Joi.string().required(),
    studentUUID: Joi.string().required(),
    lastDate: Joi.string()

}

module.exports.getCommunicationGroupMobile = { 
    parentUUID:  Joi.string().required(),
    studentUUID: Joi.string().required()

}
module.exports.setCommunicationStatusChange = { 
    parentUUID:  Joi.string().required(),
    studentUUID: Joi.string().required(),
    threadUUID: Joi.string().required(),
    communicationID:Joi.string().required(),
    readStatus:Joi.string().required()

}

module.exports.setCommunicationArchive = { 
    parentUUID:  Joi.string().required(),
    studentUUID: Joi.string().required(),
    threadUUID: Joi.string().required(),
    archiveFlag: Joi.string().required()
}
// module.exports.getCommunicationSearch = { 
//     parentUUID:  Joi.string().required(),
//     studentUUID: Joi.string().required(),
//     threadSubject: Joi.string().required()
// }

