const Joi = require('joi')

module.exports.assignTeacher = Joi.object().keys({
    teacherSubjectLinkID :Joi.string(),
    userID:Joi.string().required(),
    classID:Joi.string().required(),
    subjectID:Joi.string().required(),
    isClassTeacherFlag:Joi.string().required()
})

module.exports.deleteSubjectTeacher = Joi.object().keys({
    teacherSubjectLinkID:Joi.string().required()
})

module.exports.assignNSTeacher = Joi.object().keys({
    userID:Joi.string().required(),
    classID:Joi.string().required(),
    nonsubjectID:Joi.string().required(),
    isClassTeacherFlag:Joi.string().required()
})

module.exports.deleteNSTeacher = Joi.object().keys({
    nonSubjectLinkID:Joi.string().required()
})