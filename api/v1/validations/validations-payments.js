const Joi = require('joi')

module.exports.getPaymentProviderDetails = Joi.object().keys({
    providerID: Joi.string().required(),
    sourceUUID: Joi.string().required(),
    parentUUID: Joi.string().required(),
    voucherUUID: Joi.string().required(),
})

module.exports.setPaymentTransactionLogWeb = Joi.object().keys({
    providerID: Joi.string(),
    sourceUUID:Joi.string(),
    parentUUID: Joi.string(),
    userUUID:Joi.string(),
    IPAddress:Joi.string().required(),
    providerTxnRefNo: Joi.string().required(),
    internalTxnRefNo: Joi.string().required(),
    maskedInstrument: Joi.string().required(),
    paymentCurrency: Joi.string().required(),
    paymentAmount: Joi.string().required(),
    tranRequestDateTime: Joi.string().required(),
    tranRequestCode: Joi.string(),
    additionalData: Joi.string().allow(""),
    tranResponseDateTime: Joi.string().required(),
    tranResponseCode:Joi.string(),
    tranResponseData:Joi.string().required()

})
module.exports.getPaymentVoucherCharges = Joi.object().keys({
    providerID: Joi.string().required(),
    bankUUID: Joi.string(),
    voucherUUID: Joi.string().required()
})

module.exports.getBankPaymentVoucherCharges = Joi.object().keys({
    providerID: Joi.string().required(),
    bankUUID: Joi.string(),
    voucherUUID: Joi.string().required()
})


module.exports.getPaymentProviderTypes = Joi.object().keys({
    schoolID: Joi.string().required()
})

module.exports.getPaymentProviders = Joi.object().keys({
    transactionTypeID: Joi.string().required(),
    schoolID: Joi.string()
})

module.exports.getPaymentLogs = Joi.object().keys({
    internalTxnRefNo: Joi.string().required(),
    tranRequestCode: Joi.string(),
    paymentCurrency: Joi.string(),
    paymentAmount: Joi.string(),
    maskedInstrument: Joi.string(),
    providerTxnRefNo: Joi.string()
})

module.exports.setSchoolServiceChargesGenerate = Joi.object().keys({
    schoolID: Joi.string(),
    monthNumber: Joi.string()

})

module.exports.getSchoolServiceCharges = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    billingFromDate: Joi.string().required(),
    billingToDate: Joi.string().required(),
    userID: Joi.string()
})

module.exports.getSchoolOutstandingList = Joi.object().keys({
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    userID: Joi.string()
})

module.exports.setCreateInvoicesInERP = Joi.object().keys({
    
    schoolID: Joi.string(),
    monthNumber: Joi.string()
})

module.exports.getCustomerIDfromERP = Joi.object().keys({
    customerName:Joi.string()
})

module.exports.setInvoiceStatusUpdatefromERP = Joi.object().keys({
    invoiceNumber:Joi.string().required(),
    paymentStatus: Joi.string().required()
})




module.exports.getAvanzaValidateCustomer = Joi.object().keys({
    schoolID:Joi.string().required(),
    branchID:Joi.string().required(),
    bankUUID:Joi.string().required(),
    parentUUID:Joi.string().required(),
    transactionAmount:Joi.string().required(),
    cnicNumber:Joi.string().required(),
    bankAccountNumber:Joi.string().required()

})

module.exports.getAvanzaValidateOTP = Joi.object().keys({
    sourceUUID:Joi.string().required(),
    parentUUID:Joi.string().required(),
    IPAddress:Joi.string().allow(""),
    voucherUUID:Joi.string().required(),
    schoolID:Joi.string().required(),
    branchID:Joi.string().required(),
    bankUUID:Joi.string().required(),
    transactionAmount:Joi.string().required(),
    cnicNumber:Joi.string().required(),
    bankAccountNumber:Joi.string().required(),
    basketID:Joi.string().required(),
    transactionOTP: Joi.string().required(),
    transactionRefNo: Joi.string().required()
})


module.exports.getNIFTInitialize = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    transactionAmount:Joi.string().required(),
    bankUUID:Joi.string().required(),
    referenceNotes: Joi.string().required(),
    productDesc:Joi.string().required(),
    paymentCurrency:Joi.string().required()
    
})

module.exports.getNIFTCustomerValidation = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID:Joi.string().required(),
    bankUUID: Joi.string().required(),
    transactionToken:Joi.string().required(),
    transactionRefNo:Joi.string().required(),
    transactionAmount:Joi.string().required(),
    cnicNumber:Joi.string().required(),
    bankAccountNumber:Joi.string().required()
})

module.exports.setNIFTValidateOTP = Joi.object().keys({
    schoolID:Joi.string().required(),
    branchID:Joi.string().required(),
    sourceUUID:Joi.string().required(),
    parentUUID:Joi.string().required(),
    IPAddress:Joi.string().allow(""),
    voucherUUID:Joi.string().required(),
    bankUUID:Joi.string().required(),
    transactionToken:Joi.string().required(),
    transactionRefNo:Joi.string().required(),
    transactionOTP:Joi.string().required(),
    transactionAmount:Joi.string().required(),
    cnicNumber:Joi.string().required(),
    bankAccountNumber:Joi.string().required()
    
})

module.exports.getNIFTPaymentStatus = Joi.object().keys({
    schoolID:Joi.string().required(),
    branchID:Joi.string().required(),
    voucherUUID:Joi.string().required(),
    transactionAmount:Joi.string().required(),
    cnicNumber:Joi.string().required(),
    bankAccountNumber:Joi.string().required()   
    
})

module.exports.setPaymentLogHBLCC = Joi.object().keys({
    providerID: Joi.string(),
    sourceUUID:Joi.string(),
    parentUUID: Joi.string(),
    userUUID:Joi.string(),
    IPAddress:Joi.string().required(),
    providerTxnRefNo: Joi.string().required(),
    internalTxnRefNo: Joi.string().required(),
    maskedInstrument: Joi.string().required(),
    paymentCurrency: Joi.string().required(),
    paymentAmount: Joi.string().required(),
    tranRequestDateTime: Joi.string().required(),
    tranRequestCode: Joi.string(),
    additionalData: Joi.string().allow(""),
    tranResponseDateTime: Joi.string().required(),
    tranResponseCode:Joi.string(),
    tranResponseData:Joi.string().required()

})

module.exports.getProviderLogo = Joi.object().keys({
    
    voucherUUID: Joi.string().required()
})