const Joi = require('joi')

module.exports.getStudents = { 
    parentID: Joi.string(),
    parentUUID: Joi.string() 
    }

module.exports.setStudents = {
    isActive: Joi.string().required(),
    picture: Joi.string(),
    dob: Joi.string().required(),
    rollNo: Joi.string().required(),
    gender: Joi.string().required(),
    studentName: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string(),
    classYear: Joi.string(),
    enrollDate: Joi.string()
}

module.exports.setStudentsArray = Joi.array().items(
    Joi.object({
    isActive: Joi.string().required(),
    classYear: Joi.string().required(),
    picture: Joi.string(),
    dob: Joi.string().required(),
    rollNo: Joi.string().required(),
    gender: Joi.string().required(),
    studentName: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string()
    })
)


module.exports.getStudentFees = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    classID: Joi.string().required(),
    billingPeriodID: Joi.string(),
    studentID: Joi.string()
})

module.exports.getStudentOverallResults = Joi.object().keys({
    classID: Joi.string().required(),
    examsTermID: Joi.string().required()
})

module.exports.setStudentFees = Joi.object().keys({
    challanNote: Joi.string(),
    paymentDate: Joi.string(),
    endDate:Joi.string(),
    status: Joi.string().required(),
    dueDate: Joi.string(),
    taxAmountAfterDue: Joi.string().required(),
    taxAmountAfterDue: Joi.string(),
    totalAmountAfter: Joi.string(),
    taxAmountDue: Joi.string(),
    totalAmountDue: Joi.string().required(),
    penaltyType:Joi.string(),
    penaltyValue:Joi.string(),
    billingPeriod: Joi.string().required(),
    invoiceDate: Joi.string().required(),
    invoiceNumber: Joi.string(),
    studentID: Joi.string().required(),
    challanID: Joi.string(),
    detail:  Joi.array().items(
        Joi.object({
            challanID: Joi.string(),
            feeTypeID: Joi.string().required(),
            feeAmount:Joi.string().required(),
    		challanDetailID:Joi.string()
        })        
    )
})

module.exports.setFeeVoucherGenerate = Joi.object().keys({

    studentID: Joi.string(),
    classID: Joi.string().required(),
    invoiceStartNo: Joi.string(),
    invoiceDate: Joi.string().required(),
    billingPeriodID: Joi.string().required(),
    penaltyType : Joi.string().required(),
    penaltyAmount: Joi.string().required().required(),
    dueDate:Joi.string().required(),
    voucherNote:Joi.string().allow(""),
    endDate: Joi.string(),
    
    voucherDetails:  Joi.array().items(
        Joi.object({
  
            feeTypeID: Joi.string().required(),
            feeTypeAmount:Joi.string().required()
    		
        })        
    )
})


module.exports.setFeeVoucherStatus = Joi.object().keys({
    billingPeriodID: Joi.string().required(),
    isPublished: Joi.string().required(),
    classID: Joi.string().required(),
    voucherUUID: Joi.string()
    
})


module.exports.getAttendance = Joi.object().keys({
    
    classID: Joi.string().required(),
    studentID: Joi.string(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required()
})

// module.exports.setAttendance = Joi.object().keys({
//     userID: Joi.string().required(),
//     isPresent: Joi.string().required(),
//     attendanceDate: Joi.string().required(),
//     studentID: Joi.string().required(),
//     classID: Joi.string().required(),
//     notes: Joi.string(),
//     attendanceID: Joi.string()
// })

module.exports.setAttendanceArray = Joi.array().items(
    Joi.object({
    userID: Joi.string().required(),
    isPresent: Joi.string().required(),
    attendanceDate: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string().required(),
    notes: Joi.string().allow(""),
    attendanceID: Joi.string()
    })
)


module.exports.setFeeTypes = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    feeTypeID: Joi.string(),
    feeType: Joi.string().required(),
    taxRate: Joi.string(),
    discountRate:Joi.string(),
    isRecurring: Joi.string(),
    isActive: Joi.string().required()
})

module.exports.setFeeStatus = Joi.object().keys({
    voucherUUID: Joi.string().required(),
    paymentStatus: Joi.string().required(),
    paymentDate: Joi.string(),
    paidAmount: Joi.string(),
    channelType:Joi.string()


})

// module.exports.setFeeDetails = Joi.object().keys({
//     feeAmount: Joi.string().required(),
//     feeTypeID: Joi.string(),
//     challanID: Joi.string().required(),
//     challanDetailID: Joi.string()
// })

module.exports.deleteFeeChallanDetail = Joi.object().keys({
    feeChallanDetailID: Joi.string().required()
})

module.exports.setStudentImage = Joi.object().keys({
    studentID: Joi.string().required(),
    picture: Joi.string().required()
})
module.exports.getStudentsbyParentID = { 
    parentID: Joi.string().required(),
    schoolID:Joi.string().required()
}

module.exports.getStudentSearch = { 
    schoolID: Joi.string().required(),
    branchID:Joi.string().required(),
    rollNo: Joi.string(),
    studentName: Joi.string()
}

module.exports.getRecurringFee = {
    
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    classID:  Joi.string().required(),
    studentID: Joi.string(),
    feeTypeID: Joi.string().required(),
    feeLinkID: Joi.string()
    
}

module.exports.setRecurringFeeArray = Joi.array().items(
    Joi.object({
    feeLinkID: Joi.string(),
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    studentID: Joi.string().required(),
    feeTypeID: Joi.string().required(),
    feeAmount: Joi.string().required()    
    })
)


module.exports.getFeeVoucher = Joi.object().keys({
    branchID:Joi.string().required(),
    billingPeriodID: Joi.string(),
    voucherID: Joi.string(),
    studentID: Joi.string(),
    classID: Joi.string(),
    paymentStatus:Joi.string()
})

module.exports.getFeeVoucherDetail = Joi.object().keys({
    billingPeriodID: Joi.string().required(),
    studentID: Joi.string().required(),
    classID:Joi.string().required()
})

module.exports.setFeeVoucherDelete = Joi.object().keys({
    voucherUUID: Joi.string().required(),
    feeVoucherDetailID: Joi.string()
})

module.exports.setFeeVoucher = Joi.object().keys({
    voucherID: Joi.string(),
    studentID: Joi.string().required(),
    classID: Joi.string().required(),
    invoiceNumber: Joi.string().required(),
    invoiceDate: Joi.string().required(),
    billingPeriodID: Joi.string().required(),
    penaltyType: Joi.string().required(),
    penaltyValue: Joi.string().required(),
    dueDate: Joi.string().required(),
    paymentStatus: Joi.string(),
    paidAmount: Joi.string(),
    paymentDate: Joi.string(),
    voucherNote: Joi.string(),
    endDate: Joi.string(),
    voucherDetails:  Joi.array().items(
        Joi.object({
            feeVoucherDetailID: Joi.string(),
            feeTypeID: Joi.string().required(),
            feeTypeAmount:Joi.string().required()
    		
        })        
    )
})

module.exports.getFeesVoucherPrint = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    classID: Joi.string().required(),
    billingPeriodID: Joi.string(),
    studentID: Joi.string()
})

module.exports.getAttendanceReport = Joi.object().keys({

    studentID: Joi.string(),
    examsTermID: Joi.string(),
    classID: Joi.string().required(),
    fromDate: Joi.string(),
    toDate: Joi.string()
})

module.exports.setSubjectAttendanceArray = Joi.array().items(
    Joi.object({
    attendanceID: Joi.string(),
    attendanceDate: Joi.string().required(),
    batchNo: Joi.string().required(),
    timetableSubjectID:Joi.string().required(),
    classID:  Joi.string().required(),
    studentID: Joi.string().required(),
    isPresent: Joi.string().required(),
    notes: Joi.string().allow(""),
    userID: Joi.string().required()  
    })
)

// module.exports.setSubjectAttendance = Joi.object().keys({

//     subjectAttendanceID: Joi.string().required(),
//     attendanceDate: Joi.string().required(),
//     batchNo: Joi.string().required(),
//     timetableSubjectID:Joi.string().required(),
//     studentID: Joi.string().required(),
//     isPresent: Joi.string().required(),
//     notes: Joi.string().allow(""),
//     userID: Joi.string().required()  
//     })



module.exports.getStudentSubjectAttendance = Joi.object().keys({
    attendanceDate: Joi.string().required(),
    batchNo: Joi.string(),
    timetableSubjectID:Joi.string().required()
})


module.exports.assignSubjectToStudentArray = Joi.array().items(
    Joi.object({
    studentSubjectID: Joi.string(),
    studentID: Joi.string().required(),
    subjectID: Joi.string().required(),
    actionFlag:Joi.string().required(),
    timetableSubjectID:Joi.string().required()
    
})
)

module.exports.getStudentSubjects = Joi.object().keys({
    studentID: Joi.string(),
    subjectID:Joi.string(),
    timetableSubjectID:Joi.string()
})



module.exports.deleteSubjectToStudentArray = Joi.array().items(
    Joi.object({
    studentSubjectID: Joi.string().required()
    
})
)

module.exports.setStudentTimeTableLinkArray =  Joi.array().items(
    Joi.object({
    studentTimetableID: Joi.string(),
    studentID: Joi.string().required(),
    timetableSubjectID:Joi.string().required()
})
)

module.exports.setStudentTimeTableDelink = Joi.object().keys({
    studentTimetableID: Joi.string().required()
})

module.exports.getStudentTimeTableLinked = Joi.object().keys({
    studentID: Joi.string().required(),
    timetableSubjectID:Joi.string()
})
module.exports.getActiveSubjectsInTimetable = Joi.object().keys({
    branchID: Joi.string().required(),
    subjectID: Joi.string().required(),
    userID: Joi.string(),
    weekdayNo:Joi.string(),
    attendanceDate: Joi.string()
})

module.exports.getStudentsInTimetable = Joi.object().keys({
    subjectID: Joi.string().required(),
    weekdayNo:Joi.string().required(),
    timetableSubjectID: Joi.string().required()
})

module.exports.getStudentOwnSubjects = Joi.object().keys({
    studentID: Joi.string().required() 
})

module.exports.getStudentDailyAttendance = Joi.object().keys({
    attendanceDateFrom: Joi.string().required(),
    attendanceDateTo: Joi.string().required(),
    batchNo: Joi.string(),
    studentID:Joi.string().required()
})


module.exports.getStudentAttendance = Joi.object().keys({
    schoolID: Joi.string().required(),
    classID: Joi.string().required(),
    studentID: Joi.string().required(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required()
})


module.exports.setStudentNotesTag = Joi.object().keys({
    tagUUID: Joi.string(),
    branchID: Joi.string().required(),
    tagText:  Joi.string().required(),
    userID:  Joi.string().required(),
    isActive: Joi.string().required()

})

module.exports.setStudentNotes = Joi.object().keys({
    branchID: Joi.string().required(),
    classID: Joi.string().required(),
    studentNotesUUID: Joi.string(),
    studentUUID: Joi.string().required(),
    tagUUID: Joi.string(),
    tagText:  Joi.string(),
    studentNotes:  Joi.string().required(),
    userID:  Joi.string().required()

})

module.exports.getActiveStudentTags = Joi.object().keys({
    branchID: Joi.string().required()
})

module.exports.getStudentTags = Joi.object().keys({
    branchID: Joi.string().required()
})

module.exports.getStudentNotes = Joi.object().keys({
    studentUUID: Joi.string().required(),
    branchID: Joi.string().required()
})

module.exports.deleteStudentNotesTag = Joi.object().keys({
    tagUUID: Joi.string().required()
})

module.exports.deleteStudentNotes = Joi.object().keys({
    studentNotesUUID: Joi.string().required()
    
})

module.exports.getParentAndInsert = { 
    mobileNumber: Joi.string().required(),
    schoolID: Joi.string().required(),
    sourceUUID: Joi.string().required()
}

module.exports.setStudentSelfJoin = {
    studentUUID:Joi.string(), 
    parentUUID: Joi.string().required(),
    studentName: Joi.string().required(),
    gender: Joi.string().required(),
    rollNo: Joi.string().required(),
    dob: Joi.string().required(),    
    classID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    branchID: Joi.string().required(),
    schoolID: Joi.string().required(),
    
}

module.exports.deleteStudentSelfJoin = { 
    studentUUID: Joi.string().required(),
    parentUUID: Joi.string().required()
}

module.exports.getAttendanceMobile = Joi.object().keys({
    
    classID: Joi.string().required(),
    studentID: Joi.string(),
    dateFrom: Joi.string().required(),
    dateTo: Joi.string().required()
})

module.exports.getFeeVoucherExport = Joi.object().keys({
    branchID: Joi.string().required(),
    classID: Joi.string(),
    studentID: Joi.string(),
    billingPeriodID: Joi.string(),
    paymentStatus: Joi.string()
})


// module.exports.setParentLanguage = { 
//     sourceUUID: Joi.string().required(),
//     preferredLanguage: Joi.string().required()
// }