
const Joi = require('joi')
module.exports.setMeetingInvite= Joi.object({
    
    meetingID: Joi.string(),
    branchID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    classID: Joi.string().required(),
    subjectID: Joi.string().allow(""),
    studentIDList: Joi.string().allow(""),
    meetingTopic: Joi.string().required(),
    roomName: Joi.string().required(),
    agendaDesc: Joi.string().allow(""),
    meetingRefNo: Joi.string(),
    meetingPassword: Joi.string(),
    isRecurring: Joi.string().required(),
    recurringType: Joi.string(),
    webLink: Joi.string().required(),
    weekdayNo: Joi.string(),
    noOfRecurrence: Joi.string(),
    startDate: Joi.string().required(),
    startTime: Joi.string().required(),
    endTime: Joi.string().required(),
    endDate: Joi.string().allow(""),
    timezone: Joi.string().required(),
    reminderOn: Joi.string().required(),
    reminderTime: Joi.string(),
    hostUserID: Joi.string().required(),
    createdBy: Joi.string().required(),
    isActive: Joi.string().required(),
})



module.exports.getMeetingInvite = Joi.object({
    
    meetingID: Joi.string(),
    branchID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    classID: Joi.string(),
    subjectID: Joi.string(),
    hostUserID:Joi.string(),
    createdBy:Joi.string()
})


module.exports.getUserMeetingInvite = Joi.object({
    branchID: Joi.string().required(),
    classLevelID: Joi.string(),
    classID: Joi.string(),
    hostUserID:Joi.string().required(),
})

module.exports.getMeetingDetails = Joi.object({
    meetingID: Joi.string().required()
})

module.exports.deleteMeetingOccurence = Joi.object({
    recurrenceID: Joi.string().required()
})

module.exports.deleteMeetingInvite = Joi.object({
    meetingID: Joi.string().required()
})

module.exports.getMeetingInvitees = Joi.object({
    studentIDList: Joi.string(),
    meetingID:    Joi.string()
})

module.exports.getMeetingJoin = Joi.object({
    meetingID: Joi.string().required()
})

module.exports.setMeetingSession = Joi.object({
    sessionID: Joi.string(),
    meetingID: Joi.string().required(),
    recurrenceID: Joi.string().required(),
    parentUUID: Joi.string(),
    studentUUID: Joi.string(),
    sourceUUID: Joi.string().required(),
    isHost: Joi.string().required(),
    roomSID: Joi.string().required(),
    participantSID: Joi.string().required()
})

module.exports.getMeetingRecurrence = Joi.object({
    meetingID: Joi.string().required()
})
