const Joi = require('joi')

// module.exports.getTimeTableCount = Joi.object().keys({   
//     classId: Joi.string().required(),
//     branchId: Joi.string().required(), 
//     schoolId: Joi.string().required()
// }).with('classId', 'branchId', 'schoolId')

module.exports.getTimeTable = Joi.object().keys({    
    classID: Joi.string().required(),
    branchID: Joi.string().required(), 
    schoolID: Joi.string().required()
})

module.exports.setTimeTable = Joi.object().keys({    
    tableID: Joi.string(), 
    classID: Joi.string().required(),  
    weekday: Joi.string().required(), 
    time1: Joi.string(),
    subjectID1: Joi.string(),
    time2: Joi.string(),
    subjectID2: Joi.string(),
    time3: Joi.string(),
    subjectID3: Joi.string(),
    time4: Joi.string(),
    subjectID4: Joi.string(),
    time5: Joi.string(),
    subjectID5: Joi.string(),
    time6: Joi.string(),
    subjectID6: Joi.string(),
    time7: Joi.string(),
    subjectID7: Joi.string(),
    time8: Joi.string(),
    subjectID8: Joi.string(),
    time9: Joi.string(),
    subjectID9: Joi.string(),
    time10: Joi.string(),
    subjectID10 : Joi.string()
})

module.exports.setTimeTableArray = Joi.array().items(
    Joi.object({
    tableID: Joi.string(), 
    classID: Joi.string().required(),  
    weekdayNo: Joi.string().required(), 
    timetableTimeID: Joi.string(), 
    subjectID: Joi.string().required(),
    subjectType: Joi.string().required(),
    startTime: Joi.string().required(),
    endTime: Joi.string().required()
    })
)



module.exports.deleteTimeTable = Joi.object().keys({    
    tableID: Joi.string().required()
})

module.exports.setActivityArray = Joi.array().items(  
    Joi.object({ 
    activityID: Joi.string(),
    branchID: Joi.string().required(),
    activityName: Joi.string().required(),
    startTime: Joi.string().required(),
    userID: Joi.string().allow(""),
    endTime: Joi.string().required(),
    isActive: Joi.string().required()

    })
)


module.exports.getActivity = Joi.object().keys({    
    activityID: Joi.string(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string()
})

module.exports.getActiveActivity = Joi.object().keys({    
    activityID: Joi.string(),
    branchID: Joi.string().required(),
    weekdayNo: Joi.string()
})

module.exports.deleteActivity = Joi.object().keys({    
    activityID: Joi.string().required()
})

module.exports.getStudentActivities = Joi.object().keys({    
    activityStudentID: Joi.string(),
    activityID:Joi.string().required(),
    classID: Joi.string().required(),
    studentID:Joi.string(),
    weekdayNo: Joi.string()
    
})

module.exports.setStudentActivitiesArray = Joi.array().items(
    Joi.object({   
    activityStudentID: Joi.string(),
    actionFlag: Joi.string().required(),
    activityID: Joi.string().required(),
    studentID: Joi.string().required(),
    weekdayNo:Joi.string().required()
})
)

module.exports.deleteStudentActivities = Joi.object().keys({    
    activityStudentID: Joi.string().required()
    
})


module.exports.getTimeTableTimings = Joi.object().keys({    
    classID: Joi.string().required(),
    weekdayNo:Joi.string().required()
})

module.exports.deleteTimeTableTimings = Joi.object().keys({ 
    tableID: Joi.string().required(),
    timetableTimeID: Joi.string().required()
    
})

module.exports.getStudentTimeTable = Joi.object().keys({    
    studentID: Joi.string().required()
})



module.exports.setSubjectTimetable = Joi.object().keys({
    timetableSubjectID: Joi.string(),
    subjectID: Joi.string().required(),
    weekdayNo: Joi.string().required(),
    startTime:Joi.string().required(),
    endTime:Joi.string().required(),
    roomID:Joi.string(),
    userID:Joi.string()
})

module.exports.getSubjectTimetable = Joi.object().keys({
    
    branchID: Joi.string().required(),
    weekdayNo: Joi.string(),
    subjectID: Joi.string(),
    userID: Joi.string()
})

module.exports.deleteSubjectTimetable = Joi.object().keys({
    
    timetableSubjectID: Joi.string().required()
})


