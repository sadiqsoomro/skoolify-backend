const Joi = require('joi')

module.exports.getRoles = Joi.object().keys({
    schoolID: Joi.string().required()
    
})

module.exports.deleteRole = Joi.object().keys({
    roleID: Joi.string().required()
    
})

module.exports.getUserAssignedRoles = Joi.object().keys({
    schoolID: Joi.string().required(),
    branchID: Joi.string().required(),
    userID : Joi.string(),
    userRoleID: Joi.string()    
})

module.exports.getRoleAccessRights = Joi.object().keys({
    schoolID: Joi.string().required(),
    roleID: Joi.string().required(),
    moduleID: Joi.string()    
})


module.exports.setRole= Joi.object().keys({
    roleID: Joi.string(),
    roleName: Joi.string().required(),
    roleDesc: Joi.string(),
    schoolID: Joi.string().required(),
    isActive: Joi.string().required()
    
})

module.exports.setAssignRoles= Joi.object().keys({
    roleLinkID: Joi.string(),
    userID: Joi.string().required(),
    schoolID: Joi.string().required(),
    branchID: Joi.string(),
    roleID: Joi.string().required()
})

module.exports.setDeAssignRoles= Joi.object().keys({
    roleLinkID: Joi.string().required()
})

module.exports.setRoleAccessRights= Joi.object().keys({
    accessID: Joi.string(),
    moduleID: Joi.string().required(),
    roleID: Joi.string().required(),
    readAccess: Joi.string().required(),
    addAccess: Joi.string().required(),
    editAccess: Joi.string().required(),
    deleteAccess: Joi.string().required()
})

module.exports.setRoleAccessRightsArray = Joi.array().items(
    Joi.object({
        accessID: Joi.string(),
        moduleID: Joi.string().required(),
        roleID: Joi.string().required(),
        readAccess: Joi.string().required(),
        addAccess: Joi.string().required(),
        editAccess: Joi.string().required(),
        deleteAccess: Joi.string().required()
    }))

    module.exports.setAssignRolesArray = Joi.array().items(
        Joi.object({
            roleLinkID: Joi.string(),
            userID: Joi.string().required(),
            schoolID: Joi.string().required(),
            branchID: Joi.string(),
            roleID: Joi.string().required()
        }))
    