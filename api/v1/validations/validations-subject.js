const Joi = require('joi')

module.exports.getSubjects = Joi.object().keys({
   
    classLevelID: Joi.string().required(),
    subjectID: Joi.string(),
    classID: Joi.string(),
    userID: Joi.string()
})

module.exports.getActiveSubjects = Joi.object().keys({
   
    classLevelID: Joi.string().required(),
    subjectID: Joi.string(),
    classID: Joi.string(),
    userID: Joi.string()
})

module.exports.getExamSubjects = Joi.object().keys({
   
    classLevelID: Joi.string().required(),
    subjectID: Joi.string(),
    classID: Joi.string(),
    userID: Joi.string()
})


module.exports.getSubjectsTeachers = Joi.object().keys({
    
    classLevelID: Joi.string().required(),
    subjectID: Joi.string(),
    classID: Joi.string(),
    userID: Joi.string()
})

module.exports.getUserAllSubjects = Joi.object().keys({
    
    classID: Joi.string().allow(""),
    classLevelID: Joi.string().required(),
    userID: Joi.string().required(),
    subjectID: Joi.string()
})

module.exports.getUserExamSubjects = Joi.object().keys({
    
    classID: Joi.string().required(),
    userID: Joi.string().required(),
    subjectID: Joi.string()
})

module.exports.getNonSubjects = Joi.object().keys({
    //schoolID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    nonSubjectID: Joi.string()
})

module.exports.getActiveNonSubjects = Joi.object().keys({
    //schoolID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    nonSubjectID: Joi.string()
})

module.exports.deleteNonSubject = Joi.object().keys({
    nonSubjectID: Joi.string().required()
})

module.exports.getClassSubjects = Joi.object().keys({
    schoolID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    branchID: Joi.string().required()
})

module.exports.setClassSubjects = Joi.object().keys({
    subjectID: Joi.string().required(),
    classLevelID: Joi.string().required(),
    subjectClassLinkID: Joi.string()
})

module.exports.setSubjects= Joi.object().keys({
    subjectID: Joi.string(),
    classLevelID: Joi.string().required(),
    subjectName: Joi.string().required(),
    isActive: Joi.string().required()
})

module.exports.setSubjectsArray= Joi.array().items(
    Joi.object({
        subjectID: Joi.string(),
        classLevelID: Joi.string().required(),
        subjectName: Joi.string().required(),
        isActive: Joi.string().required() 
    })
)

module.exports.setNonSubjects= Joi.object().keys({
    nonSubjectID: Joi.string(),
    classLevelID: Joi.string().required(),
    nonSubjectName: Joi.string().required(),
    isActive: Joi.string().required()
})
module.exports.setClassNonSubjects= Joi.object().keys({
    nonSubjectClassLinkID: Joi.string(),
    nonSubjectID: Joi.string().required(),
    classLevelID: Joi.string().required()
})



module.exports.getNonSubjectLegends= Joi.object().keys({
    classLevelID: Joi.string().required(),
    nonSubjectID: Joi.string()
    
})

module.exports.setNonSubjectLegends= Joi.object().keys({
    nsLegendID: Joi.string(),
    nonSubjectID: Joi.string().required(),
    legendValue: Joi.string().required(),
    isActive: Joi.string().required()
})

module.exports.setDelinkSubjectTeacher= Joi.object().keys({
    subjectID: Joi.string().required(),
    userID: Joi.string().required()
})

module.exports.deleteSubject= Joi.object().keys({
    subjectID: Joi.string().required()
})

module.exports.getUserSubjects = Joi.object().keys({
    
    classID: Joi.string().allow(""),
    classLevelID: Joi.string().allow(""),
    userID: Joi.string().required(),
    subjectID: Joi.string()
})

module.exports.getUserNonSubjects = Joi.object().keys({
    
    classID: Joi.string().allow(""),
    classLevelID: Joi.string().allow(""),
    userID: Joi.string().required()
})

module.exports.getUserSubjects2 = Joi.object().keys({
    
    classID: Joi.string().allow(""),
    classLevelID: Joi.string().allow(""),
    userID: Joi.string().required(),
})

module.exports.getUserNonSubjectsWithTeacher = Joi.object().keys({
    userID: Joi.string().required(),
    classID: Joi.string(),
    classLevelID: Joi.string()
    
})