const Joi = require('joi'), utils =  require('../../utils/helper-methods'),
    moduleValidations = require('../validations/validations-module'),
    db = require('../../../db'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging  = 'module-controller'

import Error from '../response-classes/error'

// ==================================================================

exports.getModules = function(req, res) {
    const {moduleID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({moduleID}, moduleValidations.getModules, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = " Select * From v1_get_modules"; 
                    if(moduleID)
                    dbquery += ` where moduleID = '${moduleID}'` 
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
            }
        })
    }

exports.setModules = function(req, res) {
    const {moduleID,moduleName,moduleDescription,moduleURL,parentModuleID,isParent,isActive} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({moduleID,moduleName,moduleDescription,moduleURL,parentModuleID,isParent,isActive}, moduleValidations.setModules, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=` set @moduleID = ${utils.OptionalKey(moduleID)};
                    set @moduleName = ${utils.OptionalKeyString(moduleName)};
                    set @moduleDescription = ${utils.OptionalKeyString(moduleDescription)};
                    set @moduleURL = ${utils.OptionalKey(moduleURL)};
                    set @isParent = ${utils.OptionalKey(isParent)};
                    set @parentModuleID = ${utils.OptionalKey(parentModuleID)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_module
                    (@moduleID, @moduleName, @moduleDescription,@moduleURL,@parentModuleID,@isParent,@isActive);`

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                if(!res.finished)res.status(200).json({ response: "success"})
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
    })
}

exports.getActiveModules = function(req, res) {
    const {moduleID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({moduleID}, moduleValidations.getActiveModules, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = "select * from v1_get_modules_active"; 
                    if(moduleID)
                    dbquery += ` where moduleID = '${moduleID}'` 
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
            }
        })
    }