    const Joi = require('joi'), utils =  require('../../utils/helper-methods'),
    passwordValidations = require('../validations/validations-password'),
    db = require('../../../db'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging  = 'password-controller'

import Error from '../response-classes/error'

// ==================================================================

exports.passwordValidation = function(req, res) {
    const {  mobileNo,password,domain} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({mobileNo,password,domain}, passwordValidations.passwordValidation, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery= `  set @in_mobileNo = ${utils.OptionalKey(mobileNo)};
                                    set @in_password = ${utils.OptionalKey(utils.EncryptAES(password))};
                                    set @in_domain = ${utils.OptionalKey(domain)};
                                    call ${appConfig.getDbConfig().database}.v1_password_validation
                                    (@in_mobileNo, @in_password,@in_domain, @out_userID,@out_fullName,@out_lastLogin,@out_status); 
                                    select @out_userID as userID,@out_fullName as fullName, @out_lastLogin as lastLoginDate, @out_status as status;`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if(element[0].userID == null) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.NO_USER_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element[0] })
                                    }
                                   }
                               })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })

        }
    })
}


exports.changePassword = function(req, res) {
    const {  userID,newPassword} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({userID,newPassword}, passwordValidations.changePassword, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`   set @userID = ${utils.OptionalKey(userID)};
                                set @newPassword = ${utils.OptionalKey(utils.EncryptAES(newPassword))};
                                call ${appConfig.getDbConfig().database}.v1_change_password
                                (@userID, @newPassword);`
                console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success"})
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })

        }
    })
}

exports.resetPassword = function(req, res) {
    const {  userID,newPassword,updateUserID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({userID,newPassword,updateUserID}, passwordValidations.resetPassword, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`   set @userID = ${utils.OptionalKey(userID)};
                                set @newPassword = ${utils.OptionalKey(utils.EncryptAES(newPassword))};
                                set @updateUserID = ${utils.OptionalKey(updateUserID)};
                                call ${appConfig.getDbConfig().database}.v1_reset_password
                                (@userID, @newPassword,@updateUserID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success"})
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

