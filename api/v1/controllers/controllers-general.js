const Joi = require('joi'), 
    utils =  require('../../utils/helper-methods'),
    generalValidations = require('../validations/validations-general'),
    db = require('../../../db'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging  = 'general-controller'

import Error from '../response-classes/error'

// ==================================================================

exports.getParent = function (req, res) {
    const { schoolID, mobileNumber, bundleIdentifier, parentName, parentCNIC } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, mobileNumber,bundleIdentifier, parentName, parentCNIC }, generalValidations.getParent, 
        function (err, value) {
        if (!err) {
            db.getConnection(function(err, db) {
                
                if(!err){
                    let dbquery=`   set @schoolID = ${utils.OptionalKey(schoolID)};
                                    set @mobileNumber = ${utils.OptionalKey(mobileNumber)};
                                    set @parentName = ${utils.OptionalKeyString(parentName)};
                                    set @parentCNIC = ${utils.OptionalKeyString(parentCNIC)};
                                    set @bundleIdentifier = ${utils.OptionalKey(bundleIdentifier)};
                                    call ${appConfig.getDbConfig().database}.v1_get_parents
                                    (@schoolID, @mobileNumber,@parentName,@parentCNIC,@bundleIdentifier);`
                    
                    console.log(dbquery)   
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        data.forEach(element => {
                                            if(element.constructor==Array) {
                                                if (!_.isEmpty(element)) {
                                                    if(!res.finished)res.status(200).json({response: element })
                                                }
                                                else{
                                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                    if(!res.finished)res.status(404).json({ response: errorResponse })

                                                }
                                            }
                                        })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        console.log(errorResponse)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
            
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            
        }
    })
}

exports.getActiveParent = function (req, res) {
    const { mobileNumber } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ mobileNumber }, generalValidations.getActiveParent, function (err, value) {
        if (!err) {
            db.getConnection(function(err, db) {
                
                if(!err){
                    let dbquery=`  set @mobileNumber = ${utils.OptionalKey(mobileNumber)};
                                call ${appConfig.getDbConfig().database}.v1_get_parents_active
                                (@mobileNumber);`
                    console.log(dbquery)            
                    
                    try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        data.forEach(element => {
                                            if(element.constructor==Array) {
                                                if (!_.isEmpty(element)) {
                                                    if(!res.finished)res.status(200).json({response: element })
                                                }
                                                else{
                                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                    if(!res.finished)res.status(404).json({ response: errorResponse })

                                                }
                                            }
                                        })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        console.log(errorResponse)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }  
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
            
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            
        }
    })
}

exports.getParentByStudentID = function (req, res) {
    const { studentID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ studentID}, generalValidations.getParentByStudentID, function (err, value) {
        if (!err) {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_parent_by_student WHERE studentID = '${studentID}' `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
                                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                     
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}


exports.setParentStudentLink= function(req, res) {
    const {parentID,studentID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({parentID,studentID}, generalValidations.setParentStudentLink, 
        function(err, value) {
        if(!err) {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @parentID = ${utils.OptionalKey(parentID)};
                                    set @studentID = ${utils.OptionalKey(studentID)};
                                    call ${appConfig.getDbConfig().database}.v1_set_student_parent_link
                                    (@parentID,@studentID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })
                                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}

exports.setParentStudentDeLink= function(req, res) {
    const {parentID,studentID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({parentID,studentID}, generalValidations.setParentStudentDeLink, 
        function(err, value) {
        if(!err) {
            
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @parentID = ${utils.OptionalKey(parentID)};
                            set @studentID = ${utils.OptionalKey(studentID)};
                            call ${appConfig.getDbConfig().database}.v1_set_student_parent_delink
                            (@parentID,@studentID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                             
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })

        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}


exports.setParents= function(req, res) {
    const {parentID,parentName,parentLastName,parentCNIC,mobileNumber,email,parentType,isTaxRegistered, taxID,isActiveTaxPayer,addressLine1,addressLine2, addressState,addressPostalCode,cityID,countryID, isActive} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({parentID,parentName,parentLastName,parentCNIC,mobileNumber,email,parentType,isTaxRegistered, taxID,isActiveTaxPayer, addressLine1,addressLine2, addressState,addressPostalCode,cityID,countryID, isActive}, generalValidations.setParents, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            //console.error(utils.OptionalKeyString(utils.EncryptBase64(addressLine1)))
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @parentID = ${utils.OptionalKey(parentID)};
                                set @email = ${utils.OptionalKeyString(email)};
                                set @parentName = ${utils.OptionalKeyString(parentName)};
                                set @parentLastName = ${utils.OptionalKeyString(parentLastName)};
                                set @parentCNIC = ${utils.OptionalKeyString(parentCNIC)};
                                set @mobileNumber = ${utils.OptionalKey(mobileNumber)};
                                set @isTaxRegistered = ${utils.OptionalKey(isTaxRegistered)};
                                set @taxID = ${utils.OptionalKey(taxID)};
                                set @isActiveTaxPayer = ${utils.OptionalKey(isActiveTaxPayer)};
                                set @addressLine1 = ${utils.OptionalKeyString(utils.EncryptBase64(addressLine1))};
                                set @addressLine2 = ${utils.OptionalKeyString(utils.EncryptBase64(addressLine2))};
                                set @addressState = ${utils.OptionalKeyString(addressState)};
                                set @addressPostalCode = ${utils.OptionalKeyString(addressPostalCode)};
                                set @cityID = ${utils.OptionalKey(cityID)};
                                set @countryID = ${utils.OptionalKey(countryID)};
                                set @isActive = ${utils.OptionalKey(isActive)};
                                set @parentType = ${utils.OptionalKey(parentType)};
                                call ${appConfig.getDbConfig().database}.v1_set_parents
                                (@parentID,@parentName,@parentLastName,@parentCNIC,@mobileNumber,@email,@parentType,@isTaxRegistered,@taxID,@isActiveTaxPayer,@addressLine1, 
                                    @addressLine2,@addressState, @addressPostalCode, @cityID, @countryID, @isActive);`
                        console.log(dbquery)            
                        
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                //console.log(error.code)
                                if(!error){
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else if (error.code== 'ER_DUP_ENTRY'){
                                    log.doFatalLog(fileNameForLogging, req.url, 'duplication error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(400).json({ response: "unsuccessful", code: error.code, reason : "Duplication/Mobile No. already exist!" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: error.code, reason : appConfig.GENERIC_ERROR_MSG })
                                }
                            }); 
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
         }
    })
}

exports.getCountries = function (req, res) {
    const { countryID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ countryID }, generalValidations.getCountries, function (err, value) {
        if (err ) { 
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json({userMessage: appConfig.INVALID_PARAMETERS})
        } 
        
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_countries`;
                    if(countryID)
                        dbquery += ` where countryID=${countryID}`
                   
                    console.log(dbquery)
                        try {
                                db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    if (_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: data })
                                    }
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        console.log(errorResponse)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
})
}

exports.getActiveCountries = function (req, res) {
    const { countryID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ countryID }, generalValidations.getActiveCountries, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_countries where isActive=1 `;
                    if(countryID)
                        dbquery += ` and countryID=${countryID}`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: data })
                                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                               
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
            }
})
}


exports.getCities = function (req, res) {
    const { countryID,cityID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ countryID ,cityID}, generalValidations.getCities, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_cities where countryID = '${countryID}'`;
                    if(cityID)
                    dbquery += ` and cityID = '${cityID}'`;

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
                                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}

exports.getActiveCities = function (req, res) {
    const { countryID,cityID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ countryID ,cityID}, generalValidations.getActiveCities, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_cities_active where  countryID = '${countryID}'`;
                    if(cityID)
                    dbquery += ` and cityID = '${cityID}'`;

                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
                                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}

exports.getAreas = function (req, res) {
    const { cityID,areaID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ cityID,areaID }, generalValidations.getAreas, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_areas where cityID = '${cityID}'`;
                    if(areaID)
                        dbquery+= ` and areaID = '${areaID}'`;
                    console.log(dbquery)
                    
                    try {
                       db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                } 
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        }); 
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }   
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        

            
        }
    })
}

exports.getActiveAreas = function (req, res) {
    const { cityID,areaID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ cityID,areaID }, generalValidations.getActiveAreas, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_areas where isActive=1 and cityID = '${cityID}'`;
                    if(areaID)
                        dbquery+= ` and areaID = '${areaID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                } 
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        

            
        }
    })
}

exports.setCountries = function(req, res) {
    const { countryID, countryName,  dialingCode, currencyISOCode,currencyAlphaCode,currencyName,currencyDecimalValue, isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({countryID,  countryName,  dialingCode, currencyISOCode,currencyAlphaCode,currencyName,currencyDecimalValue, isActive }, generalValidations.setCountries, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    
                    let dbquery=`set @countryID = ${utils.OptionalKey(countryID)};
                                    set @countryName = ${utils.OptionalKeyString(countryName)};
                                    set @dialingCode = ${utils.OptionalKey(dialingCode)};
                                    set @currencyISOCode = ${utils.OptionalKey(currencyISOCode)};
                                    set @currencyAlphaCode = ${utils.OptionalKey(currencyAlphaCode)};
                                    set @currencyName = ${utils.OptionalKey(currencyName)};
                                    set @currencyDecimalValue = '${utils.OptionalKey(currencyDecimalValue)};
                                    set @isActive = ${utils.OptionalKey(isActive)};
                                    call ${appConfig.getDbConfig().database}.v1_set_countries
                                    (@countryID, @countryName, @dialingCode,@currencyISOCode,@currencyAlphaCode,@currencyName,@currencyDecimalValue, @isActive);`
                    console.log(dbquery)

                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}


exports.setCities = function(req, res) {
    const { cityID, cityName, gmtOffset, countryID, isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({cityID,cityName, gmtOffset, countryID, isActive }, generalValidations.setCities, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            
            db.getConnection(function(err, db) {
                if(!err){
                    
                    let dbquery=`  set @cityID = ${utils.OptionalKey(cityID)};
                    set @countryID = ${utils.OptionalKey(countryID)};
                    set @cityName = ${utils.OptionalKeyString(cityName)};
                    set @gmtOffset = ${utils.OptionalKey(gmtOffset)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_cities
                    (@cityID,@countryID, @cityName, @gmtOffset,@isActive);`
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })                               
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })

        }
    })
}


exports.setAreas = function(req, res) {
    const { cityID,  areaName, areaID, isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({cityID,  areaName, areaID, isActive}, generalValidations.setAreas, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){                    
                    let dbquery=`set @areaID = ${utils.OptionalKey(areaID)};
                                set @cityID = ${utils.OptionalKey(cityID)};
                                set @areaName = ${utils.OptionalKeyString(areaName)};
                                set @isActive = ${utils.OptionalKey(isActive)};
                                call ${appConfig.getDbConfig().database}.v1_set_areas
                                (@areaID, @cityID, @areaName,@isActive);`
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}


exports.deleteParent = function (req, res) {
    const { parentID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ parentID }, generalValidations.deleteParent, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_parent(${utils.OptionalKey(parentID)});`;
                    
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.UploadParentStudentArray= function(req, res) {
    const {array} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, generalValidations.UploadParentStudentArray, 
        function(err, value) {
        if(!err) {
            db.getConnection(function(err, db) {
                if(!err){
                    if (array !== undefined){
                        let dbquery=`` 
                        for(let i=0;i<array.length;i++){
                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_upload_parent_student
                                (${utils.OptionalKey(array[i].schoolID)}, 
                                ${utils.OptionalKey(array[i].branchID)}, 
                                ${utils.OptionalKeyString(array[i].studentName)}, 
                                ${utils.OptionalKey(array[i].gender)},
                                ${utils.OptionalKeyString(array[i].classLevelName)},
                                ${utils.OptionalKeyString(array[i].classSection)},
                                ${utils.OptionalKeyString(array[i].rollNo)},
                                ${utils.OptionalKey(array[i].dob)},
                                ${utils.OptionalKeyString(array[i].fatherName)},
                                ${utils.OptionalKeyString(array[i].fatherCNIC)},
                                ${utils.OptionalKeyString(array[i].fatherMobileNo)},
                                ${utils.OptionalKeyString(array[i].fatherEmail)},
                                ${utils.OptionalKeyString(array[i].motherName)},
                                ${utils.OptionalKeyString(array[i].motherCNIC)},
                                ${utils.OptionalKeyString(array[i].motherMobileNo)},
                                ${utils.OptionalKeyString(array[i].motherEmail)});  `
                        }
                        console.log(dbquery)
                        try {
                          db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });  
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'no array found')
                        if(!res.finished)res.status(500).json({ response: "no array found" })

                    }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}



exports.setTraceDump = function(req, res) {
    const { sourceUUID,  OSVersion, buildVersion, stackTrace, navigationStates, parameters, stackData } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({sourceUUID,  OSVersion, buildVersion, stackTrace, navigationStates, parameters, stackData}, generalValidations.setTraceDump, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    try{
                    let base64tackTrace,base64navigationStates,base64parameters,base64stackData
                    if(stackTrace){

                        let buffstackTrace = new Buffer(stackTrace);
                        let base64tackTrace = buffstackTrace.toString('base64');
                    }
                    
                    
                    if (navigationStates){
                        let buffnavigationStates = new Buffer(navigationStates);
                        let base64navigationStates = buffnavigationStates.toString('base64');
                    }
                    
                    if(parameters){
                        let buffparameters = new Buffer (parameters);
                        let base64parameters = buffparameters.toString('base64');
                    }
                    
                    if(stackData){
                        let buffstackData = new Buffer(stackData);
                        let base64stackData = buffstackData.toString('base64');
                    }
                    

                    let dbquery=`set @sourceUUID = ${utils.OptionalKey(sourceUUID)};
                                set @OSVersion = ${utils.OptionalKey(OSVersion)};
                                set @buildVersion = ${utils.OptionalKey(buildVersion)};
                                set @stackTrace = ${utils.OptionalKey(base64tackTrace)};
                                
                                set @navigationStates = ${utils.OptionalKey(base64navigationStates)};
                                set @parameters = ${utils.OptionalKey(base64parameters)};
                                set @stackData = ${utils.OptionalKey(base64stackData)};
                                call ${appConfig.getDbConfig().database}.v1_set_tracedump
                                
                                (@sourceUUID, @OSVersion, @buildVersion,@stackTrace,@navigationStates,@parameters,@stackData);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                    }
                    catch(error){
                        console.log(error.message)
                        if(!res.finished)res.status(500).json({ response: error.message })
                    }
                    db.release();
                    console.log('released')
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}


exports.setEncryptData = function(req, res) {
    const { inputData} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({inputData}, generalValidations.setEncryptData, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            try {
                let inputString= utils.EncryptAES(inputData);
                if(!res.finished)res.status(200).json({ response: inputString })
              }
              catch (err) {
                
                console.log(err);
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
              }
        
        
        }
    })
}



exports.setDecryptData = function(req, res) {
    const { inputData} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({inputData}, generalValidations.setDecryptData, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            try {
                let inputString= utils.DecryptAES(inputData);
                if(!res.finished)res.status(200).json({ response: inputString })
              }
              catch (err) {
                
                console.log(err);
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
              }

                
        
        
        }
    })
}

exports.setDeviceToken = function(req, res) {
    const { sourceUUID, parentID,parentUUID, deviceToken, deviceType, appVersion,osVersion,deviceModel,preferredLanguage } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({sourceUUID, parentID,parentUUID, deviceToken, deviceType,appVersion,osVersion,deviceModel,preferredLanguage}, generalValidations.setDeviceToken, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    
                    let dbquery=`set @sourceUUID = ${utils.OptionalKey(sourceUUID)};
                                set @parentID = ${utils.OptionalKey(parentID)};
                                set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @deviceToken = ${utils.OptionalKey(deviceToken)};
                                set @deviceType = ${utils.OptionalKey(deviceType)};
                                set @appVersion = ${utils.OptionalKey(appVersion)};
                                set @osVersion = ${utils.OptionalKey(osVersion)};
                                set @deviceModel = ${utils.OptionalKey(deviceModel)};
                                set @preferredLanguage = ${utils.OptionalKey(preferredLanguage)};
                                call ${appConfig.getDbConfig().database}.v1_set_device_token
                                (@sourceUUID, @parentID,@parentUUID,@deviceToken, @deviceType,@appVersion,@osVersion,@deviceModel,@preferredLanguage);`
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getTimezoneList = function (req, res) {
    const { } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ }, generalValidations.getTimezoneList, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_timezones `;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                } 
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            }) 
        }
    })
}

exports.getOTPDetails = function (req, res) {
    const { mobileNumber, schoolID, branchID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({  mobileNumber, schoolID, branchID }, generalValidations.getOTPDetails, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_otp_details where mobileNumber=${utils.OptionalKey(mobileNumber)}  `;
                    if(schoolID)
                        dbquery+= ` and schoolID = ${utils.OptionalKey(schoolID)}`;
                    if(branchID)
                        dbquery+= ` and branchID=${utils.OptionalKey(branchID)}`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){  
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                } 
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })

        }
    })
}


exports.setParentMobile= function(req, res) {
    const {parentUUID,firstName,lastName,nationalID,mobileNumber,email,parentType, taxID,addressLine1,addressLine2, addressState,addressPostalCode,cityID,countryID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({parentUUID,firstName,lastName,nationalID,mobileNumber,email,parentType, taxID,addressLine1,addressLine2, addressState,addressPostalCode,cityID,countryID}, generalValidations.setParentMobile, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                        
                    let dbquery=`set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @firstName = ${utils.OptionalKeyString(firstName)};
                                set @lastName = ${utils.OptionalKeyString(lastName)};
                                set @nationalID = ${utils.OptionalKeyString(nationalID)};
                                set @mobileNumber = ${utils.OptionalKeyString(mobileNumber)};
                                set @email = ${utils.OptionalKeyString(email)};
                                set @parentType = ${utils.OptionalKeyString(parentType)};
                                set @taxID = ${utils.OptionalKey(taxID)};
                                set @addressLine1 = ${utils.OptionalKeyString(utils.EncryptBase64(addressLine1))};
                                set @addressLine2 = ${utils.OptionalKeyString(utils.EncryptBase64(addressLine2))};
                                set @addressState = ${utils.OptionalKey(addressState)};
                                set @addressPostalCode = ${utils.OptionalKey(addressPostalCode)};
                                set @cityID = ${utils.OptionalKey(cityID)};
                                set @countryID = ${utils.OptionalKey(countryID)};
                                
                                call ${appConfig.getDbConfig().database}.v1_set_parent_mobile
                                (@parentUUID,@firstName,@lastName,@nationalID,@mobileNumber,@email,@parentType,@taxID,@addressLine1, 
                                    @addressLine2,@addressState, @addressPostalCode, @cityID, @countryID);`
                        console.log(dbquery)            
                        
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (!_.isEmpty(element)) {
                                                if(!res.finished)res.status(200).json({response: element })
                                            }
                                            else{
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(404).json({ response: errorResponse })

                                            }
                                        }
                                    })
                                    
                                }
                                else if (error.code== 'ER_DUP_ENTRY'){
                                    log.doFatalLog(fileNameForLogging, req.url, 'duplication error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(400).json({ response: "unsuccessful", code: error.code, reason : "Duplication/Mobile No. already exist!" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: error.code, reason : appConfig.GENERIC_ERROR_MSG })
                                }
                            }); 
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}

exports.setPaymentProviders = function(req, res) {
    const { providerID, countryID, transactionTypeID, providerName, instrumentFormat, tncLink, notes, providerChargeType, providerChargeRate, accessTokenKey, profileOrMerchantID, secretSalt, hostedURL, providerURL, providerSuccessURL,providerFailureURL, brandIcon, isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({providerID, countryID, transactionTypeID, providerName, instrumentFormat, tncLink, notes, providerChargeType, providerChargeRate, accessTokenKey, profileOrMerchantID, secretSalt, hostedURL, providerURL, providerSuccessURL,providerFailureURL, brandIcon, isActive}, generalValidations.setPaymentProviders, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    
                    let dbquery=`set @providerID = ${utils.OptionalKey(providerID)};
                                set @countryID = ${utils.OptionalKey(countryID)};
                                set @transactionTypeID = ${utils.OptionalKey(transactionTypeID)};
                                set @providerName = ${utils.OptionalKey(providerName)};
                                set @instrumentFormat = ${utils.OptionalKey(instrumentFormat)};
                                set @tncLink = ${utils.OptionalKey(tncLink)};
                                set @notes = ${utils.OptionalKey(notes)};
                                set @providerChargeType = ${utils.OptionalKey(providerChargeType)};
                                set @providerChargeRate = ${utils.OptionalKey(providerChargeRate)};
                                set @accessTokenKey = ${utils.OptionalKey(accessTokenKey)};
                                set @profileOrMerchantID = ${utils.OptionalKey(profileOrMerchantID)};
                                set @secretSalt = ${utils.OptionalKey(secretSalt)};
                                set @hostedURL = ${utils.OptionalKey(hostedURL)};
                                set @providerURL = ${utils.OptionalKey(providerURL)};
                                set @providerSuccessURL = ${utils.OptionalKey(providerSuccessURL)};
                                set @providerFailureURL = ${utils.OptionalKey(providerFailureURL)};
                                set @brandIcon = ${utils.OptionalKey(brandIcon)};
                                set @isActive = ${utils.OptionalKey(isActive)};
                                call ${appConfig.getDbConfig().database}.v1_set_payment_provider
                                (@providerID, @countryID, @transactionTypeID, @providerName, @instrumentFormat, @tncLink, @notes, @providerChargeType, @providerChargeRate, @accessTokenKey, @profileOrMerchantID, @secretSalt, @hostedURL, @providerURL,@providerSuccessURL, @providerFailureURL, @brandIcon, @isActive);`
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getPaymentProviderList = function (req, res) {
    const { } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ }, generalValidations.getPaymentProviderList, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_payment_providers_list `;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                } 
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            }) 
        }
    })
}


exports.getParentMobile= function(req, res) {
    const {parentUUID,mobileNumber} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({parentUUID,mobileNumber}, generalValidations.getParentMobile, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                        
                    let dbquery=`set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @mobileNumber = ${utils.OptionalKeyString(mobileNumber)};
                                call ${appConfig.getDbConfig().database}.v1_get_parent_mobile
                                (@mobileNumber,@parentUUID);`
                        console.log(dbquery)            
                        
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (!_.isEmpty(element)) {
                                                if(!res.finished)res.status(200).json({response: element })
                                            }
                                            else{
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(404).json({ response: errorResponse })

                                            }
                                        }
                                    })
                                    
                                }
                                
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: error.code, reason : appConfig.GENERIC_ERROR_MSG })
                                }
                            }); 
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        }
    })
}