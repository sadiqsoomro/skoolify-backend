const Joi = require("joi"),
    utils = require("../../utils/helper-methods"),
    finjaValidations = require("../validations/validations-finja"),
    axios = require("axios"),
    db = require("../../../db"),
    _ = require("lodash"),
    appConfig = require("../../../app-config"),
    log = require("../../utils/utils-logging"),
    helper = require("../../utils/helper-methods"),
    fileNameForLogging = "finja-controller",
    httpsAgent = require("https-agent");

import Error from "../response-classes/error";

// ==================================================================

exports.getFinjaCustomerInfo = function (req, res) {
    const {mobileNo} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

    Joi.validate({mobileNo}, finjaValidations.getFinjaCustomerInfo,
        function (err, value) {
                if (err) {
                    log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
                    if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));

                } 
                else {
                    let API_URL= appConfig.FINJA_API + "getCustInfo"
                    let merchantID = appConfig.FINJA_MERCHANT_ID;
                    let httpsAgent_ = httpsAgent({rejectUnauthorized: false,keepAlive: true });
                    let _data = {
                        mobileNo: mobileNo,
                        customerIdMerchant: merchantID
                    };
                    console.log(API_URL + ":" + JSON.stringify(_data))
                    //temp response

                    // if(!res.finished)res.status(200).json({ response: "OTP Sent",
                    //     customerName:"Hassan",
                    //     customerId:"234234234",
                    //     email:"",
                    //     //txnToken:response.headers["fn-token"]
                    //     txnToken:"toekn123-123"
                        
                    // })
                    // ---- temp response ----

                    axios({
                        method: "post",
                        url: API_URL,
                        data: _data,
                        httpsAgent: httpsAgent_,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    })
                    .then(function (response) {
                        console.log(response.status + "-" + response.data.msg)

                        if (response.status== 200 ){
                            if (response.data.code==200){
                                if(!res.finished)res.status(200).json({ response: response.data.msg,
                                                                        customerName:response.data.data.customerName,
                                                                        customerId:response.data.data.customerId,
                                                                        email:response.data.data.email,
                                                                        txnToken:response.headers["fn-token"]
                                                                        
                                                                    })
                            }
                            else{
                                if(!res.finished)res.status(406).json({ response: response.data.msg })
                            }
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'response error on finja', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })

                        }
                        
                    })
                    .catch(function (error) {
                        if (error.response) {
                          // Request made and server responded
                          console.log(error.response.data);
                          console.log(error.response.status);
                          console.log(error.response.headers);
                        } else if (error.request) {
                          // The request was made but no response was received
                          console.log(error.request);
                        } else {
                          // Something happened in setting up the request that triggered an Error
                          console.log('Error', error.message);
                        }
                    
                      }); 

                }
            })
      
}

exports.setFinjaPayment = function (req, res) {
    const { txnHash,txnOTP,txnToken,customerID,customerName,parentUUID,userUUID,
        walletAccountNo,providerID,ipAddress,internalTxnRefNo,paymentCurrency,paymentAmount,
        tranRequestCode,emailAddress,additionalData} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    

    Joi.validate(
        {txnHash,txnOTP,txnToken,customerID,customerName,parentUUID,userUUID,
            walletAccountNo,providerID,ipAddress,internalTxnRefNo,paymentCurrency,paymentAmount,
            tranRequestCode,emailAddress,additionalData  }, finjaValidations.setFinjaPayment,
        function (err, value) {
            if (!err) {

                let parsedHash = helper.TxnHashVerification(txnHash);
                let API_URL= appConfig.FINJA_API + "paymentToMerchant"
                let merchantID = appConfig.FINJA_MERCHANT_ID;

                if (parsedHash == "error parsing") {
                    if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS,"Invalid format of TxnHash",""));
                } 
                else if (parsedHash == "timeout") {
                    if(!res.finished)res.status(406).json(new Error("Transaction time Elapsed","Check the provided hash",""));
                } 
                else {
                    const httpsAgent_ = httpsAgent({rejectUnauthorized: false,keepAlive: true });
                    
                    
                db.getConnection(function(err, db) {
		        if(!err){
                    let dbquery=`call ${appConfig.getDbConfig().database}.v1_verify_parent_txn
                                (${utils.OptionalKey(parsedHash[1])}, 
                                ${utils.OptionalKey(parsedHash[0])}, 
                                ${utils.OptionalKey(parsedHash[2])})`;
                    console.log(dbquery)
                    try {
                       db.query(dbquery, function (error, data, fields) {
                        if(!error){
                            data.forEach(element => {
                                
                            if(element.constructor==Array) {
                                
                                    if (element[0].parentID == '0' || element[0].parentID==null ) {
                                        log.doFatalLog(fileNameForLogging,req.url,"Not a valid parent or Unauthorize Transaction/Time Elapsed","Check the provided hash");
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,"Not a valid parent or Unauthorize Transaction","Check the provided hash");
                                        if(!res.finished)res.status(406).json(new Error("Not a valid parent or Unauthorize Transaction/Time Elapsed","Check the provided hash",""));
                                    } 
                                    else{
                                        let transactionRequestTime = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
                                    
                                        let _data = {
                                        mobileNo: walletAccountNo,
                                        customerIdMerchant: merchantID,
                                        amount: paymentAmount.replace(".",""), // last two digits are decimal values 
                                        otp: txnOTP,
                                        customerId: customerID,
                                        customerName: customerName,
                                        invoiceID: internalTxnRefNo
                                    };
                                    console.log(API_URL + ":" + JSON.stringify(_data))
                                    // temp response
                                    // if(!res.finished)res.status(200).json({ 
                                    //     response: "Success",
                                    //     txnRefCode:"112233440099",
                                    //     customerName: "Hassan",
                                    //     customerId:"234234234",
                                    //     walletAccountNo: walletAccountNo,
                                    //     internalRefNo:"INV-20399433"
                                        
                                    // })
                                    // ---- temp respone end 
                                    axios({
                                            method: "post",
                                            url: API_URL,
                                            data: _data,
                                            httpsAgent: httpsAgent_,
                                            headers: {
                                                "FN-Token": txnToken,
                                                "Content-Type": "application/json"
                                            }
                                        })
                                        .then(function (response) {
                                            //console.log(response)
                                            if (response.status== 200 ){
                                                if (response.data.code==200){
                                                    if(!res.finished)res.status(200).json({ 
                                                        response: response.data.msg,
                                                        txnRefCode:response.data.data.transactionCode,
                                                        customerName: customerName,
                                                        customerId:customerID,
                                                        walletAccountNo: walletAccountNo,
                                                        internalRefNo:response.data.data.invoiceID
                                                        
                                                    })
                                                    let responseTime = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
                                                    let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_payment_logs
                                                    ( ${utils.OptionalKey(providerID)},
                                                    ${utils.OptionalKey(parsedHash[0])},
                                                    ${utils.OptionalKey(parentUUID)}, 
                                                    ${utils.OptionalKey(userUUID)},
                                                    ${utils.OptionalKey(ipAddress)},
                                                    ${utils.OptionalKey(response.data.data.transactionCode)},
                                                    ${utils.OptionalKey(internalTxnRefNo)},
                                                    ${utils.OptionalKey(walletAccountNo)},
                                                    ${utils.OptionalKey(paymentCurrency)},
                                                    ${utils.OptionalKey(paymentAmount)},
                                                    ${utils.OptionalKey(transactionRequestTime)},
                                                    ${utils.OptionalKey(tranRequestCode)},
                                                    ${utils.OptionalKey(additionalData)},
                                                    ${utils.OptionalKey(responseTime)},
                                                    'SUCCESS',
                                                    ${utils.OptionalKeyString(helper.EncryptBase64(JSON.stringify(response.data)))});`
                                                    console.log(dbquery)
                                                    db.query(dbquery,function (error, data, fields) {
                                                            if(!error){
                                                                log.doInfoLog(fileNameForLogging,req.url,"Transaction Success Log Inserted",error);
                                                            }
                                                            else{
                                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                                console.log(errorResponse)
                                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                                            }
                                                        });
                                                        
                                                    
                                                }
                                                else{
                                                    console.log(response.data)
                                                    if(!res.finished)res.status(406).json({ response: response.data.msg })
                                                    let responseTime = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
                                                    let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_payment_logs
                                                    ( ${utils.OptionalKey(providerID)},
                                                        ${utils.OptionalKey(parsedHash[0])},
                                                        ${utils.OptionalKey(parentUUID)}, 
                                                        ${utils.OptionalKey(userUUID)},
                                                    ${utils.OptionalKey(ipAddress)},
                                                    ${utils.OptionalKey(response.data.code)},
                                                    ${utils.OptionalKey(internalTxnRefNo)},
                                                    ${utils.OptionalKey(walletAccountNo)},
                                                    ${utils.OptionalKey(paymentCurrency)},
                                                    ${utils.OptionalKey(paymentAmount)},
                                                    ${utils.OptionalKey(transactionRequestTime)},
                                                    ${utils.OptionalKey(tranRequestCode)},
                                                    ${utils.OptionalKey(response.data.msg)},
                                                    ${utils.OptionalKey(responseTime)},
                                                    'FAILED',
                                                    ${utils.OptionalKeyString(helper.EncryptBase64(JSON.stringify(response.data)))});`
                                                    console.log(dbquery)
                                                    db.query(dbquery,function (error, data, fields) {
                                                            if(!error){
                                                                log.doInfoLog(fileNameForLogging,req.url,"Transaction Failure Log Inserted",error);
                                                            }
                                                            else{
                                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                                console.log(errorResponse)
                                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                                            }
                                                        });
                                                        
                                                }
                                                
                                            }
                                            else{
                                                log.doFatalLog(fileNameForLogging, req.url, 'response error on finja', error)
                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                    
                                            }

                                            
                                        })
                                        .catch(function (error) {
                                            if (error.response) {
                                            // Request made and server responded
                                            console.log(error.response.data);
                                            console.log(error.response.status);
                                            console.log(error.response.headers);
                                            } else if (error.request) {
                                            // The request was made but no response was received
                                            console.log(error.request);
                                            } else {
                                            // Something happened in setting up the request that triggered an Error
                                            console.log('Error', error.message);
                                            }
                                        
                                        });
                                    }
                        
                                }
                                else{
                                    // do nothing
                                }
                            });
                        

                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            console.log(errorResponse)
                            if(!res.finished)res.status(500).json({ response: errorResponse })

                        }
                    }) 
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                } 
                else {
                    log.doErrorLog(fileNameForLogging, req.url,"error in getting connection pool");
                    if(!res.finished)res.status(400).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err));
                }
                    
                })


                }

                
            }
            else{
                    log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
                    if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));

            }


        });
}


