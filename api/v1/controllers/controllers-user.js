const Joi = require('joi'), utils =  require('../../utils/helper-methods'),
    userValidations = require('../validations/validations-user'),
    db = require('../../../db'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging  = 'user-controller'

import Error from '../response-classes/error'

// ==================================================================

exports.userValidation = function(req, res) {
    const {  mobileNo,domain} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({mobileNo,domain}, userValidations.userValidation, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`  
                    set @in_mobileNo = ${utils.OptionalKey(mobileNo)};
                    set @in_domain = ${ utils.OptionalKey(domain)};
                    call ${appConfig.getDbConfig().database}.v1_user_validation
                    (@in_mobileNo, @in_domain,@out_userID,@out_fullName,@out_lastLogin,@out_status); 
                    select @out_userID as userID,@out_fullName as fullName,@out_lastLogin as lastLoginDate,
                    @out_status as status;`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                       console.log(element)
                                    if(_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.NO_USER_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        if(element[0].userID != null)
                                        {
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({ response: element[0] })
                                        }
                                        else
                                        {
                                            log.doInfoLog(fileNameForLogging, req.url, 'No User Found')
                                            const errorResponse = new Error(appConfig.NO_USER_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
                                        }
                                    }
        
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}

exports.userDetails = function(req, res) {
    const {  userID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({userID}, userValidations.userDetails, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`
                    set @userID = ${utils.OptionalKey(userID)};
                    call ${appConfig.getDbConfig().database}.v1_get_user_detail
                    (@userID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {

                                    if(_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.USER_NO_ROLE, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } 
                                    else{
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')

                                        let innerArray = new Array();
                                        let noOfIterations = element.length;
                                        let meetingDetails = {};
                                        for(let loop =0; loop < noOfIterations;loop++){
                                            
                                  
                                            if(element[loop].hasOwnProperty("classAlias"))
                                            {
                                                if (element[loop].classAlias) 
                                                meetingDetails["classAlias"] = element[loop].classAlias;
                                                delete element[loop].classAlias;
                                            }
                                            if(element[loop].hasOwnProperty("classSection"))
                                            {
                                                
                                                if (element[loop].classSection) 
                                                meetingDetails["classSection"] = element[loop].classSection;
                                                delete element[loop].classSection;
                                            }
                    
                                            if(element[loop].hasOwnProperty("subjectName"))
                                            {
                                                if(element[loop].subjectName) 
                                                meetingDetails["subjectName"] = element[loop].subjectName;
                                                delete element[loop].subjectName;
                                            }
                                            if(element[loop].hasOwnProperty("webLink"))
                                            {
                                                if (element[loop].webLink) 
                                                meetingDetails["webLink"] = element[loop].webLink;
                                                delete element[loop].webLink;
                                            }
                                            if(element[loop].hasOwnProperty("startDateTime"))
                                            {
                                                if(element[loop].startDateTime) 
                                                meetingDetails["startDateTime"] = element[loop].startDateTime;
                                                delete element[loop].startDateTime;
                                            }
                                            
                                            if(element[loop].hasOwnProperty("endDateTime"))
                                            {
                                                if (element[loop].endDateTime) 
                                                meetingDetails["endDateTime"] = element[loop].endDateTime;
                                                //if(loop==0)
                                                delete element[loop].endDateTime;
                                            }
                    
                                            
                                        
                                        }
                                        
                                        if(meetingDetails["webLink"]){
                                            if(!res.finished)res.status(200).json({ response: element,meetingDetails })
                                        }
                                        else{
                                            if(!res.finished)res.status(200).json({ response: element })
                                        }


                                        
                                    }
                                        

                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}

exports.getUserModules = function(req, res) {
    const {userID,roleID,branchID,schoolID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({userID,roleID,branchID,schoolID}, userValidations.getUserModules, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`Select * FROM v1_get_user_modules WHERE userID = '${userID}' AND roleID = '${roleID}' `;
        
                    if(branchID)
                    dbquery+= ` AND branchID = '${branchID}' `;
                                
                    if(schoolID)
                    {
                        dbquery+= `AND schoolID = '${schoolID}'`;
                    }
                console.log(dbquery)
                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    //Formatting data before sending response
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
                                    for(let loop =0; loop < noOfIterations;loop++)
                                    {
                                    let innerObject = {};
                              
                                    if(data[loop].hasOwnProperty("moduleID"))
                                    {
                                        innerObject["moduleID"] = data[loop].moduleID;
                                        if(loop==0)
                                        delete data[loop].moduleID;
                                    }
                                    if(data[loop].hasOwnProperty("parentModuleID"))
                                    {
                                        innerObject["parentModuleID"] = data[loop].parentModuleID;
                                        if(loop==0)
                                        delete data[loop].parentModuleID;
                                    }
                                    if(data[loop].hasOwnProperty("parentModuleURL"))
                                    {
                                        innerObject["parentModuleURL"] = data[loop].parentModuleURL;
                                        if(loop==0)
                                        delete data[loop].parentModuleURL;
                                    }
            
                                    if(data[loop].hasOwnProperty("moduleName"))
                                    {
                                        innerObject["moduleName"] = data[loop].moduleName;
                                        if(loop==0)
                                        delete data[loop].moduleName;
                                    }
                                    if(data[loop].hasOwnProperty("moduleName_ar"))
                                    {
                                        innerObject["moduleName_ar"] = data[loop].moduleName_ar;
                                        if(loop==0)
                                        delete data[loop].moduleName;
                                    }
                                    if(data[loop].hasOwnProperty("moduleURL"))
                                    {
                                        innerObject["moduleURL"] = data[loop].moduleURL;
                                        if(loop==0)
                                        delete data[loop].moduleURL;
                                    }
                                    if(data[loop].hasOwnProperty("isParent"))
                                    {
                                        innerObject["isParent"] = data[loop].isParent;
                                        if(loop==0)
                                        delete data[loop].isParent;
                                    }
                                    if(data[loop].hasOwnProperty("isSubParent"))
                                    {
                                        innerObject["isSubParent"] = data[loop].isSubParent;
                                        if(loop==0)
                                        delete data[loop].isSubParent;
                                    }
                                    if(data[loop].hasOwnProperty("readAccess"))
                                    {
                                        innerObject["readAccess"] = data[loop].readAccess;
                                        if(loop==0)
                                        delete data[loop].readAccess;
                                    }
                                    if(data[loop].hasOwnProperty("editAccess"))
                                    {
                                        innerObject["editAccess"] = data[loop].editAccess;
                                        if(loop==0)
                                        delete data[loop].editAccess;
                                    }
                                    if(data[loop].hasOwnProperty("addAccess"))
                                    {
                                        innerObject["addAccess"] = data[loop].addAccess;
                                        if(loop==0)
                                        delete data[loop].addAccess;
                                    }
                                    if(data[loop].hasOwnProperty("deleteAccess"))
                                    {
                                        innerObject["deleteAccess"] = data[loop].deleteAccess;
                                        if(loop==0)
                                        delete data[loop].deleteAccess;
                                    }
                                    innerArray[loop] = innerObject;
                                }
                                data[0].moduleDetails = innerArray;
                                    //Formation complete
                                    if(!res.finished)res.status(200).json({ response: data[0]})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}


exports.getUserRoleList = function(req, res) {
    const {  branchID,userID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({userID,branchID}, userValidations.getUserRoleList, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`Select * FROM v1_get_user_role_list WHERE branchID = '${branchID}'`;
                    if(userID)
                        dbquery+= ` AND userID = '${userID}'`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                }else {
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
                                    for(let loop =0; loop < noOfIterations;loop++)
                                    {
                                    let innerObject = {};
                                    if(data[loop].hasOwnProperty("roleID"))
                                    {
                                        innerObject["roleID"] = data[loop].roleID;
                                        if(loop==0)
                                        delete data[loop].roleID;
                                    }
            
                                    if(data[loop].hasOwnProperty("roleName"))
                                    {
                                        innerObject["roleName"] = data[loop].roleName;
                                        if(loop==0)
                                        delete data[loop].roleName;
                                    }
            
                                    if(data[loop].hasOwnProperty("roleLinkID"))
                                    {
                                        innerObject["roleLinkID"] = data[loop].roleLinkID;
                                        if(loop==0)
                                        delete data[loop].roleLinkID;
                                    }
            
                                    innerArray[loop] = innerObject;
                                }
                                data[0].roleDetails = innerArray;
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ 
                                        response:data[0]
                                    })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}

exports.setUser= function(req, res) {
    const {fullName,  mobileNo,password,  emailAddress, schoolID, createdByUserID,  isActive,userID, isAdmin,isSignatory, feePaymentAllowed} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({fullName,  mobileNo,password,  emailAddress, schoolID, createdByUserID,  isActive,userID, isAdmin,isSignatory,feePaymentAllowed}, userValidations.setUser, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    
                    let dbquery =`
                    set @userID = ${utils.OptionalKey(userID)};
                    set @fullName = ${utils.OptionalKeyString(fullName)};
                    set @mobileNo = ${utils.OptionalKey(mobileNo)};
                    set @password = ${utils.OptionalKey(utils.EncryptAES(password))};
                    set @emailAddress = ${utils.OptionalKey(emailAddress)};
                    set @schoolID = ${utils.OptionalKey(schoolID)};
                    set @createdByUserID = ${utils.OptionalKey(createdByUserID)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    set @isAdmin = ${utils.OptionalKey(isAdmin)};
                    set @isSignatory = ${utils.OptionalKey(isSignatory)};
                    set @feePaymentAllowed = ${utils.OptionalKey(feePaymentAllowed)};
                    call ${appConfig.getDbConfig().database}.v1_set_user
                    (@userID,@mobileNo,@password,@fullName, @emailAddress,@schoolID,@createdByUserID,@isActive,@isAdmin,@isSignatory,@feePaymentAllowed);`
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                        if(!res.finished)res.status(200).json({ response: element[0] })

                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}

// exports.setUsersArray= function(req, res) {
//     const { array } = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate(array, userValidations.setUsersArray, 
//         function(err, value) {
//         if(err) {
//             log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//             if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//         } else {
//             db.getConnection(function(err, db) {
//                 if(!err){
//                     let dbquery ="";
//                     for(let i=0;i<array.length;i++)
//                     {
//                         dbquery = procdbqueryedure +  ` call ${appConfig.getDbConfig().database}.v1_set_user
//                         (${utils.OptionalKey(array[i].userID)}, 
//                         ${utils.OptionalKey(array[i].mobileNo)}, 
//                         ${utils.EncryptAES(array[i].password)}, 
//                         ${utils.OptionalKey(array[i].fullName)},
//                         ${utils.OptionalKey(array[i].emailAddress)}, 
//                         ${utils.OptionalKey(array[i].schoolID)}, 
//                         ${utils.OptionalKey(array[i].createdByUserID)}, 
//                         ${utils.OptionalKey(array[i].isActive)});  `  
                      
//                     }
//                     console.log(dbquery)
//                     try {
//                         db.query(dbquery,function (error, data, fields) {
//                             if(!error){
//                                if(!res.finished)res.status(200).json({ response: "success" })       
//                             }
//                             else{
//                                 log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
//                                 const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
//                                 console.log(errorResponse)
//                                 if(!res.finished)res.status(500).json({ response: errorResponse })
//                             }
//                         });
//                     } 
//                     finally {
//                         db.release();
//                         console.log('released')
//                     }
                    
                        
        
//                 }
//                 else{
//                     log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
//                     const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                     if(!res.finished)res.status(500).json({ response: errorResponse })
        
//                 }
//             })
        
        
//         }
//     })
// }

exports.updateUser= function(req, res) {
    const {userID,fullName, mobileNo, emailAddress, schoolID, branchID, isActive} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({userID,fullName, mobileNo, emailAddress, schoolID, branchID, isActive}, userValidations.updateUser, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`
                    set @userID = ${utils.OptionalKey(userID)};
                    set @fullName = ${utils.OptionalKeyString(fullName)};
                    set @mobileNo = ${utils.OptionalKey(mobileNo)};
                    set @emailAddress = ${utils.OptionalKey(emailAddress)};
                    set @schoolID = ${utils.OptionalKey(schoolID)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_update_user
                    (@userID, @fullName, @mobileNo,@emailAddress,@schoolID,@isActive);`
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}

exports.getUserList = function (req, res) {
    const { schoolID,branchID, userID } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate(
        { schoolID,branchID, userID },  userValidations.getUserList,
        function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(404).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `   SELECT * FROM v1_get_user_branch WHERE schoolID = '${schoolID}'`
                        if (!_.isEmpty(userID)) {
                            dbquery += ` AND userID = '${userID}'`
                        }

                        // if (!_.isEmpty(branchID)) {
                        //     dbquery += ` and (branchID = ${branchID} or branchID is NULL)`

                        // }
                        dbquery += ` order by fullName`
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, userData, fields) {
                                if(!error){
                                   
                                    if (_.isEmpty(userData)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        let userID = "";
                                        for (let loop = 0; loop < userData.length; loop++) {
                                            if (userData[loop].userID != null || userData[loop].userID != '')
                                                userID = userData[loop].userID + "," + userID;
                                                delete userData[loop].branchID;
                                        }
                                        userID = userID.substr(0, userID.length - 1);
                                        //console.log(userID);
            
                                        if (userID != '' || userID != null) {
                                            let IDs = userID.split(',');
                                            let whereClause = "";
                                            if (IDs.length != 0) {
            
                                                for (let x = 0; x < IDs.length; x++) {
                                                    whereClause = whereClause + ` userID = '${IDs[x]}' or`;
                                                }
                                                whereClause = whereClause.substr(0, whereClause.length - 2);
                                                whereClause = "WHERE (" + whereClause;
                                            }
                                            let dbquery='';
                                            dbquery = `SELECT * FROM v1_get_user_branch_module  `
                                            
                                            dbquery  += whereClause + ")";
                                            if (!_.isEmpty(branchID)) {
                                                dbquery += ` and branchID = ${branchID}`
            
                                            }
                                            db.query(dbquery,function (error, userRoleData, fields) {
                                                    if(!error){
                                                       
                                                        if (_.isEmpty(userRoleData)) {
                                                            let data = new Array();
                                                            let userID = "";
                                                            for (let x = 0; x < userData.length; x++) {
                                                                userID = userData[x].userID;
                                                                let data = new Array();
                                                                let looper = 0;
                                                                for (let y = 0; y < userRoleData.length; y++) {
                                                                    
                                                                    if (userID == userRoleData[y].userID) {
                                                                        delete userRoleData[y].userID;
                                                                        data[looper] = userRoleData[y];
                                                                        looper++;
                                                                    }
                                                                    
                                                                }
                                                                
                                                                userData[x].userRoleData = data;
                                                            }
                                                        
                                                        } else {
                                                           // console.log(userRoleData);
                                                            let userID = "";
                                                            for (let x = 0; x < userData.length; x++) {
                                                                userID = userData[x].userID;
                                                                let data = new Array();
                                                                let looper = 0;
                                                                for (let y = 0; y < userRoleData.length; y++) {
                                                                    
                                                                    if (userID == userRoleData[y].userID) {
                                                                        delete userRoleData[y].userID;
                                                                        data[looper] = userRoleData[y];
                                                                        looper++;
                                                                    }
                                                                    //data[looper] = userRoleData[y];
                                                                    
                                                                }
                                                                
                                                                userData[x].userRoleData = data;
                                                            }
                
                                                            
                                                        }
                                                        if(!res.finished)res.status(200).json({  response: [  {userData: userData,}]})
                                
                                                    }
                                                    else{
                                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                                    }
                                                });
                                        } else {
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
                                        }
            
                                    }
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            
            }
        })
}

exports.getRoleLinkedUsers = function (req, res) {
    const { branchID, roleID } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate(
        { branchID, roleID }, userValidations.getRoleLinkedUsers, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(404).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `   SELECT * FROM v1_get_role_linked_users WHERE branchID = '${branchID}'`
                        if (!_.isEmpty(roleID)) {
                            dbquery += ` AND roleID = '${roleID}'`
                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    if (_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data returned')
                                        if(!res.finished)res.status(200).json({ response: data})

            
                                    }
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            
            }
        })
}



exports.getActiveUserList = function (req, res) {
    const { schoolID,branchID, userID } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ schoolID,branchID, userID },  userValidations.getActiveUserList,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(404).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `   SELECT * FROM v1_get_user_active WHERE schoolID = '${schoolID}'`
                        if (!_.isEmpty(userID)) {
                            dbquery += ` AND userID = '${userID}'`
                        }

                        if (!_.isEmpty(branchID)) {
                            dbquery += ` and (branchID = ${branchID} or branchID is NULL)`

                        }
                        dbquery += ` order by fullName`
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, userData, fields) {
                                if(!error){
                                   
                                    if (_.isEmpty(userData)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        if(!res.finished)res.status(200).json({  response: userData})
            
                                        
            
                                    }
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            
            }
        })
}
