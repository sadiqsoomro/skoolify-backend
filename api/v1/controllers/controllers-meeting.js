const Joi = require('joi'), utils = require('../../utils/helper-methods'),
    meetingValidations = require('../validations/validations-meeting'),
    _ = require('lodash'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    helper = require('../../utils/helper-methods'),
    randomstring = require('randomstring'),
    fileNameForLogging = 'meeting-controller'
import Error from '../response-classes/error'
const uuidv4 = require("uuid/v4");
const push = require("../../utils/push-notification");
// ==================================================================

exports.setMeetingInvite = function (req, res) {
    const { meetingID,branchID,classLevelID,classID, subjectID, studentIDList,meetingTopic,roomName,agendaDesc,
              meetingRefNo,meetingPassword,isRecurring,recurringType,webLink,weekdayNo,noOfRecurrence,startDate,startTime,
              endTime,endDate,timezone,reminderOn,reminderTime,hostUserID,createdBy,isActive
    } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({meetingID,branchID,classLevelID,classID, subjectID, studentIDList,meetingTopic,roomName,agendaDesc,
      meetingRefNo,meetingPassword,isRecurring,recurringType,webLink,weekdayNo,noOfRecurrence,startDate,startTime,
      endTime,endDate,timezone,reminderOn,reminderTime,hostUserID,createdBy,isActive}, meetingValidations.setMeetingInvite, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){
            
  
            // let var_agendaDesc,var_meetingTopic,var_roomName
            let var_meetingID
            if (meetingID){
              var_meetingID= meetingID
            }
            else{
              var_meetingID = uuidv4();
            }
            
            // if meeting number is empty, Generate new else use existing
            let var_meetingRefNo
            if (meetingRefNo) {
              var_meetingRefNo = meetingRefNo
            }
            else{
              var_meetingRefNo = randomstring.generate({
                length: 9,
                charset: 'numeric'
              })
            }
            // if meeting password is empty, Generate new else use existing
            let var_meetingPwd
            if(meetingPassword){
              var_meetingPwd=meetingPassword
            }
            else{
              var_meetingPwd = randomstring.generate({
                length: 6,
                charset: 'numeric'
              })

            }
            
             // generate webLink 
             let var_webLink
             if (webLink){
               var_webLink = webLink
             }
             else{
              var_webLink = process.env.TWILIO_VIDEO_BASE_URL + `?meeting=` + utils.EncryptAES(encodeURI(`roomName=${roomName}&meetingRefNo=${var_meetingRefNo}&meetingPassword=${var_meetingPwd}`)) 
             }
             
            let dbquery = ""; 
            
              dbquery = `call ${appConfig.getDbConfig().database}.v1_set_meeting_invite(
                ${utils.OptionalKey(var_meetingID)}, 
                ${utils.OptionalKey(branchID)}, 
                ${utils.OptionalKey(classLevelID)}, 
                ${utils.OptionalKey(classID)}, 
                ${utils.OptionalKey(subjectID)},
                ${utils.OptionalKey(studentIDList)},  
                ${utils.OptionalKeyString(meetingTopic)}, 
                ${utils.OptionalKeyString(roomName)},
                ${utils.OptionalKeyString(agendaDesc)}, 
                ${utils.OptionalKey(var_meetingRefNo)}, 
                ${utils.OptionalKey(var_meetingPwd)}, 
                ${utils.OptionalKey(isRecurring)},
                ${utils.OptionalKey(recurringType)},
                ${utils.OptionalKeyString(var_webLink)}, 
                ${utils.OptionalKey(weekdayNo)},
                ${utils.OptionalKey(noOfRecurrence)},
                ${utils.OptionalKey(startDate)},
                ${utils.OptionalKey(startTime)},
                ${utils.OptionalKey(endTime)},
                ${utils.OptionalKey(endDate)},
                ${utils.OptionalKey(timezone)},
                ${utils.OptionalKey(reminderOn)},
                ${utils.OptionalKey(reminderTime)},
                ${utils.OptionalKey(hostUserID)},
                ${utils.OptionalKey(createdBy)}, 
                ${utils.OptionalKey(isActive)});  `;
                console.log(dbquery)

                try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                        if(!res.finished)res.status(200).json({ response: "success" });

                          dbquery = `call ${appConfig.getDbConfig().database}.v1_get_meeting_notifications(
                            ${utils.OptionalKey(branchID)},
                            ${utils.OptionalKey(classLevelID)}, 
                            ${utils.OptionalKey(classID)}, 
                            ${utils.OptionalKey(subjectID)},
                            ${utils.OptionalKey(studentIDList)});`
                          console.log(dbquery)
                          db.query(dbquery,function (error, data, fields) {
                          if(!error){
                            
                            
                            if (!_.isEmpty(data)) {
                              data.forEach(element => {
                                if(element.constructor==Array) {
                                    if (!_.isEmpty(element)) {
                                      let messageText = "";
                                      
                                      for (let i = 0; i < element.length; i++) {
                                        if (element[i].preferredLanguage=='ar'){
                                          messageText = "يرجى مراجعة الجدول الزمني للفصول الدراسية عبر الإنترنت وتحديث الجدول الزمني الخاص بهم";
                                          
                                        }
                                        else {
                                          messageText = "Please check time table for online classes and their schedule update";
                                        }
                                        var messageConfig = appConfig.messageSettings(
                                                  "Time Table",
                                                  messageText,
                                                  "TimeTableScreen",
                                                  element[i].parentID,
                                                  element[i].studentID,
                                                  element[i].classID,
                                                  element[i].branchID,
                                                  element[i].schoolID,
                                                  null
                                        );
                                        
                                        push.SendNotification(element[i].deviceToken, messageConfig);
                                      }
                                      //console.log(messageConfig)
                                    }
                                    else{
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                    }
                                }
                              })
                              
                            }
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            console.log(errorResponse)
                            if(!res.finished)res.status(500).json({ response: errorResponse });
                          }
                          });

                      }
                      else{
                        log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                        if(!res.finished)res.status(500).json({ response: error.message });
                      }
                  });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    })
  }



  
  exports.getMeetingInvite = function (req, res) {
    const { meetingID, branchID, classLevelID, classID, subjectID, hostUserID, createdBy } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({meetingID, branchID, classLevelID, classID,subjectID,  hostUserID, createdBy}, meetingValidations.getMeetingInvite, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = `call ${appConfig.getDbConfig().database}.v1_get_meeting_invite(
                                  ${utils.OptionalKey(meetingID)}, 
                                  ${utils.OptionalKey(branchID)}, 
                                  ${utils.OptionalKey(classLevelID)}, 
                                  ${utils.OptionalKey(classID)}, 
                                  ${utils.OptionalKey(subjectID)}, 
                                  ${utils.OptionalKey(hostUserID)},
                                  ${utils.OptionalKey(createdBy)});  `;

            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){

                      data.forEach(element => {  
                          if(element.constructor==Array) { 
                            if (_.isEmpty(element)) {
                              log.doErrorLog(fileNameForLogging, req.url, "data not present");
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                              if(!res.finished)res.status(404).json({ response: errorResponse });
                            } 
                            else{
                              log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                              if(!res.finished)res.status(200).json({ response: element });

                            }
                        
                          }
                      })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                    if(!res.finished)res.status(500).json({ response: error.message })
                    
                  }
                });
            } 
            finally {
                db.release();
                console.log('released')
            }
 
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    });
  };


  exports.getUserMeetingInvite = function (req, res) {
    const {branchID, classLevelID, classID, hostUserID } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ branchID, classLevelID, classID, hostUserID}, meetingValidations.getUserMeetingInvite, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){
  
            
           
            let dbquery = `call ${appConfig.getDbConfig().database}.v1_get_user_meeting_invite(
                                   
                                  ${utils.OptionalKey(branchID)}, 
                                  ${utils.OptionalKey(classLevelID)}, 
                                  ${utils.OptionalKey(classID)}, 
                                  ${utils.OptionalKey(hostUserID)});  `;

                    console.log(dbquery)
                    try {
                      db.query(dbquery,function (error, data, fields) {
                          if(!error){

                              data.forEach(element => {  
                                  if(element.constructor==Array) { 
                                    if (_.isEmpty(element)) {
                                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                      if(!res.finished)res.status(404).json({ response: errorResponse });
                                    } 
                                    else{
                                      log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                                      if(!res.finished)res.status(200).json({ response: element });

                                    }
                                
                                  }
                              })
                          }
                          else{
                            log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                            
                          }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
       
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    })
  }

  exports.getMeetingDetails = function (req, res) {
    const {meetingID } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ meetingID}, meetingValidations.getMeetingDetails, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = `call ${appConfig.getDbConfig().database}.v1_get_meeting_detail(
                                   
                                  ${utils.OptionalKey(meetingID)});  `;

                    console.log(dbquery)
                    try {
                      db.query(dbquery,function (error, data, fields) {
                          if(!error){

                              data.forEach(element => {  
                                  if(element.constructor==Array) { 
                                    if (_.isEmpty(element)) {
                                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                      if(!res.finished)res.status(200).json({ response: errorResponse });
                                    } 
                                    else{
                                      log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                                      if(!res.finished)res.status(200).json({ response: element });

                                    }
                                
                                  }
                              })
                          }
                          else{
                            log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                            
                          }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    });
  };


  exports.deleteMeetingInvite = function (req, res) {
    const {meetingID } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ meetingID}, meetingValidations.deleteMeetingInvite, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_meeting_invite(
                                  ${utils.OptionalKey(meetingID)});  `;
                    console.log(dbquery)
                    try {
                      db.query(dbquery,function (error, data, fields) {
                          if(!error){
                            log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                            if(!res.finished)res.status(200).json({ response: "success" });
                              
                          }
                          else{
                            log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                            
                          }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    })
  }

  exports.deleteMeetingOccurence = function (req, res) {
    const {recurrenceID } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ recurrenceID}, meetingValidations.deleteMeetingOccurence, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_meeting_occurence(
                                  ${utils.OptionalKey(recurrenceID)});  `;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                          if(!error){
                            log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                            if(!res.finished)res.status(200).json({ response: "success" });
                              
                          }
                          else{
                            log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                            if(!res.finished)res.status(500).json({ response: error.message })
                          }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    })
  }



  exports.getMeetingInvitees = function (req, res) {
    const { studentIDList,meetingID } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({studentIDList,meetingID}, meetingValidations.getMeetingInvitees, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = `call ${appConfig.getDbConfig().database}.v1_get_meeting_invitees(
                                  ${utils.OptionalKey(meetingID)}, 
                                  ${utils.OptionalKey(studentIDList)});  `;

                    console.log(dbquery)
                    try {
                      db.query(dbquery,function (error, data, fields) {
                          if(!error){

                              data.forEach(element => {  
                                  if(element.constructor==Array) { 
                                    if (_.isEmpty(element)) {
                                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                      if(!res.finished)res.status(200).json({ response: errorResponse });
                                    } 
                                    else{
                                      log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                                      if(!res.finished)res.status(200).json({ response: element });

                                    }
                                
                                  }
                              })
                          }
                          else{
                            log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                            if(!res.finished)res.status(500).json({ response: error.message })
                            
                          }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
 
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    });
  };


  exports.getMeetingJoin = function(req, res) {
    const { meetingID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate( {meetingID}, meetingValidations.getMeetingJoin,
      function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery = `SELECT * FROM v1_get_meeting_join where meetingID='${meetingID}' `;
              
              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                          if (_.isEmpty(data)) {
                            log.doErrorLog(fileNameForLogging, req.url, "data not present");
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,  "data not present" );
                            if(!res.finished)res.status(404).json({ response: errorResponse });
                          } else {
                            log.doInfoLog(fileNameForLogging,req.url, "successfully data fetched from db" );
                            if(!res.finished)res.status(200).json({ response: data });
                          }
                          
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              }
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }
  
          })

        }
      }
    )
  }

  exports.setMeetingSession = function (req, res) {
    const {sessionID, meetingID, recurrenceID, parentUUID, studentUUID, sourceUUID, isHost, roomSID, participantSID } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ sessionID, meetingID, recurrenceID, parentUUID, studentUUID, sourceUUID, isHost, roomSID, participantSID}, meetingValidations.setMeetingSession, function ( err, value ) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      
      else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = `call ${appConfig.getDbConfig().database}.v1_set_meeting_session(
                                  ${utils.OptionalKey(sessionID)}, 
                                  ${utils.OptionalKey(meetingID)}, 
                                  ${utils.OptionalKey(recurrenceID)}, 
                                  ${utils.OptionalKey(parentUUID)}, 
                                  ${utils.OptionalKey(studentUUID)}, 
                                  ${utils.OptionalKey(sourceUUID)}, 
                                  ${utils.OptionalKey(isHost)}, 
                                  ${utils.OptionalKey(roomSID)}, 
                                  ${utils.OptionalKey(participantSID)});  `;
                    console.log(dbquery)
                    try {
                      db.query(dbquery,function (error, data, fields) {
                          if(!error){
                            log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                            data.forEach(element => {  
                              if(element.constructor==Array) { 
                                if (_.isEmpty(element)) {
                                  log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                  const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                  if(!res.finished)res.status(200).json({ response: errorResponse });
                                } 
                                else{
                                  log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                                  if(!res.finished)res.status(200).json({ response: "success",sessionID:element[0].sessionID });

                                }
                            
                              }
                          })
                              
                          }
                          else{
                            log.doFatalLog(fileNameForLogging,req.url,"error in db execution",error);
                            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                          }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    })
  }



  exports.getMeetingRecurrence = function(req, res) {
    const { meetingID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate( {meetingID}, meetingValidations.getMeetingRecurrence,
      function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery = `SELECT * FROM v1_get_meeting_recurrence where meetingID='${meetingID}' `;
              
              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,  "data not present" );
                        if(!res.finished)res.status(404).json({ response: errorResponse });
                      } else {
                        log.doInfoLog(fileNameForLogging,req.url, "successfully data fetched from db" );
                        if(!res.finished)res.status(200).json({ response: data });
                      }
                          
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              }
              
     
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }
  
          })

        }
      }
    )
  }