/**
 *  VARIABLES AND CONSTSTANTS
 */
const Joi = require('joi'), 
    utils =  require('../../utils/helper-methods'),
    authValidations = require('../validations/validations-auth'),
    jwt = require('jsonwebtoken'), 
    path = require('path'), 
    twilio = require('twilio'),
    
    appConfig = require('../../../app-config'),
    _ = require('lodash/core'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging = 'auth-controller',
    randomstring = require('randomstring'),
    db = require('../../../db'),
    request = require('request'),
    emails = require('../../utils/node-mailer'),
    client = new twilio(appConfig.ACCOUNT_SID, appConfig.AUTH_TOKEN);
    

import Error from '../response-classes/error'
import { exists } from 'fs';


exports.getToken = function (req, res) {
    const { apiKey, sourceUuid } = req.body

    log.doInfoLog( fileNameForLogging,req.url, 'body', req.body)

    Joi.validate({ apiKey, sourceUuid }, authValidations.getToken, function (err, value) {
        if (err) {
            log.doErrorLog(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            //log.doInfoLog(fileNameForLogging,req.url, 'acquiring connection','')
            try{
            //     db.getConnection(function(err, db) {
            //         if(!err){
    
            //             let dbquery=`  set @sourceUuid = '${sourceUuid}'; \
            //                         set @apiKey='${apiKey}'; \
            //                         set @out_Token = 0; \
            //                         set @out_expiry = 0; \
            //                         call ${appConfig.getDbConfig().database}.v1_get_token_status(@sourceUuid,@apiKey,@out_Token,@out_expiry); \
            //                         select @out_Token as token,@out_expiry as expiryTime;`
            //                         //console.log(dbquery)
    
            //         db.query(dbquery,
            //             function (err, data, fields) {
    
            //             if (!err) {
            //             data.forEach(element => {
            //                 if(element.constructor==Array) {
            //                 let token = element[0].token
            //                 let expiryTime =  String(element[0].expiryTime)
                            
            //                 if (_.isEmpty(token)) {
            //                     jwt.sign(
            //                         { apiKey, sourceUuid }, appConfig.JWT_SECRET_KEY, function (err, token) {
                                        
    
            //                             if (err) {
            //                                 log.doFatalLog(fileNameForLogging,req.url, 'error in token generation', err)
            //                                 const errorResponse = new Error('Access not allowed.', err.message, err)
            //                                 if(!res.finished)res.status(401).json({ response: errorResponse })
            //                             } else {
            //                                 let expiryTime = jwt.decode(token).iat;
            //                                 //expiryTime = tokenExpiryTime;
            //                                 db.query(`call ${appConfig.getDbConfig().database}.v1_insert_token('${token}', '${apiKey}', '${sourceUuid}', '${expiryTime}');`,
            //                                 function (error, rows, fields) {
            //                                     if(!error){
            //                                         log.doInfoLog( fileNameForLogging,req.url, 'token generated', '')
            //                                         if(!res.finished)res.status(200).json({ response: token, expiry: expiryTime })
            //                                     }
            //                                     else{
            //                                         log.doErrorLog(fileNameForLogging,req.url, 'error in v1_insert_token', error)
            //                                         const errorResponse = new Error('error in v1_insert_token.', error.message, error)
            //                                         if(!res.finished)res.status(401).json({ response: errorResponse })
            //                                     }
            //                                 });
                                                
                                                    
                                                
                                                
            //                             }
            //                         });
                                    
            //                 } else {
            //                     if(!res.finished)res.status(200).json({ response: token, expiry : expiryTime })
            //                 }
    
            //                 }
            //             });
                            
            //             }
            //             else{
            //                 log.doFatalLog( fileNameForLogging,req.url, 'error in db execution', err)
            //                 if(!res.finished)res.status(500).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err))
            //             }
    
                                    
            //         });
                  
            //         db.release();
            //     }
            //     else{
            //         log.doFatalLog( fileNameForLogging,req.url, 'error in getting connection pool', err)
            //         if(!res.finished)res.status(500).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err))
            //     }
               
            // });
            var signOptions = {       
                expiresIn:  process.env.JWT_EXPIRY_TIME
               };
            jwt.sign(
                { apiKey, sourceUuid }, appConfig.JWT_SECRET_KEY,signOptions, function (err, token) {
                    
                    if (err) {
                        log.doFatalLog(fileNameForLogging,req.url, 'error in token generation', err)
                        const errorResponse = new Error('Access not allowed.', err.message, err)
                        if(!res.finished)res.status(401).json({ response: errorResponse })
                    } else {
                        let expiryTime = jwt.decode(token).exp;
                        log.doInfoLog( fileNameForLogging,req.url, 'token generated', '')
                        if(!res.finished)res.status(200).json({ response: token, expiry: expiryTime })
                              
                    }
                });
            }
            catch(error){
               
                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                console.log(errorResponse)
                if(!res.finished)res.status(500).json({ response: errorResponse })
              }
        
    }
    });
}

exports.generateOTP = function (req, res) {
    let { mobileNumber, sourceUuid } = req.body
    log.doInfoLog( fileNameForLogging,req.url, '', req.body)
    Joi.validate({ mobileNumber, sourceUuid }, authValidations.generateOTP, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`select * from v1_get_otp_status where mobileNumber = '${mobileNumber}' AND sourceUUID = '${sourceUuid}';`
                    // console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                        
                                if (_.isEmpty(data) ) {
                                    const code = randomstring.generate({
                                        length: 6,
                                        charset: 'numeric'
                                    })
                                    dbquery=`call ${appConfig.getDbConfig().database}.v1_insert_otp(
                                        ${utils.OptionalKey(code)}, 
                                        ${utils.OptionalKey(mobileNumber)}, 
                                        ${utils.OptionalKey(sourceUuid)});`
                                    db.query(dbquery, function (error, data, fields) {
                                        if(!error){
                                            let callback = generateChannelSMS(mobileNumber,code,0)
                                            log.doInfoLog( fileNameForLogging,req.url,` mobileNumber: ${mobileNumber} ...OTP: ${code}`)
                                            if(!res.finished)res.status(200).json({response: "Your verification code has been sent to your mobile number."})
                                            
                                        }
                                        else{
                                            log.doFatalLog(fileNameForLogging, req.url, 'error in inserting OTP in database', error)
                                            if(!res.finished)res.status(500).json({response: "Error generating SMS"})
                                        }
                                    })
                                    
                    
                                } 
                                else {
                                    // if database already has unused OTP
                                    let callback = generateChannelMessage(mobileNumber,data[0].otpString,1)
                                    log.doInfoLog( fileNameForLogging,req.url,` mobileNumber: ${mobileNumber} ...OTP: ${data[0].otpString}...Resending`)
                                    if(!res.finished)res.status(200).json({response: "Your verification code has been sent to your mobile number."})
                                
                                }
                                

                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })

            }
        })
       
        }
    })
}
// will be depricated after app opens to all
exports.verifyOTP = function (req, res) {
    const { mobileNumber, otp, uuid,deviceToken,deviceType} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ mobileNumber, otp, uuid,deviceToken, deviceType }, authValidations.verifyOTP, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            
             
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`  call ${appConfig.getDbConfig().database}.v1_verify_otp(
                        ${utils.OptionalKey(mobileNumber)}, 
                        ${utils.OptionalKey(otp)}, 
                        ${utils.OptionalKey(uuid)} , 
                        ${utils.OptionalKey(deviceToken)},
                        ${utils.OptionalKey(deviceType)});`
                console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (!_.isEmpty(data)){
                                    data.forEach(element => {                                
                                            if(element.constructor==Array) {
                                                const parentId = element[0].parentId
                                                    if (parentId == null) {
                                                        log.doErrorLog(fileNameForLogging, req.url, 'otp not found.')
                                                        const errorResponse = new Error(appConfig.INVALID_OTP_ERROR_MSG, 'otp not found.')
                                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                                    } 
                                                    else {
                                                        if(!res.finished)res.status(200).json({response: [{ parentId }]})
                                                        if (!deviceToken){
                                                            emails.SendEmail(`OTP.Alerts@infinitystudio.pk`,`device Token failure`,`Device Registered but Unable to generate device token for ${mobileNumber} .. UUID:${uuid}, deviceToken:${deviceToken},deviceType: ${deviceType}`)
                                                        }

                                                    }
                                            }
                                            
                                    })
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({response: errorResponse})
                            }
                        })
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    if(!res.finished)res.status(500).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err))
                }
                
            });
        }
    })
}

exports.validateRequestToken = function (req, res, next) {
    //let decodedToken
    try {
        let decodedToken = jwt.decode(req.headers.authorization.split(' ')[1])
        let AuthToken=req.headers.authorization.split(' ')[1] 
        const apiKey = decodedToken.apiKey
        const sourceUuid = decodedToken.sourceUuid
        
        // -------
        jwt.verify(AuthToken, appConfig.JWT_SECRET_KEY, function(err, decoded){
            if(!err){
                //console.log('token verified! Expiry: '+ decodedToken.exp)
                
              next()
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'error in token validation', err)   
                if(!res.finished)res.status(511).json({ response: new Error("invalid token") })
            }
          })
        // -------
        //console.log('getting connection from pool')

        // Old Working code below

        // db.getConnection(function(err, db) {
        //     if(!err){
        //         let dbquery=`set @inout_apiKey = '${apiKey}';            
        //                     set @inout_SourceUUID = '${sourceUuid}';            
        //                     set @out_Token = '0'; 
        //                     set @out_expiry = '0';                       
        //                     call ${appConfig.getDbConfig().database}.v1_get_token_status(@inout_SourceUUID, @inout_apiKey,@out_Token,@out_expiry); 
        //                     select @inout_SourceUUID, @inout_apiKey, @out_Token as token, @out_expiry as expiry;`

        //         db.query(dbquery,
        //                 function (error, data, fields) {
        //                     if(!error){
        //                         data.forEach(element => {
        //                             if(element.constructor==Array) {
        //                                 let token = element[0].token
        //                                let expiryTime = element[0].expiry
        //                                 if (_.isEmpty(token)) {
        //                                     log.doErrorLog(fileNameForLogging, req.url, 'token does not exist in db')
        //                                     if(!res.finished)res.status(503).json({ response: new Error("invalid token") })
        //                                 } else {
        //                                     if (token == req.headers.authorization.split(' ')[1]) {
        //                                         //console.log('token validated')
        //                                         next()
        //                                     } else {
        //                                         log.doErrorLog(fileNameForLogging, req.url, 'token does not exist in db')
        //                                         if(!res.finished)res.status(503).json({ response: new Error("invalid token") })
        //                                     }
        //                                 }
        //                             }
        //                         })
                                
        //                     }
        //                 }
        //     )
        //     db.release();
        //     }
        //     else{
        //         log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
        //         const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
        //         if(!res.finished)res.status(500).json({ response: errorResponse })
        //     }

        // }) 
    } catch (error) {
        console.error(req.url + ': bearer token not in header')
        if(!res.finished)res.status(401).json({ response: new Error("invalid token", 'bearer token not in header', error.toString()) })
    }
    
}



// function generateTwilioMsg(mobileNumber, code, callback) {
//     console.log ('sending via Twilio SMS Service')
//     if (mobileNumber!='+923002036717' && mobileNumber!='+923232402932' && mobileNumber!='+923008288255' && mobileNumber!='+923008260228' && mobileNumber!='+923360824990' && mobileNumber!='+923328223096')  {
//         client.messages
//             .create({
//                 body: `<#> ${code} is your verification code for Skoolify \nhMsSUOwI5c1`,
//                 to: `${mobileNumber}`,  // Text this number
//                 from: `${appConfig.FROM_TWILIO_NUMBER}` // From a valid Twilio number
//                 //log.doErrorLog(fileNameForLogging, req.url, '`To: ${mobileNumber}, from ${appConfig.FROM_TWILIO_NUMBER}`')
//             })
            
//                 .then(message => {
//                     callback(null)
//                 })
//             .catch(error => {
//                 console.log(error)
//                 callback(error)
//             });
//     }
//     else{
        
//         callback(null)
//     }
  
    
// }

// function generateWhatsAppMsg(mobileNumber, code, callback) {
//     console.log ('sending WhatsApp via Twilio  Service')
//     const accountSid = 'AC600db655a3f02ddb69d3cfd44c1fe49c'; 
//     const authToken = '91a5f70fde9475b0624ae9ebf7d29202'; 
//     const client = require('twilio')(accountSid, authToken); 
//         client.messages
//             .create({
//                 body: `Your Skoolify code is ${code}`,
//                 to: `whatsapp:${mobileNumber}`,  // Text this number
//                 from: `whatsapp:+14155238886` // From a valid Twilio number
//             })
            
//                 .then(message => {
//                     callback(null)
//                 })
                
//             .catch(error => {
//                 console.log(error)
//                 callback(error)
//             });
    
  
    
// }

// function generateITSMsg(mobileNumber, code, callback) {
//         console.log ('sending via ITS SMS Service')
 

//         let ITS_SMS_API = process.env.ITS_SMS_API;
//         let msgData = `${code} is your verification code for Skoolify`;

//         let fullURL = 'http://bsms.its.com.pk/api.php?key=' + ITS_SMS_API + '&receiver=' + mobileNumber+ '&sender=8874&msgdata=' + msgData
//         console.log(fullURL)
//         try{
            
//             request(encodeURI(fullURL), { json: true }, (err, res, body) => {
                                            
//                 if (err) { 
//                     log.doErrorLog(fileNameForLogging, "generateITSMessage",err.message); 
//                     callback(null)
//                 }

//                 else{
//                     log.doInfoLog(fileNameForLogging, "generateITSMessage", JSON.stringify(body));
//                     callback(null)
//                 }
            
        
//             })
//         }
//         catch(error){
//             callback(error)
//         }

// }

function generateChannelSMS(mobileNumber, code,resendFlag, callback) {
     console.log(`Development Environment: ` + process.env.DEVELOPMENT)
     let development = process.env.DEVELOPMENT
     if(development=='true' && mobileNumber.startsWith("+92")){
        console.log ('sending via ITS SMS Service')
        let ITS_SMS_API = process.env.ITS_SMS_API;
        let ITS_SMS_URL = process.env.ITS_SMS_URL;
        const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});
        let msgData = `${code} is your one time login for Skoolify`;
        let fullURL = ITS_SMS_URL + '?key=' + ITS_SMS_API + '&receiver=' + mobileNumber+ '&sender=Skoolify&msgdata=' + msgData
        console.log(fullURL)
        try{
            fixieRequest(encodeURI(fullURL), { json: true }, (err, res, body) => {
                if (err) { 
                    log.doErrorLog(fileNameForLogging, "generateChannelSMS Failure",err.message); 
                    // callback(null)
                }
                else{
                    log.doInfoLog(fileNameForLogging, "generateChannelSMS Success", JSON.stringify(body));
                    // callback(null)
                }
            });

           


        }
        catch(error){
            // callback(error)
            console.error(error)
        }

    }
    else if(mobileNumber.startsWith("+92") && development=='false' && resendFlag=='0'){
        console.log ('sending via ITS SMS Service')
        let ITS_SMS_API = process.env.ITS_SMS_API;
        let ITS_SMS_URL = process.env.ITS_SMS_URL;
        const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});
        let msgData = `${code} is your one time login for Skoolify`;
        let fullURL = ITS_SMS_URL + '?key=' + ITS_SMS_API + '&receiver=' + mobileNumber+ '&sender=Skoolify&msgdata=' + msgData
        console.log(fullURL)
        try{
            fixieRequest(encodeURI(fullURL), { json: true }, (err, res, body) => {
                if (err) { 
                    log.doErrorLog(fileNameForLogging, "generateChannelSMS Dev False - Failure",err.message); 
                    // callback(null)
                }
                else{
                    log.doInfoLog(fileNameForLogging, "generateChannelSMS Dev False - Success", JSON.stringify(body));
                    // callback(null)
                }
            });
        }
        catch(error){
            // callback(error)
            console.error(error)
        }

    }
}

exports.getTnC = function(req, res) {
    const { tncID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({tncID, userID}, authValidations.getTnC, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        let dbquery=`  set @tncID = ${utils.OptionalKey(tncID)};            
                                        set @userID = ${utils.OptionalKey(userID)};            
                                        call ${appConfig.getDbConfig().database}.v1_get_web_tnc(@tncID,@userID); `
                            console.log(dbquery)
                            db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present','')
                                    if(!res.finished)res.status(400).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db','')
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if(!res.finished)res.status(200).json({ response: element })
                                            }
                                        })
                                    
                                }
                                    
                            }
                        
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{

                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
                
            })
            
        }
    })
}

exports.getAppTnC = function(req, res) {
    const { requiredLanguage } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({requiredLanguage}, authValidations.getAppTnC, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            
            
            db.getConnection(function(err, db) {
                if(!err){
                    try {
                            let dbquery=`  set @requiredLanguage = ${utils.OptionalKey(requiredLanguage)};            
                            call ${appConfig.getDbConfig().database}.v1_get_app_tnc(@requiredLanguage); `
                            console.log(dbquery)
                            db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present','')
                                    if(!res.finished)res.status(400).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db','')
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if(!res.finished)res.status(200).json({ response: element[0] })
                                            }
                                        })
                                    
                                }
                                    
                            }
                        
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
                
            })
        }
    })
    
}

exports.setAppAcceptTnC = function(req, res) {
    const { tncID, sourceUUID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    
    Joi.validate({tncID, sourceUUID}, authValidations.setAppAcceptTnC, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            let dbquery=`  set @tncID = ${utils.OptionalKey(tncID)};            
            set @sourceUUID = ${utils.OptionalKey(sourceUUID)};           
            call ${appConfig.getDbConfig().database}.v1_set_app_accept_tnc(@tncID, @sourceUUID); `
            console.log(dbquery)

            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db','')
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doInfoLog(fileNameForLogging, req.url, 'error from db',error)
                                if(!res.finished)res.status(500).json({ response: error })
                            }

                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
               
            })
        }
    })
}

exports.setTnC = function(req, res) {
    const { tncID,tncText,tncText_ar,version,isPublished,userID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({tncID,tncText,tncText_ar,version,isPublished,userID}, authValidations.setTnC, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            let dbquery=`set @tncID = ${utils.OptionalKey(tncID)}; 
                        set @tncText = ${utils.OptionalKey(tncText)}; 
                        set @tncText_ar = ${utils.OptionalKey(tncText_ar)}; 
                        set @version = ${utils.OptionalKey(version)}; 
                        set @isPublished = ${utils.OptionalKey(isPublished)}; 
                        set @userID = ${utils.OptionalKey(userID)}; 
                        call ${appConfig.getDbConfig().database}.v1_set_app_tnc 
                        (@tncID,@tncText,@tncText_ar,@version,@isPublished,@userID);`
                        console.log(dbquery)

                        db.getConnection(function(err, db) {
                            if(!err){
                                try {
                                    db.query(dbquery,function (error, data, fields) {
                                            if(!error){
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({ response: "success" })
                                            
                                            }
                                            else{
                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                console.log(errorResponse)
                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                            }
                                    });
                                } 
                                finally {
                                    db.release();
                                    console.log('released')
                                }

                            }
                            else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting conection pool', err)
                                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                        
                                }
                                
                        })
        }
    })
}

exports.setContract = function(req, res) {
    const { contractID,contractText, chargesText, collectionText, version,isPublished,userID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({contractID,contractText,chargesText, collectionText, version,isPublished,userID}, authValidations.setContract, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            let dbquery=`set @contractID = ${utils.OptionalKey(contractID)}; 
                        set @contractText = ${utils.OptionalKeyString(contractText)}; 
                        set @chargesText = ${utils.OptionalKeyString(chargesText)}; 
                        set @collectionText = ${utils.OptionalKeyString(collectionText)}; 
                        set @version = ${utils.OptionalKey(version)}; 
                        set @isPublished = ${utils.OptionalKey(isPublished)}; 
                        set @userID = ${utils.OptionalKey(userID)}; 
                        call ${appConfig.getDbConfig().database}.v1_set_contract 
                        (@contractID,@contractText,@chargesText,@collectionText, @version,@isPublished,@userID);`
                        console.log(dbquery)

            db.getConnection(function(err, db) {
            if(!err){
                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
                
            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
    
            }
            
        })
                    
                    
                    
        }
    })
}


exports.getContract = function(req, res) {
    const { contractID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({contractID, userID}, authValidations.getContract, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            let dbquery = `select * from v1_get_contract `
            if (contractID || userID)
            dbquery = dbquery + ` Where `;

            if(contractID && !userID )
            dbquery = dbquery + ` contractID = '${contractID}' `;
            
            if(userID && !contractID )
            dbquery = dbquery + ` userID = '${userID}' `;

            if(contractID && userID){
                dbquery = dbquery + ` contractID = '${contractID}' `;
                dbquery = dbquery + `  AND userID = '${userID}' `;
            
            }
            console.log(dbquery)

            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if(_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } 
                                    else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: data})
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
            
                }
               
            })
            
        }
    })
}

exports.setSMSSendArray = function (req, res) {
    const { array } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
   
    console.log(req.body);
    //let ITS_SMS_API = process.env.ITS_SMS_API;
    //console.log(ITS_SMS_API)
    Joi.validate(array, authValidations.setSMSSendArray, function(err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
          db.getConnection(function(err, db) {
            if(!err){
                let INVITE_SMS = process.env.INVITE_SMS;
                let url_short_android = process.env.url_short_android
                let url_short_ios = process.env.url_short_ios
                
                let dbquery=``
                let schoolName =``
                if (array != undefined){
                    
                    // get school name for message
                    dbquery = `set @mobileNumber = ${utils.OptionalKey(array[0].mobileNumber)}; 
                    call ${appConfig.getDbConfig().database}.v1_get_mobile_to_schoolname 
                    (@mobileNumber);`
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                                if (!_.isEmpty(element)) {
                                                  schoolName = element[0].schoolName;
                                                }
                                            }
                                        })
                                        dbquery=``
                                        if(schoolName){
                                            for (let i = 0; i < array.length; i++) {
                                            console.log(`Aray Length: ` + array.length)
                                            
                                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_parent_sms
                                                (${utils.OptionalKey(array[i].mobileNumber)}, 
                                                ${utils.OptionalKey(INVITE_SMS + '\n android:' + url_short_android + '\n ios:' + url_short_ios + '\n' + schoolName)}, 
                                                ${utils.OptionalKey('INVITE_SMS')});  `
                                        
                                            console.log(dbquery)
                                                let messageText= INVITE_SMS + '\n android:' + url_short_android + '\n ios:' + url_short_ios + '\n' + schoolName
                                            db.query(dbquery,function (error, data, fields) {
                                                    if(!error){
                                                    
                                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data update in db')
                                                        sendMessagToITS(array[i].mobileNumber, messageText, error => {
                                                            if (error) {
                                                                log.doFatalLog( req.url, 'error in ITS Service', error)
                                                                if(!res.finished)res.status(500).json(new Error(appConfig.GENERIC_ERROR_MSG, "messaging failed!", error))
                                                            } else {
                                                                
                                                                if(!res.finished)res.status(200).json({ response: "success" })
                                                            }
                                                        })
                                
                                                    }
                                                    else{
                                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        console.log(errorResponse)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                                    }
                                                });
                                                
                                        
                                                
                                        
                                            }
                                        }
                                        else{
                                            if(!res.finished)res.status(404).json({ response: "no data found" })
                                        }
                                        
                                    
                        
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                    if(!res.finished)res.status(500).json({ response: "'array not found'" })
                }             
                
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }
            
          })
        }
    })
}

function sendMessagToITS(mobileNumber, msgText, callback) {
    console.log ('sending via ITS SMS Service')
    
    let ITS_SMS_API = process.env.ITS_SMS_API;
    let ITS_SMS_URL = process.env.ITS_SMS_URL;
    const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});

        let fullURL = ITS_SMS_URL + '?key=' + ITS_SMS_API + '&receiver=' + mobileNumber+ '&sender=Skoolify&msgdata=' + msgText
        console.log(fullURL)
        try{
            
            fixieRequest(encodeURI(fullURL), { json: true }, (err, res, body) => {
                if (err) { 
                    log.doErrorLog(fileNameForLogging, "generateITSMessage Failure",err.message); 
                     callback(null)
                }
                else{
                    log.doInfoLog(fileNameForLogging, "generateITSMessage Success", JSON.stringify(body));
                     callback(null)
                }
            });
        }
        catch(error){
            callback(error)
        }
    
}

// exports.getVideoToken = function(req, res) {
//     const { identity, roomName} = req.body
//         const AccessToken = twilio.jwt.AccessToken;
       

//         const VideoGrant = AccessToken.VideoGrant;
//         const twilioAccountSid = process.env.TWILIO_API_KEY_ACCOUNT_SID.trim();
//         const twilioApiKeySID = process.env.TWILIO_API_KEY_VIDEO_SID.trim();
//         const twilioApiKeySecret = process.env.TWILIO_API_KEY_VIDEO_SECRET.trim();
//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
//     console.log ('Account SID:' + twilioAccountSid + '- Key SID:' + twilioApiKeySID + '- Secret:' + twilioApiKeySecret + '-' )
//     Joi.validate({identity, roomName}, authValidations.getVideoToken, function(err, value) {
//         if(err) {
//             log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//             if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//         } else {
//             const token = new AccessToken(twilioAccountSid, twilioApiKeySID, twilioApiKeySecret, {
//                 ttl: process.env.TWILIO_MAX_ALLOWED_SESSION_DURATION,
//               });
//                 token.identity = identity.trim();
//                 const videoGrant = new VideoGrant({ room: roomName.trim() });
//                 token.addGrant(videoGrant);
                
//                 console.log(`issued token for ${identity} in room ${roomName}`);
//                 if(!res.finished)res.status(200).json({ response: token.toJwt() })
                
                

//         }
//     })
// }



exports.setVerifyOTP = function (req, res) {
    const { mobileNumber, OTP, sourceUUID,deviceToken,deviceType} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ mobileNumber, OTP, sourceUUID,deviceToken, deviceType }, authValidations.setVerifyOTP, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            emails.SendEmail(`OTP.Alerts@infinitystudio.pk`,`device Token failure`,`Unable to generate device token for ${utils.OptionalKey(mobileNumber)} .. UUID:${utils.OptionalKey(sourceUUID)}, deviceToken:${utils.OptionalKey(deviceToken)},deviceType: ${utils.OptionalKey(deviceType)}`)
             
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`  call ${appConfig.getDbConfig().database}.v1_set_verify_otp(
                        ${utils.OptionalKey(mobileNumber)}, 
                        ${utils.OptionalKey(OTP)}, 
                        ${utils.OptionalKey(sourceUUID)} , 
                        ${utils.OptionalKey(deviceToken)},
                        ${utils.OptionalKey(deviceType)});`
                console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (!_.isEmpty(data)){
                                   
                                    data.forEach(element => {     
                                            if(element.constructor==Array) {
                                                if (!_.isEmpty(element)){
                                                    if(!res.finished)res.status(200).json({response: element})
                                                } 
                                            
                                                else{
                                                    log.doErrorLog(fileNameForLogging, req.url, 'otp not found.')
                                                    const errorResponse = new Error(appConfig.INVALID_OTP_ERROR_MSG, 'otp not found.')
                                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                                }
                                            }      
                                    })
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({response: errorResponse})
                            }
                        })
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    if(!res.finished)res.status(500).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err))
                }
                
            });
        }
    })
}


exports.generateTextVoiceOTP = function (req, res) {
    let { mobileNumber, sourceUUID, OTPType, SMSHash } = req.body
    log.doInfoLog( fileNameForLogging,req.url, '', req.body)
    Joi.validate({ mobileNumber, sourceUUID, OTPType,SMSHash }, authValidations.generateTextVoiceOTP, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            
            if (OTPType=='voice'){
                let callback = generateChannelVoice(mobileNumber,sourceUUID)
                if(!res.finished)res.status(200).json({response: "Your will recieve a call shortly, verbalizing your OTP"})
            }
            
            else {
                let callback = generateChannelMessage(mobileNumber,sourceUUID,SMSHash)
                    if(!res.finished)res.status(200).json({response: "Your verification code has been sent to your mobile number."})
            }

        }
    })
}

function generateChannelSMS(mobileNumber, code,resendFlag, callback) {
    console.log(`Development Environment: ` + process.env.DEVELOPMENT)
    let development = process.env.DEVELOPMENT
    if(development=='true' && mobileNumber.startsWith("+92")){
       console.log ('sending via ITS SMS Service')
       let ITS_SMS_API = process.env.ITS_SMS_API;
       let ITS_SMS_URL = process.env.ITS_SMS_URL;
       const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});
       let msgData = `<%23> ${code} is your verification code for Skoolify`;
       let fullURL = ITS_SMS_URL + '?key=' + ITS_SMS_API + '&receiver=' + mobileNumber+ '&sender=Skoolify&msgdata=' + msgData
       console.log(fullURL)
       try{
           fixieRequest(encodeURI(fullURL), { json: true }, (err, res, body) => {
                if (err) { 
                    log.doErrorLog(fileNameForLogging, "function generateChannelSMS - Failure",err.message); 
                    // callback(null)
                }
                else{
                    log.doInfoLog(fileNameForLogging, "function generateChannelSMS - Success", JSON.stringify(body));
                    // callback(null)
                }
            });
       }
       catch(error){
           // callback(error)
           console.error(error)
       }

   }
   else if(mobileNumber.startsWith("+92") && development=='false' && resendFlag=='0'){
       console.log ('sending via ITS SMS Service')
       let ITS_SMS_API = process.env.ITS_SMS_API;
       let ITS_SMS_URL = process.env.ITS_SMS_URL;
       const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});
       let msgData = `${code} is your one time login for Skoolify`;
       let fullURL = ITS_SMS_URL + '?key=' + ITS_SMS_API + '&receiver=' + mobileNumber+ '&sender=Skoolify&msgdata=' + msgData
       console.log(fullURL)
       try{
            fixieRequest(encodeURI(fullURL), { json: true }, (err, res, body) => {
                if (err) { 
                    log.doErrorLog(fileNameForLogging, "function generateChannelSMS Dev False - Failure",err.message); 
                    // callback(null)
                }
                else{
                    log.doInfoLog(fileNameForLogging, "function generateChannelSMS Dev False - Success", JSON.stringify(body));
                    // callback(null)
                }
            });
       }
       catch(error){
           // callback(error)
           console.error(error)
       }

   }
   else   {
       console.log ('sending SMS via Twilio  Service')
       client.messages
           .create({
               body: `<#> ${code} is your verification code for Skoolify`,
               to: `${mobileNumber}`,  // Text this number
               from: `${appConfig.FROM_TWILIO_NUMBER}` // From a valid Twilio number
               //log.doErrorLog(fileNameForLogging, req.url, '`To: ${mobileNumber}, from ${appConfig.FROM_TWILIO_NUMBER}`')
           })
           .then(message => {
               console.log(message.sid)
           })
           // .then(message => {
           //         callback(null)
           //     })
           .catch(error => {
               console.log(error)
               emails.SendEmail('twilo.issues@infinitystudio.pk','Twilio SMS Issue',error.message)
               // callback(error)
           });
   }  
}


function generateChannelMessage(mobileNumber, sourceUUID,SMSHash, callback) {
    let development = process.env.DEVELOPMENT
    console.log(`Development Environment: ` + development)
    
    db.getConnection(function(err, db) {
        if(!err){
            try {
                  const code = randomstring.generate({length: 6,charset: 'numeric' })
  
                  let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_create_otp(
                                  ${utils.OptionalKey(code)}, 
                                  ${utils.OptionalKey(mobileNumber)}, 
                                  ${utils.OptionalKey(sourceUUID)});`
                  //console.log(dbquery)
                  db.query(dbquery, function (error, data, fields) {
                    if(!error){
                        data.forEach(element => {
                        if(element.constructor==Array) {
                            if (!_.isEmpty(element)) {
                                   
                            console.log(` mobileNumber: ${mobileNumber} ...OTP: ${element[0].otpString} ...sourceUUID: ${sourceUUID}`)
                            // if development than send through local provider or use local provider if sent for the first time
                            let currentUTCTime = Math.floor((new Date()).getTime() / 1000);
                            let OTPGenerateUTCTime = new Date(`${element[0].insertionDate} GMT`).getTime()/1000;
                            
                            if( (development=='true' && mobileNumber.startsWith("+92") && appConfig.ENABLE_ITS_SMS=='true' ) || (mobileNumber.startsWith("+92") && development=='false' && currentUTCTime < OTPGenerateUTCTime +120 && appConfig.ENABLE_ITS_SMS=='true' ) ){
                                console.log ('sending via ITS SMS Service')
                                let ITS_SMS_API = process.env.ITS_SMS_API;
                                let ITS_SMS_URL = process.env.ITS_SMS_URL;
                                const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});
                                let msgData = `<${encodeURIComponent('#')}> ${element[0].otpString} is your one time login for Skoolify`;
                                if (SMSHash){
                                    msgData = msgData + `\n${SMSHash}`
                                }
                                let fullURL = ITS_SMS_URL + '?key=' + ITS_SMS_API + '&receiver=' + mobileNumber.replace("+","")+ '&sender=Skoolify&msgdata=' + msgData
                                console.log(fullURL)
                                fixieRequest(fullURL, { json: true }, (err, res, body) => {
                                    if (err) { 
                                        log.doErrorLog(fileNameForLogging, "generateChannelMessage - Failure",err.message); 
                                        // callback(null)
                                    }
                                    else{
                                        log.doInfoLog(fileNameForLogging, "generateChannelMessage - Success", JSON.stringify(body));
                                        if(body.response.status=="Error"){
                                            emails.SendEmail(`SMS.ErrorAlert@infinitystudio.pk`,`ITS SMS Error`,`SMS Not going through ITS ${JSON.stringify(body)}`)
                                        }
                                        
                                    }
                                });
                                    
                            }
                            
                            else   {
                                console.log ('sending SMS via Twilio  Service')
                                let msgData = `<#> ${element[0].otpString} is your verification code for Skoolify`
                                if (SMSHash){
                                    msgData = msgData + `\n${SMSHash}`
                                }
                                client.messages
                                    .create({
                                        body: msgData,
                                        to: `${mobileNumber}`,  // Text this number
                                        from: `${appConfig.FROM_TWILIO_NUMBER}` // From a valid Twilio number
                                    })
                                    .then(message => {
                                        console.log(`Message sent sucessfully: ` + message.sid)
                                    })
                                    
                                    .catch(error => {
                                        console.log(error)
                                        emails.SendEmail('twilo.issues@infinitystudio.pk','Twilio SMS Issue',error.message)
                                    });
                            } 
                                
                                
                                }
                            else{
                                console.log('element array present but data not present')
                                
                                

                            }
                        }
                        
                        })
                    }
                    else{
                        console.log( 'error in inserting OTP in database', error)
                        
                    }
                  })
                
    
            } 
            finally {
                db.release();
                console.log('released')
            }
        }
        else{
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          console.log(`error in getting connection pool` + errorResponse)
        }
    })
   
  }
  
  
  
  function generateChannelVoice(mobileNumber, sourceUUID, callback) {
    let development = process.env.DEVELOPMENT
    console.log(`Development Environment: ` + development)
    
    db.getConnection(function(err, db) {
        if(!err){
            try {
                  const code = randomstring.generate({length: 6,charset: 'numeric' })
  
                  let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_create_otp(
                                  ${utils.OptionalKey(code)}, 
                                  ${utils.OptionalKey(mobileNumber)}, 
                                  ${utils.OptionalKey(sourceUUID)});`
                  console.log(dbquery)
                  db.query(dbquery, function (error, data, fields) {
                    data.forEach(element => {
                      if(element.constructor==Array) {
                            if (!_.isEmpty(element)) {
                                
                                if(!error){
                                  console.log(` mobileNumber: ${mobileNumber} ...OTP: ${element[0].otpString}`) 
                                  if((development=='true' && mobileNumber.startsWith("+92")) || (mobileNumber.startsWith("+92") && development=='false' )){
                                    console.log ('calling via ITS SMS Service')
                                    let ITS_SMS_API = process.env.ITS_SMS_API;
                                    let ITS_SMS_URL = process.env.ITS_SMS_URL;
                                    const fixieRequest = request.defaults({'proxy': process.env.FIXIE_URL});
                                    let msgData = `<#> ${code} is your one time login for Skoolify \n SGnpgthFIQs`;
                                    let fullURL = 'http://bsms.its.com.pk/apiotp.php?key=' + ITS_SMS_API + '&receiver=' + mobileNumber.replace("+","") + '&otp=' + element[0].otpString
                                    console.log(fullURL)
                                   
                                    fixieRequest(encodeURI(fullURL), { json: true }, (err, res, body) => {
                                        if (err) { 
                                            log.doErrorLog(fileNameForLogging, "generateChannelVoice",err.message); 
                                            // callback(null)
                                        }
                                        else{
                                            log.doInfoLog(fileNameForLogging, "generateChannelVoice", JSON.stringify(body));
                                            // callback(null)
                                        }
                                    });
                                  }
                                
                                  else   {
                                    console.log ('calling via Twilio  Service')
                                    client.calls
                                        .create({
                                          twiml: `<Response><Say voice="alice" loop="2" language="en-GB">This is a call from Skoolify!! Your One Time Password is,<emphasis level="moderate"> ${element[0].otpString.split('').join(' ')} </emphasis> </Say></Response>`,
                                            to: `${mobileNumber}`,  // Text this number
                                            from: `${appConfig.FROM_TWILIO_NUMBER}` // From a valid Twilio number
                                        })
                                        .then(message => {
                                            console.log(`Voice call submitted` + message.sid)
                                        })
                                        .catch(error => {
                                            console.log(error)
                                            emails.SendEmail('twilo.issues@infinitystudio.pk','Twilio SMS Issue',error.message)
                                        });
                                } 
                              }
                              else{
                                  console.log( 'error in inserting OTP in database', error)
                                  
                              }
                            }
                            else{
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(404).json({ response: errorResponse })
  
                            }
                      }
                  })
                      
                  })
    
            } 
            finally {
                db.release();
                console.log('released')
            }
        }
        else{
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          console.log(`error in getting connection pool` + errorResponse)
        }
    })
   
}