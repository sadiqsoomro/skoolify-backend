const Joi = require('joi'), utils = require('../../utils/helper-methods'),
    subjectValidations = require('../validations/validations-subject'),
    db = require('../../../db'),
    log = require('../../utils/utils-logging'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    fileNameForLogging = 'subject-controller'
import Error from '../response-classes/error'
import { format } from 'util';

// ==================================================================

exports.getSubjects = function (req, res) {
    const { classLevelID, subjectID, classID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, subjectID, classID, userID }, subjectValidations.getSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_subjects WHERE classLevelID IN (${classLevelID}) `;
                    if (userID)
                    dbquery += `AND userID= '${userID}'`

                    if (subjectID)
                    dbquery += `AND subjectID= '${subjectID}'`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
            
            
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
            
                                    let distinctSubjects = new Array();
                                    let distinctObjects = new Array();
            
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        if (!distinctSubjects.includes(data[loop].subjectID)) {
                                            distinctSubjects.push(data[loop].subjectID);
            
                                            var partialData = Object.assign({}, data[loop]);
            
                                            if (partialData.hasOwnProperty("classSection")) {
                                                delete partialData.classSection;
                                            }
                                            if (partialData.hasOwnProperty("classShift")) {
            
                                                delete partialData.classShift;
                                            }
                                            if (partialData.hasOwnProperty("teacherSubjectLinkID")) {
            
                                                delete partialData.teacherSubjectLinkID;
                                            }
            
                                            if (partialData.hasOwnProperty("fullName")) {
            
                                                delete partialData.fullName;
                                            }
                                            distinctObjects.push(partialData);
            
                                        }
                                    }
                                    
                                    for (let j = 0; j < distinctObjects.length; j++) {
                                        let formattedData = new Array();
                                        if (distinctObjects[j].classID ){
                                        for (let i = 0; i < data.length; i++) {
                                            if (distinctObjects[j].subjectID == data[i].subjectID) {
                                                
                                                if (data[i].hasOwnProperty("subjectID")) {
            
                                                    delete data[i].subjectID;
                                                }
                                                if (data[i].hasOwnProperty("subjectName")) {
            
                                                    delete data[i].subjectName;
                                                }
                                                if (data[i].hasOwnProperty("classLevelID")) {
            
                                                    delete data[i].classLevelID;
                                                }
                                                if (data[i].hasOwnProperty("classLevelName")) {
            
                                                    delete data[i].classLevelName;
                                                }
                                                if (data[i].hasOwnProperty("classAlias")) {
            
                                                    delete data[i].classAlias;
                                                }
                                                if (data[i].hasOwnProperty("isActive")) {
            
                                                    delete data[i].isActive;
                                                }
                                                formattedData.push(data[i]);
                                            }
                                        }
                                        distinctObjects[j].subjectDetails = formattedData;
                                    }}
            
                                    if(!res.finished)res.status(200).json({
                                        response: distinctObjects
                                    })
            
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveSubjects = function (req, res) {
    const { classLevelID, subjectID, classID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, subjectID, classID, userID }, subjectValidations.getActiveSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_subjects WHERE isActive=1 and classLevelID IN (${classLevelID}) `;
                    if (userID)
                    dbquery += `AND userID= '${userID}'`

                    if (subjectID)
                    dbquery += `AND subjectID= '${subjectID}'`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
            
            
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
            
                                    let distinctSubjects = new Array();
                                    let distinctObjects = new Array();
            
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        if (!distinctSubjects.includes(data[loop].subjectID)) {
                                            distinctSubjects.push(data[loop].subjectID);
            
                                            var partialData = Object.assign({}, data[loop]);
            
                                            if (partialData.hasOwnProperty("classSection")) {
                                                delete partialData.classSection;
                                            }
                                            if (partialData.hasOwnProperty("classShift")) {
            
                                                delete partialData.classShift;
                                            }
                                            if (partialData.hasOwnProperty("teacherSubjectLinkID")) {
            
                                                delete partialData.teacherSubjectLinkID;
                                            }
            
                                            if (partialData.hasOwnProperty("fullName")) {
            
                                                delete partialData.fullName;
                                            }
                                            distinctObjects.push(partialData);
            
                                        }
                                    }
                                    
                                    for (let j = 0; j < distinctObjects.length; j++) {
                                        let formattedData = new Array();
                                        if (distinctObjects[j].classID ){
                                        for (let i = 0; i < data.length; i++) {
                                            if (distinctObjects[j].subjectID == data[i].subjectID) {
                                                
                                                if (data[i].hasOwnProperty("subjectID")) {
            
                                                    delete data[i].subjectID;
                                                }
                                                if (data[i].hasOwnProperty("subjectName")) {
            
                                                    delete data[i].subjectName;
                                                }
                                                if (data[i].hasOwnProperty("classLevelID")) {
            
                                                    delete data[i].classLevelID;
                                                }
                                                if (data[i].hasOwnProperty("classLevelName")) {
            
                                                    delete data[i].classLevelName;
                                                }
                                                if (data[i].hasOwnProperty("classAlias")) {
            
                                                    delete data[i].classAlias;
                                                }
                                                if (data[i].hasOwnProperty("isActive")) {
            
                                                    delete data[i].isActive;
                                                }
                                                formattedData.push(data[i]);
                                            }
                                        }
                                        distinctObjects[j].subjectDetails = formattedData;
                                    }}
            
                                    if(!res.finished)res.status(200).json({
                                        response: distinctObjects
                                    })
            
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getUserAllSubjects = function (req, res) {
    const { classID, classLevelID,userID, subjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, classLevelID,userID, subjectID }, subjectValidations.getUserAllSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @userID = ${userID};
                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                    set @classID = ${utils.OptionalKey(classID)};
                    call ${appConfig.getDbConfig().database}.v1_get_user_all_subjects
                    (@userID,@classLevelID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')

                                        
                                        let noOfIterations = element.length;
                
                                        let distinctSubjects = new Array();
                                        let distinctNonSubject = new Array();
                                        
                
                                        for (let loop = 0; loop < noOfIterations; loop++) {
                                            
                                                
                                                
                                                var partialData = Object.assign({}, element[loop]);
                                                if (element[loop].subjectType == "subject") {
                                                    // if (element[loop].hasOwnProperty("subjectType")) {
                                                    //     delete element[loop].subjectType;
                                                    // }
                                                
                                                    distinctSubjects.push(element[loop]);
                                                }
                                                else{
                                                    // if (element[loop].hasOwnProperty("subjectType")) {
                                                    //     delete element[loop].subjectType;
                                                    // }
                                                    distinctNonSubject.push(element[loop]);
                                                }
  
                                        }
                                        
                                        
                                        if(!res.finished)res.status(200).json({   response: {subjects: distinctSubjects}, nonsubjects: distinctNonSubject  })
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getUserExamSubjects = function (req, res) {
    const { classID, userID, subjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, userID, subjectID }, subjectValidations.getUserExamSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @userID = ${utils.OptionalKey(userID)};
                    set @classID = ${utils.OptionalKey(classID)};
                    call ${appConfig.getDbConfig().database}.v1_get_user_exam_subjects
                    (@userID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({   response: element  })
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getNonSubjects = function (req, res) {
    const { classLevelID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, nonSubjectID }, subjectValidations.getNonSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * From v1_get_nonsubjects WHERE classLevelID = '${classLevelID}'`;
                    if (nonSubjectID)
                        dbquery += ` and nonSubjectID = '${nonSubjectID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
            
                                    let distinctnonSubjects = new Array();
                                    let distinctObjects = new Array();
            
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        if (!distinctnonSubjects.includes(data[loop].nonSubjectID)) {
                                            distinctnonSubjects.push(data[loop].nonSubjectID);
            
                                            var partialData = Object.assign({}, data[loop]);
            
                                            if (partialData.hasOwnProperty("classSection")) {
                                                delete partialData.classSection;
                                            }
                                            if (partialData.hasOwnProperty("classShift")) {
            
                                                delete partialData.classShift;
                                            }
                                            if (partialData.hasOwnProperty("nonSubjectLinkID")) {
            
                                                delete partialData.nonSubjectLinkID;
                                            }
            
                                            if (partialData.hasOwnProperty("fullname")) {
            
                                                delete partialData.fullname;
                                            }
                                            distinctObjects.push(partialData);
            
                                        }
                                    }
                                    
                                    for (let j = 0; j < distinctObjects.length; j++) {
                                        let formattedData = new Array();
                                        if (distinctObjects[j].classID ){
                                        for (let i = 0; i < data.length; i++) {
                                            if (distinctObjects[j].nonSubjectID == data[i].nonSubjectID) {
                                                
                                                if (data[i].hasOwnProperty("nonSubjectID")) {
            
                                                    delete data[i].nonSubjectID;
                                                }
                                                if (data[i].hasOwnProperty("nonSubjectName")) {
            
                                                    delete data[i].nonSubjectName;
                                                }
                                                
                                                if (data[i].hasOwnProperty("classLevelID")) {
            
                                                    delete data[i].classLevelID;
                                                }
                                                if (data[i].hasOwnProperty("classLevelName")) {
            
                                                    delete data[i].classLevelName;
                                                }
                                                if (data[i].hasOwnProperty("classAlias")) {
            
                                                    delete data[i].classAlias;
                                                }
                                                if (data[i].hasOwnProperty("isActive")) {
            
                                                    delete data[i].isActive;
                                                }
                                                formattedData.push(data[i]);
                                            }
                                        }
                                        distinctObjects[j].subjectDetails = formattedData;
                                    }}
            
                                    if(!res.finished)res.status(200).json({
                                        response: distinctObjects
                                    })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveNonSubjects = function (req, res) {
    const { classLevelID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, nonSubjectID }, subjectValidations.getActiveNonSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_nonsubjects WHERE isActive =1 and classLevelID = '${classLevelID}'`;
                    if (nonSubjectID)
                        dbquery += ` and nonSubjectID = '${nonSubjectID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteNonSubject = function (req, res) {
    const { nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ nonSubjectID }, subjectValidations.deleteNonSubject, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_nonsubject(${utils.OptionalKey(nonSubjectID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getClassSubjects = function (req, res) {
    const { schoolID, classLevelID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, classLevelID, branchID }, subjectValidations.getClassSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT *    From v1_get_class_subjects   WHERE schoolID='${schoolID}' AND classLevelID = '${classLevelID}'   AND branchID = '${branchID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        let innerObject = {};
                                        if (data[loop].hasOwnProperty("subjectID")) {
                                            innerObject["subjectID"] = data[loop].subjectID;
                                            if (loop == 0)
                                                delete data[loop].subjectID;
                                        }
                                        if (data[loop].hasOwnProperty("subjectName")) {
                                            innerObject["subjectName"] = data[loop].subjectName;
                                            if (loop == 0)
                                                delete data[loop].subjectName;
                                        }
                                        if (data[loop].hasOwnProperty("isActive")) {
                                            innerObject["isActive"] = data[loop].isActive;
                                            if (loop == 0)
                                                delete data[loop].isActive;
                                        }
            
                                        innerArray[loop] = innerObject;
                                    }
                                    data[0].subjects = innerArray;
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data[0] })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setClassSubjects = function (req, res) {
    const { subjectID, classLevelID, subjectClassLinkID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ subjectID, classLevelID, subjectClassLinkID }, subjectValidations.setClassSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`set @subjectClassLinkID = ${utils.OptionalKey(subjectClassLinkID)};
                set @classLevelID = ${utils.OptionalKey(classLevelID)};
                set @subjectID = ${utils.OptionalKey(subjectID)};
                call ${appConfig.getDbConfig().database}.v1_set_class_subjects
                (@subjectClassLinkID,@classLevelID,@subjectID);`
                console.log(dbquery)

                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                }   
                finally {
                    db.release();
                    console.log('released')
                }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setSubjects = function (req, res) {
    const { subjectID,classLevelID,subjectName,isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({subjectID,classLevelID,subjectName,isActive}, subjectValidations.setSubjects, 
        function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    
                    let dbquery=`set @subjectID = ${utils.OptionalKey(subjectID)};
                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                    set @subjectName = ${utils.OptionalKeyString(subjectName)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_subjects
                    (@subjectID,@classLevelID,@subjectName,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success" })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setSubjectsArray = function (req, res) {
    const { array } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, subjectValidations.setSubjectsArray, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    if(array != undefined){

                    let dbquery = "";
                    for (let i = 0; i < array.length; i++) {
                        dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_subjects
                        (${utils.OptionalKey(array[i].subjectID)}, 
                        ${utils.OptionalKey(array[i].classLevelID)}, 
                        ${utils.OptionalKey(array[i].subjectName)}, 
                        ${utils.OptionalKey(array[i].isActive)});  `
                    }
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: "success" })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                        if(!res.finished)res.status(500).json({ response: "'array not found'" })
          
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setNonSubjects = function (req, res) {
    const { isActive, nonSubjectName, classLevelID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ isActive, nonSubjectName, classLevelID, nonSubjectID }, subjectValidations.setNonSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @nonSubjectID = ${utils.OptionalKey(nonSubjectID)};
                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                    set @nonSubjectName = ${utils.OptionalKeyString(nonSubjectName)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_nonsubjects
                    (@nonSubjectID,@classLevelID,@nonSubjectName,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: element })
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setClassNonSubjects = function (req, res) {
    const { classLevelID, nonSubjectID, nonSubjectClassLinkID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, nonSubjectID, nonSubjectClassLinkID }, subjectValidations.setClassNonSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `set @nonSubjectClassLinkID = ${utils.OptionalKey(nonSubjectClassLinkID)};
                        set @nonSubjectID = ${utils.OptionalKey(nonSubjectID)};
                        set @classLevelID = ${utils.OptionalKey(classLevelID)};
                        call ${appConfig.getDbConfig().database}.v1_set_class_nonsubjects
                        (@nonSubjectClassLinkID,@nonSubjectID,@classLevelID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: "success"})
                    
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}





exports.getNonSubjectLegends = function (req, res) {
    const { classLevelID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, nonSubjectID }, subjectValidations.getNonSubjectLegends, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_nonsubject_legends 
                    WHERE classLevelID = '${classLevelID}'`;
                    if (nonSubjectID)
                    dbquery += ` and nonSubjectID = '${nonSubjectID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setNonSubjectLegends = function (req, res) {
    const { nsLegendID, nonSubjectID, legendValue, isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ nsLegendID, nonSubjectID, legendValue, isActive }, subjectValidations.setNonSubjectLegends, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @nsLegendID = ${utils.OptionalKey(nsLegendID)};
                    set @nonSubjectID = ${utils.OptionalKey(nonSubjectID)};
                    set @legendValue = ${utils.OptionalKey(legendValue)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_nonsubject_legends
                    (@nsLegendID,@nonSubjectID,@legendValue,@isActive);`
                    console.log(dbquery)

                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: "success" })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getSubjectsTeachers = function (req, res) {
    const { classLevelID, subjectID, classID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, subjectID, classID, userID }, subjectValidations.getSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_subjects WHERE classLevelID= '${classLevelID}' `;
                    if (userID)
                        dbquery += `AND userID= '${userID}'`
                    if (subjectID)
                        dbquery += `AND subjectID= '${subjectID}'`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
            
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        let innerObject = {};
                                        if (data[loop].hasOwnProperty("fullName")) {
                                            innerObject["fullName"] = data[loop].fullName;
                                            if (loop == 0)
                                                delete data[loop].fullName;
                                        }
                                        if (data[loop].hasOwnProperty("userID")) {
                                            innerObject["userID"] = data[loop].userID;
                                            if (loop == 0)
                                                delete data[loop].userID;
                                        }
                                        innerArray[loop] = innerObject;
                                    }
                                    data[0].LinkedTeachers = innerArray;
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({  response: data[0] })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setDelinkSubjectTeacher = function (req, res) {
    const { subjectID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ subjectID, userID }, subjectValidations.setDelinkSubjectTeacher, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `
                    set @userID = ${utils.OptionalKey(userID)};
                    set @subjectID = ${utils.OptionalKey(subjectID)};
                    call ${appConfig.getDbConfig().database}.v1_set_delink_subject_teacher
                    (@subjectID,@userID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({    response: "success"  })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                     
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getExamSubjects = function (req, res) {
    const { classLevelID, subjectID, classID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classLevelID, subjectID, classID, userID }, subjectValidations.getSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_exam_subjects WHERE classLevelID IN (${classLevelID}) `;
                    if (userID)
                    dbquery += `AND userID= '${userID}'`

                    if (subjectID)
                    dbquery += `AND subjectID= '${subjectID}'`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
        
                                    let innerArray = new Array();
                                    let noOfIterations = data.length;
            
                                    let distinctSubjects = new Array();
                                    let distinctObjects = new Array();
            
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        if (!distinctSubjects.includes(data[loop].subjectID)) {
                                            distinctSubjects.push(data[loop].subjectID);
            
                                            var partialData = Object.assign({}, data[loop]);
            
                                            if (partialData.hasOwnProperty("classSection")) {
                                                delete partialData.classSection;
                                            }
                                            if (partialData.hasOwnProperty("classShift")) {
            
                                                delete partialData.classShift;
                                            }
                                            if (partialData.hasOwnProperty("teacherSubjectLinkID")) {
            
                                                delete partialData.teacherSubjectLinkID;
                                            }
            
                                            if (partialData.hasOwnProperty("fullName")) {
            
                                                delete partialData.fullName;
                                            }
                                            distinctObjects.push(partialData);
            
                                        }
                                    }
                                    
                                    for (let j = 0; j < distinctObjects.length; j++) {
                                        let formattedData = new Array();
                                        if (distinctObjects[j].classID ){
                                        for (let i = 0; i < data.length; i++) {
                                            if (distinctObjects[j].subjectID == data[i].subjectID) {
                                                
                                                if (data[i].hasOwnProperty("subjectID")) {
            
                                                    delete data[i].subjectID;
                                                }
                                                if (data[i].hasOwnProperty("subjectName")) {
            
                                                    delete data[i].subjectName;
                                                }
                                                if (data[i].hasOwnProperty("classLevelID")) {
            
                                                    delete data[i].classLevelID;
                                                }
                                                if (data[i].hasOwnProperty("classLevelName")) {
            
                                                    delete data[i].classLevelName;
                                                }
                                                if (data[i].hasOwnProperty("classAlias")) {
            
                                                    delete data[i].classAlias;
                                                }
                                                if (data[i].hasOwnProperty("isActive")) {
            
                                                    delete data[i].isActive;
                                                }
                                                formattedData.push(data[i]);
                                            }
                                        }
                                        distinctObjects[j].subjectDetails = formattedData;
                                    }}
            
                                    if(!res.finished)res.status(200).json({response: distinctObjects})
            
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteSubject = function (req, res) {
    const { subjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({subjectID}, subjectValidations.deleteSubject, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @subjectID = ${utils.OptionalKey(subjectID)};
                    
                    call ${appConfig.getDbConfig().database}.v1_delete_subject
                    (@subjectID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){

                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: "success"})

                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getUserSubjects = function (req, res) {
    const { classID, classLevelID,userID, subjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, classLevelID,userID, subjectID }, subjectValidations.getUserSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @userID = ${userID};
                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                    set @classID = ${utils.OptionalKey(classID)};
                    call ${appConfig.getDbConfig().database}.v1_get_user_subjects
                    (@userID,@classLevelID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } 
                                    else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        let noOfIterations = element.length;
            
                                        let distinctSubjects = new Array();
                                        let distinctNonSubject = new Array();
                                        
                
                                        for (let loop = 0; loop < noOfIterations; loop++) {
                                            
                                                
                                                
                                                var partialData = Object.assign({}, element[loop]);
                                                if (element[loop].subjectType == "subject") {
                                                    if (element[loop].hasOwnProperty("subjectType")) {
                                                        delete element[loop].subjectType;
                                                    }
                                                
                                                    distinctSubjects.push(element[loop]);
                                                }
                                                else{
                                                    if (element[loop].hasOwnProperty("subjectType")) {
                                                        delete element[loop].subjectType;
                                                    }
                                                    distinctNonSubject.push(element[loop]);
                                                }

                                        }
                                        
                                        
                                        if(!res.finished)res.status(200).json({   response: {subjects: distinctSubjects}, nonsubjects: distinctNonSubject  })
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getUserNonSubjects = function (req, res) {
    const { classID, classLevelID,userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, classLevelID,userID }, subjectValidations.getUserNonSubjects, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`set @userID = ${utils.OptionalKey(userID)};
                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                    set @classID = ${utils.OptionalKey(classID)};
                    call ${appConfig.getDbConfig().database}.v1_get_user_nonsubjects
                    (@userID,@classLevelID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')

                                        
                                        let noOfIterations = element.length;
                
                                        let distinctSubjects = new Array();
                                        let distinctNonSubject = new Array();
                                        
                
                                        for (let loop = 0; loop < noOfIterations; loop++) {
                                            
                                                
                                                
                                                var partialData = Object.assign({}, element[loop]);
                                                if (element[loop].subjectType == "subject") {
                                                    if (element[loop].hasOwnProperty("subjectType")) {
                                                        delete element[loop].subjectType;
                                                    }
                                                
                                                    distinctSubjects.push(element[loop]);
                                                }
                                                else{
                                                    if (element[loop].hasOwnProperty("subjectType")) {
                                                        delete element[loop].subjectType;
                                                    }
                                                    distinctNonSubject.push(element[loop]);
                                                }
                                            
                                        }                                        
                                        if(!res.finished)res.status(200).json({   response:  {nonsubjects: distinctNonSubject }})
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
        }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getUserSubjects2 = function (req, res) {
    const { classID, classLevelID,userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, classLevelID,userID }, subjectValidations.getUserSubjects2, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`   set @userID = ${utils.OptionalKey(userID)};
                                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                                    set @classID = ${utils.OptionalKey(classID)};
                                    call ${appConfig.getDbConfig().database}.v1_get_user_subjects2
                                    (@userID,@classLevelID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        
                                        
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                    
                    
                                            let innerArray = new Array();
                                            let noOfIterations = element.length;
                    
                                            let distinctSubjects = new Array();
                                            let distinctObjects = new Array();
                    
                                            for (let loop = 0; loop < noOfIterations; loop++) {
                                                if (!distinctSubjects.includes(element[loop].subjectID)) {
                                                    distinctSubjects.push(element[loop].subjectID);
                    
                                                    var partialData = Object.assign({}, element[loop]);
                    
                                                    if (partialData.hasOwnProperty("classSection")) {
                                                        delete partialData.classSection;
                                                    }
                                                    if (partialData.hasOwnProperty("fullname")) {
                                                        delete partialData.fullname;
                                                    }
                                                    if (partialData.hasOwnProperty("userID")) {
                                                        delete partialData.userID;
                                                    }
                                                    if (partialData.hasOwnProperty("subjectType")) {
                                                        delete partialData.subjectType;
                                                    }
                                                    if (partialData.hasOwnProperty("classShift")) {
                    
                                                        delete partialData.classShift;
                                                    }
                                                    if (partialData.hasOwnProperty("teacherSubjectLinkID")) {
                    
                                                        delete partialData.teacherSubjectLinkID;
                                                    }
                    
                                                    if (partialData.hasOwnProperty("fullName")) {
                    
                                                        delete partialData.fullName;
                                                    }
                                                    distinctObjects.push(partialData);
                    
                                                }
                                            }
                                            
                                            for (let j = 0; j < distinctObjects.length; j++) {
                                                let formattedData = new Array();
                                                if (distinctObjects[j].classID ){
                                                for (let i = 0; i < element.length; i++) {
                                                    if (distinctObjects[j].subjectID == element[i].subjectID) {
                                                        
                                                        if (element[i].hasOwnProperty("subjectID")) {
                    
                                                            delete element[i].subjectID;
                                                        }
                                                        if (element[i].hasOwnProperty("subjectName")) {
                    
                                                            delete element[i].subjectName;
                                                        }
                                                        if (element[i].hasOwnProperty("classLevelID")) {
                    
                                                            delete element[i].classLevelID;
                                                        }
                                                        if (element[i].hasOwnProperty("classLevelName")) {
                    
                                                            delete element[i].classLevelName;
                                                        }
                                                        if (element[i].hasOwnProperty("classAlias")) {
                    
                                                            delete element[i].classAlias;
                                                        }
                                                        if (element[i].hasOwnProperty("isActive")) {
                    
                                                            delete element[i].isActive;
                                                        }
                                                        formattedData.push(element[i]);
                                                    }
                                                }
                                                distinctObjects[j].subjectDetails = formattedData;
                                            }}
                    
                                            if(!res.finished)res.status(200).json({
                                                response: distinctObjects
                                            })
    
                                        }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getUserNonSubjectsWithTeacher = function (req, res) {
    const { classID, classLevelID,userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, classLevelID,userID }, subjectValidations.getUserNonSubjectsWithTeacher, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`   set @userID = ${utils.OptionalKey(userID)};
                                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                                    set @classID = ${utils.OptionalKey(classID)};
                                    call ${appConfig.getDbConfig().database}.v1_get_user_nonsubjects_teachers
                                    (@userID,@classLevelID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        let innerArray = new Array();
                                    let noOfIterations = element.length;
            
                                    let distinctnonSubjects = new Array();
                                    let distinctObjects = new Array();
            
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        if (!distinctnonSubjects.includes(element[loop].nonSubjectID)) {
                                            distinctnonSubjects.push(element[loop].nonSubjectID);
            
                                            var partialData = Object.assign({}, element[loop]);
            
                                            if (partialData.hasOwnProperty("classSection")) {
                                                delete partialData.classSection;
                                            }
                                            if (partialData.hasOwnProperty("classShift")) {
            
                                                delete partialData.classShift;
                                            }
                                            if (partialData.hasOwnProperty("nonSubjectLinkID")) {
            
                                                delete partialData.nonSubjectLinkID;
                                            }
            
                                            if (partialData.hasOwnProperty("fullname")) {
            
                                                delete partialData.fullname;
                                            }
                                            distinctObjects.push(partialData);
            
                                        }
                                    }
                                    
                                    for (let j = 0; j < distinctObjects.length; j++) {
                                        let formattedData = new Array();
                                        if (distinctObjects[j].classID ){
                                        for (let i = 0; i < element.length; i++) {
                                            if (distinctObjects[j].nonSubjectID == element[i].nonSubjectID) {
                                                
                                                if (element[i].hasOwnProperty("nonSubjectID")) {
            
                                                    delete element[i].nonSubjectID;
                                                }
                                                if (element[i].hasOwnProperty("nonSubjectName")) {
            
                                                    delete element[i].nonSubjectName;
                                                }
                                                
                                                if (element[i].hasOwnProperty("classLevelID")) {
            
                                                    delete element[i].classLevelID;
                                                }
                                                if (element[i].hasOwnProperty("classLevelName")) {
            
                                                    delete element[i].classLevelName;
                                                }
                                                if (element[i].hasOwnProperty("classAlias")) {
            
                                                    delete element[i].classAlias;
                                                }
                                                if (element[i].hasOwnProperty("isActive")) {
            
                                                    delete element[i].isActive;
                                                }
                                                formattedData.push(element[i]);
                                            }
                                        }
                                        distinctObjects[j].nonSubjectDetails = formattedData;
                                    }}
            
                                    if(!res.finished)res.status(200).json({response: distinctObjects})
    
                                        }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}