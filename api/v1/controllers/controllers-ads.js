const db = require('../../../db'),
    Joi = require("joi"),
    utils = require("../../utils/helper-methods"),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    authValidations = require('../validations/validations-ads'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging = 'ads-controller'
import { getFullDateString } from '../../utils/helper-methods'
import Error from '../response-classes/error'


exports.getAllAds = function (req, res) {
    const { countryID,cityID, areaID, schoolID, branchID, classLevelID, classID } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ countryID,cityID, areaID, schoolID, branchID, classLevelID, classID }, authValidations.getAllAds, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
                let dbquery=` set @classID = ${utils.OptionalKey(classID)};
                                set @classLevelID = ${utils.OptionalKey(classLevelID)};
                                set @branchID = ${utils.OptionalKey(branchID)};
                                set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @areaID = ${utils.OptionalKey(areaID)};
                                set @cityID = ${utils.OptionalKey(cityID)};
                                set @countryID = ${utils.OptionalKey(countryID)};

                                call ${appConfig.getDbConfig().database}.v1_get_ads_all (@countryID,@cityID,@areaID,@schoolID,@branchID,@classLevelID,@classID);`
                console.log(dbquery)
                db.getConnection(function(err, db) {
                    if(!err){
                        try{
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (!_.isEmpty(element)) {
                                                if(!res.finished)res.status(200).json({response: element })
                                            }
                                            else{
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(404).json({ response: errorResponse })
                
                                            }
                                        }
                                    })
                                
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                
                                }
                            }); 
                        }
                        finally{
                            db.release();
                            console.log('released')
                        }
                            
                
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                
                    }
                })
        }
    });   
}

exports.getTopAds = function (req, res) {
    const { countryID,cityID, areaID, schoolID, branchID, classLevelID, classID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ countryID,cityID, areaID, schoolID, branchID, classLevelID, classID}, authValidations.getTopAds, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            let dbquery=` set @classID = ${utils.OptionalKey(classID)};
                        set @classLevelID = ${utils.OptionalKey(classLevelID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @areaID = ${utils.OptionalKey(areaID)};
                        set @cityID = ${utils.OptionalKey(cityID)};
                        set @countryID = ${utils.OptionalKey(countryID)};

                        call ${appConfig.getDbConfig().database}.v1_get_ads_top (@countryID,@cityID,@areaID,@schoolID,@branchID,@classLevelID,@classID);`
            console.log(dbquery)

            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                            
                                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                data.forEach(element => {
                                    if(element.constructor==Array) {
                                        if (!_.isEmpty(element)) {
                                            if(!res.finished)res.status(200).json({response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                        }
                                    }
                                })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
            
                }
            })
        }
    });
}

exports.getDateTest = function (req, res) {
    const { myDate, language, dateFlag } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ myDate, language, dateFlag}, authValidations.getDateTest, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            // if(!res.finished)res.status(200).json({response: utils.formatReadableDate(utils.OptionalKey(myDate),utils.OptionalKey(language),utils.OptionalKey(dateFlag)) })
            // if(!res.finished)res.status(200).json({normalDate: utils.getFullDateString(0), increaseDate: utils.getFullDateString(1)  })
            // if(!res.finished)res.status(200).json({response:utils.HMAC256Encrypt('tFcs174Pgv8=&1000&billref&Headphones&EN&MC9002&DUgJiEQBYmE=&https://www.infinitystudio.pk/secure/postbackNift.php&PKR&20210412224539&20210419224539&T20210412224539&1.1','tFcs174Pgv8=') })
            //if(!res.finished)res.status(200).json({response: utils.createStringAmpersand('') })
            // if(!res.finished)res.status(200).json({response: utils.ConvertDecimalToPaddingZeros('100.59') })
            if(!res.finished)res.status(200).json({response: utils.maskInstrumentString('123400001111') })
        }
    });
}
