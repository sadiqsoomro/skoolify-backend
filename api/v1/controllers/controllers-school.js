const Joi = require('joi'), utils =  require('../../utils/helper-methods'),
    schoolValidations = require('../validations/validations-school'),
    db = require('../../../db'),
    log = require('../../utils/utils-logging'),
    pdf = require('html-pdf'),
    xl = require('excel4node'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    fileNameForLogging = 'school-controller'
import Error from '../response-classes/error'

// ==================================================================

exports.getcalendarDays = function (req, res) {
    const { schoolID, dateFrom, dateTo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, dateFrom, dateTo },schoolValidations.getcalendarDays,function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_calendar WHERE schoolID = '${schoolID}' AND (calendarDate BETWEEN '${dateFrom}' AND '${dateTo}')`;
                    console.log(dbquery)

                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                    });
                } 
                finally {
                    db.release();
                    console.log('released')
                }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })

        }
    })
}

exports.setCalendarDaysArray= function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array,schoolValidations.setCalendarDaysArray,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=``
                        if (array!= undefined){
                            for(let i=0;i<array.length;i++){
                                
                                dbquery += `
                                call ${appConfig.getDbConfig().database}.v1_set_calendar_days
                                (${utils.OptionalKey(array[i].calendarID)},
                                ${utils.OptionalKey(array[i].schoolID)},
                                ${utils.OptionalKey(array[i].branchID)},
                                ${utils.OptionalKey(array[i].calendarDate)},
                                ${utils.OptionalKeyString(array[i].holidayName)},
                                ${utils.OptionalKey(array[i].isHoliday)});`
                            }
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                    if(!error){
    
                                        if(!res.finished)res.status(200).json({ response: "success" })
                                    }
                                    else if (error.code== 'ER_DUP_ENTRY'){
                                        log.doFatalLog(fileNameForLogging, req.url, 'duplication error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(406).json({ response: "unsuccessful", code: error.code, reason : "Holiday already marked for the given date" })
                                    }
                                    else{
                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                           
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
                        }
                       
                        
                    }
 
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
            })
    
        }
    })
}


exports.setCommsRequest = function (req, res) {
    const { parentID,parentUUID, studentID, requestType, requestText } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ parentID,parentUUID, studentID, requestType, requestText },schoolValidations.setCommsRequest,function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    //console.log(utils.OptionalKeyString(requestText))
                    let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_comm_request
                            (${utils.OptionalKey(studentID)}, 
                            ${utils.OptionalKey(parentID)},
                            ${utils.OptionalKey(parentUUID)},
                            ${utils.OptionalKeyString(requestText.replace(/\n/g,"<BR>"))},
                            ${utils.OptionalKeyString(requestType)})`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data updated in db')
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setCommsIsRead = function (req, res) {
    const { communicationListID,status} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ communicationListID,status },schoolValidations.setCommsIsRead,
        function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=  `call ${appConfig.getDbConfig().database}.v1_set_comms_status
                                    (${utils.OptionalKey(communicationListID)},
                                     ${utils.OptionalKey(status)})`
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data update in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            }
        })
}

exports.getCommsCount = function (req, res) {
    const { schoolID, branchID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID },schoolValidations.getCommsCount,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`   SELECT * from v1_get_comms_count where schoolID='${schoolID}' and branchID = '${branchID}'`

                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    if(!res.finished)res.status(200).json({ response : data })
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                        });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            }
        })
}



exports.getSchoolLogo = function (req, res) {
    const { domain} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ domain},schoolValidations.getSchoolLogo,function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = ` Select * from v1_get_school_logo Where domain='${domain}'   `;
                console.log(dbquery)
                try {
                    db.query(dbquery,function (error, data, fields) {
                        if(!error){
                            
                            if (_.isEmpty(data)) {
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(404).json({ response: errorResponse })
                            } else {
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                
                                if(!res.finished)res.status(200).json({ response: data})
                            }
    
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                        }
                    });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
                

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
    })
}


exports.getCommRequest = function (req, res) {
    const { schoolID, branchID,classID,classLevelID,studentID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID, classID, classLevelID,studentID }, schoolValidations.getCommRequest,function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = ` Select * from v1_get_commrequest where schoolID='${schoolID}'   `;
                    
                    if(branchID)
                    dbquery += ` and branchID = '${branchID}' `;
    
                    if(studentID)
                    dbquery += ` and studentID = '${studentID}' `;
                    
                    if(classID)
                    dbquery += ` and classID = '${classID}' `;
                    
    
                    if(classLevelID)
                    dbquery += ` and classLevelID = '${classLevelID}' `;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
    })
}

exports.getSchools = function (req, res) {
    const { schoolID,domain } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,domain }, schoolValidations.getSchools, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * From v1_get_schools  `;
                    if (schoolID && !domain)
                        dbquery += `where schoolID = '${schoolID}'`
                    if (domain && !schoolID)
                        dbquery += `where domain = '${domain}'`
                    if (schoolID && domain)
                        dbquery += `where schoolID = '${schoolID}' and domain = '${domain}'`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
    })
}

exports.setSchools = function (req, res) {
    const { schoolID, schoolName, schoolLogo, domain,legalEntityName, legalEntityAddress, legalEntityPerson, legalEntityContactNo, websiteURL, owner, selfRegistered, isActive, schoolParameters } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, schoolName, schoolLogo, domain,legalEntityName, legalEntityAddress, legalEntityPerson, legalEntityContactNo, websiteURL, owner, selfRegistered, isActive, schoolParameters }, schoolValidations.setSchools,function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = "";
                    
                    dbquery += `set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @schoolName = ${utils.OptionalKeyString(schoolName)};
                                set @schoolLogo = ${utils.OptionalKey(schoolLogo)};
                                set @domain = ${utils.OptionalKey(domain)};
                                set @legalEntityAddress = ${utils.OptionalKeyString(legalEntityAddress)};
                                set @legalEntityPerson = ${utils.OptionalKeyString(legalEntityPerson)};
                                set @legalEntityContactNo = ${utils.OptionalKeyString(legalEntityContactNo)};
                                set @website = ${utils.OptionalKey(websiteURL)};
                                set @owner = ${utils.OptionalKeyString(owner)};
                                set @selfRegistered = ${utils.OptionalKeyString(selfRegistered)};
                                set @isActive = ${utils.OptionalKey(isActive)};
                                set @legalEntityName = ${utils.OptionalKeyString(legalEntityName)};
                                call ${appConfig.getDbConfig().database}.v1_set_school
                                (@schoolID, @schoolName, @schoolLogo,@domain,@legalEntityName,@legalEntityAddress,@legalEntityPerson,@legalEntityContactNo,@website,@owner,@selfRegistered,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                    if(element.constructor==Array) {
                                        if (_.isEmpty(element)) {
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(200).json({ response: errorResponse })
                                        } else {
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            let var_schoolID=element[0].schoolID
                                            if (!schoolParameters || _.isEmpty(schoolParameters)) {
                                                if(!res.finished)res.status(200).json({response: "success"})
                                                
                                            }
                                            else{
                                            
                                            let dbquery=  ``
                                            
                                            dbquery += `set @schoolID = ${var_schoolID};
                                                        set @studentAttendanceGracePeriod = ${utils.OptionalKey(schoolParameters.studentAttendanceGracePeriod)};
                                                        set @staffAttendanceGracePeriod = ${utils.OptionalKey(schoolParameters.staffAttendanceGracePeriod)};
                                                        set @weekStartDay = ${utils.OptionalKey(schoolParameters.weekStartDay)};
                                                        set @feePaymentAllowed = ${utils.OptionalKey(schoolParameters.feePaymentAllowed)};
                                                        set @showAccNoOnVoucher = ${utils.OptionalKey(schoolParameters.showAccNoOnVoucher)};
                                                        set @appHomework = ${utils.OptionalKey(schoolParameters.appHomework)};
                                                        set @appTimetable = ${utils.OptionalKey(schoolParameters.appTimetable)};
                                                        set @appExamQuiz = ${utils.OptionalKey(schoolParameters.appExamQuiz)};
                                                        set @appfeeVoucher = ${utils.OptionalKey(schoolParameters.appfeeVoucher)};
                                                        set @appAttendance = ${utils.OptionalKey(schoolParameters.appAttendance)};
                                                        set @appMessage = ${utils.OptionalKey(schoolParameters.appMessage)};
                                                        set @appCommunicate = ${utils.OptionalKey(schoolParameters.appCommunicate)};
                                                        set @appEvents = ${utils.OptionalKey(schoolParameters.appEvents)};

                                                        call ${appConfig.getDbConfig().database}.v1_set_school_parameters
                                                            (@schoolID,@studentAttendanceGracePeriod,@staffAttendanceGracePeriod,@weekStartDay,@feePaymentAllowed,@showAccNoOnVoucher,
                                                                @appHomework,@appTimetable,@appExamQuiz,@appfeeVoucher,@appAttendance,@appMessage,@appCommunicate,@appEvents )`
                                                console.log(dbquery)
                                                db.query(dbquery,function (error, data, fields) {
                                                        if(!error){
                                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data update in db')
                                                            if(!res.finished)res.status(200).json({ response: "success" })
                                                            
                                                        }
                                                        else{
                                                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                            if(!res.finished)res.status(500).json({ response: errorResponse })
                                                        }
                                                    });
                                                    

                                            }
                                                
                                        }
                                        
                                        
                                                
                                                
                                    }
                                    
                                })

                                
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setSchoolBranches = function (req, res) {
    const { schoolID,branchID,branchCode,branchName,address1,address2,address3,principleName,email,
        contact1,contact2,contact3,gpsCordinates,bankName,bankBranch,bankAccNo,areaID,cityID,countryID,isActive } = req.body

        
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({schoolID,branchID,branchCode,branchName,address1,address2,address3,principleName,email,
        contact1,contact2,contact3,gpsCordinates,bankName,bankBranch,bankAccNo,areaID,cityID,countryID,isActive}, schoolValidations.setSchoolBranches,
        function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery= `
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchCode = ${utils.OptionalKey(branchCode)};
                        set @branchName = ${utils.OptionalKeyString(branchName)};
                        set @address1 = ${utils.OptionalKeyString(address1)};
                        set @address2 = ${utils.OptionalKeyString(address2)};
                        set @address3 = ${utils.OptionalKeyString(address3)};
                        set @principleName = ${utils.OptionalKeyString(principleName)};
                        set @email = ${utils.OptionalKey(email)};
                        set @contact1 = ${utils.OptionalKeyString(contact1)};
                        set @contact2 = ${utils.OptionalKeyString(contact2)};
                        set @contact3 = ${utils.OptionalKeyString(contact3)};
                        set @gpsCordinates = ${utils.OptionalKey(gpsCordinates)};
                        set @bankName = ${utils.OptionalKeyString(bankName)};
                        set @bankBranch = ${utils.OptionalKeyString(bankBranch)};
                        set @bankAccNo = ${utils.OptionalKeyString(bankAccNo)};
                        set @areaID = ${utils.OptionalKey(areaID)};
                        set @cityID = ${utils.OptionalKey(cityID)};
                        set @isActive = ${utils.OptionalKey(isActive)};
                        set @countryID = ${utils.OptionalKey(countryID)};
                        call ${appConfig.getDbConfig().database}.v1_set_branches
                        (@schoolID, @branchID, @branchCode,@branchName,@address1,@address2,@address3,@principleName
                        ,@email, @contact1, @contact2,@contact3,@gpsCordinates,@bankName,@bankBranch,@bankAccNo,@areaID,@cityID,@countryID,@isActive);`
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data update in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        

            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            }
        })
}

// exports.setSchoolBranchesArray = function (req, res) {
//     const { array } = req.body
//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate(array, schoolValidations.setSchoolBranchesArray,
//         function (err, value) {
//             if (err) {
//                 log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//                 if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//             } else {
//                 let procedure ="";
//              for(let i=0;i<array.length;i++)
//              {
//                 procedure = procedure +  ` call ${appConfig.getDbConfig().database}.v1_set_branches
//                  (${utils.OptionalKey(array[i].schoolID)}, ${utils.OptionalKey(array[i].branchID)}, 
//                  ${utils.OptionalKey(array[i].branchCode)}, ${utils.OptionalKey(array[i].branchName)},
//                  ${utils.OptionalKey(array[i].address1)},${utils.OptionalKey(array[i].address2)},
//                  ${utils.OptionalKey(array[i].address3)},${utils.OptionalKey(array[i].principleName)},
//                  ${utils.OptionalKey(array[i].email)}, ${utils.OptionalKey(array[i].contact1)}, 
//                  ${utils.OptionalKey(array[i].contact2)},${utils.OptionalKey(array[i].contact3)},
//                  ${utils.OptionalKey(array[i].gpsCordinates)},${utils.OptionalKey(array[i].areaID)},
//                  ${utils.OptionalKey(array[i].cityID)},${utils.OptionalKey(array[i].countryID)},
//                  ${utils.OptionalKey(array[i].isActive)});  `  
               
//              }
//                 db.query(procedure)
//                     .then(data => {
//                         if(!res.finished)res.status(200).json({ response: "success" })
//                     })
//                     .catch(err => {
//                         log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', err)
//                         const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                         if(!res.finished)res.status(500).json({ response: errorResponse })
//                     })
//             }
//         })
// }

exports.getSchoolBranches = function (req, res) {
    const { schoolID, branchID, areaID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID , areaID}, schoolValidations.getSchoolBranches, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_school_branches WHERE schoolID='${schoolID}' `;
                    if (branchID)
                        dbquery += `AND branchID = '${branchID}'`;
                    if (areaID)
                        dbquery += ` AND areaID = '${areaID}'`;
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        }); 
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveBranches = function (req, res) {
    const { schoolID, branchID, areaID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID , areaID}, schoolValidations.getActiveBranches, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_school_branches WHERE isActive = 1 and schoolID='${schoolID}' `;
                    if (branchID)
                        dbquery += `AND branchID = '${branchID}'`;
                    if (areaID)
                        dbquery += ` AND areaID = '${areaID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getFeeTypes = function (req, res) {
    const { schoolID,branchID,feetypeID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID,feetypeID }, schoolValidations.getFeeTypes, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_fee_types WHERE schoolID='${schoolID}' `;
                    if(branchID)
                        dbquery += ` and branchID = '${branchID}'`;
                    if(feetypeID)
                        dbquery += ` and feetypeID = '${feetypeID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveFeeTypes = function (req, res) {
    const { schoolID,feetypeID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,feetypeID }, schoolValidations.getActiveFeeTypes, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_fee_types WHERE isActive =1 and schoolID='${schoolID}' `;
                    if(feetypeID)
                        dbquery += ` and feetypeID = '${feetypeID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getClassLevels = function (req, res) {
    const { schoolID,branchID, classLevelID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,classLevelID }, schoolValidations.getClassLevels, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_class_levels WHERE schoolID='${schoolID}' `;
                    if(branchID)
                        dbquery += ` and branchID = '${branchID}'`             
                    if(classLevelID)
                        dbquery +=  ` and classLevelID IN (${classLevelID})` 
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveClassLevels = function (req, res) {
    const { schoolID,branchID, classLevelID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,classLevelID }, schoolValidations.getActiveClassLevels, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_class_levels WHERE isActive=1 and schoolID='${schoolID}' `;
                    if(branchID)
                        dbquery += ` and branchID = '${branchID}'`             
                    if(classLevelID)
                        dbquery +=  ` and classLevelID IN (${classLevelID})` 
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}



exports.getClassStudents = function (req, res) {
    const { classID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID }, schoolValidations.getClassStudents, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery= `SELECT *  From v1_get_class_students  WHERE classID IN (${classID}) `;
                console.log(dbquery)
                try {
                  db.query(dbquery,function (error, data, fields) {
                        if(!error){
                            
                            if (_.isEmpty(data)) {
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(200).json({ response: errorResponse })
                            } else {
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: data})
                            }

                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                        }
                    });  
                } 
                finally {
                    db.release();
                    console.log('released')
                }
                

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getClassActiveStudents = function (req, res) {
    const { classID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID }, schoolValidations.getClassActiveStudents, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
            if(!err){
            let dbquery= `SELECT *  From v1_get_class_students  WHERE isActive=1 and classID IN (${classID}) `;
            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                        
                        if (_.isEmpty(data)) {
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(200).json({ response: errorResponse })
                        } else {
                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            if(!res.finished)res.status(200).json({response: data})
                        }

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                }); 
            } 
            finally {
                db.release();
                console.log('released')
            }
            
    
            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
    
            }
            })
        }
    })
}

exports.getAllClassStudents = function (req, res) {
    const { classID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID }, schoolValidations.getAllClassStudents, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `SELECT * from v1_get_all_class_students WHERE classID IN (${classID}) `;
                console.log(dbquery)
                    
                    try {
                       db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        }); 
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setClassLevels = function (req, res) {
    const { schoolID,branchID,classLevelID,classLevelName,classAlias,isSubjectLevelAttendance,assessmentOnly,isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({schoolID,branchID,classLevelID,classLevelName,classAlias,isSubjectLevelAttendance,assessmentOnly,isActive }, schoolValidations.setClassLevels, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`   set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @branchID = ${utils.OptionalKey(branchID)};
                                set @classLevelID = ${utils.OptionalKey(classLevelID)};
                                set @classLevelName = ${utils.OptionalKeyString(classLevelName)};
                                set @isSubjectLevelAttendance = ${utils.OptionalKey(isSubjectLevelAttendance)};
                                set @classAlias = ${utils.OptionalKeyString(classAlias)};
                                set @assessmentOnly = ${utils.OptionalKey(assessmentOnly)};
                                set @isActive = ${utils.OptionalKey(isActive)};
                                call ${appConfig.getDbConfig().database}.v1_set_class_levels
                                (@schoolID,@branchID, @classLevelID, @classLevelName,@classAlias,@isSubjectLevelAttendance,@assessmentOnly,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                if(!res.finished)res.status(200).json({response: "success"})
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getClasses = function (req, res) {
    const { schoolID, branchID,classLevelID,classTeacherID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID,classLevelID,classTeacherID }, schoolValidations.getClasses, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_classes WHERE schoolID = '${schoolID}' and branchID='${branchID}' `;

                    if(classTeacherID)
                        dbquery+= ` and classTeacherID = '${classTeacherID}'`;    
                    if(classLevelID)
                        dbquery += ` and classLevelID IN (${classLevelID})`;  

                    console.log(dbquery)
                    try {
                       db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        }); 
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveClasses = function (req, res) {
    const { schoolID, branchID,classLevelID,classTeacherID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID,classLevelID,classTeacherID }, schoolValidations.getActiveClasses, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_classes WHERE isActive=1 and schoolID = '${schoolID}' and branchID='${branchID}' `;

                    if(classTeacherID)
                        dbquery+= ` and classTeacherID = '${classTeacherID}'`;    
                    if(classLevelID)
                        dbquery += ` and classLevelID IN (${classLevelID})`;  

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setClasses = function (req, res) {
    const { isActive, classTeacherID, classShift, classSection, classLevelID, branchID, classID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ isActive, classTeacherID, classShift, classSection, classLevelID, branchID, classID }, schoolValidations.setClasses, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`set @classID = ${utils.OptionalKey(classID)};
                            set @branchID = ${utils.OptionalKey(branchID)};
                            set @classLevelID = ${utils.OptionalKey(classLevelID)};
                            set @classSection = ${utils.OptionalKey(classSection)};
                            set @classShift = ${utils.OptionalKey(classShift)};
                            set @classTeacherID = ${utils.OptionalKey(classTeacherID)};
                            set @isActive = ${utils.OptionalKey(isActive)};
                            call ${appConfig.getDbConfig().database}.v1_set_classes
                            (@classID,@branchID,@classLevelID,@classSection,@classShift,@classTeacherID,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: "success"})
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}



exports.getUserClasses = function (req, res) {
    const { userID, classID,classLevelID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ userID, classID,classLevelID }, schoolValidations.getUserClasses, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`set @classID = ${utils.OptionalKey(classID)};
                                set @classLevelID = ${utils.OptionalKey(classLevelID)};
                                set @userID = ${utils.OptionalKey(userID)};
                                call ${appConfig.getDbConfig().database}.v1_get_user_classes
                                (@userID,@classLevelID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                        if (_.isEmpty(element)) {
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(200).json({ response: errorResponse })
                                        } else {
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({response: element})
                                        }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getUserClassLevels = function (req, res) {
    const { userID, classID,branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ userID, classID, branchID }, schoolValidations.getUserClassLevels, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`set @branchID = ${utils.OptionalKey(branchID)};
                            set @userID = ${utils.OptionalKey(userID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            call ${appConfig.getDbConfig().database}.v1_get_user_class_levels
                            (@branchID, @userID,@classID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element})
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getBillingPeriod = function (req, res) {
    const { schoolID,branchID, billingPeriodID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, billingPeriodID }, schoolValidations.getBillingPeriod, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_billing_periods  WHERE schoolID='${schoolID}' and branchID='${branchID}' ` ;
                    if(billingPeriodID)
                    dbquery +=` and billingPeriodID = '${billingPeriodID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveBillingPeriod = function (req, res) {
    const { schoolID,branchID, billingPeriodID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, billingPeriodID }, schoolValidations.getActiveBillingPeriod, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_billing_periods WHERE isActive=1 and schoolID='${schoolID}' and branchID='${branchID}' ` ;
                    if(billingPeriodID)
                    dbquery +=` and billingPeriodID = '${billingPeriodID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setBillingPeriod = function(req, res) {
    const { billingPeriodID, schoolID, branchID, periodName, isActive } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({billingPeriodID, schoolID, branchID, periodName, isActive }, schoolValidations.setBillingPeriod,
      function(err, value) {
        if (err) {
          log.doErrorLog(   fileNameForLogging,   req.url,  "required params validation failed."  );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `
                    set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};    
                    set @schoolID = ${utils.OptionalKey(schoolID)};
                    set @branchID = ${utils.OptionalKey(branchID)};
                    set @periodName = ${utils.OptionalKey(periodName)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_billing_period
                    (@billingPeriodID, @schoolID, @branchID,@periodName,@isActive);`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(!res.finished)res.status(200).json({ response: "success" });
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };

  exports.deleteBillingPeriod = function(req, res) {
    const { billingPeriodID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({billingPeriodID }, schoolValidations.deleteBillingPeriod,
      function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `
                        set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};    
                        call ${appConfig.getDbConfig().database}.v1_delete_billing_period
                        (@billingPeriodID);`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(!res.finished)res.status(200).json({ response: "success" });
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };

  

  exports.getStudentLevelFee = function (req, res) {
    const { feeTypeID, classID, studentID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { feeTypeID, classID, studentID }, schoolValidations.getStudentLevelFee,
        function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery =`SELECT * FROM v1_get_student_level_fee 
                        WHERE (feeTypeID = '${feeTypeID}' or feeTypeID is NULL) 
                        AND classID = '${classID}' ` 
                        if(studentID)
                            dbquery += ` and studentID = '${studentID}'`;
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            
                                    if(!res.finished)res.status(200).json({response: data })
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        

            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            }
        })
}




exports.getReportRegistrations = function (req, res) {
    const { branchID, mobileNumber,recordSequenceNo,requiredCount } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID, mobileNumber, recordSequenceNo, requiredCount}, schoolValidations.getReportRegistrations, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                    set @branchID = ${utils.OptionalKey(branchID)};
                    set @mobileNumber = ${utils.OptionalKey(mobileNumber)};
                    set @recordSequenceNo = ${utils.OptionalKey(recordSequenceNo)};
                    set @requiredCount = ${utils.OptionalKey(requiredCount)};       
                    call ${appConfig.getDbConfig().database}.v1_get_report_registered_parents
                    (@branchID,@mobileNumber,@recordSequenceNo,@requiredCount);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        let innerArray = new Array();
                                        let noOfIterations = element.length;
                                        //console.log(noOfIterations);
                                        let overallCount = {};
                                        for(let loop =0; loop < noOfIterations;loop++)
                                        {
                                            
                                  
                                            if(element[loop].hasOwnProperty("totalParentCount"))
                                            {
                                                overallCount["totalParentCount"] = element[loop].totalParentCount;
                                                //if(loop==0)
                                                delete element[loop].totalParentCount;
                                            }
                                            if(element[loop].hasOwnProperty("registeredParentCount"))
                                            {
                                                overallCount["registeredParentCount"] = element[loop].registeredParentCount;
                                                //if(loop==0)
                                                delete element[loop].registeredParentCount;
                                            }
                    
                                            if(element[loop].hasOwnProperty("unregisteredParentCount"))
                                            {
                                                overallCount["unregisteredParentCount"] = element[loop].unregisteredParentCount;
                                                //if(loop==0)
                                                delete element[loop].unregisteredParentCount;
                                            }
                                            if(element[loop].hasOwnProperty("ActiveStudents"))
                                            {
                                                overallCount["ActiveStudents"] = element[loop].ActiveStudents;
                                                //if(loop==0)
                                                delete element[loop].ActiveStudents;
                                            }
                                            if(element[loop].hasOwnProperty("NonActiveStudents"))
                                            {
                                                overallCount["NonActiveStudents"] = element[loop].NonActiveStudents;
                                                //if(loop==0)
                                                delete element[loop].NonActiveStudents;
                                            }
                                            
                                            if(element[loop].hasOwnProperty("UniqueRegisteredStudents"))
                                            {
                                                overallCount["UniqueRegisteredStudents"] = element[loop].UniqueRegisteredStudents;
                                                //if(loop==0)
                                                delete element[loop].UniqueRegisteredStudents;
                                            }
                    
                                            
                                        
                                    }
                                        if(!res.finished)res.status(200).json({response: element,overallCount })
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
            
        }
    })
}

exports.getReportDatewiseRegistrations = function (req, res) {
    const { schoolID, branchID, fromDate, toDate } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID,fromDate, toDate }, schoolValidations.getReportDatewiseRegistrations, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                                set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @branchID = ${utils.OptionalKey(branchID)};
                                set @fromDate = ${utils.OptionalKey(fromDate)};
                                set @toDate =   ${utils.OptionalKey(toDate)};
                                
                                call ${appConfig.getDbConfig().database}.v1_get_report_datewise_registration
                                (@schoolID,@branchID,@fromDate,@toDate);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        let innerArray = new Array();
                                        let noOfIterations = element.length;
                                        console.log(noOfIterations);
                                        let overallCount = {};
                                        for(let loop =0; loop < noOfIterations;loop++)
                                        {
                                            
                                  
                                            if(element[loop].hasOwnProperty("totalCount"))
                                            {
                                                overallCount["totalCount"] = element[loop].totalCount;
                                                //if(loop==0)
                                                delete element[loop].totalCount;
                                            }
                                            if(element[loop].hasOwnProperty("registeredCount"))
                                            {
                                                overallCount["registeredCount"] = element[loop].registeredCount;
                                                //if(loop==0)
                                                delete element[loop].registeredCount;
                                            }
                
                                            if(element[loop].hasOwnProperty("unregisteredCount"))
                                            {
                                                overallCount["unregisteredCount"] = element[loop].unregisteredCount;
                                                //if(loop==0)
                                                delete element[loop].unregisteredCount;
                                            }
                                            if(element[loop].hasOwnProperty("ActiveStudents"))
                                            {
                                                overallCount["ActiveStudents"] = element[loop].ActiveStudents;
                                                //if(loop==0)
                                                delete element[loop].ActiveStudents;
                                            }
                                            if(element[loop].hasOwnProperty("NonActiveStudents"))
                                            {
                                                overallCount["NonActiveStudents"] = element[loop].NonActiveStudents;
                                                delete element[loop].NonActiveStudents;
                                            }
                                        
                                        
                                
                                    }
                                        
                                        if(!res.finished)res.status(200).json({response: element,overallCount })
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteSchool = function (req, res) {
    const { schoolID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID }, schoolValidations.deleteSchool, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_school(
                                    ${utils.OptionalKey(schoolID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteSchoolBranch = function (req, res) {
    const { branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID }, schoolValidations.deleteSchoolBranch, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_branch(${utils.OptionalKey(branchID)});`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getCalendarEvents = function (req, res) {
    const { schoolID,branchID, classLevelID, classID,  dateFrom, dateTo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { schoolID, branchID, classLevelID, classID, dateFrom, dateTo }, schoolValidations.getCalendarEvents,
        function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `
                        set @schoolID = ${utils.OptionalKey(schoolID)}; 
                        set @branchID = ${utils.OptionalKey(branchID)}; 
                        set @classLevelID = ${utils.OptionalKey(classLevelID)}; 
                        set @classID = ${utils.OptionalKey(classID)}; 
                        set @dateFrom = ${utils.OptionalKey(dateFrom)}; 
                        set @dateTo = ${utils.OptionalKey(dateTo)};    
                        call ${appConfig.getDbConfig().database}.v1_get_calendar_academics
                            (@schoolID,@branchID,@classLevelID,@classID,@dateFrom,@dateTo);`;
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    data.forEach(element => {
                                    
                                        if(element.constructor==Array) {
                                             if (_.isEmpty(element)) {
                                                 log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                 const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                 if(!res.finished)res.status(404).json({ response: errorResponse })
                                             } else {
                                                 log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                 if(!res.finished)res.status(200).json({response: element})
                                             }
                                        }
                                        
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        

            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            
            
            
            
            }
        })
}

exports.getActiveSchools = function (req, res) {
    const { schoolID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID }, schoolValidations.getActiveSchools, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_schools_active where isActive=1 `;
                    if (schoolID)
                        dbquery += `and schoolID = '${schoolID}' `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
    })
}

exports.deleteCalendarDays = function (req, res) {
    const { calendarID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ calendarID }, schoolValidations.deleteCalendarDays, function ( err, value ) {
      if (!err) {
        db.getConnection(function(err, db) {
          console.log('getConnection executed')
          if(!err){
            let dbquery=`set @calendarID = ${utils.OptionalKey(calendarID)} ;
                          call ${appConfig.getDbConfig().database}.v1_delete_calendar (@calendarID);`
            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                
                  if(!error){
                                  
                    if (_.isEmpty(data)) {
                        
                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                        if(!res.finished)res.status(200).json({ response: errorResponse })
                    } else {
                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                        if(!res.finished)res.status(200).json({response: "success"})
                    }
  
                }
                else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }

            

      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      
      } 
      else {
        log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      }
    });
  };

  exports.getCalendarHolidays = function (req, res) {
    const { schoolID, dateFrom, dateTo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, dateFrom, dateTo },schoolValidations.getCalendarHolidays,function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_calendar WHERE isHoliday=1 and schoolID = '${schoolID}' AND (calendarDate BETWEEN '${dateFrom}' AND '${dateTo}')`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        
        
        
        }
    })
}
  
exports.deleteClass = function(req, res) {
    const { classID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({classID }, schoolValidations.deleteClass, function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `
                        set @classID = ${utils.OptionalKey(classID)};    
                        call ${appConfig.getDbConfig().database}.v1_delete_class
                        (@classID);`;
                console.log(dbquery)
                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                } 
                finally {
                    db.release();
                    console.log('released')
                }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };

  exports.deleteClassLevel = function(req, res) {
    const { classLevelID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({classLevelID }, schoolValidations.deleteClassLevel, function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `
                        set @classLevelID = ${utils.OptionalKey(classLevelID)};    
                        call ${appConfig.getDbConfig().database}.v1_delete_class_level
                        (@classLevelID);`;
                

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };

  exports.setClassRoom = function (req, res) {
    const { roomID,branchID,roomName,roomDesc,isClassable,deviceIdentifier,isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ roomID,branchID,roomName,roomDesc,isClassable,deviceIdentifier,isActive }, schoolValidations.setClassRoom, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                
                    let dbquery=`set @roomID = ${utils.OptionalKey(roomID)};
                            set @branchID = ${utils.OptionalKey(branchID)};
                            set @roomName = ${utils.OptionalKeyString(roomName)};
                            set @roomDesc = ${utils.OptionalKeyString(roomDesc)};
                            set @isClassable = ${utils.OptionalKey(isClassable)};
                            set @deviceIdentifier = ${utils.OptionalKeyString(deviceIdentifier)};
                            set @isActive = ${utils.OptionalKey(isActive)};
                            call ${appConfig.getDbConfig().database}.v1_set_class_room
                            (@roomID,@branchID,@roomName,@roomDesc,@isClassable,@deviceIdentifier,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({response: "success"})
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getClassRoom = function (req, res) {
    const { branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID}, schoolValidations.getClassRoom, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_class_room WHERE branchID='${branchID}' `;

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveClassRoom = function (req, res) {
    const { branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID}, schoolValidations.getActiveClassRoom, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_class_room_active WHERE branchID='${branchID}' `;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteClassRoom = function(req, res) {
    const { roomID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({roomID }, schoolValidations.deleteClassRoom, function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `
                        set @roomID = ${utils.OptionalKey(roomID)};    
                        call ${appConfig.getDbConfig().database}.v1_delete_class_room
                        (@roomID);`;
                console.log(dbquery)
                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
                

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };


  exports.getSchoolContract = function (req, res) {
    const { userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ userID }, schoolValidations.getSchoolContract, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`set @userID = ${utils.OptionalKey(userID)};
                                call ${appConfig.getDbConfig().database}.v1_get_school_contract
                                (@userID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                    
                                   if(element.constructor==Array) {
                                        if (_.isEmpty(element)) {
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
                                        } else {
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({response: element})
                                        }
                                   }
                                   
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setContractAcceptance = function (req, res) {
    const { acceptanceID,schoolID, userID, ipAddress, contractID,contractText, chargesID,chargesText, collectionID , collectionText} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ acceptanceID,schoolID, userID, ipAddress, contractID,contractText, chargesID,chargesText, collectionID , collectionText }, schoolValidations.setContractAcceptance, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {

                if(!err){
                    let dbquery=`
                    set @acceptanceID = ${utils.OptionalKey(acceptanceID)};
                    set @schoolID = ${utils.OptionalKey(schoolID)};
                    set @userID = ${utils.OptionalKey(userID)};
                    set @ipAddress = ${utils.OptionalKey(ipAddress)};
                    set @contractID = ${utils.OptionalKey(contractID)};
                    set @contractText = ${utils.OptionalKey(contractText)};
                    set @chargesID = ${utils.OptionalKey(chargesID)};
                    set @chargesText = ${utils.OptionalKey(chargesText)};
                    set @collectionID = ${utils.OptionalKey(collectionID)};
                    set @collectionText = ${utils.OptionalKey(collectionText)};
                    call ${appConfig.getDbConfig().database}.v1_set_contract_acceptance
                                (@acceptanceID,@schoolID, @userID, @ipAddress, @contractID,@contractText, @chargesID,@chargesText, @collectionID,@collectionText);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data update in db')
                                    //if(!res.finished)res.status(200).json({response: "success"})
                                    
                                    var html  = utils.DecryptBase64(contractText)
                                    html = html + "<div style=\"page-break-before: always; width: 100%;text-align: center\">"
                                    html = html + utils.OptionalKey(utils.DecryptBase64(chargesText))
                                    html = html + "<div style=\"page-break-before: always; width: 100%;text-align: center\">"
                                    html = html + utils.OptionalKey(utils.DecryptBase64(collectionText))
                                    var options = { 
                                        format: 'Letter',
                                        "orientation": "portrait",
                                        "border": {
                                            "top": "1in",            // default is 0, units: mm, cm, in, px
                                            "right": "0.25in",
                                            "bottom": "1in",
                                            "left": "0.25in"
                                          },
                                          
                                         
                                    };

                                    pdf.create(html,options).toStream(function(err, stream) {
                                        if (err) {
                                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                            if(!res.finished)res.status(500).json({ response: errorResponse })
                                        } else {
                                            res.set('Content-type', 'application/pdf');
                                            res.set('Content-disposition', 'attachment; filename=Skoolify-Contract.pdf' );
                                            stream.pipe(res)
                                        }
                                    });
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteContract = function(req, res) {
    const { contractID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({contractID }, schoolValidations.deleteContract, function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `
                        set @contractID = ${utils.OptionalKey(contractID)};    
                        call ${appConfig.getDbConfig().database}.v1_delete_contract
                        (@contractID);`;
                

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };

  exports.getUserCommunications = function (req, res) {
    const { branchID, classID, userID,classLevelID, studentID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID, classID, userID,classLevelID, studentID }, schoolValidations.getUserCommunications, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                    set @branchID = ${utils.OptionalKey(branchID)};
                    set @classID = ${utils.OptionalKey(classID)};
                    set @classLevelID = ${utils.OptionalKey(classLevelID)};
                    set @userID = ${utils.OptionalKey(userID)}; 
                    set @studentID = ${utils.OptionalKey(studentID)};                  
                    call ${appConfig.getDbConfig().database}.v1_get_communication_users
                    (@branchID,@classLevelID,@classID,@userID,@studentID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element})
                                   
                                    }
                                   }
                                });
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getBranchClasses = function (req, res) {
    const {branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID }, schoolValidations.getBranchClasses, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_branch_classes WHERE branchID='${branchID}' `;

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getAcademicEvents = function (req, res) {
    const { branchID, classLevelID, classID , dateFrom, dateTo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { branchID, classLevelID, classID , dateFrom, dateTo },schoolValidations.getAcademicEvents, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @branchID = ${utils.OptionalKey(branchID)};
                            set @classLevelID = ${utils.OptionalKey(classLevelID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            set @dateFrom = ${utils.OptionalKey(dateFrom)}; 
                            set @dateTo = ${utils.OptionalKey(dateTo)};                  
                            call ${appConfig.getDbConfig().database}.v1_get_academic_events
                            (@branchID,@classLevelID,@classID,@dateFrom,@dateTo);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (_.isEmpty(element)) {
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(200).json({ response: errorResponse })
                                            } else {
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({response: element})
                                            }
                                        }
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        

            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            
            
            
            
            }
        })
}

exports.setAcademicEventsArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate( array ,schoolValidations.setAcademicEventsArray,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                if(array!=undefined){
                    let dbquery=''
                    db.getConnection(function(err, db) {
                        if(!err){
                            for (let i = 0; i < array.length; i++) {
                                
                                dbquery +=`call ${appConfig.getDbConfig().database}.v1_set_academic_event
                                    (${utils.OptionalKey(array[i].eventID)},
                                    ${utils.OptionalKey(array[i].classLevelID)}, 
                                    ${utils.OptionalKey(array[i].classID)},
                                    ${utils.OptionalKey(array[i].eventDate)}, 
                                    ${utils.OptionalKeyString(array[i].eventName)},
                                    ${utils.OptionalKey(array[i].userID)} );`
                        
                            }
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                    if(!error){
                                    
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data updated in db')
                                        if(!res.finished)res.status(200).json({ response: "success" })
                                    }
                                    else if (error.code=='ER_DUP_ENTRY'){
                                        log.doInfoLog(fileNameForLogging, req.url, 'failed to insert record')
                                        if(!res.finished)res.status(406).json({ response: "unsuccessful", code: error.code, reason : "Event already added with these dates and class" })
                                    }
                                    else{
                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                            

                
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                
                        }
                    })
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                    if(!res.finished)res.status(500).json({ response: "'array not found'" })
                }
            }
        })
}


exports.getCalendarList = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { schoolID, branchID },schoolValidations.getCalendarList,
        function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `SELECT * FROM v1_get_calendar_list WHERE schoolID = '${schoolID}' AND branchID ='${branchID}'`;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        

            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            
            
            
            
            }
        })
}

exports.deleteAcademicEvents = function(req, res) {
    const { eventID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({eventID }, schoolValidations.deleteAcademicEvents, function(err, value) {
        if (err) {
          log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery = `
                        set @eventID = ${utils.OptionalKey(eventID)};    
                        call ${appConfig.getDbConfig().database}.v1_delete_academic_events
                        (@eventID);`;
                

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: error.code, reason : error.message })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
      }
    );
  };



//   exports.setContractChargesArray = function (req, res) {
//     const { array } = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate( array ,schoolValidations.setContractChargesArray,function (err, value) {
//             if (err) {
//                 log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//                 if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//             } else {
//                 if(array!=undefined){
//                     let dbquery=''
//                     db.getConnection(function(err, db) {
//                         if(!err){
//                             for (let i = 0; i < array.length; i++) {
                                

//                                 dbquery +=`call ${appConfig.getDbConfig().database}.v1_set_contract_charges
//                                     (${utils.OptionalKey(array[i].schoolID)},
//                                     ${utils.OptionalKey(array[i].chargesID)}, 
//                                     ${utils.OptionalKey(array[i].collectionID)}, 
//                                     ${utils.OptionalKey(array[i].currencyCode)},
//                                     ${utils.OptionalKey(array[i].unitPrice)},
//                                     ${utils.OptionalKey(array[i].taxRate)},
//                                     ${utils.OptionalKey(array[i].billingFrequency)}, 
//                                     ${utils.OptionalKeyString(array[i].chargesNotes)}, 
//                                     ${utils.OptionalKey(array[i].startDate)},
//                                     ${utils.OptionalKey(array[i].endDate)},
//                                     ${utils.OptionalKey(array[i].collectionType)},
//                                     ${utils.OptionalKey(array[i].collectionRate)},
//                                     ${utils.OptionalKeyString(array[i].collectionNotes)},
//                                     ${utils.OptionalKeyString(array[i].collectionAcTitle)},
//                                     ${utils.OptionalKeyString(array[i].collectionAcNumber)},
//                                     ${utils.OptionalKeyString(array[i].collectionAcBranch)},
//                                     ${utils.OptionalKeyString(array[i].collectionAcBank)},
//                                     ${utils.OptionalKey(array[i].userID)},
//                                     ${utils.OptionalKey(array[i].isActiveCharges)},
//                                     ${utils.OptionalKey(array[i].isActiveCollection)}
//                                     );`
    
//                             }
//                             console.log(dbquery)
//                             try {
//                                 db.query(dbquery,function (error, data, fields) {
//                                     if(!error){
//                                         log.doInfoLog(fileNameForLogging, req.url, 'successfully data updated in db')
//                                         if(!res.finished)res.status(200).json({ response: "success" })
//                                     }
//                                     else if (error.code=='ER_DUP_ENTRY'){
//                                         log.doInfoLog(fileNameForLogging, req.url, 'failed to insert record')
//                                         if(!res.finished)res.status(406).json({ response: "unsuccessful", code: error.code, reason : error.message })
//                                     }
//                                     else{
//                                         log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
//                                         const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
//                                         if(!res.finished)res.status(500).json({ response: errorResponse })
//                                     }
//                                 });
//                             } 
//                             finally {
//                                 db.release();
//                                 console.log('released')
//                             }
                
//                         }
//                         else{
//                             log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
//                             const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                             if(!res.finished)res.status(500).json({ response: errorResponse })
                
//                         }
//                     })
//                 }
//                 else{
//                     log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
//                     if(!res.finished)res.status(500).json({ response: "'array not found'" })
//                 }
//             }
//         })
// }


// exports.getContractCharges = function (req, res) {
//     const { schoolID } = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate(
//         { schoolID },schoolValidations.getContractCharges, function (err, value) {
//             if (err) {
//                 log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//                 if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//             } else {
//                 db.getConnection(function(err, db) {
//                     if(!err){
//                         let dbquery=`
//                             set @schoolID = ${utils.OptionalKey(schoolID)};   
//                             call ${appConfig.getDbConfig().database}.v1_get_contract_charges
//                             (@schoolID);`
//                     console.log(dbquery)
//                     try {
//                         db.query(dbquery,function (error, data, fields) {
//                                 if(!error){
//                                     data.forEach(element => {
//                                         if(element.constructor==Array) {
//                                             if (_.isEmpty(element)) {
//                                                 log.doErrorLog(fileNameForLogging, req.url, 'data not present')
//                                                 const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
//                                                 if(!res.finished)res.status(200).json({ response: errorResponse })
//                                             } else {
//                                                 log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
//                                                 if(!res.finished)res.status(200).json({response: element})
//                                             }
//                                         }
//                                     })
//                                 }
//                                 else{
//                                     log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
//                                     const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
//                                     if(!res.finished)res.status(500).json({ response: errorResponse })
//                                 }
//                             });
//                     } 
//                     finally {
//                         db.release();
//                         console.log('released')
//                     }
                        
            
//                     }
//                     else{
//                         log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
//                         const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                         if(!res.finished)res.status(500).json({ response: errorResponse })
            
//                     }
//                 })

//             }
//         })
// }


exports.deleteSchoolContract = function (req, res) {
    const { schoolID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID }, schoolValidations.deleteSchoolContract, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=`set @schoolID = ${utils.OptionalKey(schoolID)};
                                call ${appConfig.getDbConfig().database}.v1_delete_school_contract
                                (@schoolID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
                                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.deleteFeeType = function (req, res) {
    const { feeTypeID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ feeTypeID }, schoolValidations.deleteFeeType, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_feetype(
                        ${utils.OptionalKey(feeTypeID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } 
                                else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Cannot delete a fee type that has a fee created" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getBranchGroups = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate( { schoolID, branchID },schoolValidations.getBranchGroups, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @schoolID = ${utils.OptionalKey(schoolID)};   
                            set @branchID = ${utils.OptionalKey(branchID)};   
                            call ${appConfig.getDbConfig().database}.v1_get_school_branch_group
                            (@schoolID,@branchID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (_.isEmpty(element)) {
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(200).json({ response: errorResponse })
                                            } else {
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({response: element})
                                            }
                                        }
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
        })
}

exports.getSelfJoinSchools = function (req, res) {
    const {  } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({  }, schoolValidations.getSelfJoinSchools, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * from v1_get_schools_self_join  `;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        }
    })
}

exports.getClassesCombined = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate( { schoolID, branchID },schoolValidations.getClassesCombined, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @schoolID = ${utils.OptionalKey(schoolID)};   
                            set @branchID = ${utils.OptionalKey(branchID)};   
                            call ${appConfig.getDbConfig().database}.v1_get_classes_combined
                            (@schoolID,@branchID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (_.isEmpty(element)) {
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(200).json({ response: errorResponse })
                                            } else {
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({response: element})
                                            }
                                        }
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
        })
}

exports.getContractGroupCharges = function (req, res) {
    const { schoolID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { schoolID },schoolValidations.getContractGroupCharges, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @schoolID = ${utils.OptionalKey(schoolID)};   
                            call ${appConfig.getDbConfig().database}.v1_get_contract_group_charges
                            (@schoolID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (_.isEmpty(element)) {
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(200).json({ response: errorResponse })
                                            } else {
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({response: element})
                                            }
                                        }
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
        })
}

exports.setContractGroupCharges = function (req, res) {
    const { chargesID,branchGroupID,currencyCode,unitPrice,taxRate,billingFrequency,chargesNotes,startDate,endDate,userUUID,isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate( {chargesID,branchGroupID,currencyCode,unitPrice,taxRate,billingFrequency,chargesNotes,startDate,endDate,userUUID,isActive} ,schoolValidations.setContractGroupCharges,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                
                    let dbquery=''
                    db.getConnection(function(err, db) {
                        if(!err){
                            
                                dbquery +=`call ${appConfig.getDbConfig().database}.v1_set_contract_group_charges
                                    (${utils.OptionalKey(branchGroupID)},
                                    ${utils.OptionalKey(chargesID)}, 
                                    ${utils.OptionalKey(currencyCode)},
                                    ${utils.OptionalKey(unitPrice)},
                                    ${utils.OptionalKey(taxRate)},
                                    ${utils.OptionalKey(billingFrequency)}, 
                                    ${utils.OptionalKeyString(chargesNotes)}, 
                                    ${utils.OptionalKey(startDate)},
                                    ${utils.OptionalKey(endDate)},
                                    ${utils.OptionalKey(userUUID)},
                                    ${utils.OptionalKey(isActive)}
                                    );`
    
                            
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                    if(!error){
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data updated in db')
                                        if(!res.finished)res.status(200).json({ response: "success" })
                                    }
                                    else if (error.code=='ER_DUP_ENTRY'){
                                        log.doInfoLog(fileNameForLogging, req.url, 'failed to insert record')
                                        if(!res.finished)res.status(406).json({ response: "unsuccessful", code: error.code, reason : error.message })
                                    }
                                    else{
                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                
                        }
                    })
                
            }
        })
}


exports.getContractCollection = function (req, res) {
    const { schoolID,branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID },schoolValidations.getContractCollection, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @schoolID = ${utils.OptionalKey(schoolID)};   
                            set @branchID = ${utils.OptionalKey(branchID)};   
                            call ${appConfig.getDbConfig().database}.v1_get_contract_collection_charges
                            (@schoolID,@branchID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (_.isEmpty(element)) {
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(200).json({ response: errorResponse })
                                            } else {
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({response: element})
                                            }
                                        }
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
        })
}


exports.setContractCollectionArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate( array ,schoolValidations.setContractCollectionArray,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                if(array!=undefined){
                    let dbquery=''
                    db.getConnection(function(err, db) {
                        if(!err){
                            for (let i = 0; i < array.length; i++) {
                                

                                dbquery +=`call ${appConfig.getDbConfig().database}.v1_set_contract_collection
                                    ( 
                                    ${utils.OptionalKey(array[i].collectionID)}, 
                                    ${utils.OptionalKey(array[i].schoolID)},
                                    ${utils.OptionalKey(array[i].branchID)},
                                    ${utils.OptionalKey(array[i].collectionType)},
                                    ${utils.OptionalKey(array[i].collectionRate)},
                                    ${utils.OptionalKeyString(array[i].collectionNotes)},
                                    ${utils.OptionalKey(array[i].startDate)},
                                    ${utils.OptionalKey(array[i].userUUID)},
                                    ${utils.OptionalKey(array[i].isActive)}
                                    );`
    
                            }
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                    if(!error){
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data updated in db')
                                        if(!res.finished)res.status(200).json({ response: "success" })
                                    }
                                    else if (error.code=='ER_DUP_ENTRY'){
                                        log.doInfoLog(fileNameForLogging, req.url, 'failed to insert record')
                                        if(!res.finished)res.status(406).json({ response: "unsuccessful", code: error.code, reason : error.message })
                                    }
                                    else{
                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                
                        }
                    })
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                    if(!res.finished)res.status(500).json({ response: "'array not found'" })
                }
            }
        })
}

exports.getSettlementSchoolReport = function (req, res) {
    const { schoolID, branchID, fromDate, toDate, isExport } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID, fromDate, toDate, isExport },schoolValidations.getSettlementSchoolReport, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @schoolID = ${utils.OptionalKey(schoolID)};   
                            set @branchID = ${utils.OptionalKey(branchID)};   
                            set @fromDate = ${utils.OptionalKey(fromDate)};   
                            set @toDate = ${utils.OptionalKey(toDate)};   
                            call ${appConfig.getDbConfig().database}.v1_get_report_settlement_school
                            (@schoolID,@branchID,@fromDate,@toDate);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (_.isEmpty(element)) {
                                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(404).json({ response: errorResponse })
                                            } else {
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(isExport==0){
                                                    if(!res.finished)res.status(200).json({response: element})
                                                }
                                                else{
                                                    var currentDate = utils.formatReadableDate(new Date(),'en',0)
                                                    var wb = new xl.Workbook();
                                                    var ws = wb.addWorksheet('Sheet 1');
                                                    var fileName = `School Settlement Report - ${currentDate}.xlsx`
                                                    var columnHeader = wb.createStyle({
                                                    font: {
                                                        color: '#000000',
                                                        size: 12,
                                                        bold: true
                                                    }
                                                    });
                                                    var decimalStyle = wb.createStyle({
                                                        font: {
                                                            color: '#000000',
                                                            size: 12,
                                                        },
                                                        numberFormat: 'PKR#,##0.00; (PKR#,##0.00); -',
                                                        });
                                                        
                                                    var percentageStyle = wb.createStyle({
                                                        font: {
                                                            color: '#000000',
                                                            size: 12,
                                                        },
                                                        numberFormat: '#.00%; -#.00%; -'
                                                        });

                                                        var dateStyle = wb.createStyle({
                                                        font: {
                                                            color: '#000000',
                                                            size: 12,
                                                        },
                                                        numberFormat: 'dd-mmm-yyyy' 
                                                        });
                                                    
                                                    var headingTop = wb.createStyle({
                                                    font: {
                                                        bold: true,
                                                        underline: true,
                                                        size: 16,
                                                    },
                                                    alignment: {
                                                        wrapText: true,
                                                        horizontal: 'center',
                                                    },
                                                    });
                                                    ws.cell(1, 2, 1, 18, true).string('School Settlement Report').style(headingTop);
                                                    // Set value of cell A1 to 100 as a number type styled with paramaters of style
                                                    ws.cell(5, 2).string('Student Name').style(columnHeader);
                                                    ws.cell(5, 3).string('Parent Name').style(columnHeader);                                                    
                                                    ws.cell(5, 4).string('Class').style(columnHeader);
                                                    ws.cell(5, 5).string('Invoice No.').style(columnHeader);
                                                    ws.cell(5, 6).string('Billing Period').style(columnHeader);
                                                    ws.cell(5, 7).string('Transaction Date').style(columnHeader);
                                                    ws.cell(5, 8).string('Billed Amount').style(columnHeader);
                                                    ws.cell(5, 9).string('Fix/Percentage').style(columnHeader);
                                                    ws.cell(5, 10).string('Collection Rate').style(columnHeader);
                                                    ws.cell(5, 11).string('Collection Amount').style(columnHeader);
                                                    ws.cell(5, 12).string('Tax Amount(%)').style(columnHeader);
                                                    ws.cell(5, 13).string('Total Deduction').style(columnHeader);
                                                    ws.cell(5, 14).string('Final Amount').style(columnHeader);
                                                    ws.cell(5, 15).string('Settled (Y/N)').style(columnHeader);
                                                    ws.cell(5, 16).string('Settlement Date').style(columnHeader);
                                                    ws.cell(5, 17).string('Verified (Y/N)').style(columnHeader);
                                                    ws.cell(5, 18).string('Verification Date').style(columnHeader);
                                                    ws.cell(5, 19).string('Verification By').style(columnHeader);
                                                    let sumOfTransactionAmt=0, finalRowCount=0
                                                    data.forEach(element => {
                                                        if(element.constructor==Array) {
                                                        
                                                            if (!_.isEmpty(element)) {
                                                                let isSettled,isVerified
                                                                for(let i=0;i < element.length; i++){
                                                                    ws.cell(6+i, 2).string(utils.Convert2String(element[i].studentName));
                                                                    ws.cell(6+i, 3).string(utils.Convert2String(element[i].parentName));
                                                                    ws.cell(6+i, 4).string(utils.Convert2String(element[i].classAlias + ' - ' + element[i].classSection));
                                                                    ws.cell(6+i, 5).string(utils.Convert2String(element[i].invoiceNumber.toString()));
                                                                    ws.cell(6+i, 6).string(utils.Convert2String(element[i].periodName));
                                                                    ws.cell(6+i, 7).date(element[i].transactionDate).style(dateStyle);
                                                                    ws.cell(6+i, 8).number(parseInt(element[i].billAmount)).style(decimalStyle);
                                                                    ws.cell(6+i, 9).string(utils.Convert2String(element[i].MDRType));   
                                                                    if (element[i].MDRType=='PERCENTAGE' && element[i].MDRRate){
                                                                        ws.cell(6+i, 10).number(parseInt(element[i].MDRRate)/100).style(percentageStyle);   
                                                                    }
                                                                    else if (element[i].MDRType=='FIXED' && element[i].MDRRate){
                                                                        ws.cell(6+i, 10).number(parseInt(element[i].MDRRate)).style(decimalStyle);   
                                                                    }
                                                                    else{
                                                                        ws.cell(6+i, 10).number(0);   

                                                                    }
                                                                    if (element[i].MDRAmount){
                                                                        ws.cell(6+i, 11).number(parseInt(element[i].MDRAmount)).style(decimalStyle);   
                                                                    } 
                                                                    else{
                                                                        ws.cell(6+i, 11).number(0);   
                                                                    }                                                                   
                                                                    
                                                                    if(element[i].taxValue){
                                                                        ws.cell(6+i, 12).number(parseInt(element[i].taxValue)).style(decimalStyle);   
                                                                    }
                                                                    else{
                                                                        ws.cell(6+i, 12).number(0);
                                                                    }
                                                                    if(element[i].taxValue){
                                                                        ws.cell(6+i, 13).number(parseInt(element[i].taxValue) + parseInt(element[i].MDRAmount)).style(decimalStyle);   
                                                                    }
                                                                    else{
                                                                        ws.cell(6+i, 13).number(0);
                                                                    }
                                                                    
                                                                    if(element[i].schoolSettlementAmount){
                                                                        ws.cell(6+i, 14).number(parseInt(element[i].schoolSettlementAmount)).style(decimalStyle);
                                                                    }
                                                                    else{
                                                                        ws.cell(6+i, 14).number(0);
                                                                    }

                                                                    
                                                                    if (isExport==1) {
                                                                            isSettled = element[i].isSettled==1 ? "Yes" : "No"
                                                                    }   
                                                                    ws.cell(6+i, 15).string(utils.Convert2String(isSettled));  
                                                                    if(element[i].settlementDate){
                                                                        ws.cell(6+i, 16).date(element[i].settlementDate).style(dateStyle);   
                                                                    }
                                                                    else{
                                                                        ws.cell(6+i, 16).string("");   
                                                                    }
                                                                    if (isExport==1) {
                                                                            isVerified = element[i].isVerified==1 ? "Yes" : "No"
                                                                    }
                                                                    ws.cell(6+i, 17).string(utils.Convert2String(isVerified)); 
                                                                    if (element[i].verificationDate){
                                                                        ws.cell(6+i, 18).date(element[i].verificationDate).style(dateStyle);
                                                                    } 
                                                                    else{
                                                                        ws.cell(6+i, 18).string("");
                                                                    } 
                                                                    
                                                                    ws.cell(6+i, 19).string(utils.Convert2String(element[i].fullname)); 
                                                                    finalRowCount =  finalRowCount+1
                                                                    sumOfTransactionAmt = sumOfTransactionAmt + parseInt(element[i].schoolSettlementAmount)
                                                                    
                                                                }
                                                            }
                                                        
                                                        }
                                                        
                                                        
                                                    })
                                                    // console.log(finalRowCount)
                                                    ws.cell(6+finalRowCount, 13).string('Total Amount:');   
                                                    
                                                    if (sumOfTransactionAmt){
                                                        ws.cell(6+finalRowCount, 14).number(sumOfTransactionAmt).style(decimalStyle);   
                                                    }
                                                    wb.write(fileName , res);
                                                }
                                                
                                            }
                                        }
                                    })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
        })
}

exports.setSettlementVerification = function (req, res) {
    const { schoolID, branchID,voucherUUID, isSettled, isVerified, userUUID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, branchID,voucherUUID, isSettled, isVerified, userUUID },schoolValidations.setSettlementVerification, function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`
                            set @schoolID = ${utils.OptionalKey(schoolID)};   
                            set @branchID = ${utils.OptionalKey(branchID)}; 
                            set @voucherUUID = ${utils.OptionalKey(voucherUUID)}; 
                            set @isSettled = ${utils.OptionalKey(isSettled)};   
                            set @isVerified = ${utils.OptionalKey(isVerified)};   
                            set @userUUID = ${utils.OptionalKey(userUUID)};   
                            call ${appConfig.getDbConfig().database}.v1_set_report_settlement_verified
                            (@schoolID,@branchID,@voucherUUID,@isSettled,@isVerified,@userUUID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
        })
}

