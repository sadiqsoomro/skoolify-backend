const Joi = require('joi'), utils = require('../../utils/helper-methods'),
    examValidations = require('../validations/validations-exams'),
    db = require('../../../db'),
    _ = require('lodash'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    async = require('async'),
    fileNameForLogging = 'exams-controller'
import Error from '../response-classes/error'

// ==================================================================


exports.getExamTerm = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({schoolID,  branchID }, examValidations.getExamTerm, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            
            
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`select * from v1_get_exam_term where schoolID = '${schoolID}' `
                    if (branchID){
                        dbquery += ` and branchID = '${branchID}' `;
                    }
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data})
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
        }
    })
}


exports.getActiveExamTerm = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({schoolID,  branchID }, examValidations.getActiveExamTerm, function (err, value) {
        if (!err) {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`Select * from v1_get_exam_term where isActive = 1 and schoolID = '${schoolID}' `
                    if (branchID){
                        dbquery += ` and branchID = '${branchID}' `;
                    }
                    console.log(dbquery)
                    try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if (_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: data})
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
            
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}



exports.getClassTerms = function (req, res) {
    const { studentID, classID, classLevelID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ studentID,  classID, classLevelID }, examValidations.getClassTerms, function (err, value) {
        if (!err) {
            let dbquery = "select * from v1_get_class_term ";
            if (studentID || classID || classLevelID) {
                dbquery += ` where `
            }
            if (classID  && studentID ){
                dbquery +=  ` studentID = '${studentID}' and classID = '${classID}' `;
            }
            
            else if (studentID && !classID ){
                dbquery +=  `  studentID = '${studentID}' `;
            }
           else  if (classID  && !studentID ){
                dbquery +=  ` classID = '${classID}' `;
            } 

            if ((classLevelID && classID) | (classLevelID & studentID)){
                dbquery +=  `  and classLevelID = '${classLevelID}' `;
            }
            else if (classLevelID){
                dbquery +=  ` classLevelID = '${classLevelID}' `;
            }
            console.log(dbquery)
            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(404).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
           
            
        } else {
             log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}


exports.setExamTerm = function (req, res) {
    const { termNote, termName, examsTermID, branchID, schoolID,termStartDate, termEndDate, isPublished,isActive } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { termNote, termName, examsTermID, branchID, schoolID, termStartDate, termEndDate, isPublished,isActive },
        examValidations.setExamTerm,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        // let var_termName,var_termNote
                        // if(termName){
                        //     var_termName = termName.replace(/'/g, "''").replace(/;/g, "\;").replace(/--/g, "\--");
                        // }
                        // else{
                        //     var_termName = termName
                        // }
                        // if(termNote){
                        //     var_termNote = termNote.replace(/'/g, "''").replace(/;/g, "\;").replace(/--/g, "\--");
                        // }
                        // else{
                        //     var_termNote = termNote
                        // }

                        let dbquery=`set @schoolID = ${utils.OptionalKey(schoolID)};
                            set @branchID = ${utils.OptionalKey(branchID)};
                            set @examsTermID = ${utils.OptionalKey(examsTermID)};
                            set @termName = ${utils.OptionalKeyString(termName)};
                            set @termNote = ${utils.OptionalKeyString(termNote)};
                            set @termStartDate = ${utils.OptionalKey(termStartDate)};
                            set @termEndDate = ${utils.OptionalKey(termEndDate)};
                            set @isPublished = ${utils.OptionalKey(isPublished)};
                            set @isActive = ${utils.OptionalKey(isActive)};
                            call ${appConfig.getDbConfig().database}.v1_set_exam_term
                            (@schoolID,@branchID,@examsTermID,@termName,@termNote,@termStartDate,@termEndDate,@isPublished,@isActive);`
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                        
                            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.assignClassTerm = function (req, res) {
    const { examsTermID, classLevelID, examTermLinkID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { examsTermID, classLevelID, examTermLinkID },
        examValidations.assignClassTerm,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`set @examTermLinkID = ${utils.OptionalKey(examTermLinkID)};
                            set @classLevelID = ${utils.OptionalKey(classLevelID)};
                            set @examsTermID = ${utils.OptionalKey(examsTermID)};
                            call ${appConfig.getDbConfig().database}.v1_set_exam_term_link
                            (@examTermLinkID,@classLevelID,@examsTermID);`
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                        
                                            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.getExamResults = function (req, res) {
    const { examsTermID, classID, studentID, subjectID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { examsTermID, classID, studentID, subjectID, nonSubjectID }, examValidations.getExamResults,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `   SELECT * FROM v1_get_exam_results_overall 
                                WHERE classID = '${classID}' AND examsTermID = '${examsTermID}' `;
                        if (studentID)
                            dbquery += ` and studentID='${studentID}' `;
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, firstViewData, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'firstViewData', firstViewData)
                                    if (_.isEmpty(firstViewData[0])) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        const examsTermID = firstViewData[0].examsTermID
                                        console.log(examsTermID)
                                        if (examsTermID != '' || examsTermID != null) {
                                            let dbquery = `  SELECT * FROM v1_get_exam_subject_results 
                                            WHERE examsTermID = '${examsTermID}' 
                                            and classID = '${classID}' `;
            
                                            if (studentID){
                                                dbquery +=  ` and studentID='${studentID}' `;
                                            }
                                            if (subjectID){
                                                dbquery += ` and subjectID = '${subjectID}' `;
                                            }
                                            console.log(dbquery)
                                            db.query(dbquery,function (error, secondViewData, fields) {
                                                    if(!error){
                                                        //log.doInfoLog(fileNameForLogging, req.url, 'secondViewData', secondViewData)
                                                        let dbquery = `  SELECT * FROM v1_get_exam_ns_results 
                                                                    WHERE examsTermID = '${examsTermID}' 
                                                                    and classID = '${classID}'  `

                                                        if (studentID){
                                                            dbquery +=  ` and studentID='${studentID}' `;
                                                        }
                                                        if (nonSubjectID){
                                                            dbquery +=  ` and nonSubjectID = '${nonSubjectID}' `;
                                                        }
                                                        console.log(dbquery)
                                                        db.query(dbquery,function (error, thirdViewData, fields) {
                                                                if(!error){
                                                                    //log.doInfoLog(fileNameForLogging, req.url, 'thirdViewData', thirdViewData)
                                                                    const examResultsOverallResponse = firstViewData
                                                                    const examSubjectsResultResponse = secondViewData
                                                                    const examNsResultResponse = thirdViewData
                                                                    
                                                                    if(!res.finished)res.status(200).json({
                                                                        response: [
                                                                            {
                                                                                examResultsOverallResponse
                                                                            },
                                                                            {
                                                                                examSubjectsResultResponse: examSubjectsResultResponse.map((value, index, examSubjectsResultResponse) => {
                                                                                    return {
                    
                                                                                        subjectName: value.subjectName,
                                                                                        totalMarks: value.totalMarks,
                                                                                        obtainedMarks: value.obtainedMarks,
                                                                                        showMarksOnApp: value.showMarksOnApp,
                                                                                        grade: value.grade,
                                                                                        subjectID: value.subjectID,
                                                                                        notes: value.notes
                                                                                    }
                                                                                })
                                                                            },
                                                                            {
                                                                                examNsResultResponse: examNsResultResponse.map((value, index, examNsResultResponse) => {
                                                                                    return {
                                                                                        nonSubjectName: value.nonSubjectName,
                                                                                        gradeNS: value.gradeNS,
                                                                                        nonSubjectID: value.nonSubjectID,
                                                                                        nsLegendID:value.nsLegendID,
                                                                                        legendValue: value.legendValue,
                                                                                        notesNS: value.notesNS
                                                                                    }
                                                                                })
                                                                            }
                                                                        ]
                                                                    })
                                                                    
                                                                }
                                                                else{
                                                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                                    console.log(errorResponse)
                                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                                                }
                                                            });
                                                        
                                                    }
                                                    else{
                                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        console.log(errorResponse)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                                    }
                                                });
                                        }
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.getQuizResults = function (req, res) {
    const { classID, studentID, subjectID, quizDate } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { classID, studentID, subjectID, quizDate },
        examValidations.getQuizResults,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = ` SELECT * FROM v1_get_quiz_results WHERE classID = '${classID}' `
                        if (!_.isEmpty(studentID)) {
                            dbquery += `AND studentID = '${studentID}' `
                        }
                        if (!_.isEmpty(subjectID)) {
                            dbquery += `AND subjectID = '${subjectID}' `
                        }
                        if (!_.isEmpty(quizDate)) {
                            dbquery += `AND quizDate = '${quizDate}' `
                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    if (_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: data })
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}



exports.getQuizGraphResults = function (req, res) {
    const { classID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({classID},examValidations.getQuizResults,function (err, value) {
            if (!err) {
                

                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `select DISTINCT quizDate ,subjectID 
                            from v1_get_quiz_results 
                            WHERE classID = '${classID}' 
                            ORDER by quizDate DESC LIMIT 3 `
                            
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    if(_.isEmpty(data)){
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                if(!res.finished)res.status(200).json({ response: errorResponse })
                                    }

                                    else{
                                        const Last3QuizDates = data
                                        let dbquery = ` select * from v1_get_quiz_graph 
                                                    where classID IN ('${classID}') and quizDate IN (select * from (select DISTINCT quizDate 
                                                    from v1_get_quiz_graph 
                                                    WHERE classID = '${classID}' and quizDate IN (
                                                        ` + (function () {
                                                let querySt = ""
                                                Last3QuizDates.forEach((date, index) => {
                                                    querySt += '\'' + utils.formatDate(date.quizDate) + '\''
                                                    if (index != 2) querySt += ','
                                                });
                                            

                                                if(querySt.endsWith(','))
                                                return querySt.substring(0, querySt.length - 1);
                                                else
                                                return querySt;

                                                })() + `) AND subjectID IN ( ` + (function () {
                                                
                                                let querySts = ""
                                                Last3QuizDates.forEach((date, index) => {
                                                        querySts += '\'' + date.subjectID + '\''
                                                        if (index != 2) querySts += ','
                                                    });

                                                if(querySts.endsWith(','))
                                                    return querySts.substring(0, querySts.length - 1);
                                                else
                                                    return querySts;

                                                    })() + `) 
                                                    ORDER by quizDate DESC LIMIT 3) as t1);`
                                    console.log(dbquery)
                                    db.query(dbquery,function (error, data, fields) {
                                            if(!error){
                                               
                                                let innerArray = new Array();
                                                let noOfIterations = data.length;
                        
                                                let distinctSubjects = new Array();
                                                let distinctObjects = new Array();
                        
                                                for (let loop = 0; loop < noOfIterations; loop++) {
                                                    if (!distinctSubjects.includes(data[loop].subjectID+""+data[loop].quizDate)) {
                                                    
                                                        distinctSubjects.push(data[loop].subjectID+""+data[loop].quizDate);
                        
                                                        var partialData = Object.assign({}, data[loop]);
                        
                                                        // if (partialData.hasOwnProperty("marksTotal")) {
                                                        //     delete partialData.marksTotal;
                                                        // }
                                                        if (partialData.hasOwnProperty("studentID")) {
                                                            delete partialData.studentID;
                                                        }
                                                        if (partialData.hasOwnProperty("marksObtained")) {
                        
                                                            delete partialData.marksObtained;
                                                        }
                                                        
                                                        distinctObjects.push(partialData);

                                                    }
                                                }

                                                for (let j = 0; j < distinctObjects.length; j++) {
                                                    let formattedData = new Array();
                                                    for (let i = 0; i < data.length; i++) {
        
                                                    
        
                                                        if (distinctObjects[j].subjectID+""+distinctObjects[j].quizDate == data[i].subjectID+""+data[i].quizDate) {
                                                            
                                                            if (data[i].hasOwnProperty("quizID")) {
                        
                                                                delete data[i].quizID;
                                                            }
                                                            if (data[i].hasOwnProperty("classID")) {
                        
                                                                delete data[i].classID;
                                                            }
                                                            if (data[i].hasOwnProperty("marksTotal")) {
                        
                                                                delete data[i].marksTotal;
                                                            }
                                                           
                                                            if (data[i].hasOwnProperty("subjectID")) {
                        
                                                                delete data[i].subjectID;
                                                            }
                                                            if (data[i].hasOwnProperty("subjectName")) {
                        
                                                                delete data[i].subjectName;
                                                            }
                                                            if (data[i].hasOwnProperty("quizDate")) {
                        
                                                                delete data[i].quizDate;
                                                            }
                                                            formattedData.push(data[i]);
                                                        }
                                                    }
                                                    distinctObjects[j].quizMarks = formattedData;
                                                }

                                                if(!res.finished)res.status(200).json({response: distinctObjects})

                                            }
                                            else{
                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                console.log(errorResponse)
                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                            }
                                        });
                                
                                        
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
                
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}


exports.setQuizResults = function (req, res) {
    const { quizID, quizDate, comments, marksObtained, marksTotal, subjectID, studentID, classID,showGraph } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate( { quizID, quizDate, comments, marksObtained, marksTotal, subjectID, studentID, classID ,showGraph},examValidations.setQuizResults,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`   set @quizID = ${utils.OptionalKey(quizID)};
                                        set @classID = ${utils.OptionalKey(classID)};
                                        set @studentID = ${utils.OptionalKey(studentID)};
                                        set @subjectID = ${utils.OptionalKey(subjectID)};
                                        set @marksTotal = ${utils.OptionalKey(marksTotal)};
                                        set @marksObtained = ${utils.OptionalKey(marksObtained)};
                                        set @comments = ${utils.OptionalKey(comments)};
                                        set @quizDate = ${utils.OptionalKey(quizDate)};
                                        set @showGraph = ${utils.OptionalKey(showGraph)}; 
                                        call ${appConfig.getDbConfig().database}.v1_set_quiz_results
                                        (@quizID,@classID,@studentID,@subjectID,@marksTotal,@marksObtained,@comments,@quizDate,@showGraph);`
                        
                            
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }

                                        
                       
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } 
            
            else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.setQuizResultsArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, examValidations.setQuizResultsArray,
        function (err, value) {
            if (!err) {
                
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array!=undefined){


                        let dbquery = "";
                        
                        for (let i = 0; i < array.length; i++) {
                        
                        dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_quiz_results
                            (${utils.OptionalKey(array[i].quizID)}, 
                            ${utils.OptionalKey(array[i].classID)}, 
                            ${utils.OptionalKey(array[i].studentID)}, 
                            ${utils.OptionalKey(array[i].subjectID)},
                            ${utils.OptionalKey(array[i].marksTotal)},
                            ${utils.OptionalKey(array[i].marksObtained)},
                            ${utils.OptionalKeyString(array[i].comments)},
                            ${utils.OptionalKey(array[i].quizDate)},
                            ${utils.OptionalKey(array[i].showGraph)});  `
                          console.log(dbquery)  
                        }
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
              
                        }
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}


exports.setExamTermResults = function (req, res) {
    const { examOverallID,  examsTermID,  studentID,  overallNotes,  rank, resultStatus, holdFlag,overallGrade } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { examOverallID, examsTermID, studentID,  overallNotes,rank, resultStatus,holdFlag,overallGrade}, examValidations.setExamTermResults,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`set @examOverallID = ${utils.OptionalKey(examOverallID)};
                                        set @examsTermID  = ${utils.OptionalKey(examsTermID)};
                                        set @studentID  = ${utils.OptionalKey(studentID)};
                                        set @overallNotes  = ${utils.OptionalKeyString(overallNotes)};
                                        set @rank = ${utils.OptionalKey(rank)};
                                        set @resultStatus = ${utils.OptionalKey(resultStatus)};
                                        set @holdFlag = ${utils.OptionalKey(holdFlag)};                
                                        set @overallGrade = ${utils.OptionalKey(overallGrade)};
                                        call ${appConfig.getDbConfig().database}.v1_set_exam_result_overall
                                        (@examOverallID,@examsTermID,@studentID,@overallNotes,@rank,@resultStatus,@holdFlag,@overallGrade);`
                        
                                        try {
                                            db.query(dbquery, function (error, data, fields) {
                                            if(!error){
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({ response: "success" })
                                                
                                            }
                                            else{
                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                console.log(errorResponse)
                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                            }
                                        });
                                        } 
                                        finally {
                                            db.release();
                                            console.log('released')
                                        }
                                        
                            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))

            }
        })
}

exports.setExamTermResultsArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, examValidations.setExamTermResultsArray,
        function (err, value) {
            if (!err) {
                let dbquery = "";
                
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array!=undefined){

                        for (let i = 0; i < array.length; i++) {
                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_exam_result_overall
                                (${utils.OptionalKey(array[i].examOverallID)}, 
                                ${utils.OptionalKey(array[i].examsTermID)}, 
                                ${utils.OptionalKey(array[i].studentID)}, 
                                ${utils.OptionalKey(array[i].classID)},
                                ${utils.OptionalKeyString(array[i].overallNotes)},
                                ${utils.OptionalKey(array[i].rank)},
                                ${utils.OptionalKey(array[i].resultStatus)},
                                ${utils.OptionalKey(array[i].holdFlag)},
                                ${utils.OptionalKey(array[i].overallGrade)});  `
        
                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
              
                        }
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })

            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.setExamSubjectResults = function (req, res) {
    const { notes, grade, showMarksOnApp,isAbsent,obtainedMarks, totalMarks, subjectID, studentID, examsTermID, examSubjectResultID, userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ notes, grade, isAbsent, obtainedMarks,showMarksOnApp, totalMarks, subjectID, studentID, examsTermID, examSubjectResultID, userID },examValidations.setExamSubjectResults,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`set @examSubjectResultID = ${utils.OptionalKey(examSubjectResultID)};
                                    set @examsTermID = ${utils.OptionalKey(examsTermID)};
                                    set @studentID = ${utils.OptionalKey(studentID)};
                                    set @subjectID = ${utils.OptionalKey(subjectID)};
                                    set @totalMarks = ${utils.OptionalKey(totalMarks)};
                                    set @isAbsent = ${utils.OptionalKey(isAbsent)};
                                    set @obtainedMarks = ${utils.OptionalKey(obtainedMarks)};
                                    set @showMarksOnApp = ${utils.OptionalKey(showMarksOnApp)};
                                    set @grade = ${utils.OptionalKey(grade)};
                                    set @notes = ${utils.OptionalKeyString(notes)};
                                    set @userID = ${utils.OptionalKey(userID)};
                                    
                                    call ${appConfig.getDbConfig().database}.v1_set_exam_subject_results
                                    (@examSubjectResultID,@examsTermID,@studentID , @subjectID,@totalMarks,@isAbsent,@obtainedMarks,@showMarksOnApp,@grade,@notes, @userID);`
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully inserted data in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                            
                            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {

                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.setExamSubjectResultsArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, examValidations.setExamSubjectResultsArray,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array!=undefined){

                        let dbquery = "";
                        for (let i = 0; i < array.length; i++) {
                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_exam_subject_results
                            (${utils.OptionalKey(array[i].examSubjectResultID)}, 
                            ${utils.OptionalKey(array[i].examsTermID)}, 
                            ${utils.OptionalKey(array[i].studentID)}, 
                            ${utils.OptionalKey(array[i].subjectID)},
                            ${utils.OptionalKey(array[i].totalMarks)},
                            ${utils.OptionalKey(array[i].isAbsent)},
                            ${utils.OptionalKey(array[i].obtainedMarks)},
                            ${utils.OptionalKey(array[i].showMarksOnApp)},
                            ${utils.OptionalKey(array[i].grade)},
                            ${utils.OptionalKey(array[i].notes)},
                            ${utils.OptionalKey(array[i].userID)});  `
                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery, function (error, data, fields) {
                                if(!error){
                                   log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
              
                        }
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}


// exports.setExamNonSubjectResults = function (req, res) {
//     const { userID, notesNS, gradeNS, nonSubjectID, examsTermID, studentID, examNSResultID } = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate(
//         { userID, notesNS, gradeNS, nonSubjectID, examsTermID, studentID, examNSResultID }, examValidations.setExamNonSubjectResults,
//         function (err, value) {
//         db.getConnection(function(err, db) {
//             if(!err){
                
//             }
//         })
//     })
// }


exports.setExamNonSubjectResultsArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, examValidations.setExamNonSubjectResultsArray,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array != undefined){

                        let dbquery = "";
                        for (let i = 0; i < array.length; i++) {
                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_exam_ns_results
                                ( 
                                ${utils.OptionalKey(array[i].examNSResultID)},
                                ${utils.OptionalKey(array[i].examsTermID)}, 
                                ${utils.OptionalKey(array[i].studentID)}, 
                                ${utils.OptionalKey(array[i].nonSubjectID)},
                                ${utils.OptionalKey(array[i].nsLegendID)},
                                ${utils.OptionalKey(array[i].gradeNS)},
                                ${utils.OptionalKeyString(array[i].notesNS)},
                                ${utils.OptionalKey(array[i].userID)});  `
                                }
                                console.log(dbquery)
                                try {
                                    db.query(dbquery,function (error, data, fields) {
                                        if(!error){
                                        
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({ response: "success" })
                                        }
                                        else{
                                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                            console.log(errorResponse)
                                            if(!res.finished)res.status(500).json({ response: errorResponse })
                                        }
                            });
                                } 
                                finally {
                                    db.release();
                                    console.log('released')
                                }
                        
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
              
                        }
                       
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}



exports.getExamOverallResult = function (req, res) {
    const { examsTermID,classID,studentID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({examsTermID,  classID,  studentID }, examValidations.getExamOverallResult, 
        function (err, value) {
        if (!err) {
            db.getConnection(function(err, db) {
                if(!err){

                    let dbquery = `Select * from v1_get_exam_results_overall where examsTermID = '${examsTermID}'`;
                    if (classID)
                        dbquery += ` and classID = '${classID}' `;
                    if (studentID)
                        dbquery += ` and studentID = '${studentID}' `;

                    console.log(dbquery)
                    try {
                        db.query(dbquery, function (error, data, fields) {
                            if(!error){ 
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }

                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}

exports.getExamSubjectResult = function (req, res) {
    const { examsTermID, classID, studentID,subjectID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ examsTermID, classID, studentID,  subjectID }, examValidations.getExamSubjectResult, 
        function (err, value) {
        if (!err) {
            
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `select * from v1_get_exam_subject_results where examsTermID = '${examsTermID}'`;
                        if (classID)
                            dbquery +=  ` and classID = '${classID}' `;
                        if (studentID)
                            dbquery +=  ` and studentID = '${studentID}' `;
                        if (subjectID)
                            dbquery +=  ` and subjectID = '${subjectID}' `;
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    if (_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: data })
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
                    
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
        } else {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        }
    })
}

exports.getExamNSResult = function (req, res) {
    const { examsTermID,classID, studentID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({examsTermID, classID,  studentID,   nonSubjectID }, examValidations.getExamNSResult, 
        function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `select * from v1_get_exam_ns_results where examsTermID = '${examsTermID}' `;
                if (classID)
                    dbquery +=  ` and classID = '${classID}' `;
                if (studentID)
                    dbquery +=   ` and studentID = '${studentID}' `;
                if (nonSubjectID)
                    dbquery += ` and nonSubjectID = '${nonSubjectID}' `;
                console.log(dbquery)
                try {
                    db.query(dbquery,function (error, data, fields) {
                        if(!error){
                        
                        if (_.isEmpty(data)) {
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(200).json({ response: errorResponse })
                        } else {
                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            if(!res.finished)res.status(200).json({ response: data })
                        }
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        console.log(errorResponse)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
                
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
        }
    })
}

// exports.getAssessmentResults = function (req, res) {
//     const { examsTermID, classID, studentID } = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate(
//         { examsTermID, classID, studentID }, examValidations.getAssessmentResults,
//         function (err, value) {
//             if (err) {
//                 log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//                 if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//             } else {
//                 let query = `   SELECT * FROM v1_get_assessment_overall 
//                                 WHERE classID = '${classID}' 
//                                 AND studentID = '${studentID}'
//                                 AND examsTermID = '${examsTermID}' `

//                 db.query(query)
//                     .then(firstViewData => {
//                         log.doInfoLog(fileNameForLogging, req.url, 'view : v1_get_assessment_overall data fetched from db')
//                         if (_.isEmpty(firstViewData[0])) {
//                             log.doErrorLog(fileNameForLogging, req.url, 'data not present')
//                             const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
//                             if(!res.finished)res.status(200).json({ response: errorResponse })

                               
//                         } else {
//                             log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
//                             if(!res.finished)res.status(200).json({ response: firstViewData[0] })
//                         }
//                     })
                    
//                     .catch(err => {
//                         log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', err)
//                         const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                         if(!res.finished)res.status(500).json({ response: errorResponse })
//                     })
//             }
//         })
// }

exports.deleteFeeChallan = function (req, res) {
    const { challanID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ challanID }, examValidations.deleteFeeChallan,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `
                                set @challanID = ${utils.OptionalKey(challanID)};
                                call ${appConfig.getDbConfig().database}.v1_delete_challan
                                (@challanID);`
                                console.log(dbquery)
                                try {
                                    db.query(dbquery, function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                   
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                                } 
                                finally {
                                    db.release();
                                    console.log('released')
                                }
  
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.setExamTermStatus = function (req, res) {
    const { examsTermID, isPublished } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(
        { examsTermID, isPublished }, examValidations.setExamTermStatus,
        function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`  set @examsTermID = ${utils.OptionalKey(examsTermID)};
                                        set @isPublished = ${utils.OptionalKey(isPublished)};
                                        call ${appConfig.getDbConfig().database}.v1_set_exam_term_status
                                        (@examsTermID,@isPublished);`
                        console.log(dbquer)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
                
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.setClassChange = function (req, res) {
    const { studentID, classID,classYear } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ studentID, classID,classYear },examValidations.setClassChange,function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `set @studentID = ${utils.OptionalKey(studentID)};
                                        set @classID = ${utils.OptionalKey(classID)};
                                        set @classYear = ${utils.OptionalKey(classYear)};
                                        call ${appConfig.getDbConfig().database}.v1_set_class_change
                                        (@studentID,@classID,@classYear);`
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully inserted data in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                                   
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.setClassChangeArray = function (req, res) {
    const { array } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, examValidations.setClassChangeArray,function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array != undefined){

                        let dbquery = "";
                        for (let i = 0; i < array.length; i++) {
                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_class_change
                        (
                        ${utils.OptionalKey(array[i].studentID)}, 
                        ${utils.OptionalKey(array[i].classID)}, 
                        ${utils.OptionalKey(array[i].classYear)});  `

                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })                
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
              
                        }

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
                
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}

exports.deleteExamTerm = function (req, res) {
    const { examsTermID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ examsTermID }, examValidations.deleteExamTerm, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_exam_term(${utils.OptionalKey(examsTermID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                         
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
        }
    })
}


exports.deleteExamSubjectResult = function (req, res) {
    const { examSubjectResultID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ examSubjectResultID }, examValidations.deleteExamSubjectResult, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_exam_subject_result(${utils.OptionalKey(examSubjectResultID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    } 
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
        }
    })
}

exports.deleteExamNonSubjectResult = function (req, res) {
    const { examNSResultID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ examNSResultID }, examValidations.deleteExamNonSubjectResult, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_exam_nsresults(${utils.OptionalKey(examNSResultID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
  
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
                
            })
        }
    })
}

exports.getExamResultReport = function (req, res) {
    const { examsTermID, classID, studentID, subjectID, nonSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ examsTermID, classID, studentID, subjectID, nonSubjectID }, examValidations.getExamResultReport,function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery = `   SELECT * FROM v1_get_exam_results_report 
                                WHERE classID = '${classID}' AND examsTermID = '${examsTermID}' `;
                        if (studentID)
                            dbquery += ` and studentID='${studentID}' `;
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, firstViewData, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'firstViewData', firstViewData)
                                    if (_.isEmpty(firstViewData[0])) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        const examsTermID = firstViewData[0].examsTermID
                                        console.log(examsTermID)
                                        if (examsTermID != '' || examsTermID != null) {
                                            let dbquery = `  SELECT * FROM v1_get_exam_subject_results 
                                            WHERE examsTermID = '${examsTermID}' 
                                            and classID = '${classID}' `;
            
                                            if (studentID){
                                                dbquery +=  ` and studentID='${studentID}' `;
                                            }
                                            if (subjectID){
                                                dbquery += ` and subjectID = '${subjectID}' `;
                                            }
                                            console.log(dbquery)
                                            db.query(dbquery,function (error, secondViewData, fields) {
                                                    if(!error){
                                                        //log.doInfoLog(fileNameForLogging, req.url, 'secondViewData', secondViewData)
                                                        let dbquery = `  SELECT * FROM v1_get_exam_ns_results 
                                                                    WHERE examsTermID = '${examsTermID}' 
                                                                    and classID = '${classID}'  `

                                                        if (studentID){
                                                            dbquery +=  ` and studentID='${studentID}' `;
                                                        }
                                                        if (nonSubjectID){
                                                            dbquery +=  ` and nonSubjectID = '${nonSubjectID}' `;
                                                        }
                                                        console.log(dbquery)
                                                        db.query(dbquery,function (error, thirdViewData, fields) {
                                                                if(!error){
                                                                    let dbquery = `select * from v1_get_exam_attendance where studentID='${studentID}' and examsTermID = '${examsTermID}' `
                                                                    console.log(dbquery)
                                                                    db.query(dbquery,function (error, attendanceData, fields) {
                                                                            if(!error){
                                                                            
                                                                                if (_.isEmpty(attendanceData)) {
                                                                                    log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                                                                    
                                                                                } 
                                                                                else {
                                                                                    log.doInfoLog(fileNameForLogging,   req.url,  "successfully data fetched from db" );
                                                                                    
                                                                                }
                                                                                    const examResultsOverallResponse = firstViewData
                                                                                    const examSubjectsResultResponse = secondViewData
                                                                                    const examNsResultResponse = thirdViewData
                                                                                    const examAttendanceResponse = attendanceData
                                                                                    if(!res.finished)res.status(200).json({
                                                                                        response: [
                                                                                            {
                                                                                                examResultsOverallResponse
                                                                                            },
                                                                                            {
                                                                                                examSubjectsResultResponse: examSubjectsResultResponse.map((value, index, examSubjectsResultResponse) => {
                                                                                                    return {
                                    
                                                                                                        subjectName: value.subjectName,
                                                                                                        totalMarks: value.totalMarks,
                                                                                                        obtainedMarks: value.obtainedMarks,
                                                                                                        showMarksOnApp: value.showMarksOnApp,
                                                                                                        grade: value.grade,
                                                                                                        subjectID: value.subjectID,
                                                                                                        notes: value.notes
                                                                                                    }
                                                                                                })
                                                                                            },
                                                                                            {
                                                                                                examNsResultResponse: examNsResultResponse.map((value, index, examNsResultResponse) => {
                                                                                                    return {
                                                                                                        nonSubjectName: value.nonSubjectName,
                                                                                                        gradeNS: value.gradeNS,
                                                                                                        nonSubjectID: value.nonSubjectID,
                                                                                                        nsLegendID:value.nsLegendID,
                                                                                                        legendValue: value.legendValue,
                                                                                                        notesNS: value.notesNS
                                                                                                    }
                                                                                                })
                                                                                            },
                                                                                            {
                                                                                                examAttendanceResponse: examAttendanceResponse.map((value, index, examAttendanceResponse) => {
                                                                                                    return {
                                                                                                        TotalDays: value.TotalDays,
                                                                                                        Present: value.Present,
                                                                                                        Absent: value.Absent,
                                                                                                        Late:value.Late,                                                                                                      
                                                                                                        Leaves: value.Leaves
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                        ]
                                                                                    })
                                                                    
                                                                            }
                                                                            else{
                                                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                                                console.log(errorResponse)
                                                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                                                            }
                                                                        });
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                                    console.log(errorResponse)
                                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                                                }
                                                            });
                                                        
                                                    }
                                                    else{
                                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                        console.log(errorResponse)
                                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                                    }
                                                });
                                        }
                                    }
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}


exports.deleteQuiz = function (req, res) {
    const { quizID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ quizID},examValidations.deleteQuiz, function (err, value) {
            if (!err) {
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=`   set @quizID = ${utils.OptionalKey(quizID)};
                                        call ${appConfig.getDbConfig().database}.v1_delete_quiz
                                        (@quizID);`
                        try {
                            db.query(dbquery, function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                    
                })
            } 
            
            else {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            }
        })
}




exports.getExamResultsMobile= function (req, res) {
    const { examsTermID, classID, studentUUID, subjectID, nonsubjectID, fromDate, toDate } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ examsTermID, classID, studentUUID, subjectID, nonsubjectID, fromDate, toDate},examValidations.getExamResultsMobile,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbqueryOverall, dbquerySubjectExamResults,dbqueryNonSubjectExamResults, dbqueryAttendanceReport
              
           
              try {
                async.parallel([
                  function(callback){
                    dbqueryOverall = `  set @examsTermID = ${utils.OptionalKey(examsTermID)}; 
                                        set @classID = ${utils.OptionalKey(classID)};
                                        set @studentUUID = ${utils.OptionalKey(studentUUID)}; 
                                        call ${appConfig.getDbConfig().database}.v1_get_exam_overall_mobile
                                        (@examsTermID,@classID,@studentUUID);`
                    console.log(dbqueryOverall)
                    db.query(dbqueryOverall, function (error, overallResults) {
                          callback(error,overallResults)
                      });
                  },
                  function(callback){
                    dbquerySubjectExamResults=`set @examsTermID = ${utils.OptionalKey(examsTermID)}; 
                                                set @classID = ${utils.OptionalKey(classID)};
                                                set @studentUUID = ${utils.OptionalKey(studentUUID)}; 
                                                set @subjectID = ${utils.OptionalKey(subjectID)}; 
                                                call ${appConfig.getDbConfig().database}.v1_get_exam_subject_mobile
                                                (@examsTermID,@classID,@studentUUID,@subjectID);`    
                    console.log(dbquerySubjectExamResults)
                    db.query(dbquerySubjectExamResults, function (error, examSubjectResults) { 
                          callback(error,examSubjectResults)
                      });
                  },
                  function(callback){
                    dbqueryNonSubjectExamResults = `set @examsTermID = ${utils.OptionalKey(examsTermID)}; 
                                                    set @classID = ${utils.OptionalKey(classID)};
                                                    set @studentUUID = ${utils.OptionalKey(studentUUID)}; 
                                                    set @nonsubjectID = ${utils.OptionalKey(nonsubjectID)}; 
                                                    call ${appConfig.getDbConfig().database}.v1_get_exam_nonsubject_mobile
                                                    (@examsTermID,@classID,@studentUUID,@nonsubjectID)`;
                        
                    console.log(dbqueryNonSubjectExamResults)
                    db.query(dbqueryNonSubjectExamResults, function (error, examNonSubjectResults) { 
                          callback(error,examNonSubjectResults)
                      });
                  },
                  function(callback){
                    dbqueryAttendanceReport =` set @classID = ${utils.OptionalKey(classID)};
                                                set @studentUUID = ${utils.OptionalKey(studentUUID)}; 
                                                set @examsTermID = ${utils.OptionalKey(examsTermID)}; 
                                                set @fromDate = ${utils.OptionalKey(fromDate)}; 
                                                set @toDate = ${utils.OptionalKey(toDate)};
      
                                        call ${appConfig.getDbConfig().database}.v1_get_term_attendance
                                        (@studentUUID,@examsTermID,@classID,@fromDate,@toDate);`
                    console.log(dbqueryAttendanceReport)
                    db.query(dbqueryAttendanceReport, function (error, attendanceResults) { 
                          callback(error,attendanceResults)
                      });
                  }

          
                  ],function(err,results){
                      if(err){
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting the data', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                      else{
                        let overallOutcome, subjectExamOutcome, nonsubjectExamOutcome, attendnaceOutcome
                        results[0].forEach(element => {
                            if(element.constructor==Array) {
                                overallOutcome=element
                            }
                        })
                        results[1].forEach(element => {
                            if(element.constructor==Array) {
                                subjectExamOutcome=element
                            }
                        })
                        results[2].forEach(element => {
                            if(element.constructor==Array) {
                                nonsubjectExamOutcome=element
                            }
                        })
                        results[3].forEach(element => {
                            if(element.constructor==Array) {
                                attendnaceOutcome=element
                            }
                        })

                        if(!res.finished)res.status(200).json({overallExamResults : overallOutcome,
                                                                subjectExamResults: subjectExamOutcome,
                                                                nonsubjectExamResults: nonsubjectExamOutcome,
                                                                attendanceReport: attendnaceOutcome });
                      }
                  })
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };