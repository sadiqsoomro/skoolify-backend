const db = require('../../../db'),
    Joi = require("joi"),
    utils = require("../../utils/helper-methods"),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    tabletValidations = require('../validations/validations-tablet'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging = 'ads-controller'
import Error from '../response-classes/error'
const push = require("../../utils/push-notification");
// async function pushNotificationArray(registrationIDs, messageConfig) {
//     return await push.SendNotification(registrationIDs, messageConfig);
//   }

exports.getUserTablet = function (req, res) {
    const {username, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ username, branchID }, tabletValidations.getUserTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                        set @username = ${utils.OptionalKeyString(username)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        call ${appConfig.getDbConfig().database}.v1_get_user_tablet
                        (@username,@branchID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.NO_USER_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element })
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}



exports.getUserPasswordTablet = function (req, res) {
    const {userUUID, schoolID,branchID, password, deviceToken, sourceUUID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ userUUID, schoolID,branchID, password, deviceToken, sourceUUID }, tabletValidations.getUserPasswordTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                        set @userUUID = ${utils.OptionalKey(userUUID)};
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @password = ${utils.OptionalKeyString(utils.EncryptAES(password))};
                        set @deviceToken = ${utils.OptionalKey(deviceToken)};
                        set @sourceUUID = ${utils.OptionalKey(sourceUUID)};
                        call ${appConfig.getDbConfig().database}.v1_get_user_tablet_verify
                        (@userUUID, @schoolID,@branchID, @password, @deviceToken, @sourceUUID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                       
                                    if (_.isEmpty(element)) {

                                        
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        
                                        
                                        if(element[0].message=='successful'){
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({ response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
                                        }
                                    }
                                   }
                                   
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}



exports.getTimeTableTablet = function (req, res) {
    const {schoolID,branchID, weekdayNo,userUUID,classID,  subjectID, roomID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, weekdayNo,userUUID, classID, subjectID, roomID }, tabletValidations.getTimeTableTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`                        
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @weekdayNo = ${utils.OptionalKey(weekdayNo)};
                        set @userUUID = ${utils.OptionalKey(userUUID)};
                        set @classID = ${utils.OptionalKey(classID)};
                        set @subjectID = ${utils.OptionalKey(subjectID)};
                        set @roomID = ${utils.OptionalKey(roomID)};
                        call ${appConfig.getDbConfig().database}.v1_get_tablet_timetable
                        ( @schoolID,@branchID, @weekdayNo, @userUUID, @classID, @subjectID, @roomID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element })
                                       
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.getClassesTablet = function (req, res) {
    const {schoolID,branchID, weekdayNo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, weekdayNo }, tabletValidations.getClassesTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`                        
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @weekdayNo = ${utils.OptionalKey(weekdayNo)};
                       
                        call ${appConfig.getDbConfig().database}.v1_get_tablet_classes
                        ( @schoolID,@branchID, @weekdayNo);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element })
                                       
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}





exports.getTeacherTablet = function (req, res) {
    const {schoolID,branchID, weekdayNo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, weekdayNo }, tabletValidations.getTeacherTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`                        
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @weekdayNo = ${utils.OptionalKey(weekdayNo)};
                       
                        call ${appConfig.getDbConfig().database}.v1_get_tablet_teachers
                        ( @schoolID,@branchID, @weekdayNo);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        if(element[0].userUUID!=null){
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({ response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
                                        }
                                       
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}



exports.getRoomTablet = function (req, res) {
    const {schoolID,branchID, weekdayNo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, weekdayNo }, tabletValidations.getRoomTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`                        
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @weekdayNo = ${utils.OptionalKey(weekdayNo)};
                       
                        call ${appConfig.getDbConfig().database}.v1_get_tablet_rooms
                        ( @schoolID,@branchID, @weekdayNo);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        if(element[0].roomID!=null){
                                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                            if(!res.finished)res.status(200).json({ response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
                                        }
                                       
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}




exports.getSubjectTablet = function (req, res) {
    const {schoolID,branchID, weekdayNo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,branchID, weekdayNo }, tabletValidations.getSubjectTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {

            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`                        
                        set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @weekdayNo = ${utils.OptionalKey(weekdayNo)};
                       
                        call ${appConfig.getDbConfig().database}.v1_get_tablet_subjects
                        ( @schoolID,@branchID, @weekdayNo);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                
                                data.forEach(element => {
                                   if(element.constructor==Array) {
                                    if (_.isEmpty(element)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({ response: element })
                                       
                                    }
                                   }
                               })
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setStudentAttendanceTablet = function (req, res) {
    const { array } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate(array, tabletValidations.setStudentAttendanceTablet, function (err,value) {
      if (err) {log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
  
        db.getConnection(function(err, db) {
          if(!err){
            if(array != undefined){
  
              let dbquery = "";
              var var_timetableID = array[0].timetableSubjectID
              for (let i = 0; i < array.length; i++) {
                dbquery =
                  dbquery +
                  ` call ${appConfig.getDbConfig().database}.v1_set_tablet_attendance
                  (${utils.OptionalKey(array[i].attendanceID)}, 
                  ${utils.OptionalKey(array[i].attendanceDate)}, 
                  ${utils.OptionalKey(array[i].batchNo)}, 
                  ${utils.OptionalKey(array[i].timetableSubjectID)}, 
                  ${utils.OptionalKey(array[i].classID)},
                  ${utils.OptionalKey(array[i].studentID)},
                  ${utils.OptionalKey(array[i].isPresent)},
                  ${utils.OptionalKeyString(array[i].notes)}, 
                  ${utils.OptionalKey(array[i].userID)} );  `;
              }
              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                    if(!res.finished)res.status(200).json({ response: "success" });
                    let attendanceAlertArray=[
                                            [],
                                            [],
                                            [],
                                            []];
                    
                    // index 0=Abset, 1=Off, 2=Leave, 3=Late
                    for (let i = 0; i < array.length; i++) {
                      
                      if(array[i].isPresent=='A'){
                        //Absent
                        // console.log('Absent Loop')
                        attendanceAlertArray[0].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='O'){
                        // console.log('Off Loop')
                        // Off Day
                        attendanceAlertArray[1].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='L'){
                        // console.log('Leave Loop')
                        // Leave
                        attendanceAlertArray[2].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='T'){
                        //Late
                        // console.log('Late Loop')
                        attendanceAlertArray[3].push(array[i].studentID)
                      }
                    }
                    
                      if (attendanceAlertArray[0].length > 0){
                      //Absent for 0 index of sub array
                      console.log('Absent Notification')
                      let dbAbsentQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[0].join(",")}) `;
                      console.log(dbAbsentQuery)

                      db.query(dbAbsentQuery,function (error, data, fields) {
                            if(!error){
                              const studentIDs = data
                              let messageConfig, messageText
                              //const regIDs = [];
                              Object.keys(studentIDs).forEach(stud => {
                                //regIDs.push(studentIDs[stud].deviceToken);
                                if (studentIDs[stud].preferredLanguage=='ar'){
                                  messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                                  " غیر حاضر (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                                }
                                else {
                                  messageText = studentIDs[stud].studentName +
                                    " has been marked Absent for the day (" + utils.formatReadableDate(array[0].attendanceDate,'en') + ")";
                                }
                                  console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                                  messageConfig = appConfig.messageSettings(
                                    "Attendance",
                                    messageText,
                                    "AttendanceScreen",
                                    studentIDs[stud].parentID, 
                                    studentIDs[stud].studentID,
                                    studentIDs[stud].classID,
                                    studentIDs[stud].branchID,
                                    studentIDs[stud].schoolID,
                                    null
                                    );
                                    push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                    // if (regIDs.length > 0)
                                    //   pushNotificationArray(regIDs, messageConfig)
                                    //     .then(d => { })
                                    //     .catch(err => {
                                    //       console.log(  "Error in sending Push notification Array"  );
                                    //       //log.doFatalLog(   fileNameForLogging,  req.url,  "Error in sending Push notification Array",  err );
                                      
                                    //     });

                              });
                            }
                            else{
                              log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            }
                        });
                      }
                      if (attendanceAlertArray[1].length > 0){
                      // Day Off for 1 index of sub array
                      console.log('Day Off Notification')
                      let dbOffQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[1].join(",")}) `;                      
                      console.log(dbOffQuery)
                      db.query(dbOffQuery,function (error, data, fields) {
                            if(!error){
                            const studentIDs = data
                            let messageConfig, messageText
                            //const regIDs = [];
                            Object.keys(studentIDs).forEach(stud => {
                              //regIDs.push(studentIDs[stud].deviceToken);
                            
                              if (studentIDs[stud].preferredLanguage=='ar'){
                                messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                                " يوم عطلة (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                              }
                              else{
                                messageText = studentIDs[stud].studentName +
                                " has been given Off for " + utils.formatReadableDate(array[0].attendanceDate,'en');
                              }
                              console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                              messageConfig = appConfig.messageSettings(
                                  "Attendance",
                                  messageText,
                                  "AttendanceScreen",
                                  studentIDs[stud].parentID, 
                                  studentIDs[stud].studentID,
                                  studentIDs[stud].classID,
                                  studentIDs[stud].branchID,
                                  studentIDs[stud].schoolID,
                                  null
                                  );
                              push.SendNotification(studentIDs[stud].deviceToken, messageConfig);

                              });
                                  // if (regIDs.length > 0)
                                    
                                  
                                    // pushNotificationArray(regIDs, messageConfig)
                                    //   .then(d => { })
                                    //   .catch(err => {
                                    //     console.log(  "Error in sending Push notification Array"  );
                                    
                            // });
                           
                        }
                        else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        }
                    });
                    
                      }
                      if (attendanceAlertArray[2].length > 0){
                      // Leave for 2 index of sub array
                      console.log('Leave Notification')
                      let dbLeaveQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[2].join(",")}) `;
                      console.log(dbLeaveQuery)

                      db.query(dbLeaveQuery,function (error, data, fields) {
                              if(!error){
                              const studentIDs = data
                              let messageConfig, messageText
                              // const regIDs = [];
                              Object.keys(studentIDs).forEach(stud => {
                                // regIDs.push(studentIDs[stud].deviceToken);
                                if (studentIDs[stud].preferredLanguage=='ar'){
                                  messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                                  " على الرخصة (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                                }
                                else{
                                  messageText = studentIDs[stud].studentName +
                                    " has been marked Leave on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                                }
                                console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                                messageConfig = appConfig.messageSettings(
                                    "Attendance",
                                    messageText,
                                    "AttendanceScreen",
                                    studentIDs[stud].parentID, 
                                    studentIDs[stud].studentID,
                                    studentIDs[stud].classID,
                                    studentIDs[stud].branchID,
                                    studentIDs[stud].schoolID,
                                    null
                                    );
                                    push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                    // if (regIDs.length > 0)
                                    //   pushNotificationArray(regIDs, messageConfig)
                                    //     .then(d => { })
                                    //     .catch(err => {
                                    //       console.log("Error in sending Push notification Array"  );
                                    //       //log.doFatalLog(fileNameForLogging,  req.url,  "Error in sending Push notification Array",  err );
                                    // });
                              });
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          }
                      });
                      }
                      if (attendanceAlertArray[3].length > 0){
                      // Late for 3 index of sub array
                      console.log('Late Notification')
                      let dbLateQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[3].join(",")}) `;
                      console.log(dbLateQuery)
                      db.query(dbLateQuery,function (error, data, fields) {
                            if(!error){
                            const studentIDs = data
                            let messageConfig, messageText
                            // const regIDs = [];
                            Object.keys(studentIDs).forEach(stud => {
                              // regIDs.push(studentIDs[stud].deviceToken);
                              if (studentIDs[stud].preferredLanguage=='ar'){
                                messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                                " متأخر (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                              }
                              else{
                                messageText = studentIDs[stud].studentName +
                                " has been marked Late for " + utils.formatReadableDate(array[0].attendanceDate,'en');
                              }
                              // console.log(messageText + ' : ' + studentIDs[stud].deviceToken)

                              messageConfig = appConfig.messageSettings(
                                  "Attendance",
                                  messageText,
                                  "AttendanceScreen",
                                  studentIDs[stud].parentID, 
                                  studentIDs[stud].studentID,
                                  studentIDs[stud].classID,
                                  studentIDs[stud].branchID,
                                  studentIDs[stud].schoolID,
                                  null
                                  );
                                  push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                  // if (regIDs.length > 0)
                                  //   pushNotificationArray(regIDs, messageConfig)
                                  //     .then(d => { })
                                  //     .catch(err => {
                                  //       console.log(  "Error in sending Push notification Array"  );
                                  //       //log.doFatalLog(fileNameForLogging,  req.url,"Error in sending Push notification Array",  err );                                    
                                  // });

                            });
                        }
                        else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        }
                      });
                      }
 
                  }
                  else{
                    log.doInfoLog( fileNameForLogging, req.url,   "no absent notificaiton",    error  );
                    if(!res.finished)res.status(500).json({ response: "error in database query" })
                  }
              
                });
            } 
              finally {
                  db.release();
                  console.log('released')
              }
              
                
    
    
    }
              else{
                log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                if(!res.finished)res.status(500).json({ response: "'array not found'" })
    
                }
          
          }
          else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    });
  };

  exports.getClassStudentsTablet = function (req, res) {
    const { timetableSubjectID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ timetableSubjectID }, tabletValidations.getClassStudentsTablet, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
            if(!err){
            let dbquery =` call ${appConfig.getDbConfig().database}.v1_get_tablet_class_students 
                            (${utils.OptionalKey(timetableSubjectID)});  `;
            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                        
                        data.forEach(element => {
                            if(element.constructor==Array) {
                             if (_.isEmpty(element)) {
                                 log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                 const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                 if(!res.finished)res.status(404).json({ response: errorResponse })
                             } else {
                                 log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                 if(!res.finished)res.status(200).json({ response: element })
                                
                             }
                            }
                        })

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                }); 
            } 
            finally {
                db.release();
                console.log('released')
            }
            
    
            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
    
            }
            })
        }
    })
}

