const Joi = require("joi"),
    utils = require("../../utils/helper-methods"),
    paymentsValidations = require("../validations/validations-payments"),
    db = require("../../../db"),
    _ = require("lodash"),
    appConfig = require("../../../app-config"),
    log = require("../../utils/utils-logging"),
    helper = require("../../utils/helper-methods"),
    async = require('async'),
    axios = require("axios"),
    qs = require('query-string'),
    httpsAgent = require("https-agent"),
    emails = require('../../utils/node-mailer'),
    fileNameForLogging = "payments-controller",
    push = require("../../utils/push-notification");
import Error from "../response-classes/error";
import { isEmpty } from "lodash";
import { ZOHO_SECRET, ZOHO_ORGID, ZOHO_INVOICE_URL, ZOHO_CONTACT_URL } from "../../../app-config";
//import { formatReadableDate } from "../../utils/helper-methods";

exports.getPaymentProviderDetails= function (req, res) {
    const { providerID,sourceUUID,parentUUID,voucherUUID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ providerID,sourceUUID,parentUUID,voucherUUID},paymentsValidations.getPaymentProviderDetails,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });

        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let procedureName;
              if (providerID==1)
                procedureName = 'v1_get_payment_provider_easypay'
              else if (providerID==11)
                procedureName = 'v1_get_payment_provider_easyaccount'
              else if (providerID==21)
              procedureName = 'v1_get_payment_provider_konnect'
              else if (providerID==31)
                procedureName = 'v1_get_payment_provider_hbl_account'
              else if (providerID==41)
                procedureName = 'v1_get_payment_provider_jazzcash'
              else if (providerID==51)
                procedureName = 'v1_get_payment_provider_jazzacc'
              else if (providerID==61)
              procedureName = 'v1_get_payment_provider_hblcc'
              else if (providerID==71)
                procedureName = 'v1_get_payment_provider_simsim'
              else if (providerID==81)
                procedureName = 'v1_get_payment_provider_payfast'
              else if (providerID==82)
                procedureName = 'v1_get_payment_provider_payfast'
              else if (providerID==91)
                procedureName = 'v1_get_payment_provider_nift'
              else if (providerID==92)
                procedureName = 'v1_get_payment_provider_nift'
              else  
                procedureName = 'v1_get_payment_provider_unknown'

              let dbquery=`set @providerID = ${utils.OptionalKey(providerID)} ;
                          set @sourceUUID = ${utils.OptionalKey(sourceUUID)};  
                          set @parentUUID = ${utils.OptionalKey(parentUUID)}; 
                          set @voucherUUID = ${utils.OptionalKey(voucherUUID)}; 
                          call ${appConfig.getDbConfig().database}.${procedureName}
                          (@providerID,@sourceUUID,@parentUUID,@voucherUUID);`
            
              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                       
                      data.forEach(element => {
                        if(element.constructor==Array) {

                          if (_.isEmpty(element)) {
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(404).json({ response: errorResponse })
                          } 
                          else{
                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            if(!res.finished)res.status(200).json({response: element })

                          }
                        }
                           
                       })
        
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              }
              

        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };

  exports.setPaymentTransactionLogWeb = function (req, res) {
    const {providerID,sourceUUID,parentUUID,userUUID,IPAddress, providerTxnRefNo,internalTxnRefNo,
      maskedInstrument,paymentCurrency,paymentAmount,tranRequestDateTime,tranRequestCode,additionalData,
      tranResponseDateTime, tranResponseCode,tranResponseData} = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({providerID,sourceUUID,parentUUID,userUUID,IPAddress, providerTxnRefNo,internalTxnRefNo,
      maskedInstrument,paymentCurrency,paymentAmount,tranRequestDateTime,tranRequestCode,additionalData,
      tranResponseDateTime, tranResponseCode,tranResponseData},paymentsValidations.setPaymentTransactionLogWeb,function (err, value) {
        if (!err) {
          db.getConnection(function(err, db) {
            if(!err){
              
              let dbquery=`set @providerID = ${utils.OptionalKey(providerID)} ;
                          set @sourceUUID = ${utils.OptionalKey(sourceUUID)};  
                          set @parentUUID = ${utils.OptionalKey(parentUUID)}; 
                          set @userUUID = ${utils.OptionalKey(userUUID)};                     
                          set @IPAddress = ${utils.OptionalKey(IPAddress)}; 
                          set @providerTxnRefNo = ${utils.OptionalKey(providerTxnRefNo)};
                          set @internalTxnRefNo = ${utils.OptionalKey(internalTxnRefNo)};
                          set @maskedInstrument = ${utils.OptionalKey(maskedInstrument)};
                          set @paymentCurrency = ${utils.OptionalKey(paymentCurrency)};
                          set @paymentAmount = ${utils.OptionalKey(paymentAmount)};
                          set @tranRequestDateTime = ${utils.OptionalKey(tranRequestDateTime)};
                          set @tranRequestCode = ${utils.OptionalKey(tranRequestCode)};
                          set @additionalData = ${utils.OptionalKey(additionalData)};
                          set @tranResponseDateTime = ${utils.OptionalKey(tranResponseDateTime)};
                          set @tranResponseCode = ${utils.OptionalKey(tranResponseCode)};
                          set @tranResponseData = ${utils.OptionalKey(tranResponseData)};
                          call ${appConfig.getDbConfig().database}.v1_set_payment_logs_web
                          (@providerID,@sourceUUID,@parentUUID,@userUUID,@IPAddress, @providerTxnRefNo,@internalTxnRefNo,
                            @maskedInstrument,@paymentCurrency,@paymentAmount,@tranRequestDateTime,@tranRequestCode,@additionalData,
                            @tranResponseDateTime, @tranResponseCode,@tranResponseData);`
                          console.log(dbquery)
                          try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    emails.SendEmail('payment.alerts@infinitystudio.pk','Successful Payment Alert',dbquery)
                                     
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                    emails.SendEmail('payment.alerts@infinitystudio.pk','Failed Attempt: Payment Alert',dbquery)
                                }
                            });
                          } 
                          finally {
                              db.release();
                              console.log('released')
                          }
                          

            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }
          })
          
        } 
        
        else {
          log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        }
      }
    );
  };
  
  exports.getPaymentVoucherCharges= function (req, res) {
    const { providerID, bankUUID,voucherUUID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ providerID, bankUUID,voucherUUID},paymentsValidations.getPaymentVoucherCharges,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`set @providerID = ${utils.OptionalKey(providerID)};
                        set @bankUUID = ${utils.OptionalKey(bankUUID)};
                        set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
                        call ${appConfig.getDbConfig().database}.v1_get_voucher_charges
                        (@providerID,@bankUUID,@voucherUUID);`
            console.log(dbquery)
            
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                          //console.log(data)
                          data.forEach(element => {
                            if(element.constructor==Array) {
                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(404).json({ response: errorResponse })
                              } 
                              else{
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                //if(element[0].endDate) > 
                                if(!res.finished)res.status(200).json({response: element[0] })
                              }
                            }
                              
                          })
                          log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                          if(!res.finished)res.status(404).json({ response: errorResponse })

                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
 
  exports.getBankPaymentVoucherCharges= function (req, res) {
    const { providerID, bankUUID,voucherUUID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ providerID, bankUUID,voucherUUID},paymentsValidations.getBankPaymentVoucherCharges,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`set @providerID = ${utils.OptionalKey(providerID)};
                        set @bankUUID = ${utils.OptionalKey(bankUUID)};
                        set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
                        call ${appConfig.getDbConfig().database}.v1_get_voucher_charges_banks
                        (@providerID,@bankUUID,@voucherUUID);`
            console.log(dbquery)
            
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                        
                      data.forEach(element => {
                        if(element.constructor==Array) {
                          //console.log(element[0])
                          if (!_.isEmpty(element)) {
                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            if(!res.finished)res.status(200).json({response: element[0] })
                          }
                          else {
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(404).json({ response: errorResponse })
                          }  
                        }
                      })
                      log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                      if(!res.finished)res.status(404).json({ response: errorResponse })

                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
 

  exports.getPaymentProviderTypes = function (req, res) {
    const { schoolID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ schoolID},paymentsValidations.getPaymentProviderTypes,function (err, value) {
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery = `SELECT * FROM v1_get_payment_provider_types  WHERE schoolID = '${schoolID}' `;
            
              console.log(dbquery)
              
                  try {
                    db.query(dbquery,function (error, data, fields) {
                      if(!error){
                        
                        if (_.isEmpty(data)) {
                          log.doErrorLog(fileNameForLogging, req.url, "data not present");
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present" );
                          res.status(404).json({ response: errorResponse });
                        } 
                        else {
                          log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                          return res.status(200).json({ response: data });
                        }
          
                      }
                      else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        res.status(500).json({ response: errorResponse })
                      }
                  });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
  
  
  exports.getPaymentProviders = function (req, res) {
    const { transactionTypeID, schoolID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ transactionTypeID,schoolID},paymentsValidations.getPaymentProviders,function (err, value) {
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`set @schoolID = ${utils.OptionalKey(schoolID)};
                          set @transactionTypeID = ${utils.OptionalKey(transactionTypeID)};
                          call ${appConfig.getDbConfig().database}.v1_get_payment_providers
                          (@schoolID,@transactionTypeID);`
              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      //console.log(data)
                      data.forEach(element => {
                        if(element.constructor==Array) {

                          if (_.isEmpty(element)) {
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(404).json({ response: errorResponse })
                          } 
                          else{
                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            if(!res.finished)res.status(200).json({response: element })

                            
                          }
                        }
                          
                      })
        
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              }
              

        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
  

  exports.getPaymentLogs = function (req, res) {
    const { internalTxnRefNo, tranRequestCode,paymentCurrency,paymentAmount,maskedInstrument,providerTxnRefNo } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ internalTxnRefNo, tranRequestCode,paymentCurrency,paymentAmount,maskedInstrument,providerTxnRefNo},paymentsValidations.getPaymentLogs,function (err, value) {
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery = `SELECT * FROM v1_get_payment_logs WHERE internalTxnRefNo = '${internalTxnRefNo}'  `;
            
              if (!_.isEmpty(tranRequestCode)) {
                dbquery += ` and tranRequestCode='${tranRequestCode}'`;
              }
              if (!_.isEmpty(paymentCurrency)) {
                dbquery += ` and paymentCurrency='${paymentCurrency}'`;
              }
              if (!_.isEmpty(paymentAmount)) {
                dbquery += ` and paymentAmount='${paymentAmount}'`;
              }
              if (!_.isEmpty(maskedInstrument)) {
                dbquery += ` and maskedInstrument LIKE '%${maskedInstrument}%'`;
              }
              if (!_.isEmpty(providerTxnRefNo)) {
                dbquery += ` and providerTxnRefNo='${providerTxnRefNo}'`;
              }

              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                       
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present" );
                        res.status(404).json({ response: errorResponse });
                      } 
                      else {
                        log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                        return res.status(200).json({ response: data });
                      }
        
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              }
              

        
            }
            else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };

  
  exports.setSchoolServiceChargesGenerate = function (req, res) {
    const {schoolID, monthNumber} = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({schoolID, monthNumber},paymentsValidations.setSchoolServiceChargesGenerate,function (err, value) {
        if (!err) {
          db.getConnection(function(err, db) {
            if(!err){
              
              let dbquery=`set @schoolID = ${utils.OptionalKey(schoolID)};
                          set @monthNumber = ${utils.OptionalKey(monthNumber)};
                          call ${appConfig.getDbConfig().database}.v1_set_school_charges_generate
                          (@schoolID,@monthNumber);`
                          console.log(dbquery)
                          try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                          } 
                          finally {
                              db.release();
                              console.log('released')
                          }
                          

            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }
          })
          
        } 
        
        else {
          log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        }
      }
    );
  };


  exports.getSchoolServiceCharges= function (req, res) {
    const { schoolID, branchID, userID,billingFromDate,billingToDate } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ schoolID, branchID, userID,billingFromDate,billingToDate},paymentsValidations.getSchoolServiceCharges,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery, firstResult, secondResult, thirdResult
              let billHeader = new Array();
              let billDetails = new Array();
           
              try {
                async.parallel([
                  function(callback){
                    dbquery=` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @userID = ${utils.OptionalKey(userID)};
                              set @billingFromDate = ${utils.OptionalKey(billingFromDate)};
                              set @billingToDate = ${utils.OptionalKey(billingToDate)};
                              call ${appConfig.getDbConfig().database}.v1_get_school_service_charges
                              (@schoolID,@branchID,@userID,@billingFromDate,@billingToDate);`
                    
                    console.log(dbquery)
                    db.query(dbquery, function (error, result1) {
                          callback(error,result1)
                      });
                  },
                  function(callback){
                    dbquery=` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @userID = ${utils.OptionalKey(userID)};
                              set @billingFromDate = ${utils.OptionalKey(billingFromDate)};
                              set @billingToDate = ${utils.OptionalKey(billingToDate)};
                              call ${appConfig.getDbConfig().database}.v1_get_school_charges_calculate
                              (@schoolID,@branchID,@userID,@billingFromDate,@billingToDate);`
                    console.log(dbquery)
                    db.query(dbquery, function (error, result2) { 
                          callback(error,result2)
                      });
                  },
                  function(callback){
                    dbquery=`     set @schoolID = ${utils.OptionalKey(schoolID)};
                                  set @branchID = ${utils.OptionalKey(branchID)};
                                  set @userID = ${utils.OptionalKey(userID)};
                                  set @billingFromDate = ${utils.OptionalKey(billingFromDate)};
                                  set @billingToDate = ${utils.OptionalKey(billingToDate)};
                                  call ${appConfig.getDbConfig().database}.v1_get_school_payment_provider
                                  (@schoolID,@branchID,@userID,@billingFromDate,@billingToDate);`
                    console.log(dbquery)
                    db.query(dbquery, function (error, result3) { 
                          callback(error,result3)
                      });
                  }

          
                  ],function(err,results){
                      if(err){
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting the data', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                      else{
                        results[0].forEach(element => {
                          if(element.constructor==Array) {
  
                            if (_.isEmpty(element)) {
                              firstResult=element
                            } 
                            else{
                              //firstResult = element

                              let noOfIterations = element.length;

                              for (let loop = 0; loop < noOfIterations; loop++) {
                                var detailData = Object.assign({}, element[loop]);
                                var headerData = Object.assign({}, element[loop]);
                                
                                  if (detailData.hasOwnProperty("invoiceDate")) {
                                    delete detailData.invoiceDate;
                                  }
                                  if (detailData.hasOwnProperty("invoiceNumber")) {
                                    delete detailData.invoiceNumber;
                                  }
                                  if (detailData.hasOwnProperty("dueDate")) {
                                    delete detailData.dueDate;
                                  }
                                  if (detailData.hasOwnProperty("groupName")) {
                                    delete detailData.groupName;
                                  }
                                  if (detailData.hasOwnProperty("paymentStatus")) {
                                    delete detailData.paymentStatus;
                                  }
                                  if (detailData.hasOwnProperty("billingFromDate")) {
                                    delete detailData.billingFromDate;
                                  }
                                  if (detailData.hasOwnProperty("billingToDate")) {
                                    delete detailData.billingToDate;
                                  }
                                  if (detailData.hasOwnProperty("schoolName")) {
                                    delete detailData.schoolName;
                                  }
                                  if (detailData.hasOwnProperty("billingFrequency")) {
                                    delete detailData.billingFrequency;
                                  }
                                  if (detailData.hasOwnProperty("schoolID")) {
                                    delete detailData.schoolID;
                                  }
                                  billDetails.push(detailData);

                                  if (headerData.hasOwnProperty("branchID")) {
                                    delete headerData.branchID;
                                  }
                                  if (headerData.hasOwnProperty("branchName")) {
                                    delete headerData.branchName;
                                  }
                                  if (headerData.hasOwnProperty("studentCount")) {
                                    delete headerData.studentCount;
                                  }
                                  if (headerData.hasOwnProperty("unitPrice")) {
                                    delete headerData.unitPrice;
                                  }
                                  if (headerData.hasOwnProperty("taxRate")) {
                                    delete headerData.taxRate;
                                  }
                                  if (headerData.hasOwnProperty("currencyAlphaCode")) {
                                    delete headerData.currencyAlphaCode;
                                  }
                                  if (headerData.hasOwnProperty("totalAmountWithoutTax")) {
                                    delete headerData.totalAmountWithoutTax;
                                  }
                                  if (headerData.hasOwnProperty("taxAmount")) {
                                    delete headerData.taxAmount;
                                  }
                                  if (headerData.hasOwnProperty("totalAmountIncludingTax")) {
                                    delete headerData.totalAmountIncludingTax;
                                  }
                                  if (_.isEmpty(billHeader)){
                                    billHeader.push(headerData);
                                  }
                                  
                                  
                              }

                            }
                          }
                             
                         })

                         results[1].forEach(element => {
                          if(element.constructor==Array) {
  
                            if (_.isEmpty(element)) {
                              secondResult=element
                            } 
                            else{
                              secondResult = element
                            }
                          }
                             
                         })

                         results[2].forEach(element => {
                          if(element.constructor==Array) {
  
                            if (_.isEmpty(element)) {
                              thirdResult=element
                            } 
                            else{
                              thirdResult = element
                            }
                          }
                             
                         })


                        if (_.isEmpty(billDetails) || _.isEmpty(secondResult)) {
                            
                          log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                          if(!res.finished)res.status(404).json({ response: errorResponse })
                        }
                        else{
                          if(!res.finished)res.status(200).json({chargesHeader: billHeader, 
                                                                 chargesDetails: billDetails, 
                                                                 totalValues: secondResult,
                                                                 paymentProvider: thirdResult
 
                          });
                        }
                      }
                  })
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
 


  exports.getSchoolOutstandingList= function (req, res) {
    const { schoolID, branchID, userID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ schoolID, branchID, userID},paymentsValidations.getSchoolOutstandingList,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @userID = ${utils.OptionalKey(userID)};
                        
                        call ${appConfig.getDbConfig().database}.v1_get_school_outstanding_list
                        (@schoolID,@branchID,@userID);`
            console.log(dbquery)
            
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                          data.forEach(element => {
                            if(element.constructor==Array) {

                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(200).json({ response: errorResponse })
                              } 
                              else{
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                //if(element[0].endDate) > 
                                if(!res.finished)res.status(200).json({response: element })

                                
                              }
                            }
                              
                          })
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };



  exports.setCreateInvoicesInERP= function (req, res) {
    const { schoolID, monthNumber } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({ schoolID, monthNumber},paymentsValidations.setCreateInvoicesInERP,function (err, value) {
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              
              try {
                      let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                                      set @monthNumber = ${utils.OptionalKey(monthNumber)};
                                      call ${appConfig.getDbConfig().database}.v1_get_created_invoices
                                      (@schoolID,@monthNumber);`
                      console.log(dbquery)
                      db.query(dbquery,function (error, data, fields) {
                        if(!error){
                              data.forEach(element => {
                                if(element.constructor==Array) {
                                  if (_.isEmpty(element)) {
                                    console.log ("No invoices to post on zoho erp")     
                                    if(!res.finished)res.status(200).json({ response: "success" })                                         
                                  } 
                                  else{
                                    
                                    var grouped = _.mapValues(_.groupBy(element, 'ERPCustomerID'),
                                    clist => clist.map(element => _.omit(element, 'ERPCustomerID')));
                                    //console.log(grouped)
                                    let ZohoURL=appConfig.ZOHO_TOKEN_URL + "?client_id=" + appConfig.ZOHO_CLIENTID + "&client_secret=" + appConfig.ZOHO_SECRET + "&redirect_uri=https://www.skoolify.app/zoho/print.php&refresh_token=" + appConfig.ZOHO_REFRESH_TOKEN +  "&grant_type=refresh_token&access_type=offline"
                                    console.log(ZohoURL)
                                    axios({
                                      method: "post",
                                      url: ZohoURL
                                    })

                                    .then(function (responseTokenRefresh) {
                                      
                                        if (responseTokenRefresh.status==200){
                                          
                                          var access_token=responseTokenRefresh.data.access_token
                                          
                                          if(access_token){
                                            for(var k in grouped) {
                                              let innerArray= new Array;
                                              for (let innerloop=0;innerloop< grouped[k].length; innerloop++){
                                                if (grouped[k][innerloop].taxRate=='0.00'){
                                                  innerArray.push(`{
                                                    "rate": "${grouped[k][innerloop].unitPrice}","name": "Skoolify Hosted Services Charges - ${grouped[k][innerloop].branchName}",
                                                    "quantity": "${grouped[k][innerloop].studentCount}","unit": "1"}`)
                                                }
                                                else{
                                                innerArray.push(`{
                                                  "rate": "${grouped[k][innerloop].unitPrice}","name": "Skoolify Hosted Services Charges - ${grouped[k][innerloop].branchName}",
                                                  "quantity": "${grouped[k][innerloop].studentCount}","unit": "1",
                                                  "tax_id": "1556535000000380169","tax_name": "SST (General Services)",
                                                  "tax_percentage": ${grouped[k][innerloop].taxRate},"tax_type": "tax"}`)
                                                }

                                              }

                                              let postJSONString=`{
                                                "payment_terms_label": "10 Days","customer_id": "${k}","date": "${grouped[k][0].invoiceDate}","due_date": "${grouped[k][0].dueDate}","is_inclusive_tax": "false","is_discount_before_tax": "true",
                                                "notes": "All invoices are monthly in advance",
                                                "terms": "-Invoices are generated in advance for each month.\\n- Services will automatically get suspended by the system, if the payment is not received by the 5th of each invoiced month.\\n- All taxes are applicable and subject to government laws. Any changes will be applied accordingly and/or retroactively based on any requisite changes.\\n- Payment should be made in the name of Infinity Studio (Pvt.) Ltd.",
                                                "allow_partial_payments": "false",
                                                "custom_fields": [
                                                    {
                                                        "value": "${utils.formatReadableDate(grouped[k][0].invoiceDate,'en',0)}",
                                                        "customfield_id": "1556535000000118124"
                                                    },
                                                    {
                                                      "value": "${grouped[k][0].taxID}",
                                                      "customfield_id": "1556535000000125001" 
                                                    }
                                                    
                                                ],
                                                "line_items": [`

                                                if(!_.isEmpty(innerArray)){
                                                  postJSONString+= innerArray.join(',')
                                                }
                                                
                                                postJSONString+=`]}`
                                                console.log(postJSONString) 
                                                axios({
                                                  method: "post",
                                                  url: ZOHO_INVOICE_URL + "?organization_id=" + ZOHO_ORGID,
                                                  
                                                  headers: {
                                                    "Authorization": "Zoho-oauthtoken " + access_token,
                                                    "Content-Type": "application/json"
                                                  },
                                                  data:JSON.parse(postJSONString)
                                                })
                                            
                                            
                                                .then(function (responseCreateInvoice) {
                                                    //console.log(responseCreateInvoice)
                                                    if (responseCreateInvoice.status==201){
                                                      
                                                      console.log(`${responseCreateInvoice.data.message} for ${responseCreateInvoice.data.invoice.customer_name} on ${responseCreateInvoice.data.invoice.date} that due for ${responseCreateInvoice.data.invoice.due_date}. Total Amount of ${responseCreateInvoice.data.invoice.currency_symbol} ${responseCreateInvoice.data.invoice.total}` )
                                                      
                                                      let dbquery = ` set @customerID = ${utils.OptionalKey(responseCreateInvoice.data.invoice.customer_id)};
                                                                      set @invoiceNumber = ${utils.OptionalKey(responseCreateInvoice.data.invoice.invoice_number)};
                                                                      set @invoiceDate = ${utils.OptionalKey(responseCreateInvoice.data.invoice.date)};
                                                                      call ${appConfig.getDbConfig().database}.v1_set_invoice_detail_from_zoho
                                                                      (@customerID,@invoiceNumber,@invoiceDate);`
                                                      console.log(dbquery)
                                                      db.query(dbquery,function (error, data, fields) {
                                                          if(!error){
                                                            console.log('invoice details updated in database')
                                                          }
                                                          else{
                                                            console.log('error updating invoice detail in database')
                                                          }
                                                      });



                                                    }
                                                    else{
                                                        console.log("error generating invoice for " + postJSONString)
                                                        //console.log(responseCreateInvoice.data)
                                                        emails.SendEmail(`ERP.Alerts@infinitystudio.pk`,`Error in Invoice Generation`,`Unable to generate invoice for  \n${postJSONString}`)
                                                    }


                                                    if(!res.finished)res.status(200).json({ response: "success" })
                                                    
                                                    
                                                })
                                              .catch(function (error) {
                                                console.log('error generating invoice in route setCreateInvoicesInERP' + error)
                                                console.log(postJSONString)
                                                console.log (responseCreateInvoice.data)    
                                                
                                              });
                                          }
  
                                        }  
                                        else{
                                          console.log('Error:got 200 but no refresh access token returned')
                                        }
                                      }
                                      else{
                                        console.log('Error in token refresh')
                                        
                                      }
                                    })
                                    .catch(function (error) {
                                      console.log('Unable to generate refresh access token')
                                      // console.log(responseTokenRefresh)
                                      console.log (error)            
                                    });
                                
                              }
                            }
                              
                          })
                    } 
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                  });

              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };

  exports.getCustomerIDfromERP= function (req, res) {
    const { customerName } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ customerName },paymentsValidations.getCustomerIDfromERP,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`select * from v1_get_erp_customer_id `
              if (customerName){
                dbquery = dbquery + ` where groupName='${customerName}'`
              }
            console.log(dbquery)
            let access_token
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      let ZohoURL=appConfig.ZOHO_TOKEN_URL + "?client_id=" + appConfig.ZOHO_CLIENTID + "&client_secret=" + appConfig.ZOHO_SECRET + "&redirect_uri=https://www.skoolify.app/zoho/print.php&refresh_token=" + appConfig.ZOHO_REFRESH_TOKEN +  "&grant_type=refresh_token&access_type=offline"
                      //console.log(ZohoURL)
                      axios({
                        method: "post",
                        url: ZohoURL
                      })

                      .then(function (responseA) {
                        if (responseA.status== 200 ){
                          access_token= responseA.data.access_token
                          if(access_token){
                              for (let i = 0; i < data.length; i++) {
                                console.log(data[i].groupName)
                                axios({
                                  method: "get",
                                  url: ZOHO_CONTACT_URL + "?organization_id=" + ZOHO_ORGID + "&contact_name=" + data[i].groupName,
                                  
                                  headers: {
                                      "Authorization": "Zoho-oauthtoken " + access_token,
                                      "Content-Type": "application/json"
                                  }
                                })
                            
                            
                                .then(function (responseB) {
                                    
                                    if (responseB.status== 200 && !_.isEmpty(responseB.data.contacts) ){
                                      let dbquery=` set @groupName = ${utils.OptionalKey(responseB.data.contacts[0].contact_name)};
                                                    set @ERPCustomerID = ${utils.OptionalKey(responseB.data.contacts[0].contact_id)};`
                                                    if(responseB.data.contacts[0].cf_ntn=='none' ){
                                                      dbquery += `set @taxID = ${utils.OptionalKey(null)};`
                                                    }
                                                    else{
                                                      dbquery += `set @taxID = ${utils.OptionalKey(responseB.data.contacts[0].cf_ntn)};`
                                                    }
                                                    
                                                    dbquery += `call ${appConfig.getDbConfig().database}.v1_set_erp_customer_id
                                                    (@groupName,@ERPCustomerID,@taxID);`
                                      console.log(dbquery)
                                      db.query(dbquery,function (error, data, fields) {
                                        if(!error){
                                          // do nothing
                                        }
                                        else{
                                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                          console.log(errorResponse )
                                        }
                                      })
                                      
                                      
                                    }
                                    else{
                                        console.log("no record in ERP found for ",data[i].groupName)
                                        emails.SendEmail(`ERP.Alerts@infinitystudio.pk`,`ERP ID Mapping Failed`,`Unable to map ERP ID for  ${data[i].groupName}`)
                                    }
                                    
                                    
                                })
                              .catch(function (error) {
                                console.log (error)    
                                
                              });

                              } // for loop end
                              if(!res.finished)res.status(200).json({response:"success"})
                            }  
                          else{
                            console.log('Error:got 200 but no refresh access token returned')
                          }
                        }
                      })
                      .catch(function (error) {
                        console.log('Unable to generate refresh access token')
                        console.log (error)            
                      });
                      
                          
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                })
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }

  exports.setInvoiceStatusUpdatefromERP= function (req, res) {
    const { invoiceNumber, paymentStatus } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({  invoiceNumber, paymentStatus},paymentsValidations.setInvoiceStatusUpdatefromERP,function (err, value) {
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`
                           set @invoiceNumber = ${utils.OptionalKey(invoiceNumber)};
                           set @paymentStatus = ${utils.OptionalKey(paymentStatus)};
                            call ${appConfig.getDbConfig().database}.v1_set_invoice_status_erp
                            (@invoiceNumber,@paymentStatus);`
            console.log(dbquery)
            
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      if(!res.finished)res.status(200).json({response: "success" })
                         
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
  
  exports.getAvanzaValidateCustomer= function (req, res) {
    const { schoolID,branchID,bankUUID,parentUUID, transactionAmount,cnicNumber, bankAccountNumber } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ schoolID,branchID,bankUUID,parentUUID,transactionAmount, cnicNumber, bankAccountNumber },paymentsValidations.getAvanzaValidateCustomer,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
                    let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @bankUUID = ${utils.OptionalKey(bankUUID)};
                              set @parentUUID = ${utils.OptionalKey(parentUUID)};
                              set @voucherUUID = null;
                              call ${appConfig.getDbConfig().database}.v1_get_provider_details_avanza
                              (@schoolID,@branchID,@bankUUID,@parentUUID,@voucherUUID);`
                    console.log(dbquery)
              let subMerchantID  ,subMerchantPassword, baseURL,bankCode, authKey, accountType, emailAddress, mobileNumber                
              try {
                  
                  db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    // console.log(data)
                      data.forEach(element => {
                            if(element.constructor==Array) {
                                if (!_.isEmpty(element)) {
                                    subMerchantID = element[0].subMerchantID
                                    subMerchantPassword = element[0].subMerchantPassword
                                    authKey = element[0].tokenAuthKey
                                    bankCode = element[0].bankCode
                                    accountType = element[0].accountType
                                    baseURL = element[0].baseURL
                                    emailAddress = element[0].emailAddress
                                    mobileNumber = element[0].mobileNumber
                                }
            
                            }
                          })
                        
                        let AvanzaTokeURL= baseURL + "/token"
                        console.log(AvanzaTokeURL)
                        axios({
                          method: "post",
                          url: AvanzaTokeURL,
                          headers: {
                            "content-type": "application/x-www-form-urlencoded"
                          },
                          data:qs.stringify({
                            grant_type:"client_credentials",
                            merchant_id:subMerchantID,
                            secured_key:subMerchantPassword
                          })
                        })
                        .then(function (appsResponse) {
                            // console.log(appsResponse.config.data)
                            // console.log(appsResponse.data)
                            if(appsResponse.data.code=="00"){
                                let AvanzaAccessToken = appsResponse.data.token
                                console.log('Got Token')
                                
                                let AvanzaCustomerValidateURL= baseURL + "/customer/validate"
                                let AvanzaBasketID = 'skoolify-'+ utils.RandomNumGenerator()
                                console.log(AvanzaCustomerValidateURL)
                                axios({
                                  method: "post",
                                  url: AvanzaCustomerValidateURL,
                                  headers: {
                                    "content-type": "application/x-www-form-urlencoded",
                                    "Authorization":"Bearer " + AvanzaAccessToken
                                  },
                                  data:qs.stringify({
                                    txnamt:transactionAmount,
                                    basket_id: AvanzaBasketID,
                                    customer_mobile_no: mobileNumber,
                                    customer_email_address: emailAddress,
                                    account_type_id:accountType,
                                    bank_code:bankCode,
                                    order_date: utils.formatDate(new Date),
                                    cnic_number:cnicNumber,
                                    account_number: bankAccountNumber
                                  })
                                })
                                .then(function (validationResponse) {
                                    console.log(validationResponse.config.data)
                                    console.log(validationResponse.data)
                                    if(validationResponse.data.code=="00"){
                                        if(!res.finished)res.status(200).json( { transactionRefNo:validationResponse.data.transaction_id,rdvResponse:validationResponse.data.rdv_message_key, basketID:AvanzaBasketID})
                                      } 
                                    
                                    else{
                                      if(!res.finished)res.status(500).json({ response: error.response.data.message })
                                    }
                                  
                                })
                                .catch(function (error) {
                                  console.log(error.config.data)
                                  if(error.response.data.message){
                                    if(!res.finished)res.status(400).json({ response: error.response.data.message })
                                  }
                                
                                  else{
                                    console.log(error)
                                    if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                                  }          
                                });
                                
                              }  
                            else{
                              if(!res.finished)res.status(500).json({ response: error.response.data.message })
                            }
                          
                        })
                        .catch(function (error) {
                          console.log(error)
                          if (error.response.status==400 ){
                            if(!res.finished)res.status(400).json({ response: error.response.data.message })
                          }
                          else{
                            if(!res.finished)res.status(500).json({ response: error.response.data.message })
                          }          
                        });
                            
                                
                          
                      
                  }

                  else{
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    console.log(errorResponse)
                  }
                }) 
                    

              } 
              finally {
                  db.release()
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }
  
  exports.getAvanzaValidateOTP= function (req, res) {
    const { sourceUUID, parentUUID,IPAddress, voucherUUID, schoolID,branchID,bankUUID,transactionAmount,transactionRefNo, cnicNumber, bankAccountNumber, basketID, transactionOTP } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ sourceUUID, parentUUID,IPAddress, voucherUUID,schoolID,branchID,bankUUID,transactionAmount,transactionRefNo, cnicNumber, bankAccountNumber,basketID, transactionOTP },paymentsValidations.getAvanzaValidateOTP,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @bankUUID = ${utils.OptionalKey(bankUUID)};
                              set @parentUUID = ${utils.OptionalKey(parentUUID)};
                              set @voucherUUID=${utils.OptionalKey(voucherUUID)};
                              call ${appConfig.getDbConfig().database}.v1_get_provider_details_avanza
                              (@schoolID,@branchID,@bankUUID,@parentUUID,@voucherUUID);`
                    console.log(dbquery)
              let subMerchantID, subMerchantPassword, baseURL,bankCode, authKey,accountType, emailAddress, mobileNumber    

              try { 

                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    
                      data.forEach(element => {
                            if(element.constructor==Array) {
                                if (!_.isEmpty(element)) {
                                    subMerchantID = element[0].subMerchantID
                                    subMerchantPassword = element[0].subMerchantPassword
                                    bankCode = element[0].bankCode
                                    accountType = element[0].accountType
                                    authKey = element[0].tokenAuthKey
                                    baseURL = element[0].baseURL
                                    emailAddress = element[0].emailAddress, 
                                    mobileNumber = element[0].mobileNumber
                                }
                            }
                          })
                      let AvanzaTokeURL= baseURL + "/token"            
                      axios({
                        method: "post",
                        url: AvanzaTokeURL,
                        headers: {
                          "content-type": "application/x-www-form-urlencoded"
                        },
                        data:qs.stringify({
                          grant_type:"client_credentials",
                          merchant_id:subMerchantID,
                          secured_key:subMerchantPassword
                        })
                      })
                      .then(function (appsResponse) {
                          console.log(appsResponse.config.data)
                          console.log(appsResponse.data)
                          if(appsResponse.data.code=="00"){
                              let AvanzaAccessToken = appsResponse.data.token
                              let AvanzaOTPValidationURL= baseURL + "/transaction"
                              console.log(AvanzaAccessToken)
                              let transactionDateTime=utils.getFullDateString(0)
                              axios({
                                method: "post",
                                url: AvanzaOTPValidationURL,
                                headers: {
                                  "content-type": "application/x-www-form-urlencoded",
                                  "Authorization":"Bearer " + AvanzaAccessToken
                                },
                                data:qs.stringify({
                                  txnamt:transactionAmount,
                                  basket_id: basketID,
                                  customer_mobile_no: mobileNumber,
                                  customer_email_address: emailAddress,
                                  account_type_id:accountType,
                                  bank_code:bankCode,
                                  otp_required:"yes",
                                  order_date: utils.formatDate(new Date),
                                  cnic_number:cnicNumber,
                                  account_number: bankAccountNumber,
                                  otp:transactionOTP,
                                  transaction_id:transactionRefNo
                                })
                              })
                              .then(function (validationResponse) {
                                  //console.log(validationResponse.data)
                                  let transactionResponseTime=utils.getFullDateString(0)
                                  let rrn = validationResponse.data.rdv_message_key
                                  let transactionRefNo = validationResponse.data.transaction_id
                                  let dbquery=`
                                        set @sourceUUID = ${utils.OptionalKey(sourceUUID)};  
                                        set @parentUUID = ${utils.OptionalKey(parentUUID)}; 
                                        set @bankUUID = ${utils.OptionalKey(bankUUID)}; 
                                        set @IPAddress = ${utils.OptionalKey(IPAddress)}; 
                                        set @providerTxnRefNo = ${utils.OptionalKey(transactionRefNo)};
                                        set @internalTxnRefNo = ${utils.OptionalKey(voucherUUID)};
                                        set @maskedInstrument = ${utils.OptionalKey(utils.maskInstrumentString(bankAccountNumber))};
                                        set @cnicNumber = ${utils.OptionalKey(cnicNumber)};
                                        set @paymentCurrency = 'PKR';
                                        set @paymentAmount = ${utils.OptionalKey(transactionAmount)};
                                        set @tranRequestDateTime = ${utils.OptionalKey(transactionDateTime)};
                                        set @tranRequestCode = 'validateAvanzaOTP';
                                        set @additionalData = ${utils.OptionalKey(rrn)};
                                        set @tranResponseDateTime = ${utils.OptionalKey(transactionResponseTime)};
                                        set @tranResponseCode = ${utils.OptionalKey(validationResponse.data.code)};
                                        set @tranResponseData = ${utils.OptionalKey(utils.EncryptBase64(JSON.stringify(validationResponse.data)))};
                                        call ${appConfig.getDbConfig().database}.v1_set_payment_logs_payfast
                                        (@sourceUUID,@parentUUID,@bankUUID,@IPAddress, @providerTxnRefNo,@internalTxnRefNo,
                                          @maskedInstrument,@cnicNumber,@paymentCurrency,@paymentAmount,@tranRequestDateTime,@tranRequestCode,@additionalData,
                                          @tranResponseDateTime, @tranResponseCode,@tranResponseData);`
                                        console.log(dbquery)
                                      db.query(dbquery,function (error, data, fields) {
                                        if(!error){
                                            console.log('Avanza Transaction Logs Inserted Successfully')     
                                        }
                                        else{
                                          console.log(error)
                                        }
                                      }) 
                                  if(validationResponse.data.status_code=="00"){
                                      if(!res.finished)res.status(200).json( { response:"success",transactionID:validationResponse.data.transaction_id,rdvResponse:validationResponse.data.rdv_message_key})
                                      // push notification
                                        dbquery = `select * from v1_get_fee_voucher_notifications where voucherUUID='${voucherUUID}' `;
                                        console.log(dbquery)
                                        db.query(dbquery,function (error, data, fields) {
                                              if(!error){
                                                if (!_.isEmpty(data)) {
                                                  let messageText;
                                                  
                                                    
                                                    for (let i = 0; i < data.length; i++) {
                                                      if(data[i].preferredLanguage=='ar'){
                                                          messageText= `شكرا على الدفع. تم استلام مبلغ الرسوم وسيتم تحديث الحساب وفقًا لذلك`
                                                      }
                                                      else{
                                                          messageText = `Thank you for the payment. The fee amount has been received and account will be updated accordingly.`
                                                      }
                                                      var messageConfig = appConfig.messageSettings(
                                                      "Fee Confirmation",
                                                      messageText,
                                                      data[i].parentID,
                                                      "FeeChallanScreen",
                                                      data[i].studentID,
                                                      data[i].classID,
                                                      data[i].branchID,
                                                      data[i].schoolID,
                                                      null
                                                    );
                                                    console.log(messageText)  
                                                    push.SendNotification(data[i].deviceToken, messageConfig);
                                                    }
                                                  }
                                                }
                                                
                                              }); 
                                    
                                      emails.SendEmail('payment.alerts@infinitystudio.pk','Avanza Successful Payment - ' + validationResponse.data.transaction_id,JSON.stringify(validationResponse.config.data).replace(/\\/g,''))    
                                    }  
                                  else{
                                    if(!res.finished)res.status(500).json({ response: error.response.data.message })
                                    emails.SendEmail('payment.alerts@infinitystudio.pk','Avanza Payment Failed ' + error.response.data.message,JSON.stringify(validationResponse.config.data).replace(/\\/g,''))    
                                  }
                                
                              })
                              .catch(function (error) {
                                console.log(error)
                                if(error.response.data.message){
                                  if(!res.finished)res.status(400).json({ response: error.response.data.message })
                                }
                              
                                else{
                                  if(!res.finished)res.status(500).json({ response: error.response.data.message })
                                }          
                              });
                              
                            }  
                          else{
                            if(!res.finished)res.status(500).json({ response: error.response.data.message })
                          }
                        
                      })
                      .catch(function (error) {
                        console.log(error)

                        if (error.response.status_code==400 ){
                          if(!res.finished)res.status(400).json({ response: error.response.data.message })
                        }
                        else{
                          if(!res.finished)res.status(500).json({ response: error.response.data.message })
                        }          
                      });
                          
              
                  }

                  else{
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    console.log(errorResponse)
                  }
                }) 
                                 
                  
                     
                    
              
              } 
              finally {
                  db.release()
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }
  

  exports.getNIFTInitialize= function (req, res) {
    const { schoolID, branchID, transactionAmount,bankUUID, referenceNotes, productDesc,paymentCurrency} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ schoolID, branchID, transactionAmount, bankUUID,referenceNotes, productDesc,paymentCurrency },paymentsValidations.getNIFTInitialize,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @bankUUID = ${utils.OptionalKey(bankUUID)};
                              set @voucherUUID = null;
                              call ${appConfig.getDbConfig().database}.v1_get_provider_details_nift
                              (@schoolID,@branchID,@bankUUID,@voucherUUID);`
                    console.log(dbquery)
              let mainMerchantID ,subMerchantID ,subMerchantSalt ,subMerchantPassword,tokenAuthKey, postbackURL ,baseURL ,bankCode
              
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      data.forEach(element => {
                        if(element.constructor==Array) {
                            if (!_.isEmpty(element)) {
                                 mainMerchantID = element[0].mainMerchantID
                                 subMerchantID = element[0].subMerchantID
                                 subMerchantSalt = element[0].subMerchantSalt
                                 subMerchantPassword = element[0].subMerchantPassword
                                 tokenAuthKey = element[0].tokenAuthKey
                                 postbackURL = element[0].postbackURL
                                 baseURL = element[0].baseURL
                                 bankCode = element[0].bankCode
                            }
        
                        }
                      })

                      let NIFTTokeURL= baseURL + "/IRISPGW2/api/v1/oauth2/token"
                      console.log('Token URL: ' + NIFTTokeURL)
                      axios({
                        method: "post",
                        url: NIFTTokeURL,
                        headers: {
                          "Content-Type": "application/json",
                          "Authorization":"Basic " + utils.EncryptBase64(tokenAuthKey) 
                        },
                        data:{
                          grant_type:"client_credentials"
                        }
                      })
                      .then(function (appsResponse) {
                          console.log('Request>>> ' + JSON.stringify(appsResponse.config.data).replace(/\\/g,''))
                          console.log('Response<<< ' + JSON.stringify(appsResponse.data).replace(/\\/g,''))
                          let NIFTAccessToken = appsResponse.data.access_token
                          // console.log("Access Token:" + NIFTAccessToken)
                          let transactionDateTime=utils.getFullDateString(0)
                          let transactionExpiryTime=utils.getFullDateString(7)
                          let transactionRefNo = "T" + transactionDateTime
                          let NIFTCustomerValidateURL= baseURL + "/IRISPGW2Core/v2/transaction/paymentvalidation"
                          let hashData = subMerchantSalt + '&' + utils.createStringAmpersand(utils.ConvertDecimalToPaddingZeros(transactionAmount)) 
                                              +  utils.createStringAmpersand(referenceNotes)
                                              + utils.createStringAmpersand(productDesc)
                                              + "EN&"
                                              + utils.createStringAmpersand(mainMerchantID)
                                              + utils.createStringAmpersand(subMerchantPassword)
                                              + utils.createStringAmpersand(postbackURL)
                                              + utils.createStringAmpersand(subMerchantID)
                                              + utils.createStringAmpersand(paymentCurrency)
                                              + utils.createStringAmpersand(transactionDateTime)
                                              + utils.createStringAmpersand(transactionExpiryTime)
                                              + utils.createStringAmpersand(transactionRefNo)
                                              + "1.1"
                          console.log('Validation URL:'+ NIFTCustomerValidateURL)
                          axios({
                            method: "post",
                            url: NIFTCustomerValidateURL,
                            headers: {
                              "Content-Type": "application/json",
                              "Authorization":"Bearer " + NIFTAccessToken
                            },
                            data:{
                              pp_Amount:utils.ConvertDecimalToPaddingZeros(transactionAmount),
                              pp_BankID: "",
                              pp_BillReference: referenceNotes,
                              pp_Description: productDesc,
                              pp_Language:"EN",
                              pp_MerchantID:mainMerchantID,
                              pp_Password:subMerchantPassword,
                              pp_ReturnURL:postbackURL,
                              pp_SecureHash:utils.HMAC256Encrypt(hashData,subMerchantSalt),
                              pp_SubMerchantID:subMerchantID,
                              pp_TxnCurrency:"PKR",
                              pp_TxnDateTime:transactionDateTime,
                              pp_TxnExpiryDateTime:transactionExpiryTime,
                              pp_TxnRefNo: transactionRefNo,
                              pp_TxnType:"",
                              pp_Verion:"1.1",
                              usageMode:"HPC"
                              
                            }
                          })
                          .then(function (validationResponse) {
                              console.log('Request>>> ' + JSON.stringify(validationResponse.config.data).replace(/\\/g,''))
                              console.log('Response<<< ' + JSON.stringify(validationResponse.data).replace(/\\/g,''))
                      
                              if(validationResponse.data.responseCode=="000"){
                                if(!res.finished)res.status(200).json( { response:"success", accessToken:NIFTAccessToken, transactionRef:transactionRefNo})
                              }  
                              else{
                                if(!res.finished)res.status(500).json({ response: error.response.data.error_description })
                              }
                            
                          })
                          .catch(function (error) {
                            console.log(error)
                            if(error.response.data.responseError){
                              if(!res.finished)res.status(400).json({ response: error.response.data.responseError })
                            }
                          
                            else{
                              console.log(error)
                              if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                            }          
                          });
                      })
                      .catch(function (error) {
                        console.log(error)
                        if (error.response.status==400 ){
                          if(!res.finished)res.status(400).json({ response: error.response.data.error_description })
                        }
                        else{
                          if(!res.finished)res.status(500).json({ response: error.response.data.error_description })
                        }          
                      });
                    }
                    else{
                        
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        console.log(errorResponse)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                  });

              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }
  
  exports.getNIFTCustomerValidation= function (req, res) {
    const { schoolID, branchID,bankUUID, transactionToken,transactionRefNo,transactionAmount,cnicNumber,bankAccountNumber} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ schoolID, branchID,bankUUID,transactionToken,transactionRefNo,transactionAmount,cnicNumber,bankAccountNumber},paymentsValidations.getNIFTCustomerValidation,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
                  let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @bankUUID = ${utils.OptionalKey(bankUUID)};
                              set @voucherUUID = null;
                              call ${appConfig.getDbConfig().database}.v1_get_provider_details_nift
                              (@schoolID,@branchID,@bankUUID,@voucherUUID);`
                    console.log(dbquery)
              let mainMerchantID ,subMerchantID ,subMerchantPassword, baseURL,bankCode
                            
              try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                      data.forEach(element => {
                            if(element.constructor==Array) {
                                if (!_.isEmpty(element)) {
                                    mainMerchantID = element[0].mainMerchantID
                                    subMerchantID = element[0].subMerchantID
                                    bankCode = element[0].bankCode
                                    subMerchantPassword = element[0].subMerchantPassword
                                    baseURL = element[0].baseURL
                                }
            
                            }
                          })
                        let transactionDateTime=utils.getFullDateString(0)
                        let transactionExpiryTime=utils.getFullDateString(7)
                        let NIFTCustomerValidateURL=baseURL + "/IRISPGW2Core/v2/transaction/sendotp"
                        console.log('Send OTP URL: '+ NIFTCustomerValidateURL)
                        axios({
                          method: "post",
                          url: NIFTCustomerValidateURL,
                          headers: {
                            "Content-Type": "application/json",
                            "Authorization":"Bearer " + transactionToken
                          },
                            data:{
                            amount:utils.ConvertDecimalToPaddingZeros(transactionAmount),
                            bankCode: bankCode,
                            currency:"586",
                            cvv:"",
                            merchantCode:mainMerchantID,
                            merchantRefNo: transactionRefNo,
                            nationalId:cnicNumber.replace(' ',''),
                            password:subMerchantPassword,
                            relationshipId:bankAccountNumber,
                            subMerchantCode:subMerchantID,
                            transmissionDateTime:transactionDateTime,
                            txnTypeCode:"ACC"                              
                            }
                            
                        })
                        .then(function (validationResponse) {
                            console.log('Request>>> ' + JSON.stringify(validationResponse.config.data).replace(/\\/g,''))
                            console.log('Response<<< ' + JSON.stringify(validationResponse.data).replace(/\\/g,''))
                            if(validationResponse.data.responseCode=="000"){
                              if(!res.finished)res.status(200).json( { response:"success", reason:"OTP sent successfully!",accessToken:transactionToken, transactionRef:transactionRefNo})
                            }  
                            else{
                              if(!res.finished)res.status(500).json({ response: error.response.data.error_description })
                            }
                          
                        })
                        .catch(function (error) {
                          console.log(error)
                          if(error.response.data.responseError){
                            if(!res.finished)res.status(400).json({ response: error.response.data.responseError })
                          }
                          else if(error.response.data.status){
                            if(!res.finished)res.status(400).json({ response: error.response.data.status, retrievalRefNo:error.response.data.rrn  })
                          }
                          else if(error.response.data.responseDesc){
                            if(!res.finished)res.status(400).json({ response: error.response.data.responseDesc, retrievalRefNo:error.response.data.rrn  })
                          }
                          else if(error.response.data.message){
                            if(!res.finished)res.status(500).json({ response: error.response.data.message  })
                            
                          }
                          else{
                            
                            if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                          }          
                        });
                  
                  }
                  else{
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    console.log(errorResponse)
                  }

                })
              } 
              finally {
                  db.release()
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }

  exports.setNIFTValidateOTP= function (req, res) {
    const { schoolID, branchID, sourceUUID, parentUUID, IPAddress, voucherUUID, bankUUID, transactionToken,transactionRefNo,transactionOTP,transactionAmount,cnicNumber,bankAccountNumber} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ schoolID, branchID, sourceUUID, parentUUID, IPAddress, voucherUUID,bankUUID, transactionToken,transactionRefNo,transactionOTP,transactionAmount, cnicNumber,bankAccountNumber },paymentsValidations.setNIFTValidateOTP,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
                let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @bankUUID = ${utils.OptionalKey(bankUUID)};
                              set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
                              call ${appConfig.getDbConfig().database}.v1_get_provider_details_nift
                              (@schoolID,@branchID,@bankUUID,@voucherUUID);`
                    console.log(dbquery)
              let mainMerchantID ,subMerchantID ,subMerchantPassword,subMerchantSalt, baseURL,bankCode              
              try {

                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    
                      data.forEach(element => {
                            if(element.constructor==Array) {
                                if (!_.isEmpty(element)) {
                                    mainMerchantID = element[0].mainMerchantID
                                    subMerchantID = element[0].subMerchantID
                                    bankCode = element[0].bankCode
                                    subMerchantPassword = element[0].subMerchantPassword
                                    baseURL = element[0].baseURL
                                    subMerchantSalt = element[0].subMerchantSalt
                                }
            
                            }
                          })
                        let transactionDateTime=utils.getFullDateString(0)
                        let NIFTCustomerValidateURL=baseURL + "/IRISPGW2Core/v2/transaction/accountpayment"
                        console.log('Account Payment URL:' + NIFTCustomerValidateURL)
                        axios({
                          method: "post",
                          url: NIFTCustomerValidateURL,
                          headers: {
                            "Content-Type": "application/json",
                            "Authorization":"Bearer " + transactionToken
                          },
                            data:{
                            amount:utils.ConvertDecimalToPaddingZeros(transactionAmount),
                            bankCode: bankCode,
                            currency:"586",
                            merchantCode:mainMerchantID,
                            merchantRefNo: transactionRefNo,
                            nationalId:cnicNumber.replace(' ',''),
                            password:subMerchantPassword,
                            otp:transactionOTP,
                            relationshipId:bankAccountNumber,
                            subMerchantCode:subMerchantID,
                            transmissionDateTime:transactionDateTime
                            }
                            
                        })
                        .then(function (validationResponse) {
                              console.log('Request>>> ' + JSON.stringify(validationResponse.config.data).replace(/\\/g,''))
                              console.log('Response<<< ' + JSON.stringify(validationResponse.data).replace(/\\/g,''))
                              let rrn = validationResponse.data.rrn
                              let transactionResponseTime=utils.getFullDateString(0)
                              let dbquery=`
                                set @sourceUUID = ${utils.OptionalKey(sourceUUID)};  
                                set @parentUUID = ${utils.OptionalKey(parentUUID)}; 
                                set @bankUUID = ${utils.OptionalKey(bankUUID)}; 
                                set @IPAddress = ${utils.OptionalKey(IPAddress)}; 
                                set @providerTxnRefNo = ${utils.OptionalKey(transactionRefNo)};
                                set @internalTxnRefNo = ${utils.OptionalKey(voucherUUID)};
                                set @maskedInstrument = ${utils.OptionalKey(utils.maskInstrumentString(bankAccountNumber))};
                                set @cnicNumber = ${utils.OptionalKey(cnicNumber)};
                                set @paymentCurrency = 'PKR';
                                set @paymentAmount = ${utils.OptionalKey(transactionAmount)};
                                set @tranRequestDateTime = ${utils.OptionalKey(transactionDateTime)};
                                set @tranRequestCode = 'accountpayment';
                                set @additionalData = ${utils.OptionalKey(rrn)};
                                set @tranResponseDateTime = ${utils.OptionalKey(transactionResponseTime)};
                                set @tranResponseCode = ${utils.OptionalKey(validationResponse.data.responseCode)};
                                set @tranResponseData = ${utils.OptionalKey(utils.EncryptBase64(JSON.stringify(validationResponse.data)))};
                                call ${appConfig.getDbConfig().database}.v1_set_payment_logs_nift
                                (@sourceUUID,@parentUUID,@bankUUID,@IPAddress, @providerTxnRefNo,@internalTxnRefNo,
                                  @maskedInstrument,@cnicNumber,@paymentCurrency,@paymentAmount,@tranRequestDateTime,@tranRequestCode,@additionalData,
                                  @tranResponseDateTime, @tranResponseCode,@tranResponseData);`
                                console.log(dbquery)
                                db.query(dbquery,function (error, data, fields) {
                                  if(!error){
                                      console.log('NIFT Transaction Logs Inserted Successfully')   
                                  }
                                  else{
                                    console.log(error)
                                  }
                                }) 
                            
                            if(validationResponse.data.responseCode=="000"){
                                // validating payment check
                                let NIFTPaymentValidateURL= baseURL + "/IRISPGW2Core/v2/transaction/paymentresponse"
                                let RRN = validationResponse.data.pp_RetreivalReferenceNo
                                let transactionDateTime  = utils.getFullDateString(0)
                                let transactionExpiryTime = utils.getFullDateString(7)
                                
                                axios({
                                  method: "post",
                                  url: NIFTPaymentValidateURL,
                                  headers: {
                                    "Content-Type": "application/json",
                                    "Authorization":"Bearer " + transactionToken
                                  },
                                    data:{
                                    pp_Amount:utils.ConvertDecimalToPaddingZeros(transactionAmount),
                                    pp_AuthCode:"",
                                    pp_BankID: bankCode,
                                    pp_BillReference:"Skoolify",
                                    pp_Language: "",
                                    pp_MerchantID:mainMerchantID,
                                    pp_NationalID:cnicNumber,
                                    pp_Password:subMerchantPassword,
                                    pp_ProductID:"",
                                    pp_ResponseCode:"000",
                                    pp_RetreivalReferenceNo:RRN,
                                    pp_SettlementExpiry:"",
                                    pp_SubMerchantID:subMerchantID,
                                    pp_TxnCurrency:"PKR",
                                    pp_TxnDateTime:transactionDateTime,
                                    pp_TxnExpiryDateTime:transactionExpiryTime,
                                    pp_TxnRefNo:transactionRefNo,
                                    pp_TxnType:"",
                                    pp_Version:"1.1"
                                    }
                                    
                                })
                                .then(function (validationResponse) {
                                    let RRN = validationResponse.data.pp_RetreivalReferenceNo
                                    console.log('Payment Validation URL:' + NIFTPaymentValidateURL)
                                    console.log('Request>>> ' + JSON.stringify(validationResponse.config.data).replace(/\\/g,''))
                                    console.log('Response<<< ' + JSON.stringify(validationResponse.data).replace(/\\/g,''))
                                    let responseSecureHash = validationResponse.data.pp_SecureHash
                                    let arrQueryString=[];
                                    
                                    const sortedObject = Object.keys(validationResponse.data).sort().reduce(
                                      (obj, key) => { 
                                        obj[key] = validationResponse.data[key]; 
                                        return obj;
                                      }, 
                                      {}
                                    );
                                    
                                    for (const [key, value] of Object.entries(sortedObject)) {
                                      
                                      if(key=="pp_SecureHash" || key=="statusCode"){
                                        // do nothing, dont add 
                                      }
                                      else if(value){
                                          arrQueryString.push(`${value}`);
                                      }
                                    }

                                    let hashData = subMerchantSalt + '&'                                     
                                    hashData = hashData + arrQueryString.join("&")
                                    
                                    let buildHash = utils.HMAC256Encrypt(hashData,subMerchantSalt)

                                    if(validationResponse.data.pp_ResponseCode=="000" && validationResponse.data.pp_ResponseMessage=="Success" && responseSecureHash == buildHash){
                                      if(!res.finished)res.status(200).json( { response:"Thank you! Payment Successful",rrn:rrn, reason:"Payment Successful"})
                                      console.log("Payment Successful");
                                      // push notification
                                    dbquery = `select * from v1_get_fee_voucher_notifications where voucherUUID='${voucherUUID}' `;
                                    console.log(dbquery)
                                    db.query(dbquery,function (error, data, fields) {
                                          if(!error){
                                            if (!_.isEmpty(data)) {
                                              let messageText;
                                              
                                                
                                                for (let i = 0; i < data.length; i++) {
                                                  if(data[i].preferredLanguage=='ar'){
                                                      messageText= `شكرا على الدفع. تم استلام مبلغ الرسوم وسيتم تحديث الحساب وفقًا لذلك`
                                                  }
                                                  else{
                                                      messageText = `Thank you for the payment. The fee amount has been received and account will be updated accordingly.`
                                                  }
                                                  var messageConfig = appConfig.messageSettings(
                                                  "Fee Confirmation",
                                                  messageText,
                                                  data[i].parentID,
                                                  "FeeChallanScreen",
                                                  data[i].studentID,
                                                  data[i].classID,
                                                  data[i].branchID,
                                                  data[i].schoolID,
                                                  null
                                                );
                                                console.log(messageText)  
                                                push.SendNotification(data[i].deviceToken, messageConfig);
                                                }
                                              }
                                            }
                                            
                                          }); 
                                    
                                      emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Successful Payment ',JSON.stringify(validationResponse.config.data).replace(/\\/g,''))    
                                    } 
                                    else if (responseSecureHash != buildHash){
                                      if(!res.finished)res.status(401).json({ response: "Secure Hash Does not match", status:"Error verifying the transaction"})
                                      console.log("Secure Hash do not match");
                                    }
                                    else{
                                      if(!res.finished)res.status(500).json({ response: "Unknown Error - Try Again",status:"Error verifying the transaction"})
                                      console.log("Unknown Error");
                                    }
                                })
                                .catch(function (error) {
                                  console.log(error)
                                  if(error.response.statusText=='Internal Server Error'){
                                    console.log("Trnasaction Not found");
                                    if(!res.finished)res.status(400).json({ response: 'Transaction Not Found' })
                                  }
                                  else if (error.response.data.pp_ResponseCode=="030") {
                                      if(!res.finished)res.status(200).json({ response: "Transaction does not match, voucher reversed"})
                                      let dbquery=`
                                        set @voucherUUID = ${utils.OptionalKey(voucherUUID)};  
                                        set @paymentAmount = ${utils.OptionalKey(transactionAmount)}; 
                                        set @paymentStatus = 'U'; 
                                        call ${appConfig.getDbConfig().database}.v1_set_payment_status_nift
                                        (@voucherUUID, @paymentAmount,@paymentStatus);`
                                        console.log(dbquery)
                                        db.query(dbquery,function (error, data, fields) {
                                          if(!error){
                                              console.log('exceuted successfully') 
                                          }
                                          else{
                                            console.log(error)
                                          }
                                        })
                                    }
                                  else{
                                    if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                                    console.log("Unknown Error ");
                                  }          
                                });

                              
                              
                            }  
                            else{
                              if(!res.finished)res.status(500).json({ response: error.response.data.error_description })
                              emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Payment Failed : ' + JSON.stringify(error.response.data.error_description),JSON.stringify(validationResponse.config.data).replace(/\\/g,''))
                            }

                          
                        })
                        .catch(function (error) {
                          
                          if(error.response.data.responseError){
                            if(!res.finished)res.status(400).json({ response: error.response.data.responseError })
                            emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Failed Attempt - ' + JSON.stringify(error.response.data.responseError),JSON.stringify(error.response.data).replace(/\\/g,''))
                          }
                          
                          else if(error.response.data.responseCode){
                            if(!res.finished)res.status(400).json({ response: error.response.data.status, retrievalRefNo:error.response.data.rrn })
                            emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Failed Attempt - ' + JSON.stringify(error.response.data.rrn),JSON.stringify(error.response.data).replace(/\\/g,''))
                          }
                          else if(error.response.data.message){
                            if(!res.finished)res.status(500).json({ response: error.response.data.message  })
                            emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Failed Attempt ' ,JSON.stringify(error.response.data).replace(/\\/g,''))
                          }
                          else{
                            if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                            emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Failed Attempt - Unknown Error' ,JSON.stringify(error.response.data).replace(/\\/g,''))
                          }          
                        });
                  
                  }
                  else{
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    console.log(errorResponse)
                    if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                  }
                }) 
              }
              finally {
                  db.release()
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }
   exports.getNIFTPaymentStatus= function (req, res) {
    const { schoolID, branchID, voucherUUID,transactionAmount,cnicNumber,bankAccountNumber} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    Joi.validate({ schoolID, branchID, voucherUUID,transactionAmount, cnicNumber,bankAccountNumber },paymentsValidations.getNIFTPaymentStatus,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } 
        else {
          db.getConnection(function(err, db) {
            if(!err){
                let dbquery = ` set @schoolID = ${utils.OptionalKey(schoolID)};
                              set @branchID = ${utils.OptionalKey(branchID)};
                              set @bankUUID = null;
                              set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
                              call ${appConfig.getDbConfig().database}.v1_get_provider_details_nift
                              (@schoolID,@branchID,@bankUUID, @voucherUUID);`
                    console.log(dbquery)
              let tokenAuthKey, mainMerchantID ,subMerchantID ,subMerchantPassword, baseURL,bankCode, transactionRefNo, transactionRetrievalRefNo , tranRequestCode             
              try {

                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    // console.log(data)
                      data.forEach(element => {
                            if(element.constructor==Array) {
                                if (!_.isEmpty(element)) {
                                    tokenAuthKey = element[0].tokenAuthKey
                                    mainMerchantID = element[0].mainMerchantID
                                    subMerchantID = element[0].subMerchantID
                                    bankCode = element[0].bankCode
                                    subMerchantPassword = element[0].subMerchantPassword
                                    baseURL = element[0].baseURL
                                    transactionRefNo = element[0].transactionRefNo
                                    transactionRetrievalRefNo = element[0].transactionRetrievalRefNo
                                    tranRequestCode = element[0].tranRequestCode
                                }
                            }
                          })
                        
                      let NIFTTokeURL= baseURL + "/IRISPGW2/api/v1/oauth2/token"
                      axios({
                        method: "post",
                        url: NIFTTokeURL,
                        headers: {
                          "Content-Type": "application/json",
                          "Authorization":"Basic " + utils.EncryptBase64(tokenAuthKey) 
                        },
                        data:{
                          grant_type:"client_credentials"
                        }
                      })
                      .then(function (appsResponse) {
                        
                        let NIFTAccessToken = appsResponse.data.access_token
                        let transactionDateTime=utils.getFullDateString(0)
                        let NIFTCustomerValidateURL=baseURL + "/IRISPGW2Core/v2/transaction/paymentresponse"
                        let transactionExpiryTime=utils.getFullDateString(7)
                        axios({
                          method: "post",
                          url: NIFTCustomerValidateURL,
                          headers: {
                            "Content-Type": "application/json",
                            "Authorization":"Bearer " + NIFTAccessToken
                          },
                            data:{
                            pp_Amount:utils.ConvertDecimalToPaddingZeros(transactionAmount),
                            pp_AuthCode:"",
                            pp_BankID: bankCode,
                            pp_BillReference:"Skoolify",
                            pp_Language: "",
                            pp_TxnCurrency:"PKR",
                            pp_ProductID:"",
                            pp_ResponseCode:"000",
                            pp_RetreivalReferenceNo:transactionRetrievalRefNo,
                            pp_TxnRefNo:transactionRefNo,
                            pp_TxnType:"",
                            pp_Version:"1.1",
                            pp_SettlementExpiry:"",
                            pp_MerchantID:mainMerchantID,
                            pp_NationalID:cnicNumber,
                            pp_Password:subMerchantPassword,
                            relationshipId:bankAccountNumber,
                            pp_SubMerchantID:subMerchantID,
                            pp_TxnDateTime:transactionDateTime,
                            pp_TxnExpiryDateTime:transactionExpiryTime,
                            }
                        })
                        .then(function (validationResponse) {
                            if(validationResponse.data.pp_ResponseCode=="000" && validationResponse.data.pp_ResponseMessage=="Success"){
                              let transactionResponseTime=utils.getFullDateString(0)
                              let dbquery=`
                                set @voucherUUID = ${utils.OptionalKey(voucherUUID)};  
                                set @paymentAmount = ${utils.OptionalKey(transactionAmount)}; 
                                set @paymentStatus = 'P'; 
                                call ${appConfig.getDbConfig().database}.v1_set_payment_status_nift
                                (@voucherUUID, @paymentAmount,@paymentStatus);`
                                console.log(dbquery)
                                db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    if(!res.finished)res.status(200).json( { response:"Successfully Paid", reason:"Fee Voucher Updated"})
                                    emails.SendEmail('payment.alerts@infinitystudio.pk','NIFT Successful Payment - Status Change ',JSON.stringify(validationResponse.config.data).replace(/\\/g,''))      
                                    // push notification
                                    dbquery = `select * from v1_get_fee_voucher_notifications where voucherUUID='${voucherUUID}' `;
                                    console.log(dbquery)
                                    db.query(dbquery,function (error, data, fields) {
                                          if(!error){
                                            if (!_.isEmpty(data)) {
                                              let messageText;
                                                for (let i = 0; i < data.length; i++) {
                                                  if(data[i].preferredLanguage=='ar'){
                                                      messageText= `شكرا على الدفع. تم استلام مبلغ الرسوم وسيتم تحديث الحساب وفقًا لذلك`
                                                  }
                                                  else{
                                                      messageText = `Thank you for the payment. The fee amount has been received and account will be updated accordingly.`
                                                  }
                                                  var messageConfig = appConfig.messageSettings(
                                                  "Fee Confirmation",
                                                  messageText,
                                                  data[i].parentID,
                                                  "FeeChallanScreen",
                                                  data[i].studentID,
                                                  data[i].classID,
                                                  data[i].branchID,
                                                  data[i].schoolID,
                                                  null
                                                );
                                                console.log(messageText)  
                                                push.SendNotification(data[i].deviceToken, messageConfig);
                                                }
                                              }
                                            }
                                            
                                          }); 
                                       
                                }
                                else{
                                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                  console.log(errorResponse)
                                  if(!res.finished)res.status(500).json( { response:"failed", reason:"database error"})
                                }
                              }) 
                              
                            } 
                            
                            else{
                              if(!res.finished)res.status(500).json({ response: "Unknown Error - Try Again"})
                              
                            }
                          
                        })
                        .catch(function (error) {
                          console.log(error)

                          if(error.response.statusText=='Internal Server Error'){
                            if(!res.finished)res.status(400).json({ response: 'Transaction not found' })
                          }
                          else if (error.response.data.pp_ResponseCode=="030") {
                              if(!res.finished)res.status(200).json({ response: "transaction does not match, voucher reversed"})
                              let dbquery=`
                                set @voucherUUID = ${utils.OptionalKey(voucherUUID)};  
                                set @paymentAmount = ${utils.OptionalKey(transactionAmount)}; 
                                set @paymentStatus = 'U'; 
                                call ${appConfig.getDbConfig().database}.v1_set_payment_status_nift
                                (@voucherUUID, @paymentAmount,@paymentStatus);`
                                console.log(dbquery)
                                db.query(dbquery,function (error, data, fields) {
                                  if(!error){
                                      console.log('exceuted successfully') 
                                  }
                                  else{
                                    console.log(error)
                                  }
                                })
                            }
                          else{
                            if(!res.finished)res.status(500).json({ response: 'Unknown Error' })
                          }          
                        });
                  
                      })  
                  }
                  else{
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    console.log(errorResponse)
                  }
                }) 
              }
              finally {
                  db.release()
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    )
  }
  
  exports.setPaymentLogHBLCC = function (req, res) {
    const {providerID,sourceUUID,parentUUID,userUUID,IPAddress, providerTxnRefNo,internalTxnRefNo,
      maskedInstrument,paymentCurrency,paymentAmount,tranRequestDateTime,tranRequestCode,additionalData,
      tranResponseDateTime, tranResponseCode,tranResponseData} = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({providerID,sourceUUID,parentUUID,userUUID,IPAddress, providerTxnRefNo,internalTxnRefNo,
      maskedInstrument,paymentCurrency,paymentAmount,tranRequestDateTime,tranRequestCode,additionalData,
      tranResponseDateTime, tranResponseCode,tranResponseData},paymentsValidations.setPaymentLogHBLCC,function (err, value) {
        if (!err) {
          db.getConnection(function(err, db) {
            if(!err){
              
              let dbquery=`set @providerID = ${utils.OptionalKey(providerID)} ;
                          set @sourceUUID = ${utils.OptionalKey(sourceUUID)};  
                          set @parentUUID = ${utils.OptionalKey(parentUUID)}; 
                          set @userUUID = ${utils.OptionalKey(userUUID)};                     
                          set @IPAddress = ${utils.OptionalKey(IPAddress)}; 
                          set @providerTxnRefNo = ${utils.OptionalKey(providerTxnRefNo)};
                          set @internalTxnRefNo = ${utils.OptionalKey(internalTxnRefNo)};
                          set @maskedInstrument = ${utils.OptionalKey(maskedInstrument)};
                          set @paymentCurrency = ${utils.OptionalKey(paymentCurrency)};
                          set @paymentAmount = ${utils.OptionalKey(paymentAmount.replace(",",""))};
                          set @tranRequestDateTime = ${utils.OptionalKey(tranRequestDateTime)};
                          set @tranRequestCode = ${utils.OptionalKey(tranRequestCode)};
                          set @additionalData = ${utils.OptionalKey(additionalData)};
                          set @tranResponseDateTime = ${utils.OptionalKey(tranResponseDateTime)};
                          set @tranResponseCode = ${utils.OptionalKey(tranResponseCode)};
                          set @tranResponseData = ${utils.OptionalKey(tranResponseData)};
                          call ${appConfig.getDbConfig().database}.v1_set_payment_logs_hblcc
                          (@providerID,@sourceUUID,@parentUUID,@userUUID,@IPAddress, @providerTxnRefNo,@internalTxnRefNo,
                            @maskedInstrument,@paymentCurrency,@paymentAmount,@tranRequestDateTime,@tranRequestCode,@additionalData,
                            @tranResponseDateTime, @tranResponseCode,@tranResponseData);`
                            
                          console.log(dbquery)
                          try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data inserted in db')
                                    if(!res.finished)res.status(200).json({ response: "success" })

                                    // push notification
                                    dbquery = `select * from v1_get_fee_voucher_notifications where voucherUUID='${internalTxnRefNo}' `;
                                    console.log(dbquery)
                                    db.query(dbquery,function (error, data, fields) {
                                          if(!error){
                                            if (!_.isEmpty(data)) {
                                              let messageText;
                                              
                                                
                                                for (let i = 0; i < data.length; i++) {
                                                  if(data[i].preferredLanguage=='ar'){
                                                      messageText= `شكرا على الدفع. تم استلام مبلغ الرسوم وسيتم تحديث الحساب وفقًا لذلك`
                                                  }
                                                  else{
                                                      messageText = `Thank you for the payment. The fee amount has been received and account will be updated accordingly.`
                                                  }
                                                  var messageConfig = appConfig.messageSettings(
                                                  "Fee Confirmation",
                                                  messageText,
                                                  data[i].parentID,
                                                  "FeeChallanScreen",
                                                  data[i].studentID,
                                                  data[i].classID,
                                                  data[i].branchID,
                                                  data[i].schoolID,
                                                  null
                                                );
                                                console.log(messageText)  
                                                push.SendNotification(data[i].deviceToken, messageConfig);
                                                }
                                              }
                                            }
                                            
                                          }); 
                                    
                                    emails.SendEmail('payment.alerts@infinitystudio.pk','Successful Payment Alert',dbquery)    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                    emails.SendEmail('payment.alerts@infinitystudio.pk','Failed Attempt: Payment Alert',dbquery)
                                }
                            });
                          } 
                          finally {
                              db.release();
                              console.log('released')
                          }
            }
            else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
            }
          })
          
        } 
        
        else {
          log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        }
      }
    );
  };

  exports.getProviderLogo= function (req, res) {
    const { voucherUUID } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
    
    Joi.validate({voucherUUID},paymentsValidations.getProviderLogo,function (err, value) {
      
        if (err) {
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
          res.status(400).json({ response: errorResponse });
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`
                        set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
                        call ${appConfig.getDbConfig().database}.v1_get_provider_logo
                        (@voucherUUID);`
            console.log(dbquery)
            
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                          //console.log(data)
                          data.forEach(element => {
                            if(element.constructor==Array) {

                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(200).json({ response: errorResponse })
                              } 
                              else{
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                //if(element[0].endDate) > 
                                if(!res.finished)res.status(200).json({response: element[0] })

                                
                              }
                            }
                              
                          })
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
              } 
              finally {
                  db.release();
                  console.log('released')
              } 
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };
 