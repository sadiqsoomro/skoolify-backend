const Joi = require("joi"),
  utils = require("../../utils/helper-methods"),
  diaryValidations = require("../validations/validations-diary"),
  db = require("../../../db"),
  _ = require("lodash/core"),
  appConfig = require("../../../app-config"),
  log = require("../../utils/utils-logging"),
  fileNameForLogging = "diary-controller";
  const uuidv4 = require("uuid/v4"),
  async = require('async');
import Error from "../response-classes/error";
// let PushNotifications = new require("node-pushnotifications");
// const push = new PushNotifications(appConfig.pushNotificationsettings);
const push = require("../../utils/push-notification");
// ==================================================================


exports.setDailyDiaryArray = function (req, res) {
  const { array } = req.body;
  
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(array, diaryValidations.setDailyDiaryArray, function ( err, value ) {
    if (err) {
      log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } 
    
    else {
      db.getConnection(function(err, db) {
        if(!err){
          if(array!=undefined){
         
          let dbquery = "";
                  var allSubjectIDs,allStudentIDs;
                  var allClassIDs = "";
                  var oneDescription = "";
                  
                  for (let i = 0; i < array.length; i++) {
                    var eventDateTime= `${utils.OptionalKey(array[i].dueDate)}`;
                    //console.log(array[i].description)
                    //console.log(array[i].description.replace(/[\r\n]+/g,"~"));
                    dbquery += `call ${appConfig.getDbConfig().database}.v1_set_dailydiary_studentlist(
                                ${utils.OptionalKey(array[i].diaryID)}, 
                                ${utils.OptionalKey(array[i].diaryGUID)}, 
                                ${utils.OptionalKey(array[i].subjectFlag)}, 
                                ${utils.OptionalKey(array[i].studentID)}, 
                                ${utils.OptionalKey(array[i].classID)}, 
                                ${utils.OptionalKey(array[i].subjectID)},
                                ${utils.OptionalKeyString(array[i].description)}, 
                                ${utils.OptionalKey(array[i].diaryDate)},
                                ${utils.OptionalKey(array[i].userID)},
                                ${utils.OptionalKey(array[i].attachment)},
                                ${utils.OptionalKey(array[i].documentLink)},
                                ${utils.OptionalKey(array[i].dueDate)});  `;
                    
                    // adding values in separate string 
                    if (array[i].classID) {
                      allClassIDs += array[i].classID + `,`;
                    }
                    
                    if (array[i].subjectID) {
                      allSubjectIDs = array[i].subjectID + `,`;
                      
                    }
                    
                    if (array[i].studentID) {
                      allStudentIDs = array[i].studentID + `,`;
                    }
                    if (!oneDescription.trim()){
                      oneDescription = array[i].description
                    }
                    
                    
                  }
                  if(allStudentIDs){
                    allStudentIDs = allStudentIDs.slice(0,-1)
                  }

                  if(allClassIDs){
                    allClassIDs = allClassIDs.slice(0,-1)
                  }
                  if(allSubjectIDs){
                    allSubjectIDs = allSubjectIDs.slice(0,-1)
                  }
                  


                  console.log(dbquery)
                  try{
                        db.query(dbquery,function (error, data, fields) {
                        if(!error){
                          log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                            if(!res.finished)res.status(200).json({ response: "success" });
                            // let dbquery = `select * from v1_get_diary_notifications
                            //             WHERE  FIND_IN_SET(classID, '${allClassIDs.slice(0,-1)}') and  FIND_IN_SET(subjectID, '${allSubjectIDs.slice(0,-1)}') `
                            //       if (allStudentIDs){
                            //         dbquery +=  ` and FIND_IN_SET(studentID,'${allStudentIDs.slice(0,-1)}')   `;
                            //       }
                          dbquery = `call ${appConfig.getDbConfig().database}.v1_get_diary_notifications(${utils.OptionalKey(allClassIDs)},${utils.OptionalKey(allSubjectIDs)}, ${utils.OptionalKey(allStudentIDs)});`
                          console.log(dbquery)
                          db.query(dbquery,function (error, data, fields) {
                          if(!error){
                            
                            
                            if (!_.isEmpty(data)) {
                              data.forEach(element => {
                                if(element.constructor==Array) {
                                    if (!_.isEmpty(element)) {
                                      let messageText = "";
                                      for (let i = 0; i < element.length; i++) {
                                        messageText = element[i].studentName + " (" + element[i].subjectName + "): " + oneDescription;
                                        var messageConfig = appConfig.messageSettings(
                                                  "Daily Diary",
                                                  messageText,
                                                  "DailyDiaryScreen",
                                                  element[i].parentID,
                                                  element[i].studentID,
                                                  element[i].classID,
                                                  element[i].branchID,
                                                  element[i].schoolID,
                                                  eventDateTime
                                        );
                                        //console.log(data[i].parentID + '-' +  data[i].studentID + '-' + data[i].classID + '-' + data[i].branchID + '-' + data[i].schoolID)
                                        push.SendNotification(element[i].deviceToken, messageConfig);
                                      }
                                      //console.log(messageConfig)
                                    }
                                    else{
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                    }
                                }
                              })
                              
                            }
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            console.log(errorResponse)
                            if(!res.finished)res.status(500).json({ response: errorResponse });
                          }
                          });


                              
                    }
                    else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                          console.log(errorResponse)
                          if(!res.finished)res.status(500).json({ response: errorResponse });
                    }
                    });
                    
                  }
                  
                  finally{
                    db.release();
                    console.log('released')
                  }

          }
          else{
              log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
              if(!res.finished)res.status(500).json({ response: "'array not found'" })

          }

        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

exports.deleteDailyDiary = function (req, res) {
  const { diaryGUID,studentID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ diaryGUID,studentID }, diaryValidations.deleteDailyDiary, function ( err, value ) {
    if (err) {
      log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));    
    } 
    else {
      db.getConnection(function(err, db) {
        if(!err){
          let dbquery=` set @diaryGUID = ${utils.OptionalKey(diaryGUID)};
                        set @studentID = ${utils.OptionalKey(studentID)};
                        call ${appConfig.getDbConfig().database}.v1_delete_daily_diary (@diaryGUID,@studentID);`
          console.log(dbquery)
                        
          try {
                db.query(dbquery,function (error, data, fields) {
              
                if(!error){
                                
                  if (_.isEmpty(data)) {
                      
                      log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                      if(!res.finished)res.status(200).json({ response: errorResponse })
                  } else {
                      log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                      if(!res.finished)res.status(200).json({response: "success"})
                  }

              }
              else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(500).json({ response: "unsuccessful", code: error.code, reason : error.message })
              }
              else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  console.log(errorResponse)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
              }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};


exports.getDailyDiarySummary = function (req, res) {
  const { classLevelID, classID,diaryGUID, subjectID,fromDate, toDate } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ classLevelID,classID, diaryGUID,subjectID,fromDate, toDate  }, diaryValidations.getDailyDiarySummary, function (err,value) {
    if (err) {
      log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } 
    else {
      let dbquery=`     
                        set @classLevelID = ${utils.OptionalKey(classLevelID)};
                        set @classID = ${utils.OptionalKey(classID)};
                        set @diaryGUID = ${utils.OptionalKey(diaryGUID)};
                        set @subjectID = ${utils.OptionalKey(subjectID)};
                        set @fromDate = ${utils.OptionalKey(fromDate)};
                        set @toDate = ${utils.OptionalKey(toDate)};

                        call ${appConfig.getDbConfig().database}.v1_get_dailydiary_summarize (@classLevelID,@classID,@diaryGUID,@subjectID, @fromDate, @toDate);`
      console.log(dbquery)

      db.getConnection(function(err, db) {
        if(!err){
          try {
            db.query(dbquery,function (error, data, fields) {
              if(!error){
                 
                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                data.forEach(element => {
                    if(element.constructor==Array) {
                        if (!_.isEmpty(element)) {
                            if(!res.finished)res.status(200).json({response: element })
                        }
                        else{
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(404).json({ response: errorResponse })

                        }
                    }
                })
              }
              else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                console.log(errorResponse)
                if(!res.finished)res.status(500).json({ response: errorResponse })
              }
          });
          } 
          finally {
              db.release();
              console.log('released')
          }
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
        
    }
  });
};


exports.getStudentDailyDiary = function (req, res) {
  const { classID, studentID, recordSequenceNo, requiredCount } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ classID, studentID, recordSequenceNo, requiredCount}, diaryValidations.getStudentDailyDiary, function (err,value) {
    if (err) {
      log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      let dbquery=`     
                        set @studentID = ${utils.OptionalKey(studentID)};
                        set @classID = ${utils.OptionalKey(classID)};
                        set @recordSequenceNo = ${utils.OptionalKey(recordSequenceNo)};
                        set @requiredCount = ${utils.OptionalKey(requiredCount)};
                        call ${appConfig.getDbConfig().database}.v1_get_dailydiary_student (@studentID,@classID,@recordSequenceNo,@requiredCount);`
      console.log(dbquery)
      db.getConnection(function(err, db) {
        if(!err){
          try {
            db.query(dbquery,function (error, data, fields) {
              if(!error){
                 
                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                
                data.forEach(element => {
                    if(element.constructor==Array) {
                        if (!_.isEmpty(element)) {
                            if(!res.finished)res.status(200).json({response: element })
                        }
                        else{
                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                            if(!res.finished)res.status(404).json({ response: errorResponse })

                        }
                    }
                   
                })
              }
              else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                console.log(errorResponse)
                if(!res.finished)res.status(500).json({ response: errorResponse })
              }
          });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
           
    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      
          // async.parallel([
          //   function(callback){
          //     dbquery = `select * from v1_get_dailydiary_student_subjects where classID = '${classID}' `;
          //     if (studentID) {
          //       dbquery += ` AND studentID = '${studentID}'`;
          //     }
          //     if (!_.isEmpty(recordSequenceNo) && !_.isEmpty(requiredCount) ) {
          //       dbquery += ` LIMIT ${recordSequenceNo} , ${requiredCount}`;
          //     }
          //     else{
          //       dbquery += ` LIMIT 0 , 30`;
          //     }
          //     console.log(dbquery)
              
          //       db.query(dbquery, function (error, result1) {
                    
          //           callback(error,result1)
          //       });
          //   },
          //   function(callback){
          //     dbquery = `select * from v1_get_dailydiary_student_nonsubjects where classID = '${classID}' `;
          //     if (studentID) {
          //       dbquery += ` AND studentID = '${studentID}'`;
          //     }
          //     if (!_.isEmpty(recordSequenceNo) && !_.isEmpty(requiredCount) ) {
          //       dbquery += ` LIMIT ${recordSequenceNo} , ${requiredCount}`;
          //     }
          //     else{
          //       dbquery += ` LIMIT 0 , 30`;
          //     }
          //     console.log(dbquery)
              
          //       db.query(dbquery, function (error, result2) {
                    
          //           callback(error,result2)
          //       });
          //   }
    
          //   ],function(err,results){
          //       if(err){
          //         log.doFatalLog(fileNameForLogging, req.url, 'error in getting the data', err)
          //         const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          //         res.status(500).json({ response: errorResponse })
          //       }else{
          //         if (_.isEmpty(results[0]) && _.isEmpty(results[1])) {
                      
          //           log.doErrorLog(fileNameForLogging, req.url, 'data not present')
          //           const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
          //           if(!res.finished)res.status(404).json({ response: errorResponse })
          //         }
          //         else{
          //           if(!res.finished)res.status(200).json({response: results[0].concat(results[1])});
          //         }
          //       }
          //   })
      
      })
      
        
    }
  });
};


exports.getDailyDiaryListing = function (req, res) {
  const { classID, diaryGUID, subjectID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ classID, diaryGUID, subjectID}, diaryValidations.getDailyDiaryListing, function (err,value) {
    if (err) {
      log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {

      let dbquery=`     
                        
                        set @classID = ${utils.OptionalKey(classID)};
                        set @subjectID = ${utils.OptionalKey(subjectID)};
                        set @diaryGUID = ${utils.OptionalKey(diaryGUID)};
                        call ${appConfig.getDbConfig().database}.v1_get_dailydiary_listing (@classID,@subjectID,@diaryGUID);`
      console.log(dbquery)
      db.getConnection(function(err, db) {
        if(!err){
          try {
              db.query(dbquery,function (error, data, fields) {
                if(!error){
                   
                  log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                  
                  data.forEach(element => {
                      if(element.constructor==Array) {
                          if (!_.isEmpty(element)) {
                              if(!res.finished)res.status(200).json({response: element })
                          }
                          else{
                              log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                              if(!res.finished)res.status(404).json({ response: errorResponse })

                          }
                      }
                     
                  })
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  console.log(errorResponse)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
 
          // async.parallel([
          //   function(callback){
          //     dbquery = `select * from v1_get_dailydiary_student_subjects where FIND_IN_SET(classID,${classID}) `;
          //     if (diaryID) {
          //       dbquery += ` AND diaryID = '${diaryID}'`;
          //     }
          //     if (diaryGUID) {
          //       dbquery += ` AND diaryGUID = '${diaryGUID}'`;
          //     }

          //     if (studentID) {
          //       dbquery += ` AND studentID = '${studentID}'`;
          //     }
              
          //     if (subjectID) {
          //       dbquery += ` AND subjectID = '${subjectID}'`;
          //     }
          //     console.log(dbquery)
              
          //       db.query(dbquery, function (error, result1) {
                    
          //           callback(error,result1)
          //       });
          //   },
          //   function(callback){
          //     dbquery = `select * from v1_get_dailydiary_student_nonsubjects where FIND_IN_SET(classID,${classID}) `;
          //     if (diaryID) {
          //       dbquery += ` AND diaryID = '${diaryID}'`;
          //     }
          //     if (diaryGUID) {
          //       dbquery += ` AND diaryGUID = '${diaryGUID}'`;
          //     }

          //     if (studentID) {
          //       dbquery += ` AND studentID = '${studentID}'`;
          //     }
              
          //     if (subjectID) {
          //       dbquery += ` AND subjectID = '${subjectID}'`;
          //     }
          //     console.log(dbquery)
              
          //       db.query(dbquery, function (error, result2) {
                    
          //           callback(error,result2)
          //       });
          //   }
    
          //   ],function(err,results){
          //       if(err){
          //         log.doFatalLog(fileNameForLogging, req.url, 'error in getting the data', error)
          //         const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
          //         res.status(500).json({ response: errorResponse })
          //       }else{
          //         if (_.isEmpty(results[0]) && _.isEmpty(results[1])) {
                      
          //           log.doErrorLog(fileNameForLogging, req.url, 'data not present')
          //           const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
          //           if(!res.finished)res.status(404).json({ response: errorResponse })
          //         }
          //         else{
          //           if(!res.finished)res.status(200).json({response: results[0].concat(results[1])});
          //         }
          //       }
          //   })
   
        }
        else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
    
        }
      })
        
    }
  });
};