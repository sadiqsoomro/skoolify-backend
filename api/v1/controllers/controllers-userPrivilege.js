const Joi = require('joi'), utils =  require('../../utils/helper-methods'),
    userPrivilegeValidations = require('../validations/validations-userPrivilege'),
    db = require('../../../db'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging  = 'userPrivilege-controller'

import Error from '../response-classes/error'

// ==================================================================

exports.getRoles = function (req, res) {
    const { schoolID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID }, userPrivilegeValidations.getRoles, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_roles WHERE schoolID = '${schoolID}' `;
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getUserAssignedRoles = function (req, res) {
    const { schoolID,userID,userRoleID,branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID,userID,userRoleID,branchID }, userPrivilegeValidations.getUserAssignedRoles, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_user_assigned_roles WHERE schoolID = '${schoolID}' AND branchID =  '${branchID}' `;
                    if(userID)
                        dbquery+= `AND userID = '${userID}'`                        
                    if(userRoleID)
                        dbquery+= `AND userRoleID = '${userRoleID}'` 
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getRoleAccessRights = function (req, res) {
    const { schoolID, roleID,moduleID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ schoolID, roleID,moduleID }, userPrivilegeValidations.getRoleAccessRights, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`SELECT * FROM v1_get_role_access_rights WHERE schoolID = '${schoolID}' AND roleID = '${roleID}' AND isActive = "1"  `;
                    if(moduleID)
                        dbquery+= `AND moduleID = '${moduleID}'`  
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setRole= function(req, res) {
    const {roleID,roleName, roleDesc,schoolID,isActive} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({roleID,roleName, roleDesc,schoolID,isActive}, userPrivilegeValidations.setRole, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                   
                    let dbquery =`
                    set @roleID = ${utils.OptionalKey(roleID)};
                    set @roleName = ${utils.OptionalKeyString(roleName)};
                    set @roleDesc = ${utils.OptionalKeyString(roleDesc)};
                    set @schoolID = ${utils.OptionalKey(schoolID)};
                    set @isActive = ${utils.OptionalKey(isActive)};
                    call ${appConfig.getDbConfig().database}.v1_set_roles
                    (@roleID, @roleName, @roleDesc,@schoolID,@isActive);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(!res.finished)res.status(200).json({ response: "success" })
                        
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setAssignRoles= function(req, res) {
    const {roleLinkID,  userID, schoolID, branchID,roleID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({roleLinkID,  userID, schoolID, branchID,roleID}, userPrivilegeValidations.setAssignRoles, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`
                    set @roleLinkID = ${utils.OptionalKey(roleLinkID)};
                    set @userID = ${utils.OptionalKey(userID)};
                    set @schoolID = ${utils.OptionalKey(schoolID)};
                    set @branchID = ${utils.OptionalKey(branchID)};
                    set @roleID = ${utils.OptionalKey(roleID)};
                    call ${appConfig.getDbConfig().database}.v1_set_assign_roles
                    (@roleLinkID, @userID,@roleID,@branchID, @schoolID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(!res.finished)res.status(200).json({ response: "success" })

        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setDeAssignRoles= function(req, res) {
    const {roleLinkID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({roleLinkID}, userPrivilegeValidations.setDeAssignRoles, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`
                                    set @roleLinkID = ${utils.OptionalKey(roleLinkID)};
                                    call ${appConfig.getDbConfig().database}.v1_set_deassign_roles
                                    (@roleLinkID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}
exports.setRoleAccessRights= function(req, res) {
    const {accessID, moduleID, roleID,readAccess, addAccess,  editAccess, deleteAccess} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({accessID, moduleID, roleID,readAccess, addAccess,  editAccess, deleteAccess}, userPrivilegeValidations.setRoleAccessRights, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`
                    set @accessID = ${utils.OptionalKey(accessID)};
                    set @moduleID = ${utils.OptionalKey(moduleID)};
                    set @roleID = ${utils.OptionalKey(roleID)};
                    set @readAccess = ${utils.OptionalKey(readAccess)};
                    set @addAccess = ${utils.OptionalKey(addAccess)};
                    set @editAccess = ${utils.OptionalKey(editAccess)};
                    set @deleteAccess = ${utils.OptionalKey(deleteAccess)};
                    call ${appConfig.getDbConfig().database}.v1_set_roles_access_rights
                    (@accessID, @moduleID, @roleID,@readAccess,@addAccess,@editAccess,@deleteAccess);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if(!res.finished)res.status(200).json({ response: "success" })

        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setRoleAccessRightsArray = function (req, res) {
    const { array } = req.body;
    console.log(req.body)
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, userPrivilegeValidations.setRoleAccessRightsArray,function (err, value) {
            if (err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } else {
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array != undefined){

                        let dbquery = "";
                        //let array = arrayRights;
                        for (let i = 0; i < array.length; i++) {
                            dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_roles_access_rights
                                (${utils.OptionalKey(array[i].accessID)}, 
                                ${utils.OptionalKey(array[i].moduleID)}, 
                                ${utils.OptionalKey(array[i].roleID)}, 
                                ${utils.OptionalKey(array[i].readAccess)},
                                ${utils.OptionalKey(array[i].addAccess)},
                                ${utils.OptionalKey(array[i].editAccess)},
                                ${utils.OptionalKey(array[i].deleteAccess)}
         
                                
                                );  `
                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                   
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: "success" })
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                            if(!res.finished)res.status(500).json({ response: "'array not found'" })
              
                        }
                        
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            }
        })
}


exports.deleteRole = function (req, res) {
    const { roleID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ roleID }, userPrivilegeValidations.deleteRole, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `call ${appConfig.getDbConfig().database}.v1_delete_role(${utils.OptionalKey(roleID)});`;
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({response: "success"})
                                }
        
                            }
                            else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    }   
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.setAssignRolesArray= function(req, res) {
    const {array} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate(array, userPrivilegeValidations.setAssignRolesArray, 
        function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    
                    if (array !== undefined){
                        let dbquery = "";
                            //let array = arrayRights;
                            for (let i = 0; i < array.length; i++) {
                                dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_assign_roles
                                    (${utils.OptionalKey(array[i].roleLinkID)}, 
                                    ${utils.OptionalKey(array[i].userID)}, 
                                    ${utils.OptionalKey(array[i].roleID)} ,
                                    ${utils.OptionalKey(array[i].branchID)},
                                    ${utils.OptionalKey(array[i].schoolID)}
                                    
                                    );  `
                            }
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if(!res.finished)res.status(200).json({ response: "success" })

            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                            
                            
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not found')
                            if(!res.finished)res.status(500).json({ response: "array not defined" })

                        }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

