const Joi = require('joi'), utils =  require('../../utils/helper-methods'),
    teacherValidations = require('../validations/validations-teacher'),
    db = require('../../../db'),
    log = require('../../utils/utils-logging'),
    _ = require('lodash/core'),
    appConfig = require('../../../app-config'),
    fileNameForLogging  = 'teacher-controller'
import Error from '../response-classes/error'

// ==================================================================

exports.assignTeacher = function(req, res) {
    const { teacherSubjectLinkID,userID,classID,subjectID,isClassTeacherFlag} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({teacherSubjectLinkID,userID,classID,subjectID,isClassTeacherFlag}, teacherValidations.assignTeacher,function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {                        
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                            set @teacherSubjectLinkID = ${utils.OptionalKey( teacherSubjectLinkID)};
                            set @userID = ${utils.OptionalKey(userID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            set @subjectID = ${utils.OptionalKey(subjectID)};
                            set @isClassTeacherFlag = ${utils.OptionalKey(isClassTeacherFlag)};
                            call ${appConfig.getDbConfig().database}.v1_set_assign_subject_teacher
                            (@teacherSubjectLinkID,@userID,@classID,@subjectID,@isClassTeacherFlag);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success"})
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.deleteSubjectTeacher = function(req, res) {
    const { teacherSubjectLinkID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({teacherSubjectLinkID}, teacherValidations.deleteSubjectTeacher,function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {                        
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                        set @teacherSubjectLinkID = ${utils.OptionalKey( teacherSubjectLinkID)};
                        call ${appConfig.getDbConfig().database}.v1_delete_teacher_subject_link
                        (@teacherSubjectLinkID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success"})
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                    console.log(errorResponse)
                                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.assignNSTeacher = function(req, res) {
    const { userID,classID,nonsubjectID,isClassTeacherFlag} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({userID,classID,nonsubjectID,isClassTeacherFlag}, teacherValidations.assignNSTeacher,function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {                        
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                            
                            set @userID = ${utils.OptionalKey(userID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            set @nonsubjectID = ${utils.OptionalKey(nonsubjectID)};
                            set @isClassTeacherFlag = ${utils.OptionalKey(isClassTeacherFlag)};
                            call ${appConfig.getDbConfig().database}.v1_set_assign_nonsubject_teacher
                            (@userID,@classID,@nonsubjectID,@isClassTeacherFlag);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                        if(!error){
                            log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                            if(!res.finished)res.status(200).json({ response: "success"})
    
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                        }
                    });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteNSTeacher = function(req, res) {
    const { nonSubjectLinkID} = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({nonSubjectLinkID}, teacherValidations.deleteNSTeacher,function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {                        
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery=`
                        set @nonSubjectLinkID = ${utils.OptionalKey( nonSubjectLinkID)};
                        call ${appConfig.getDbConfig().database}.v1_delete_teacher_nonsubject_link
                        (@nonSubjectLinkID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                if(!res.finished)res.status(200).json({ response: "success"})
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    

                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}