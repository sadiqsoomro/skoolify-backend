const Joi = require('joi'), utils = require('../../utils/helper-methods'),
    timeTableValidations = require('../validations/validations-timetable'),
    appConfig = require('../../../app-config'),
    db = require('../../../db'),
    _ = require('lodash/core'),
    log = require('../../utils/utils-logging'),
    fileNameForLogging = 'timetable-controller'
import Error from '../response-classes/error'

// ==================================================================

exports.getTimeTable = function (req, res) {
    const { classID, branchID, schoolID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, branchID, schoolID }, timeTableValidations.getTimeTable, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_timetable 
                    WHERE classID = '${classID}'  
                    AND branchID = '${branchID}'  
                    AND schoolID = '${schoolID}' `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.setTimeTable = function (req, res) {
    const { tableID, classID,  weekday, time1,subjectID1,time2,subjectID2,time3,subjectID3
        ,time4,subjectID4,time5,subjectID5,time6,subjectID6,time7,subjectID7,time8,subjectID8,time9,subjectID9,time10,subjectID10 } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate({tableID, classID,  weekday, time1,subjectID1,time2,subjectID2,time3,subjectID3
            ,time4,subjectID4,time5,subjectID5,time6,subjectID6,time7,subjectID7,time8,subjectID8,time9,subjectID9,time10,subjectID10 }, timeTableValidations.setTimeTable,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                    let dbquery=` call  ${appConfig.getDbConfig().database}.v1_set_timetable
                    (${utils.OptionalKey(tableID)} , '${classID}', '${weekday}',${utils.OptionalKey(time1)},${utils.OptionalKey(subjectID1)}
                    ,${utils.OptionalKey(time2)},${utils.OptionalKey(subjectID2)} ,${utils.OptionalKey(time3)},${utils.OptionalKey(subjectID3)}
                    ,${utils.OptionalKey(time4)},${utils.OptionalKey(subjectID4)} ,${utils.OptionalKey(time5)},${utils.OptionalKey(subjectID5)}
                    ,${utils.OptionalKey(time6)},${utils.OptionalKey(subjectID6)} ,${utils.OptionalKey(time7)},${utils.OptionalKey(subjectID7)}
                    ,${utils.OptionalKey(time8)},${utils.OptionalKey(subjectID8)} ,${utils.OptionalKey(time9)},${utils.OptionalKey(subjectID9)}
                    ,${utils.OptionalKey(time10)},${utils.OptionalKey(subjectID10)}); `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    if(!res.finished)res.status(200).json({ response: "success" })     
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            



            }
    });
}

exports.setTimeTableArray = function (req, res) {
    const { array } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate( array , timeTableValidations.setTimeTableArray,function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                        if (array != undefined){
                            let dbquery=""
                            for (let i = 0; i < array.length; i++) {
                
                                dbquery += ` call ${appConfig.getDbConfig().database}.v1_set_timetable
                                (${utils.OptionalKey(array[i].tableID)},
                                ${utils.OptionalKey(array[i].classID)},
                                ${utils.OptionalKey(array[i].weekdayNo)},
                                ${utils.OptionalKey(array[i].timetableTimeID)},
                                ${utils.OptionalKey(array[i].subjectID)},
                                ${utils.OptionalKey(array[i].subjectType)},
                                ${utils.OptionalKey(array[i].startTime)},
                                ${utils.OptionalKey(array[i].endTime)});  `
                
                                
                
                            }
                            console.log(dbquery)
                            try {
                                db.query(dbquery,function (error, data, fields) {
                                    if(!error){
                                    
                                        if(!res.finished)res.status(200).json({ response: "success" })     
                
                                    }
                                    else{
                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } 
                            finally {
                                db.release();
                                console.log('released')
                            }
                            
                                
                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not defined')
                            if(!res.finished)res.status(500).json({ response: "array not defined" })
    
                        }
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                })
            
            }
    });
}


exports.deleteTimeTable= function(req, res) {
    const {tableID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({tableID}, timeTableValidations.deleteTimeTable, function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery =`
                    set @tableID = ${utils.OptionalKey(tableID)};
                    call ${appConfig.getDbConfig().database}.v1_delete_timetable
                    (@tableID);`
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                                if(!res.finished)res.status(200).json({ response: "success" })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        
        }
    })
}


exports.getActivity = function (req, res) {
    const { activityID,branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ activityID,branchID }, timeTableValidations.getActivity, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_activity where branchID=${branchID} `

                    if (activityID) {
                        dbquery += ` and activityID=${activityID}`
                    }
                    
                
                    
                    console.log(dbquery)
                    
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.getActiveActivity = function (req, res) {
    const { activityID,branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ activityID,branchID }, timeTableValidations.getActiveActivity, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_activity where isActive=1 and branchID=${branchID} `

                    if (activityID) {
                        dbquery += ` and activityID=${activityID}`
                    }
                    
                
                    
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                        
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}



exports.setActivityArray = function (req, res) {
    const { array } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate(array, timeTableValidations.setActivityArray,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                    let dbquery=``
                    if (array!= undefined){
                        
                        for(let i=0;i<array.length;i++){
                            dbquery +=` call  ${appConfig.getDbConfig().database}.v1_set_activity
                            (${utils.OptionalKey(array[i].activityID)},
                            '${array[i].branchID}', 
                            '${array[i].activityName.replace("'","''")}',
                            '${array[i].startTime}',
                            '${array[i].endTime}', 
                            ${utils.OptionalKey(array[i].userID)},
                            '${array[i].isActive}'); `
                            
                        }

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'array no found', )
                        if(!res.finished)res.status(500).json({ response: "array not found" })
                    }
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if(!res.finished)res.status(200).json({ response: "success" })     
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            }
    });
}



exports.deleteActivity = function (req, res) {
    const { activityID } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate({activityID }, timeTableValidations.deleteActivity,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                    let dbquery=` call  ${appConfig.getDbConfig().database}.v1_delete_activity
                    (${utils.OptionalKey(activityID)}); `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if (_.isEmpty(data)) {
                                        
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({response: "success"})
                                    }
                  
                                }
                                else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            



            }
    });
}



exports.getStudentActivities = function (req, res) {
    const {activityStudentID,activityID,classID,studentID,weekdayNo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ activityStudentID,activityID,classID,studentID ,weekdayNo}, timeTableValidations.getStudentActivities, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_student_activities where activityID=${activityID}`

                    if (classID) {
                        dbquery += ` and classID=${classID}`
                    }

                    if (activityStudentID) {
                        dbquery += ` and activityStudentID=${activityStudentID}`
                    }

                    if (studentID) {
                        dbquery += ` and studentID=${studentID}`
                    }
                    if (weekdayNo) {
                        dbquery += ` and weekdayNo IN (${weekdayNo})`
                    }

                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.setStudentActivitiesArray = function (req, res) {
    const { array } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate(array, timeTableValidations.setStudentActivitiesArray,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                        if(array != undefined){
                            let dbquery=``
                        for(let i=0;i<array.length;i++){
                            dbquery +=` call  ${appConfig.getDbConfig().database}.v1_set_student_activities
                            (${utils.OptionalKey(array[i].activityStudentID)},
                            '${array[i].actionFlag}', 
                            '${array[i].activityID}', 
                            '${array[i].studentID}',
                            '${array[i].weekdayNo}'); `
                        }
                        console.log(dbquery)
                        try {
                            db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if(!res.finished)res.status(200).json({ response: "success" })     
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                       
                            

                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'array not defined')
                            if(!res.finished)res.status(500).json({ response: 'array no defined' })

                        }
                        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
    
            }
    });
}

exports.deleteStudentActivities = function (req, res) {
    const { activityStudentID } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate({activityStudentID }, timeTableValidations.deleteStudentActivities,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                    let dbquery=` call  ${appConfig.getDbConfig().database}.v1_delete_student_activities
                    (${utils.OptionalKey(activityStudentID)}); `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if(!res.finished)res.status(200).json({ response: "success" })     
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
    });
}



exports.getTimeTableTimings = function (req, res) {
    const { classID, weekdayNo } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ classID, weekdayNo }, timeTableValidations.getTimeTableTimings, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_time_table_timings 
                    WHERE classID = '${classID}'  
                   
                    AND weekdayNo = '${weekdayNo}' order by startTime`


                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                    
                       
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}


exports.deleteTimeTableTimings = function (req, res) {
    const { tableID,  timetableTimeID } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate({ tableID, timetableTimeID }, timeTableValidations.deleteTimeTableTimings,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                    let dbquery=` call  ${appConfig.getDbConfig().database}.v1_delete_timetable_timing
                    (${utils.OptionalKey(tableID)},
                    ${utils.OptionalKey(timetableTimeID)}); `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    if (_.isEmpty(data)) {
                                        log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                        if(!res.finished)res.status(200).json({ response: errorResponse })
                                    } else {
                                        log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                        if(!res.finished)res.status(200).json({response: "success"})
                                    }
                  
                                }
                                else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
                           
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })
            
            



            }
    });
}

exports.getStudentTimeTable = function (req, res) {
    const { studentID } = req.body;
  
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
    Joi.validate({ studentID },timeTableValidations.getStudentTimeTable,function (err, value) {
        if (err) {
          log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } else {
          db.getConnection(function(err, db) {
            if(!err){
              let dbquery=`  set @studentID = ${utils.OptionalKey(studentID)};
              call ${appConfig.getDbConfig().database}.v1_get_student_timetable
              (@studentID);`
              console.log(dbquery)
              
              try {
                db.query(dbquery,function (error, data, fields) {
                    if(!error){
                    
                        data.forEach(element => {
                            if(element.constructor==Array) {
                                if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                if(!res.finished)res.status(404).json({ response: errorResponse });
                                } else {
                                log.doInfoLog(
                                    fileNameForLogging,req.url,"successfully data fetched from db");
                                    let innerArray = new Array();
                                    let noOfIterations = element.length;
            
                                    let distinctTable = new Array();
                                    let distinctObjects = new Array();
            
                                    for (let loop = 0; loop < noOfIterations; loop++) {
                                        if (!distinctTable.includes(element[loop].weekdayNo)) {
                                            distinctTable.push(element[loop].weekdayNo);
            
                                            var partialData = Object.assign({}, element[loop]);
            
                                            if (partialData.hasOwnProperty("startTime")) {
                                                delete partialData.startTime;
                                            }
                                            if (partialData.hasOwnProperty("endTime")) {
            
                                                delete partialData.endTime;
                                            }
                                            if (partialData.hasOwnProperty("subjectName")) {
            
                                                delete partialData.subjectName;
                                            }
            
                                            if (partialData.hasOwnProperty("teacherName")) {
            
                                                delete partialData.teacherName;
                                            }
                                            distinctObjects.push(partialData);
            
                                        }
                                    }
                                    
                                    for (let j = 0; j < distinctObjects.length; j++) {
                                        let formattedData = new Array();
                                        if (distinctObjects[j].weekdayName ){
                                        for (let i = 0; i < element.length; i++) {
                                            if (distinctObjects[j].weekdayNo == element[i].weekdayNo) {
                                                
                                                if (element[i].hasOwnProperty("weekdayNo")) {
            
                                                    delete element[i].weekdayNo;
                                                }
                                                if (element[i].hasOwnProperty("weekdayName")) {
            
                                                    delete element[i].weekdayName;
                                                }
                                                if (element[i].hasOwnProperty("studentID")) {
            
                                                    delete element[i].studentID;
                                                }
                                                
                                                formattedData.push(element[i]);
                                            }
                                        }
                                        distinctObjects[j].TimeTable = formattedData;
                                    }}
            
                                    if(!res.finished)res.status(200).json({
                                        response: distinctObjects
                                    })
                                }
                        
        
                            }
                        })
        
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
        
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }
          })
        }
      }
    );
  };

  exports.setSubjectTimetable = function (req, res) {
    const { timetableSubjectID, subjectID,  weekdayNo, startTime,endTime,roomID,userID } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({timetableSubjectID, subjectID,  weekdayNo, startTime,endTime,roomID, userID }, timeTableValidations.setSubjectTimetable,function(err, value) {
        if(err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } 
        else {     
            db.getConnection(function(err, db) {
                if(!err){
                let dbquery=` call  ${appConfig.getDbConfig().database}.v1_set_subject_timetable
                (${utils.OptionalKey(timetableSubjectID)},
                ${utils.OptionalKey(subjectID)},
                ${utils.OptionalKey(weekdayNo)},
                ${utils.OptionalKey(startTime)},
                ${utils.OptionalKey(endTime)},
                ${utils.OptionalKey(roomID)},
                ${utils.OptionalKey(userID)}
                ); `
                console.log(dbquery)
                try {
                    db.query(dbquery,function (error, data, fields) {
                            if(!error){
                            
                                if(!res.finished)res.status(200).json({ response: "success" })     
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        
        



        }
});
}
  


exports.getSubjectTimetable = function (req, res) {
    const { branchID, subjectID, weekdayNo,userID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({ branchID, subjectID, weekdayNo,userID  }, timeTableValidations.getSubjectTimetable, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
            db.getConnection(function(err, db) {
                if(!err){
                    let dbquery = `SELECT * FROM v1_get_subject_timetable
                    WHERE  branchID = '${branchID}'`  
                    if (weekdayNo){
                        dbquery += ` AND weekdayNo = '${weekdayNo}' `
                    }
                    if (subjectID){
                        dbquery += ` AND subjectID = '${subjectID}' `
                    }
                    if (userID){
                        dbquery += ` AND userID = '${userID}' `
                    }
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                                if (_.isEmpty(data)) {
                                    log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                    if(!res.finished)res.status(200).json({ response: errorResponse })
                                } else {
                                    log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                    if(!res.finished)res.status(200).json({ response: data })
                                }
        
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                    
        
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
        
                }
            })
        }
    })
}

exports.deleteSubjectTimetable = function (req, res) {
    const { timetableSubjectID } = req.body
        log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
        Joi.validate({timetableSubjectID }, timeTableValidations.deleteSubjectTimetable,
            function(err, value) {
            if(err) {
                log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
            } 
            else {     
                db.getConnection(function(err, db) {
                    if(!err){
                    let dbquery=` call  ${appConfig.getDbConfig().database}.v1_delete_subject_timetable
                    ('${timetableSubjectID}'); `
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                
                                    if(!res.finished)res.status(200).json({ response: "success" })     
            
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                }
                            });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                        
                            
            
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
            
                    }
                })

            }
    });
}