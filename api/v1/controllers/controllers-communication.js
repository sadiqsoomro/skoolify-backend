const   db = require('../../../db'),
        Joi = require("joi"),
        utils = require("../../utils/helper-methods"),
        _ = require('lodash/core'),
        appConfig = require('../../../app-config'),
        commValidations = require('../validations/validations-communication'),
        log = require('../../utils/utils-logging'),
        push = require("../../utils/push-notification"),
        fileNameForLogging = 'communication-controller'
import Error from '../response-classes/error'


exports.setCommunicationTypes = function (req, res) {
    const { commTypeID,schoolID,branchID,commTypeName,isActive } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ commTypeID,schoolID,branchID,commTypeName,isActive }, commValidations.setCommunicationTypes, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
                let dbquery=` set @commTypeID = ${utils.OptionalKey(commTypeID)};
                                set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @branchID = ${utils.OptionalKey(branchID)};
                                set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @commTypeName = ${utils.OptionalKey(commTypeName)};
                                set @isActive = ${utils.OptionalKey(isActive)};
                                call ${appConfig.getDbConfig().database}.v1_set_communication_types (@commTypeID,@schoolID,@branchID,@commTypeName,@isActive);`
                console.log(dbquery)
                db.getConnection(function(err, db) {
                    if(!err){
                        try{
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                    if(!res.finished)res.status(200).json({response: "success" })
                                
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                
                                }
                            }); 
                        }
                        finally{
                            db.release();
                            console.log('released')
                        }
                            
                
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                
                    }
                })
        }
    });   
}

exports.getCommunicationTypes = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ schoolID, branchID }, commValidations.getCommunicationTypes, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            let dbquery=` set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        
                        call ${appConfig.getDbConfig().database}.v1_get_communication_types (@schoolID,@branchID);`
            console.log(dbquery)

            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                            
                                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                data.forEach(element => {
                                    if(element.constructor==Array) {
                                        if (!_.isEmpty(element)) {
                                            if(!res.finished)res.status(200).json({response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                        }
                                    }
                                })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
            
                }
            })
        }
    });
}


exports.getCommunicationTypesMobile = function (req, res) {
    const { schoolID, branchID } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ schoolID, branchID }, commValidations.getCommunicationTypesMobile, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            let dbquery=` set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        
                        call ${appConfig.getDbConfig().database}.v1_get_communication_types_mobile (@schoolID,@branchID);`
            console.log(dbquery)

            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                            
                                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                data.forEach(element => {
                                    if(element.constructor==Array) {
                                        if (!_.isEmpty(element)) {
                                            if(!res.finished)res.status(200).json({response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                        }
                                    }
                                })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
            
                }
            })
        }
    });
}

exports.setCommunicationThreadMobile = function (req, res) {
    const { parentUUID, studentUUID, threadUUID, threadSubject, commTypeID,commText } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ parentUUID, studentUUID, threadUUID, threadSubject, commTypeID,commText }, commValidations.setCommunicationThreadMobile, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
                let dbquery=` set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @studentUUID = ${utils.OptionalKey(studentUUID)};
                                set @threadUUID = ${utils.OptionalKey(threadUUID)};
                                set @threadSubject = ${utils.OptionalKey(threadSubject)};
                                set @commTypeID = ${utils.OptionalKey(commTypeID)};
                                set @commText = ${utils.OptionalKey(commText)};
                                call ${appConfig.getDbConfig().database}.v1_set_communication_thread_mobile (@parentUUID,@studentUUID,@threadUUID,@threadSubject,@commTypeID,@commText);`
                console.log(dbquery)
                db.getConnection(function(err, db) {
                    if(!err){
                        try{
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                    if(!res.finished)res.status(200).json({response: "success" })

                                    
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                
                                }
                            }); 
                        }
                        finally{
                            db.release();
                            console.log('released')
                        }
                            
                
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                
                    }
                })
        }
    });   
}


exports.setCommunicationThread = function (req, res) {
    const { parentUUID, studentUUID, userUUID, threadUUID, threadSubject, commTypeID,commText } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ parentUUID, studentUUID,userUUID, threadUUID, threadSubject, commTypeID,commText }, commValidations.setCommunicationThread, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
                let dbquery=`   set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @studentUUID = ${utils.OptionalKey(studentUUID)};
                                set @userUUID = ${utils.OptionalKey(userUUID)};
                                set @threadUUID = ${utils.OptionalKey(threadUUID)};
                                set @threadSubject = ${utils.OptionalKey(threadSubject)};
                                set @commTypeID = ${utils.OptionalKey(commTypeID)};
                                set @commText = ${utils.OptionalKey(commText)};
                                call ${appConfig.getDbConfig().database}.v1_set_communication_thread (@parentUUID,@studentUUID,@userUUID,@threadUUID,@threadSubject,@commTypeID,@commText);`
                console.log(dbquery)
                db.getConnection(function(err, db) {
                    if(!err){
                        try{
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                    if(!res.finished)res.status(200).json({response: "success" })
                                    
                                    data.forEach(element => {
                                        if(element.constructor==Array) {
                                            if (!_.isEmpty(element)) {
                                                let tokenIDsArray=[]
                                                for (let i=0;i < element.length;i++){
                                                    tokenIDsArray.push(element[i].deviceToken)
                                                }
                                                var messageConfig = appConfig.messageSettings(
                                                    "Communication",
                                                    utils.OptionalKey(commText),
                                                    "Chat",
                                                    element[0].parentID,
                                                    element[0].studentID,
                                                    element[0].classID,
                                                    element[0].branchID,
                                                    element[0].schoolID,
                                                    threadUUID
                    
                                                  );
                                                //   console.log(tokenIDsArray)
                                                  push.SendNotification(tokenIDsArray,messageConfig);
                                                
                                            }
                                            
                                        }
                                    })
                                    
                                
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                
                                }
                            }); 
                        }
                        finally{
                            db.release();
                            console.log('released')
                        }
                            
                
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                
                    }
                })
        }
    });   
}


exports.getCommunicationThreadMobile = function (req, res) {
    const { threadUUID, parentUUID, studentUUID, lastDate} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ threadUUID, parentUUID, studentUUID, lastDate }, commValidations.getCommunicationThreadMobile, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            let dbquery=` set @threadUUID = ${utils.OptionalKey(threadUUID)};
                        set @parentUUID = ${utils.OptionalKey(parentUUID)};
                        set @studentUUID = ${utils.OptionalKey(studentUUID)};
                        set @lastDate = ${utils.OptionalKey(lastDate)};
                        
                        call ${appConfig.getDbConfig().database}.v1_get_communication_thread_mobile (@threadUUID, @parentUUID, @studentUUID, @lastDate );`
            console.log(dbquery)

            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                            
                                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                data.forEach(element => {
                                    if(element.constructor==Array) {
                                        if (!_.isEmpty(element)) {
                                            let mobileCommThead = new Array();
                                            for (let i=0;i < element.length; i++){
                                                if(element[i].userUUID)
                                                    element[i].user ={_id:element[i].userUUID}
                                                else
                                                    element[i].user ={_id:1}
                                                
                                                if (element[i].communicationID){
                                                        element[i]._id=element[i].communicationID
                                                        delete element[i].communicationID
                                                        
                                                }
                                                mobileCommThead.push(element[i]) 
                                                
                                            }
                                            if(!res.finished)res.status(200).json({response: mobileCommThead })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                        }
                                    }
                                })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
            
                }
            })
        }
    });
}

exports.getCommunicationGroupMobile = function (req, res) {
    const { parentUUID, studentUUID} = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ parentUUID, studentUUID }, commValidations.getCommunicationGroupMobile, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
            let dbquery=` 
                        set @parentUUID = ${utils.OptionalKey(parentUUID)};
                        set @studentUUID = ${utils.OptionalKey(studentUUID)};
                        call ${appConfig.getDbConfig().database}.v1_get_communication_group_mobile ( @parentUUID, @studentUUID );`
            console.log(dbquery)
            db.getConnection(function(err, db) {
                if(!err){
                    try {
                        db.query(dbquery,function (error, data, fields) {
                            if(!error){
                            
                                log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                data.forEach(element => {
                                    if(element.constructor==Array) {
                                        if (!_.isEmpty(element)) {
                                            if(!res.finished)res.status(200).json({response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                        }
                                    }
                                })
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
            
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                    const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
            
                }
            })
        }
    });
}



exports.setCommunicationStatusChange = function (req, res) {
    const { parentUUID, studentUUID, threadUUID, communicationID,readStatus } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ parentUUID, studentUUID, threadUUID, communicationID,readStatus}, commValidations.setCommunicationStatusChange, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
                let dbquery=` set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @studentUUID = ${utils.OptionalKey(studentUUID)};
                                set @threadUUID = ${utils.OptionalKey(threadUUID)};
                                set @communicationID = ${utils.OptionalKey(communicationID)};
                                set @readStatus = ${utils.OptionalKey(readStatus)};
                                
                                call ${appConfig.getDbConfig().database}.v1_set_communication_status_change (@parentUUID,@studentUUID,@threadUUID,@communicationID,@readStatus);`
                console.log(dbquery)
                db.getConnection(function(err, db) {
                    if(!err){
                        try{
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                    if(!res.finished)res.status(200).json({response: "success" })
                                
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                
                                }
                            }); 
                        }
                        finally{
                            db.release();
                            console.log('released')
                        }
                            
                
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                
                    }
                })
        }
    });   
}


exports.setCommunicationArchive = function (req, res) {
    const { parentUUID, studentUUID, threadUUID,archiveFlag } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    Joi.validate({ parentUUID, studentUUID, threadUUID,archiveFlag}, commValidations.setCommunicationArchive, function (err,value) {
        if (err) {
            log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
        } 
        else {
                let dbquery=` set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                set @studentUUID = ${utils.OptionalKey(studentUUID)};
                                set @threadUUID = ${utils.OptionalKey(threadUUID)};
                                set @archiveFlag = ${utils.OptionalKey(archiveFlag)};
                                
                                call ${appConfig.getDbConfig().database}.v1_set_communication_archive (@parentUUID,@studentUUID,@threadUUID,@archiveFlag);`
                console.log(dbquery)
                db.getConnection(function(err, db) {
                    if(!err){
                        try{
                        db.query(dbquery,function (error, data, fields) {
                                if(!error){
                                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                                    if(!res.finished)res.status(200).json({response: "success" })
                                
                                }
                                else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    console.log(errorResponse)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                
                                }
                            }); 
                        }
                        finally{
                            db.release();
                            console.log('released')
                        }
                            
                
                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                        const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                
                    }
                })
        }
    });   
}


// exports.getCommunicationSearch = function (req, res) {
//     const { threadSubject, parentUUID, studentUUID} = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
//     Joi.validate({ threadSubject, parentUUID, studentUUID }, commValidations.getCommunicationSearch, function (err,value) {
//         if (err) {
//             log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
//             if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
//         } 
//         else {
//             let dbquery=` set @threadSubject = ${utils.OptionalKey(threadSubject)};
//                         set @parentUUID = ${utils.OptionalKey(parentUUID)};
//                         set @studentUUID = ${utils.OptionalKey(studentUUID)};
//                         call ${appConfig.getDbConfig().database}.v1_get_communication_search ( @parentUUID, @studentUUID,@threadSubject);`
//             console.log(dbquery)

//             db.getConnection(function(err, db) {
//                 if(!err){
//                     try {
//                         db.query(dbquery,function (error, data, fields) {
//                             if(!error){
                            
//                                 log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
//                                 data.forEach(element => {
//                                     if(element.constructor==Array) {
//                                         if (!_.isEmpty(element)) {
//                                             if(!res.finished)res.status(200).json({response: element })
//                                         }
//                                         else{
//                                             log.doErrorLog(fileNameForLogging, req.url, 'data not present')
//                                             const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
//                                             if(!res.finished)res.status(404).json({ response: errorResponse })
            
//                                         }
//                                     }
//                                 })
//                             }
//                             else{
//                                 log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
//                                 const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
//                                 console.log(errorResponse)
//                                 if(!res.finished)res.status(500).json({ response: errorResponse })
//                             }
//                         });
//                     } 
//                     finally {
//                         db.release();
//                         console.log('released')
//                     }
            
//                 }
//                 else{
//                     log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
//                     const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                     if(!res.finished)res.status(500).json({ response: errorResponse })
            
//                 }
//             })
//         }
//     });
// }
