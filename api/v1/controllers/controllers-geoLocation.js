const Joi = require('joi'), utils = require('../../utils/helper-methods'),
    geoLocationValidations = require('../validations/validations-geoLocation'),
    iplocation = require("iplocation").default,
    _ = require('lodash'),
    appConfig = require('../../../app-config'),
    log = require('../../utils/utils-logging'),
    helper = require('../../utils/helper-methods'),
    fileNameForLogging = 'geoLocation-controller'
import Error from '../response-classes/error'
var countryTelData = require('country-telephone-data');

// ==================================================================

exports.getIP2Location = function (req, res) {
    const { IPAddress } = req.body

    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

    Joi.validate({IPAddress}, geoLocationValidations.getIP2Location, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
                iplocation(IPAddress).then((data) => {
                    let dialCodeData = "";
                    if(data != null && data.countryCode != "")
                    dialCodeData = countryTelData.allCountries[countryTelData.iso2Lookup[data.countryCode.toLowerCase()]].dialCode;
                    data.dialCode = dialCodeData;
                    if(!res.finished)res.status(200).json({response: data})
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'error in getting location', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                });
            
        }
    })
}

