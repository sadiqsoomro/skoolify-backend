const Joi = require("joi"),
  utils = require("../../utils/helper-methods"),
  messageValidations = require("../validations/validations-message"),
  db = require('../../../db'),
  _ = require("lodash/core"),
  appConfig = require("../../../app-config"),
  log = require("../../utils/utils-logging"),
  xl = require('excel4node'),

  fileNameForLogging = "message-controller";

const uuidv4 = require("uuid/v4");
import Error from "../response-classes/error";

const push = require("../../utils/push-notification");
// ==================================================================

exports.getMessages = function(req, res) {
  const { schoolID, messageType, messageGUID, messageDateFrom, messageDateTo, branchID, cityId,areaId, classID,studentID,parentID, parentUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( {schoolID, messageGUID, messageDateFrom, messageType, messageDateTo, branchID, cityId,areaId, classID,studentID,parentID, messageType, parentUUID }, messageValidations.getMessages,
    function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging, req.url, "required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_messages `;
            dbquery += ` WHERE messageDate BETWEEN '${messageDateFrom}' AND '${messageDateTo}' `;
            if (!_.isEmpty(messageGUID)) {
              dbquery += ` AND messageGUID = '${messageGUID}' `;
            }
            if (!_.isEmpty(schoolID)) {
              dbquery += ` AND schoolID = '${schoolID}' `;
            }
            if (!_.isEmpty(branchID)) {
              dbquery += ` AND branchID = '${branchID}'`;
            }
            if (!_.isEmpty(cityId)) {
              dbquery += ` AND cityID = '${cityId}'`;
            }
            if (!_.isEmpty(areaId)) {
              dbquery += ` AND areaID = '${areaId}'`;
            }
            if (!_.isEmpty(classID)) {
              dbquery += ` AND classID IN (${classID}) `;
            }
            if (!_.isEmpty(studentID)) {
              dbquery += ` AND studentID IN (${studentID}) `;
            }
            if (!_.isEmpty(messageType)) {
              dbquery += ` AND messageType = '${messageType}'`;
            }
            if (!_.isEmpty(parentID)) {
              dbquery += ` AND parentID = '${parentID}'`;
            }
            if (!_.isEmpty(parentUUID)) {
              dbquery += ` AND parentUUID = '${parentUUID}'`;
            }
            
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    if (_.isEmpty(data)) {
                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,  "data not present" );
                      if(!res.finished)res.status(200).json({ response: errorResponse });
                    } else {
                      log.doInfoLog(fileNameForLogging,req.url, "successfully data fetched from db" );
                      if(!res.finished)res.status(200).json({ response: data });
                    }
                        
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
            
                 
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      
      
      
      }
    }
  );
};

exports.deleteMessages = function(req, res) {
  const { messageGUID } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  
  Joi.validate({messageGUID }, messageValidations.deleteMessages,
    function(err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`set @messageGUID = ${utils.OptionalKey(messageGUID)};
                        call ${appConfig.getDbConfig().database}.v1_delete_messages
                        (@messageGUID);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, "db execution successfull", null);
                    if(!res.finished)res.status(200).json({ response: "success" });
     
                  }
                  else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
          }
          else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
          }
          
        })
      }
    });
};

exports.setConsent = function(req, res) {
  const { messageID, parentID,parentUUID, consentMessage, consentResponse } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( { messageID, parentID,parentUUID, consentMessage,consentResponse }, messageValidations.setConsent,
    function(err, value) {
      if (err) {
        log.doErrorLog(
          fileNameForLogging,req.url,"required params validation failed.");
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`  call ${appConfig.getDbConfig().database}.v1_set_consent
                          (${utils.OptionalKey(messageID)}, 
                          ${utils.OptionalKey(parentUUID)},
                          ${utils.OptionalKey(parentID)},
                          ${utils.OptionalKey(consentResponse)},
                          ${utils.OptionalKeyString(consentMessage)}
                          )`
              console.log(dbquery)
              try {
                  db.query(dbquery,function (error, data, fields) {
                    if(!error){
                      if(!res.finished)res.status(200).json({ response: "success" });
                    }
                    else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                    }
                }); 
              } 
              finally {
                  db.release();
                  console.log('released')
              }
                         
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      }
    }
  );
};

exports.setMessagesArray = function (req, res) {
    const { array } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)
    let guid = uuidv4();

    Joi.validate(array, messageValidations.setMessagesArray, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
            if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
        } else {
          if(array != undefined){
            if (array[0].notificationOnly=='1'){

            db.getConnection(function(err, db) {
            if(!err){
              let dbquery = `SELECT * FROM v1_get_global_notifications `;
              let countryIDList=[], cityIDList=[], branchIDList=[], areaIDList=[],
                    classLevelIDList=[], classIDList=[], studentIDList=[],
                    messageTypeList=[], messageTextList=[]

              for (let i=0;i<array.length;i++){

                if (!_.isEmpty(array[i].messageType)) {
                      if(!messageTypeList.includes(array[i].messageType))  {      
                        messageTypeList.push(array[i].messageType);
                      }
                     
                    }

                    if (!_.isEmpty(array[i].messageText)) {
                      if(!messageTextList.includes(array[i].messageText))  {      
                        messageTextList.push(array[i].messageText);
                      }
                     
                    }

                if (!_.isEmpty(array[i].countryID )) {
                      if(!countryIDList.includes(array[i].countryID))  {      
                        countryIDList.push(array[i].countryID);
                      }
                     
                    }
                if (!_.isEmpty(array[i].cityID)  ) {
                              
                      if(!cityIDList.includes(array[i].cityID))  {      
                        cityIDList.push(array[i].cityID);
                      }
                    }

                if (!_.isEmpty(array[i].areaID)  ) {
                              
                      if(!areaIDList.includes(array[i].areaID))  {      
                        areaIDList.push(array[i].areaID);
                      }
                    }
                if (!_.isEmpty(array[i].branchID) ) {
                              
                      if(!branchIDList.includes(array[i].branchID))  {      
                        branchIDList.push(array[i].branchID);
                      }
                    }
                if (!_.isEmpty(array[i].classLevelID) ) {
                              
                      if(!classLevelIDList.includes(array[i].classLevelID))  {      
                        classLevelIDList.push(array[i].classLevelID);
                      }
                    }

                if (!_.isEmpty(array[i].classID) ) {
                              
                      if(!classIDList.includes(array[i].classID))  {      
                        classIDList.push(array[i].classID);
                      }
                    }
                if (!_.isEmpty(array[i].studentID) ) {
                              
                      if(!studentIDList.includes(array[i].studentID))  {      
                        studentIDList.push(array[i].studentID);
                      }
                    }

                }
                
                dbquery += ` WHERE countryID IN(${countryIDList.join()}) `

                  if (cityIDList.length> 0) {
                    dbquery += ` AND cityID IN (${cityIDList.join()})`
                  }
                  if (areaIDList.length > 0) {
                    dbquery += ` AND areaID IN (${areaIDList.join()})`
                  }

                  if (branchIDList.length > 0 ) {
                    dbquery += ` AND branchID IN (${branchIDList.join()})`
                  }

                  if (classLevelIDList.length > 0) {
                    dbquery += ` AND classLevelID IN (${classLevelIDList.join()})`
                  }

                  if (classIDList.length > 0 ) {
                    dbquery += ` AND classID IN (${classIDList.join()})`
                  }
                  if (studentIDList.length > 0) {
                    dbquery += ` AND studentID IN (${studentIDList.join()})`
                  } 

              console.log(dbquery)
                try {
                  db.query(dbquery,function (error, data, fields) {
                      if(!error){
                        if (_.isEmpty(data)) {  
                          log.doErrorLog(fileNameForLogging, req.url, "data not present");
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present" );
                          if(!res.finished)res.status(200).json({ response: errorResponse });
                        } else {
                          log.doInfoLog( fileNameForLogging,req.url, "successfully data fetched from db" );

                          if(!res.finished)res.status(200).json({ response: "success" });
                          if (!_.isEmpty(data)) {
                            for (let i = 0; i < data.length; i++) {
                              var messageConfig = appConfig.messageSettings(
                                messageTypeList.join(),
                                messageTextList.join(),
                                "MessageScreen",
                                data[i].parentID,
                                data[i].studentID,
                                data[i].classID,
                                data[i].branchID,
                                data[i].schoolID,
                                null

                              );

                              
                              push.SendNotification(data[i].deviceToken,messageConfig);
                            }
                          }
                        }
                            
                      }
                      else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                  });
                } 
                finally {
                    db.release();
                    console.log('released')
                }
              


              }
              
            
              else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }  
            })

            }
            else{ // when notificationOnly flag is 0
              //console.log('inside notificaiton zero')
                db.getConnection(function(err, db) {
                  if(!err){
                  let dbquery = "";
                  let strStudentID=[],strBranchIDs=[],strClassIDs=[],strClassLevelIDs=[], strSchoolIDs=[], strUserID=[]
                  let strMessageText="";
                  if (array!= undefined){
                    for (let i = 0; i < array.length; i++) {
                      dbquery = dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_messages
                        (${utils.OptionalKey(array[i].studentID)},
                        ${utils.OptionalKey(array[i].classID)},
                        ${utils.OptionalKey(array[i].classLevelID)},
                        ${utils.OptionalKey(array[i].branchID)},
                        ${utils.OptionalKey(array[i].schoolID)},
                        ${utils.OptionalKey(array[i].areaID)},
                        ${utils.OptionalKey(array[i].cityID)},
                        ${utils.OptionalKey(array[i].messageType)},
                        ${utils.OptionalKeyString(array[i].messageText) },
                        ${utils.OptionalKey(array[i].attachment)},
                        ${utils.OptionalKey(guid)},
                        ${utils.OptionalKey(array[i].userID)});  `

                        if (!_.isEmpty(array[i].studentID)) {
                          strStudentID.push(array[i].studentID);
                        }
                        if (!_.isEmpty(array[i].classID)) {
                          strClassIDs.push(array[i].classID);
                        }

                        if (!_.isEmpty(array[i].classLevelID)) {
                          strClassLevelIDs.push(array[i].classLevelID);
                        }

                        if (!_.isEmpty(array[i].branchID)) {
                          strBranchIDs.push(array[i].branchID);
                        }
                        if (!_.isEmpty(array[i].schoolID)) {
                          strSchoolIDs.push(array[i].schoolID);
                        }
                        if (!_.isEmpty(array[i].userID)) {
                          strUserID.push(array[i].userID);
                        }
                        

                        strMessageText=array[i].messageText;

                    }
                    
                    const unique = (value, index, self) => {
                      return self.indexOf(value) === index
                    }
                    console.log(dbquery)
                    try {
                        db.query(dbquery,function (error, data, fields) {
                          if(!error){
                            if(!res.finished)res.status(200).json({ response: "success" });
                            
                            dbquery = `call ${appConfig.getDbConfig().database}.v1_get_messages_notification 
                                      (${utils.OptionalKey(strSchoolIDs.filter(unique).join())},
                                      ${utils.OptionalKey(strBranchIDs.filter(unique).join())},
                                      ${utils.OptionalKey(strClassLevelIDs.filter(unique).join())},
                                      ${utils.OptionalKey(strClassIDs.filter(unique).join())},
                                      ${utils.OptionalKey(strStudentID.filter(unique).join())},
                                      ${utils.OptionalKey(guid)},
                                      ${utils.OptionalKey(strUserID.filter(unique).join())})` 
                                      
                                      // messageGUID = '${guid}' `;
                                      //   if (!_.isEmpty(strStudentID)) {
                                      //     dbquery += ` AND studentID IN (${strStudentID.filter(unique)})`;
                                      //   }
                                        
                                      //   if (!_.isEmpty(strClassIDs)) {
                                      //     dbquery += ` AND classID IN (${strClassIDs.filter(unique)})`;
                                      //   }
          
                                      //   if (!_.isEmpty(strClassLevelIDs)) {
                                      //     dbquery += ` AND classLevelID IN (${strClassLevelIDs.filter(unique)})`;
                                      //   }
                                      //   if(strBranchIDs){
                                      //     dbquery += ` AND branchID IN (${strBranchIDs.filter(unique)})`
                                      //   }
                            console.log(dbquery)
                            db.query(dbquery,function (error, data, fields) {
                                  if(!error){
                                    
                                    data.forEach(element => {
                                      if(element.constructor==Array) {
                                        
                                          if (!_.isEmpty(element)) {
                                            for (let i = 0; i < element.length; i++) {
                                              var messageConfig = appConfig.messageSettings(
                                                "Message",
                                                strMessageText,
                                                "MessageScreen",
                                                element[i].parentID,
                                                element[i].studentID,
                                                element[i].classID,
                                                element[i].branchID,
                                                element[i].schoolID,
                                                null 
                                              );
                                                //console.log("parentID:" + data[i].parentID + " studentID:" + data[i].studentID + " classID:" + data[i].classID + " branchID:" + data[i].branchID + " schoolID:" + data[i].schoolID + ' Event: null' )
                                              push.SendNotification(element[i].deviceToken,messageConfig);
                                            }
                                          }
                                          
                                      }
                                  })
                              
                                    
                                  }
                                  else{
                                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                    if(!res.finished)res.status(500).json({ response: errorResponse })
                                  }
                              });  
                                
                          }
                          else{
                              log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                              const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                              if(!res.finished)res.status(500).json({ response: errorResponse })
                          }
                      });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
                    
                    
                  
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
                  if(!res.finished)res.status(500).json({ response: "'array not found'" })
                }
             
              
            }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
        
            }

          })

            }

          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })
          }
          
        }
    })
}

exports.getMessagesSummary = function(req, res) {
  const { messageGUID, schoolID, messageDateFrom, messageDateTo, branchID, classLevelID, classID,  messageType} = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ messageGUID, schoolID, messageDateFrom, messageDateTo, branchID,classLevelID, classID,  messageType }, messageValidations.getMessagesSummary,
    function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging,  req.url,"required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=` set @schoolID = '${schoolID}';
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @classLevelID = ${utils.OptionalKey(classLevelID)};
                        set @classID = ${utils.OptionalKey(classID)};
                        set @messageType = ${utils.OptionalKey(messageType)};
                        set @messageDateFrom = ${utils.OptionalKey(messageDateFrom)};
                        set @messageDateTo = ${utils.OptionalKey(messageDateTo)};
                        call ${appConfig.getDbConfig().database}.v1_get_message_summary (@schoolID,@branchID,@classLevelID,@classID,@messageType, @messageDateFrom, @messageDateTo);`


     
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, 'query executed successfully')
                    data.forEach(element => {
                        if(element.constructor==Array) {
                            if (!_.isEmpty(element)) {
                                if(!res.finished)res.status(200).json({response: element })
                            }
                            else{
                                log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                if(!res.finished)res.status(404).json({ response: errorResponse })
  
                            }
                        }
                    })
                        
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
                
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      
      
      }
    }
  );
};

exports.getNotificationCount = function (req, res) {
  const { parentID,parentUUID, studentID, lastDateTime } = req.body

  log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

  Joi.validate({ parentID, parentUUID, studentID, lastDateTime }, messageValidations.getNotificationCount, function (err, value) {
      if (err) {
          log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`set @parentID = ${utils.OptionalKey(parentID)};
                        set @parentUUID = ${utils.OptionalKey(parentUUID)};
                        set @studentID = ${utils.OptionalKey(studentID)};
                        set @lastDateTime = ${utils.OptionalKey(lastDateTime)};
                        call ${appConfig.getDbConfig().database}.v1_get_notification_count
                        (@parentID,@parentUUID,@studentID,@lastDateTime);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                        //console.log(data)
                        data.forEach(element => {
                             if(element.constructor==Array && element[0].parentID != 0 ) {
                              log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                              if(!res.finished)res.status(200).json({response: element})
                              
                             }
                             
                         })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
     
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      
      }
  })
}





exports.getMessageDetails = function(req, res) {
  const { messageGUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( { messageGUID }, messageValidations.getMessageDetails,function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging, req.url, "required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`  set @messageGUID = ${utils.OptionalKey(messageGUID)};
            call ${appConfig.getDbConfig().database}.v1_get_message_details
            (@messageGUID);`
            console.log(dbquery)
            
            try {
              db.query(dbquery,function (error, data, fields) {
                      if(!error){
                         
                            data.forEach(element => {
                                 if(element.constructor==Array) {
                                  if (_.isEmpty(element)) {
                                    log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                    const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                    if(!res.finished)res.status(404).json({ response: errorResponse });
                                  } else {
                                    log.doInfoLog(
                                      fileNameForLogging,req.url,"successfully data fetched from db");
                                      let innerArray = new Array();
                                      let noOfIterations = element.length;
              
                                      let distinctTable = new Array();
                                      let distinctObjects = new Array();
              
                                      for (let loop = 0; loop < noOfIterations; loop++) {
                                          if (!distinctTable.includes(element[loop].studentName)) {
                                              distinctTable.push(element[loop].studentName);
              
                                              var partialData = Object.assign({}, element[loop]);
              
                                              if (partialData.hasOwnProperty("parentType")) {
                                                  delete partialData.parentType;
                                              }
                                              if (partialData.hasOwnProperty("parentDescription")) {
                                                delete partialData.parentDescription;
                                              }
                                              if (partialData.hasOwnProperty("parentName")) {
              
                                                  delete partialData.parentName;
                                              }
                                              if (partialData.hasOwnProperty("consentDate")) {
              
                                                  delete partialData.consentDate;
                                              }
                                              if (partialData.hasOwnProperty("consentText")) {
              
                                                delete partialData.consentText;
                                              }
                                              
                                              if (partialData.hasOwnProperty("consentResponse")) {
              
                                                  delete partialData.consentResponse;
                                              }

                                              distinctObjects.push(partialData);
              
                                          }
                                      }
                                      
                                      for (let j = 0; j < distinctObjects.length; j++) {
                                          let formattedData = new Array();
                                          if (distinctObjects[j].studentName ){
                                          for (let i = 0; i < element.length; i++) {
                                              if (distinctObjects[j].studentName == element[i].studentName) {
                                                  
                                                  if (element[i].hasOwnProperty("studentName")) {
              
                                                      delete element[i].studentName;
                                                  }
                                                  if (element[i].hasOwnProperty("classAlias")) {
              
                                                      delete element[i].classAlias;
                                                  }
                                                  if (element[i].hasOwnProperty("classSection")) {
              
                                                      delete element[i].classSection;
                                                  }
                                                  if (partialData.hasOwnProperty("classShift")) {
              
                                                    delete element[i].classShift;
                                                  }
                                                  
                                                  formattedData.push(element[i]);
                                              }
                                          }
                                          distinctObjects[j].Responses = formattedData;
                                      }}
              
                                      if(!res.finished)res.status(200).json({response: distinctObjects })
                                  }
                             
          
                                 }
                             })
          
                      }
                      else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                          if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                  });
            } 
            finally {
                db.release();
                console.log('released')
            }
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      }
    }
  );
};


exports.setMessageIsDelivered = function(req, res) {
  const { messageGUID, studentID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( { messageGUID, studentID }, messageValidations.setMessageIsDelivered, function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging,    req.url,   "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`  call ${appConfig.getDbConfig().database}.v1_set_message_delivered
                          (${utils.OptionalKey(messageGUID)}, 
                          ${utils.OptionalKey(studentID)})`
              console.log(dbquery)
              try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, 'update in db', error)
                    if(!res.finished)res.status(200).json({ response: "success" });
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
              } 
              finally {
                  db.release();
                  console.log('released')
              }
              
             
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      }
    })
}

exports.setMessageIsRead = function(req, res) {
  const { messageGUID, studentID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( { messageGUID, studentID }, messageValidations.setMessageIsRead, function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging,    req.url,   "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`  call ${appConfig.getDbConfig().database}.v1_set_message_read
                          (${utils.OptionalKey(messageGUID)}, 
                          ${utils.OptionalKey(studentID)})`
              console.log(dbquery)
              
              try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, 'update in db', error)
                    if(!res.finished)res.status(200).json({ response: "success" });
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
              } 
              finally {
                  db.release();
                  console.log('released')
              }        
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      }
    })
}

exports.getStudentMessages = function(req, res) {
  const { schoolID,  branchID, classID,studentID,parentID,parentUUID, recordSequenceNo,requiredCount } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( {schoolID,  branchID, classID,studentID,parentID,parentUUID,recordSequenceNo,requiredCount }, messageValidations.getStudentMessages,
    function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging, req.url, "required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`set @schoolID = ${utils.OptionalKey(schoolID)};
                        set @branchID = ${utils.OptionalKey(branchID)};
                        set @classID = ${utils.OptionalKey(classID)};
                        set @studentID = ${utils.OptionalKey(studentID)};
                        set @parentID = ${utils.OptionalKey(parentID)};
                        set @parentUUID = ${utils.OptionalKey(parentUUID)};
                        set @recordSequenceNo = ${utils.OptionalKey(recordSequenceNo)};
                        set @requiredCount = ${utils.OptionalKey(requiredCount)};
                        call ${appConfig.getDbConfig().database}.v1_get_messages_student
                        (@schoolID,@branchID,@classID, @studentID,@parentID,@parentUUID,@recordSequenceNo,@requiredCount);`
            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    data.forEach(element => {
                      if(element.constructor==Array ) {
                        if (_.isEmpty(element)) {
                          log.doErrorLog(fileNameForLogging, req.url, "data not present");
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,  "data not present" );
                          if(!res.finished)res.status(404).json({ response: errorResponse });
                        }
                        else{
                          log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                          if(!res.finished)res.status(200).json({response: element})
                        }
                     
                      }
                    
                    })
                        
                  }
                  else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
    
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      
      
      
      }
    }
  );
};



exports.getConsentReportExport = function(req, res) {
  const { messageGUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate( { messageGUID }, messageValidations.getConsentReportExport, function(err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging,    req.url,   "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`  set @messageGUID = ${utils.OptionalKey(messageGUID)};
            call ${appConfig.getDbConfig().database}.v1_get_consent_export
            (@messageGUID);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    var wb = new xl.Workbook();
                    var ws = wb.addWorksheet('Sheet 1');
                    var fileName = 'Message Consent Report.xlsx'
                    var columnHeader = wb.createStyle({
                      font: {
                        color: '#000000',
                        size: 12,
                        bold: true
                      }
                    });
                    
                    var headingTop = wb.createStyle({
                      font: {
                        bold: true,
                        underline: true,
                        size: 16,
                      },
                      alignment: {
                        wrapText: true,
                        horizontal: 'center',
                      },
                    });
                    ws.cell(1, 2, 1, 9, true).string('Message Consent Report').style(headingTop);
                    // Set value of cell A1 to 100 as a number type styled with paramaters of style
                    ws.cell(2, 2).string('Message Text').style(columnHeader);
                    
                    
                    ws.cell(3, 2).string('Message Date').style(columnHeader);
                    
                    
                    ws.cell(5, 2).string('Student Name').style(columnHeader);
                    ws.cell(5, 3).string('Class').style(columnHeader);
                    ws.cell(5, 4).string('Section').style(columnHeader);
                    ws.cell(5, 5).string('Yes/No').style(columnHeader);
                    ws.cell(5, 6).string('Response Text').style(columnHeader);
                    ws.cell(5, 7).string('Response Date').style(columnHeader);
                    ws.cell(5, 8).string('Parent Name').style(columnHeader);
                    ws.cell(5, 9).string('Relationship').style(columnHeader);

                          data.forEach(element => {
                            if(element.constructor==Array) {
                              
                              if (!_.isEmpty(element)) {
                              ws.cell(2, 3).string(element[0].messageText);
                              ws.cell(3, 3).string(element[0].messageDate);
                              
                              for(let i=0;i < element.length; i++){
                                ws.cell(6+i, 2).string(utils.Convert2String(element[i].studentName));
                                ws.cell(6+i, 3).string(utils.Convert2String(element[i].classAlias));
                                ws.cell(6+i, 4).string(utils.Convert2String(element[i].classSection));
                                ws.cell(6+i, 5).string(utils.Convert2String(element[i].consentResponse));
                                ws.cell(6+i, 6).string(utils.Convert2String(element[i].consentText));
                                ws.cell(6+i, 7).string(utils.Convert2String(element[i].consentDate ));
                                ws.cell(6+i, 8).string(utils.Convert2String(element[i].parentName));
                                ws.cell(6+i, 9).string(utils.Convert2String(element[i].parentDescription));

                                
                              }
                                

                              }
                              
                            }
                          
                            
                          })
                          wb.write(fileName , res);

                    
                    
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, "error in db execution", error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
              
             
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, "error in getting connection pool", err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }

        })
      }
    })
}