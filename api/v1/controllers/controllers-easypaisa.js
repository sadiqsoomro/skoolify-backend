const Joi = require("joi"),
    utils = require("../../utils/helper-methods"),
    easypaisaValidations = require("../validations/validations-easypaisa"),
    axios = require("axios"),
    db = require("../../../db"),
    _ = require("lodash"),
    appConfig = require("../../../app-config"),
    log = require("../../utils/utils-logging"),
    helper = require("../../utils/helper-methods"),
    fileNameForLogging = "easypaisa-controller",
    httpsAgent = require("https-agent");

import Error from "../response-classes/error";

// ==================================================================

exports.setEasyPaisaPayment = function (req, res) {
    const {txnHash, parentUUID,userUUID, walletAccountNo, paymentCurrency, paymentAmount, emailAddress, providerID,ipAddress,
     internalTxnRefNo, tranRequestCode, additionalData } = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

    Joi.validate(
        {
            txnHash, parentUUID,userUUID,walletAccountNo, paymentCurrency, paymentAmount, emailAddress, providerID,ipAddress,
         internalTxnRefNo, tranRequestCode, additionalData  }, easypaisaValidations.setEasyPaisaPayment,
        function (err, value) {
            if (!err) {

                let parsedHash = helper.TxnHashVerification(txnHash);
                

                if (parsedHash == "error parsing") {
                    if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS,"Invalid format of TxnHash",""));
                } 
                else if (parsedHash == "timeout") {
                    if(!res.finished)res.status(500).json(new Error("Transaction time Elapsed","Check the provided hash",""));
                } 
                else {

                db.getConnection(function(err, db) {
		        if(!err){
                    let dbquery=` call ${appConfig.getDbConfig().database}.v1_verify_parent_txn
                                (
                                    ${utils.OptionalKey(parsedHash[1])}, 
                                    ${utils.OptionalKey(parsedHash[0])}, 
                                    ${utils.OptionalKey(parsedHash[2])})`;
                                    // select @out_parentID as parentID ;`
                    console.log(dbquery)
                    try {
                        db.query(dbquery, function (error, data, fields) {
                        if(!error){
                            data.forEach(element => {
                                
                            if(element.constructor==Array) {
                                
                                //console.log(element[0].out_parentID)
                                    if (element[0].parentID == '0' || element[0].parentID==null ) {
                                        log.doFatalLog(fileNameForLogging,req.url,"Not a valid parent or Unauthorize Transaction/Time Elapsed","Check the provided hash");
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,"Not a valid parent or Unauthorize Transaction","Check the provided hash");
                                        if(!res.finished)res.status(500).json(new Error("Not a valid parent or Unauthorize Transaction/Time Elapsed","Check the provided hash",""));
                                    } 
                                    else{
                                        let transactionRequestTime = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
                                        const httpsAgent_ = httpsAgent({rejectUnauthorized: false,keepAlive: true });
                                    


                                        let _data = {
                                        orderId: internalTxnRefNo,
                                        storeId: appConfig.EASYPAISA_STORE_ID,
                                        transactionAmount: paymentAmount,
                                        transactionType: tranRequestCode,
                                        mobileAccountNo: walletAccountNo,
                                        emailAddress: emailAddress
                                    };
                                    console.log('EasyPaisa Request: ' + JSON.stringify(_data))
                                    if(!res.finished)res.status(202).json({response:"processing", sourceUUID: parsedHash[0] , orderID:internalTxnRefNo,walletAccountNo:walletAccountNo });
                                    axios({
                                        method: "post",
                                        url: appConfig.EASY_PAISA_URL,
                                        data: _data,
                                        httpsAgent: httpsAgent_,
                                        headers: {
                                            credentials: appConfig.EASY_PAISA_CREDENTIALS,
                                            "Content-Type": "application/json"
                                        }
                                    })
                                    
                                    
                                    .then(function (response) {
                                        
                                        console.log('EasyPaisa Response: ' + JSON.stringify(response.data))
                                        if (response.status== 200 ){
                                            if (response.data.responseCode=='0000'){

                                                let responseTime = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
                                                let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_payment_logs
                                                ( ${utils.OptionalKey(providerID)},
                                                    ${utils.OptionalKey(parsedHash[0])},
                                                    ${utils.OptionalKey(parentUUID)}, 
                                                    ${utils.OptionalKey(userUUID)},
                                                    ${utils.OptionalKey(ipAddress)},
                                                    ${utils.OptionalKey(response.data.transactionId)},
                                                    ${utils.OptionalKey(internalTxnRefNo)},
                                                    ${utils.OptionalKey(walletAccountNo)},
                                                    ${utils.OptionalKey(paymentCurrency)},
                                                    ${utils.OptionalKey(paymentAmount)},
                                                    ${utils.OptionalKey(transactionRequestTime)},
                                                    ${utils.OptionalKey(tranRequestCode)},
                                                    ${utils.OptionalKey(additionalData)},
                                                    ${utils.OptionalKey(responseTime)},
                                                    ${utils.OptionalKey(response.data.responseCode)},
                                                    ${utils.OptionalKeyString(helper.EncryptBase64(JSON.stringify(response.data)))});`
                                                console.log(dbquery)
                                                db.query(dbquery,function (error, data, fields) {
                                                        if(!error){
                                                            //if(!res.finished)res.status(200).json({responseCode: response.data.responseCode,responseDesc: response.data.responseDesc});
                                                            log.doInfoLog(fileNameForLogging, 'Txn DB Log', "Successful", req.body)
                                                        }
                                                        else{
                                                            log.doFatalLog(fileNameForLogging,req.url,"error in db insertion",error);
                                                            // const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,error.message,error);
                                                            // if(!res.finished)res.status(500).json({ response: errorResponse });
                                                        }
                                                    });
                                            }
                                            else{
                                                let var_responseDesc = response.data.responseDesc
                                                // if (response.data.responseCode=='0000') var_responseDesc = 'Success'
                                                // else if (response.data.responseCode=='0001') var_responseDesc = 'System Error'
                                                // else if (response.data.responseCode=='0002') var_responseDesc = 'Required field missing'
                                                // else if (response.data.responseCode=='0005') var_responseDesc = 'Merchant A/c not active'
                                                // else if (response.data.responseCode=='0006') var_responseDesc = 'Invalid Store ID'
                                                // else if (response.data.responseCode=='0007') var_responseDesc = 'Store Not Active'
                                                // else if (response.data.responseCode=='0008') var_responseDesc = 'Payment Method Not Enabled'
                                                // else if (response.data.responseCode=='0010') var_responseDesc = 'Invalid Credentials'
                                                // else if (response.data.responseCode=='0015') var_responseDesc = 'Invalid Token Expiry'
                                                // else if (response.data.responseCode=='0016') var_responseDesc = 'Expiry should be future date'

                                                let responseTime = new Date().toISOString().replace(/T/, " ").replace(/\..+/, "");
                                                let dbquery=`call ${appConfig.getDbConfig().database}.v1_set_payment_logs
                                                ( ${utils.OptionalKey(providerID)},
                                                    ${utils.OptionalKey(parsedHash[0])},
                                                    ${utils.OptionalKey(parentUUID)}, 
                                                    ${utils.OptionalKey(userUUID)},
                                                    ${utils.OptionalKey(ipAddress)},
                                                    ${utils.OptionalKey(response.data.transactionId)},
                                                    ${utils.OptionalKey(internalTxnRefNo)},
                                                    ${utils.OptionalKey(walletAccountNo)},
                                                    ${utils.OptionalKey(paymentCurrency)},
                                                    ${utils.OptionalKey(paymentAmount)},
                                                    ${utils.OptionalKey(transactionRequestTime)},
                                                    ${utils.OptionalKey(tranRequestCode)},
                                                    ${utils.OptionalKey(var_responseDesc)},
                                                    ${utils.OptionalKey(responseTime)},
                                                    ${utils.OptionalKey(response.data.responseCode)},
                                                    ${utils.OptionalKeyString(helper.EncryptBase64(JSON.stringify(response.data)))});`
                                                console.log(dbquery)
                                                db.query(dbquery,function (error, data, fields) {
                                                        if(!error){
                                                            log.doInfoLog(fileNameForLogging, 'Txn DB Log', "Successful", req.body)
                                                        }
                                                        else{
                                                            log.doFatalLog(fileNameForLogging,req.url,"error in db insertion",error);
                                    
                                                        }
                                                    });
                                            }
                                            

                                        }
                                        else{
                                            log.doFatalLog(fileNameForLogging, req.url, 'response error on finja', error)
                                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                            
                
                                        }
                                        
                                    })
                                    .catch(function (error) {
                                        if (error.response) {
                                        // Request made and server responded
                                        console.log(error.response.data);
                                        console.log(error.response.status);
                                        console.log(error.response.headers);
                                        } else if (error.request) {
                                        // The request was made but no response was received
                                        console.log(error.request);
                                        } else {
                                        // Something happened in setting up the request that triggered an Error
                                        console.log('Error', error.message);
                                        }
                                    
                                    });

                                    }
                        
                                }
                                else{
                                    // do nothing
                                }
                            });
                        

                        }
                        else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            console.log(errorResponse)
                            if(!res.finished)res.status(500).json({ response: errorResponse })

                        }
                    })
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
			        
               
                } 
                else {
                    log.doErrorLog(fileNameForLogging, req.url,"error in getting connection pool");
                    if(!res.finished)res.status(400).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err));
                }
                    
                })


                }

                
            }
            else{
                    log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
                    if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));

            }


        });
}



exports.getEasyPaisaStatus = function (req, res) {
    const {sourceUUID, orderID, walletAccountNo} = req.body;
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

    Joi.validate(
        { sourceUUID, orderID, walletAccountNo  }, easypaisaValidations.getEasyPaisaStatus,function (err, value) {
            if (err) {
                log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
                if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
            }
            else{
                db.getConnection(function(err, db) {
                    if(!err){
                        let dbquery=` call ${appConfig.getDbConfig().database}.v1_get_easypaisa_status
                                    (${utils.OptionalKey(sourceUUID)},
                                    ${utils.OptionalKey(orderID)},
                                    ${utils.OptionalKey(walletAccountNo)})`;
                        console.log(dbquery)
                        try {
                            db.query(dbquery, function (error, data, fields) {
                            if(!error){
                                data.forEach(element => {
                                    
                                if(element.constructor==Array) {
                                    
                                        if (!_.isEmpty(element)) {
                                            if(!res.finished)res.status(200).json({response: element })
                                        }
                                        else{
                                            log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                            if(!res.finished)res.status(404).json({ response: errorResponse })
            
                                        }
                            
                                    }
                                
                                });
                            
    
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
    
                            }
                        })
                        } 
                        finally {
                            db.release();
                            console.log('released')
                        }
                        
                 
                    } 
                    else {
                        log.doErrorLog(fileNameForLogging, req.url,"error in getting connection pool");
                        if(!res.finished)res.status(400).json(new Error(appConfig.GENERIC_ERROR_MSG, err.message, err));
                    }
                        
                })  

            }


        });
}