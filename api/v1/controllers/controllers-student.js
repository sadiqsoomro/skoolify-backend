const Joi = require("joi"),
  utils = require("../../utils/helper-methods"),
  studentValidations = require("../validations/validations-student"),
  appConfig = require("../../../app-config"),
  _ = require("lodash"),
  db = require('../../../db'),  
  xl = require('excel4node'),
  log = require("../../utils/utils-logging"),
  helper = require("../../utils/helper-methods"),
  fileNameForLogging = "student-controller";
import Error from "../response-classes/error";

const push = require("../../utils/push-notification");



async function pushNotificationArray(registrationIDs, messageConfig) {
  return await push.SendNotification(registrationIDs, messageConfig);
}

exports.getStudents = function (req, res) {
  const { parentID, parentUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ parentID, parentUUID }, studentValidations.getStudents, function (err,value) {
    if (err) {
      log.doErrorLog( fileNameForLogging,  req.url,  "required params validation failed." );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){
          let dbquery = `SELECT * FROM v1_get_students ` ;
          if (!_.isEmpty(parentID)) {
            dbquery += `WHERE parentID = '${parentID}'`;
          }
          
          else {
            dbquery += `WHERE parentUUID = '${parentUUID}'`;
          }
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
                if(!error){
                  
                  if (_.isEmpty(data)) {
                    log.doErrorLog(fileNameForLogging, req.url, "data not present");
                    const errorResponse = new Error( appConfig.EMPTY_DATA_ERROR_MSG,   "data not present");
                    if(!res.finished)res.status(200).json({ response: errorResponse });
                  } 
                  else {
                    log.doInfoLog(fileNameForLogging,   req.url,  "successfully data fetched from db" );

                    let innerArray = new Array();
                    let noOfIterations = data.length;

                    let allowedAccess = new Array();
                    let studentObjects = new Array();

                    for (let loop = 0; loop < noOfIterations; loop++) {
                        if (!allowedAccess.includes(data[loop].studentName)) {
                            allowedAccess.push(data[loop].studentName);

                            var partialData = Object.assign({}, data[loop]);

                            if (partialData.hasOwnProperty("appHomework")) {
                                delete partialData.appHomework;
                            }
                            if (partialData.hasOwnProperty("appTimetable")) {
                              delete partialData.appTimetable;
                            }
                            if (partialData.hasOwnProperty("appExamQuiz")) {

                                delete partialData.appExamQuiz;
                            }
                            if (partialData.hasOwnProperty("appfeeVoucher")) {

                                delete partialData.appfeeVoucher;
                            }
                            if (partialData.hasOwnProperty("appAttendance")) {

                              delete partialData.appAttendance;
                            }
                            
                            if (partialData.hasOwnProperty("appMessage")) {

                                delete partialData.appMessage;
                            }
                            if (partialData.hasOwnProperty("appCommunicate")) {

                              delete partialData.appCommunicate;
                            }
                            if (partialData.hasOwnProperty("appEvents")) {

                              delete partialData.appEvents;
                            }

                            studentObjects.push(partialData);

                        }
                    }
                    
                    for (let j = 0; j < studentObjects.length; j++) {
                        let formattedData = new Array();
                        if (studentObjects[j].studentName ){
                        for (let i = 0; i < data.length; i++) {
                            if (studentObjects[j].studentName == data[i].studentName) {
                                
                                if (data[i].hasOwnProperty("studentID")) {

                                    delete data[i].studentID;
                                }
                                if (data[i].hasOwnProperty("studentUUID")) {

                                    delete data[i].studentUUID;
                                }
                                if (data[i].hasOwnProperty("parentUUID")) {

                                    delete data[i].parentUUID;
                                }
                                if (data[i].hasOwnProperty("parentID")) {

                                  delete data[i].parentID;
                              }
                                if (partialData.hasOwnProperty("classID")) {

                                  delete data[i].classID;
                                }
                                if (partialData.hasOwnProperty("classID")) {

                                  delete data[i].classID;
                                }
                                if (partialData.hasOwnProperty("studentName")) {

                                  delete data[i].studentName;
                                }
                                if (partialData.hasOwnProperty("gender")) {

                                  delete data[i].gender;
                                }
                                if (partialData.hasOwnProperty("rollNo")) {

                                  delete data[i].rollNo;
                                }
                                if (partialData.hasOwnProperty("dob")) {

                                  delete data[i].dob;
                                }
                                if (partialData.hasOwnProperty("classYear")) {

                                  delete data[i].classYear;
                                }
                                if (partialData.hasOwnProperty("picture")) {

                                  delete data[i].picture;
                                }
                                if (partialData.hasOwnProperty("selfRegistered")) {

                                  delete data[i].selfRegistered;
                                }
                                if (partialData.hasOwnProperty("classLevelID")) {

                                  delete data[i].classLevelID;
                                }
                                if (partialData.hasOwnProperty("classLevel")) {

                                  delete data[i].classLevel;
                                }
                                if (partialData.hasOwnProperty("classSection")) {

                                  delete data[i].classSection;
                                }
                                if (partialData.hasOwnProperty("classAlias")) {

                                  delete data[i].classAlias;
                                }
                                if (partialData.hasOwnProperty("isSubjectLevelAttendance")) {

                                  delete data[i].isSubjectLevelAttendance;
                                }
                                if (partialData.hasOwnProperty("classShift")) {

                                  delete data[i].classShift;
                                }
                                if (partialData.hasOwnProperty("idCardName")) {

                                  delete data[i].idCardName;
                                }
                                if (partialData.hasOwnProperty("branchID")) {

                                  delete data[i].branchID;
                                }
                                if (partialData.hasOwnProperty("branchName")) {

                                  delete data[i].branchName;
                                }
                                if (partialData.hasOwnProperty("assessmentOnly")) {

                                  delete data[i].assessmentOnly;
                                }
                                if (partialData.hasOwnProperty("schoolID")) {

                                  delete data[i].schoolID;
                                }
                                if (partialData.hasOwnProperty("schoolName")) {

                                  delete data[i].schoolName;
                                }
                                if (partialData.hasOwnProperty("schoolLogo")) {

                                  delete data[i].schoolLogo;
                                }
                                if (partialData.hasOwnProperty("areaID")) {

                                  delete data[i].areaID;
                                }
                                if (partialData.hasOwnProperty("areaName")) {

                                  delete data[i].areaName;
                                }
                                if (partialData.hasOwnProperty("cityID")) {

                                  delete data[i].cityID;
                                }
                                if (partialData.hasOwnProperty("cityName")) {

                                  delete data[i].cityName;
                                }
                                if (partialData.hasOwnProperty("countryID")) {

                                  delete data[i].countryID;
                                }
                                if (partialData.hasOwnProperty("countryName")) {

                                  delete data[i].countryName;
                                }
                                if (partialData.hasOwnProperty("updateDate")) {

                                  delete data[i].updateDate;
                                }
                                if (partialData.hasOwnProperty("isActive")) {

                                  delete data[i].isActive;
                                }
                                

                                
                                formattedData.push(data[i]);
                            }
                        }
                        studentObjects[j].allowedAppAccess = formattedData;
                    }}

                    if(!res.finished)res.status(200).json({response: studentObjects })
                  }
    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  })
}

exports.setStudents = function (req, res) {
  const { studentID,classID, studentName, gender,rollNo, dob, picture, isActive,classYear,enrollDate} = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(
    {studentID,classID, studentName, gender,rollNo, dob, picture, isActive,classYear,enrollDate}, studentValidations.setStudents,
    function (err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging,  req.url,  "required params validation failed."  );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
                    
                    let dbquery=`set @studentID = ${utils.OptionalKey(studentID)};
                                  set @classID= ${utils.OptionalKey(classID)};
                                  set @studentName = ${utils.OptionalKeyString(studentName)};
                                  set @gender = ${utils.OptionalKey(gender)};
                                  set @rollNo = ${utils.OptionalKey(rollNo)};
                                  set @dob = ${utils.OptionalKey(dob)};`
                    if (picture==null){
                          dbquery+= ` set @picture= 'iVBORw0KGgoAAAANSUhEUgAAAF4AAABeCAYAAACq0qNuAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAABARpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+eG1wLmlpZDo4QjQ0MUY4NTBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwvc3RSZWY6aW5zdGFuY2VJRD4KICAgICAgICAgICAgPHN0UmVmOmRvY3VtZW50SUQ+eG1wLmRpZDo4QjQ0MUY4NjBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwvc3RSZWY6ZG9jdW1lbnRJRD4KICAgICAgICAgPC94bXBNTTpEZXJpdmVkRnJvbT4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDo4QjQ0MUY4ODBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDo4QjQ0MUY4NzBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwveG1wTU06SW5zdGFuY2VJRD4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBJbWFnZVJlYWR5PC94bXA6Q3JlYXRvclRvb2w+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgqSiLPRAAAPe0lEQVR4Ae1de4xcVRm/r5nZebYVKitFq8hDKhTFinGRtBtTLGt3ZnaRKqQlgLFFAxJQEwNRHolETPgDY6xFQgVETQvdx3QXatC2GBqCpUjRWkEaRURKNWVnZl8z9+Hvd3fO7uzs7M7M7r3T2fae9O7ce+55fOd3vvOd7/vOObeS5AUPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ+BkwABeb604e6771aWLVsmL168eFqajx07Zh06dMhCWnO+tKsh6dy2bZu6e/duzbKsacEuRzjTMx/zl3vfCHFaIxBRSgNB27t3r7lu3TpDvHvqqac+oCjK+Zosn2Mp0pmIXyhZst0h+POepFj/tvLmEUtVD8uy/Dbe68zLTtizZ4/a2tpqPzOuEUJNnOQ2wRARGi6CbbGunp6eTyqS1IGH1Yj6eCAQiGqaJqED+JqgSgJ60zQlXdel0dHRQbw5hDzPSpKyI5FI7LcT4w87tFE6oCGAL+XK3t6udbIl3ywryuXhcEjK53Upl8tJhmGY4OZp5TfKUVRVVfx+v+Tz+aShoSHmeUGR5AfXJhK/YQdQ/DTCPHDCgScQQqSkuro+DzHyo3A4cgk5eHh4mMJdB/eSxXlVQy9HCzuHv2pTU5OMzpAGBwcPSrLy3Xg8/jTiTzj3V9MQ0ulK2LJli2/Tpk35/v7+gJEf/WmgKXgjKxoZGaE8Jm1OTI626IKY0iiihoeHfikr2qb29vYhUT/rrHc4YcCLRu/YseMCTVV6o9HoOZlMRkymTgBeiqVdNupRM9nMPzHnxiH/Dwo6ShO7/XxCgBeN7erqWqUq8i5wox+TYg6N9bvdYJSfK9RnYmr+IsB/RtBTh7rHq6g78KKRO3t6WiHAf0/5C22EoqWeqq0OsaOx7rxutEPu7xR0jSPj8k1dgRfqXF9X1yd0WXoJqqECraPeoAtIDYCvUiW1JP2z8fhVLwj6RAI3f8cUYjdrKJQN/VyhDg0tZoGhyH0Y7icSdFKlQnPSaRdIlroTc837SR9V0jrAYato9ahHop+FFTUFfI9GIpEzIdPzeKyneCnXTg32QT4cDp+mKfLjTHDPPfeUS+d4XF1EjRjCqVT3teFQ+InB7GAeSrbP8dbMskBIm3wsGvFlM9mvtSeTDwt6Z1lcVdlcB55WKYIFERMMBvyvaT7fWeAyWqB1GdJVoQCDixYvpptjwVDk3NWrVw8IuqvMX3My1xtPBxWpCgYCt0SiUYCezzcY6CRPgWKVj0Zji4eGsrcxQtDNezeCqxwvuIaWqa7n/ubTfEvz+TzNedc7fBZgmdSydMN4Jzoyem7runVZQf8syqqYxVUABNcA7HgoGCLoVB1drbNii6dPQK7Xw6FQc9bv72QyQf/0WWb/xlUQuCJE0mTL+jLEy+yprFNOGe4hcDnd/F9hlYJ+N6p3DQ0xTOEWWAi3wOuYvE6HscSOcK1OBwCyYFTJ0O/TumGe29nZ+a5ohwNlTyrCNY7fvn27XTYAvxj+8dMxiinbGxl0AkPQDbiSY5D3n2KEaAfvnQyuAT+xKG1ewoUJiJppFzCcbJADZVlcRLEsYwXLmmiHAyUXFeEa8ON1mNKy8ft5csNFGKxanW+Tu3evK1S7Bvz4xCRLZ7Eh8yhQ3HAJ64OkedVdd7lCvGvAX3311TbBEOqLCsA3unwf5w3SC/1mASPcEpGuAS9aAeXM9TpEXU78UouhSgnIA1wPLpTpONPUAZQ6VOEE4lPLMLkbYWq0MzGuoQL3aoFLzNH5YDwJOEErdXk8WoNYQzA5Avgg3jv16xrwwv8Oko+ONcR54p0CoaScAvDKfxk/b/V4LCgfUeaBu6AYfDIK5qYjjJu/erxsHcR4LW7XvLiHxDnoJqGuiZpVq1bZ6iQW8/djFxeRx35TeT70gMqtf1h6/SOBH7dHHO4F14AX+i92bB0GzYfoNoB+3NDAY2Cafp+PBtTfsSZsczy2F84vA4oMwrVLm1FkKUXgIevFTjE7utH+YCoy/YEAXXn93M9ZoN8VZnGN4wmqEDcwBH8FcUMz8ETvKqjU1wr2bUqqNbbjwC0xQyJcBZ7ihtZfMpl81TKt/mAwSJ2Yq1ANF0CrDvoUQ9d3Y1/ffujvitjF7AaxrgJfTDBWde6DT55RjpvfxfXM9l64CmRVup9lQH93lU7XgSfXkOuxOfT5fC73ZCQcVtEibmZqmEB6sKlJHR0Z6Vu7NrmL9LrJ7Wy468CzEuHzUH3+2zLZ7DBOenAzkyvaAuurMfA8jw9zkK76rFuZF55VVybUYrrqAjx9HtQQ2tra3lJU+aZQKEQaGgJ4cLuOLYUUgDe3tXW8wV3DQhUuBsrp+7oAT6ILG1bV9vbkY9jCtzkWi1HD4Z74Exly0VjMl0lnfhGPJ7dQxPCESj0IcnUCKW0AJzAEexj39nb3xqKx9nQ6Xa8DCaXk5HA6xJ9OZ55NJJM4VQgXZBF9pYmdfq4bx5Nwgi6MKnBYHJzWB873FyZb1+VqATzga+VZbzYzATrpEkzhNMjlyqsb8BzG5CiKnP3799s7hdsTibUD6YFHsKeSzxx9buv4LF9esCDmS6cHft2eGON00OYv7I2XSWc5oJyOcx14gs0Ji+oZOaqvr2/pihUr8gL8RKLjq+lM+ha8k2DAaAXud9S1ABpMlsvy6fLNprPfjic6riWYBB205XByfCnpI53kftLtNNjF5blaOLlH6MNjp/vkB7A7e41pGRsSic4n2EDIWZkd0d3dfZEiS5uhT1+GPZY8UCy4nxw4Gzo5oRgAUIKfSKOvCKLlRUtWboJN8XJx3b1dXVfBkbQd1TwLF/a3aGkTpGL6i0Fz4n42Daqq3uLDXKlUz71wGXwPHCeNjoxK/oBfGhkeuTOeTN7Hwvbt2xdsaWkZ5j2O0a/HCv+dUDk/xmf6TuAtFJ1Aeu2LI0QEgovAP+LifKKxPqaDjn4YjscforMfZUIAGgRD2PWlenpu9/l9D9CqxvEgHsmXTMu8/8CBP91BNbi4HczrVJig3qESOUS5XEZOB4jLAeLj4OrlOMPKGgigLd6gOyswprpCofANPAjAIX/8+HFLqHPI24l1oBuBZCtGQYgAEhxe3H5BsHkxnhdFCM8z8WI8fOoj8ELvlRXrkQMHXnlS2BJwfJG23K5du8KwVH8eiYSvyWazosNoW3AUStls5q+6YW3o6Oh4iZzP7Sqoh+kcCY4CjwbD8zu2VQ/fI9iIbbdbsA+RR+N5GIF6u6iPDTAAqJbNDr6L/RS3xDs6trFF3EsPIHQholKpbUtw/HUlsL4cPbYcUC8FYy9C0gDqQ7E2GKN4fg+F/gvPryiW9AcsnD6HtYA3WSaBQ0fTgGM6CUf3k5aq/CQSDi1B/WSGInHG3R2mDrp9FHkYKbfC9vgx87EcQRef5xIEEHMpw85bTFRvd/fPItHIJrqC0Qo2rKw7GJXnVU3zUf4ODQ79TtGk78NXso8FgkOVSy+91CfAsivBn1QqFQLHLwSHx2QcLsFGR3ZiBsPlvS9cdx18zxOhtBP7enpWGJJ5bzAYutIePUAWmW0NayJX4Q7eShBvc79tYCWTN/BNcTun5KkhwhHgAZKGS+eElckM9MEwugKGUQknTUsVh7cFeawSjHwu/1vw3GY1EHi6GHTKWpawceNGvcDlkwok9z/00EN2BwtxxQTMt6S5+QpTtr6hKmob5TjEECddjpZKWp39EYtoLOobSGeew+HzNeD4YbaT6uckAmp8mDPwggPGZObQ7kgk+mmIitqt0TEOUzCpKgCF4ulNcGM/xEY/hOtL0ET48Z+qAuaHM5AQu33NNcC2LdjUdDZAJuAcHVRVy47AGQrPQVTR4Pqzlh28rG39+vRcwZ8T8AL0rVu3Nr1v0aIXMVFdBPFSO+iTW2zr8NgqDaYP8Hsz7IRBEPo6ePQ1DI4juI7KpjxgoYvwy38xsO4ZmHI/gnTc5XsORlCUR+appUCioEzOBVatgBdTlgNT+NF5f0lnsi3rAb5of3Giau9nDTxlMC7bw5jq6X4+FA63OAB6Md0smxe8yIrKPevUWKi9cETwEoHczIvaji2uMCninh3IRBQnlUSKKKrSbw4KgR/tfPk/7xz9DEUa6BhXKCplLn4/W+DtjZ1orAXQd8Dk74C6OFdOL6ar9J4AshMm0C5NMfHMNhHo2bZtoqTyd2NiJ5t9Jp5IXskkAB9Q1KZqzooTIN9UVgTt5Qdwq3ZgIqUr1c1PnhBEqnwUFZWuItUQqZ0Pfs5hcLKtQfsfZPF7Cmd5a6mqZuCpJXBGT6W6ksFQ8A6Azq9gzEV21kJvo6SFOzmthyPhb8JeuYZ4CK2rWgJrGo5CnvFThD5NO4xT6DHIVMxtFdWyaumZT+lMMJyCuWTEMK0LYOH+o5bJtiaOFyvvUDgehhUfMw2jEY/H16vzCLoOTadJU+WtrFSsLVdDQNUcL/RWDK0N4VDkMczs01qk1VR8sqQBgPlwBF/+GMzexOVDgVOl9lUFvJi1UWgkm0nzsHAzRQwKr2nEVCJmnr4vfPnDPI7vIPBQ8v+KVe3p2lQVcGLWhjvgTlhwzTBIqMVUlXe6ik+ieHwDwcCXPyKLsMpyF9u1cuXKithUTMDe46wNh9NZ0KJvhSpFY+VU02Jm5BPYbvCyZmk5fB2Kx9nEi7jNlGnGl8W9p+dyt6NXg7AWyO1ViaiZKj7J3vHrE3m6njVN/U4xbtO1c0YAhWyHK/Z0yzReh/a0EDM5rccZ801X2UkeP2a+Wtaw5jPOa2u76q2ZZP2MHC9kO871b4hGIgst0/S4fXrusbkeq1dBI69dz2QzyfoZZTVllV2PJV2PL1rRUTJjR9lpT+E/WChX6Q2F92YDpMV9mAuJH6UDpcSkMC2QtMKYEqv/LfAMLkeBVB/tuEkleA/jCNCCx+4IA6dKztu5s7uVL4BjWYzLRjKDOGaoqnIn/eIIBN4LlRGwP7simcqXmFTgWJqt4iSZ6u1+1e/zXzgCjj9FfTKlmFV6NiEhFD2ff+Ptd45eUFiGnCJuynI8nWEsvb+7exnWGy4cxf9WAI2mYidVougUea9wDR3/dcNHm5ubL2aby4mbssBDm7HjDVVqwQkO5uU3dz3giUR1wRjbTGV+jsnLiZuywIuyLUO6zNPYBRq1/YJRscortzAXNlFN0WrKqpNQI+0FZ+wnXD72qUgP/lpgp6imOwtoX0RJgUA8J8n5KRxfECkWNpmeBovgw1w8RpiSrhZCTrW0RLqA24eA4xK2H1bsJFH9f/UnH28cQVdOAAAAAElFTkSuQmCC';`
                    }
                    else{
                          dbquery+=  ` set @picture = ${utils.OptionalKey(picture)};`
                    }
                          
                    dbquery+=     ` set @isActive = ${utils.OptionalKey(isActive)};
                                  set @classYear = ${utils.OptionalKey(classYear)};
                                  set @enrollDate = ${utils.OptionalKey(enrollDate)};
                                  call ${appConfig.getDbConfig().database}.v1_set_student
                                  (@studentID,@classID,@studentName,@gender,@rollNo,@dob,@picture,@isActive,@classYear,@enrollDate);`
                    console.log(dbquery)
                    try {
                      db.query(dbquery, function (error, data, fields) {
                      if(!error){
                        
                        if(!res.finished)res.status(200).json({ response: "success" });
          
                      }
                      else if (error.code== 'ER_DUP_ENTRY'){
                        log.doFatalLog(fileNameForLogging, req.url, 'duplication error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(400).json({ response: "unsuccessful", code: error.code, reason : "Record already exist, please edit and update" })
                      }
                      else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                    });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }

                    
              
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setStudentsArray = function (req, res) {
  const { array } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(array, studentValidations.setStudentsArray, function (err,value) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){
          if(array != undefined){

          let dbquery = "";
          for (let i = 0; i < array.length; i++) {
            dbquery =
              dbquery +
              ` call ${appConfig.getDbConfig().database}.v1_set_student
                    (${utils.OptionalKey(array[i].studentID)}, 
                    ${utils.OptionalKey(array[i].classID)}, 
                    ${utils.OptionalKey(array[i].studentName)}, 
                    ${utils.OptionalKey(array[i].gender)},
                    ${utils.OptionalKey(array[i].rollNo)}, 
                    ${utils.OptionalKey(array[i].dob)}, 
                    ${utils.OptionalKey(array[i].picture)}, 
                    ${utils.OptionalKey(array[i].isActive)},
                    ${utils.OptionalKey(array[i].classYear)});  `;
          }
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
                if(!error){
                  if(!res.finished)res.status(200).json({ response: "success" });                
                }
                else if (error.code== 'ER_DUP_ENTRY'){
                  log.doFatalLog(fileNameForLogging, req.url, 'duplication error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(400).json({ response: "unsuccessful", code: error.code, reason : "Record already exist, please edit and update" })
              }
                else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })

            }

    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

exports.getStudentFees = function (req, res) {
  const { schoolID, branchID, classID, studentID, billingPeriodID } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  Joi.validate({schoolID, branchID, classID, studentID, billingPeriodID },studentValidations.getStudentFees,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `   
                          set @schoolID = ${utils.OptionalKey(schoolID)};
                          set @branchID = ${utils.OptionalKey(branchID)};
                          set @classID = ${utils.OptionalKey(classID)};
                          set @studentID = ${utils.OptionalKey(studentID)};
                          call ${appConfig.getDbConfig().database}.v1_get_student_fee
                                  (@schoolID,@branchID,@classID,@studentID);`
                    console.log(dbquery)
            try {
              db.query(dbquery,function (error, feeChallanData, fields) {
                  if(!error){

                    feeChallanData.forEach(element => {
                      if(element.constructor==Array) {
                        let fullElement;
                        if (!_.isEmpty(element)) {
                            fullElement = element;
                            if (_.isEmpty(element)) {
                            log.doErrorLog(fileNameForLogging, req.url, "data not present");
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                            if(!res.finished)res.status(404).json({ response: errorResponse });
                          } 
                          else {
                            let challanId = "";
                            for (let loop = 0; loop < element.length; loop++) {
                              if (
                                element[loop].challanID != null ||
                                element[loop].challanID != ""
                              )
                                challanId = element[loop].challanID + "," + challanId;
                            }
                            challanId = challanId.substr(0, challanId.length - 1);
                            //console.log(challanId);
              
                            if (challanId != "" || challanId != null) {
                              let IDs = challanId.split(",");
                              let whereClause = "";
                              if (IDs.length != 0) {
                                for (let x = 0; x < IDs.length; x++) {
                                  whereClause = whereClause + ` challanID = '${IDs[x]}' or`;
                                }
                                whereClause = whereClause.substr(0, whereClause.length - 2);
                                whereClause = "WHERE " + whereClause;
                              }
                              dbquery = `SELECT * FROM v1_get_fee_details ${whereClause} order by challanID asc`
                              console.log(dbquery)
                              db.query(dbquery,function (error, feeDetailsData, fields) {
                                    if(!error){
                                      
                                      if (_.isEmpty(feeDetailsData)) {
                                        log.doErrorLog( fileNameForLogging,req.url,  "data not present");
                                        const errorResponse = new Error( appConfig.EMPTY_DATA_ERROR_MSG, "data not present");
                                        if(!res.finished)res.status(200).json({ response: errorResponse });
                                      } else {
                                        for (let x = 0; x < fullElement.length; x++) {
                                          let challanID = fullElement[x].challanID;
                                          let data = new Array();
                                          let looper = 0;
                                          for (let y = 0; y < feeDetailsData.length; y++) {
                                            if (challanID == feeDetailsData[y].challanID) {
                                              data[looper] = feeDetailsData[y];
                                              looper++;
                                            }
                                          }
                                          fullElement[x].feeDetailsData = data;
                                        }
                                        if(!res.finished)res.status(200).json({ response: [{feeChallan: fullElement} ] });
                                      }
                                    }
                                    else{
                                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                      if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } else {
                              log.doErrorLog(fileNameForLogging, req.url, "data not present");
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                              if(!res.finished)res.status(200).json({ response: errorResponse });
                            }
                          }
                        }
                        else{
                          log.doErrorLog(fileNameForLogging, req.url, "data not present");
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                          if(!res.finished)res.status(404).json({ response: errorResponse });
                        }
                        
                      }
                    })

      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};



exports.deleteFeeChallanDetail = function (req, res) {
  const { feeChallanDetailID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(
    { feeChallanDetailID }, studentValidations.deleteFeeChallanDetail,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `
            set @feeChallanDetailID = ${utils.OptionalKey(feeChallanDetailID)};
            call ${appConfig.getDbConfig().database}.v1_delete_fee_challan_detail
            (@feeChallanDetailID);`;
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if(!res.finished)res.status(200).json({ response: "success" });
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

// exports.setStudentFeesArray = function (req, res) {
//     const { array } = req.body

//     log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//     Joi.validate(array, studentValidations.setStudentFeesArray,
//         function (err, value) {
//             if (err) {
//                 log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//                 if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
//             } else {

//                 let procedure = "";
//                 for (let i = 0; i < array.length; i++) {
//                     procedure = procedure + ` call ${appConfig.getDbConfig().database}.v1_set_student_fees
//                 (${utils.OptionalKey(array[i].challanID)}, ${utils.OptionalKey(array[i].studentID)},
//                 ${utils.OptionalKey(array[i].invoiceNumber)}, ${utils.OptionalKey(array[i].invoiceDate)},
//                 ${utils.OptionalKey(array[i].billingPeriod)},${utils.OptionalKey(array[i].totalAmountDue)},
//                 ${utils.OptionalKey(array[i].taxAmountDue)},${utils.OptionalKey(array[i].totalAmountAfter)},
//                 ${utils.OptionalKey(array[i].taxAmountAfterDue)}, ${utils.OptionalKey(array[i].dueDate)},
//                 ${utils.OptionalKey(array[i].status)},${utils.OptionalKey(array[i].paymentDate)},
//                 ${utils.OptionalKey(array[i].challanNote)},${utils.OptionalKey(array[i].endDate)});  `

//                 }
//                 db.query(procedure).then(data => {
//                     log.doInfoLog(fileNameForLogging, req.url, 'data successfully inserted in db', err)

//                     if(!res.finished)res.status(200).json({ response: "success" })
//                 })
//                     .catch(err => {
//                         log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', err)
//                         const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                         if(!res.finished)res.status(500).json({ response: errorResponse })
//                     })
//             }
//         })
// }

exports.getAttendance = function (req, res) {
  const { classID, dateFrom, dateTo, studentID } = req.body;
  
  Joi.validate({ classID, dateFrom, dateTo, studentID },studentValidations.getAttendance,function (err, value) {
      if (err) {
        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
        if(!res.finished)res.status(400).json({ response: errorResponse });
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            
            let dbquery = ` set @studentID = ${utils.OptionalKey(studentID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            set @dateFrom = ${utils.OptionalKey(dateFrom)};
                            set @dateTo = ${utils.OptionalKey(dateTo)};
                            call ${appConfig.getDbConfig().database}.v1_get_attendance
                            (@studentID,@classID,@dateFrom,@dateTo);`;

            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                        data.forEach(element => {
                          if(element.constructor==Array) {
                            if (_.isEmpty(element)) {
                              log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                              if(!res.finished)res.status(200).json({ response: errorResponse })
                            } 
                            else{
                              
                                  
                              if(!error){
                                var objectsResultArray = [];
                                 if (!_.isEmpty(element[0])) {
                                  log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                    
                                  let responseData = element;
                    
                                  let result = [];
                                  let map = new Map();
                                  let count = 0;
                                  
                                  var dateArray = [];
                                  var index = 0;
                    
                                  for (let data of responseData) {
                                    if (map.has(data.studentID)) {
                                      let tempObj = {};
                                      tempObj[data.attendanceDate] = data.isPresent;
                                      tempObj["notes"] = data.notes;
                                      dateArray.push(tempObj);
                                      index++;
                                    } else {
                                      if (count != 0) {
                                        let t = responseData[index - 1];
                    
                                        let tempObj = {};
                    
                                        tempObj["attendanceID"] = t.attendanceID;
                                        tempObj["studentID"] = t.studentID;
                                        tempObj["classID"] = t.classID;
                                        tempObj["rollNo"] = t.rollNo;
                                        tempObj["studentName"] = t.studentName;
                                        tempObj["userID"] = t.userID;
                                        tempObj["teacherName"] = t.teacherName;
                    
                                        tempObj["data"] = dateArray;
                                        objectsResultArray.push(tempObj);
                                        dateArray = [];
                                      }
                                      count = 0;
                                      map.set(data.studentID, result);
                                      let tempObj = {};
                                      tempObj[data.attendanceDate] = data.isPresent;
                                      tempObj["notes"] = data.notes;
                                      dateArray.push(tempObj);
                                      count++;
                                      index++;
                                    }
                                  }
                                  let t = responseData[index - 1];
                                  let tempObj = {};
                                  tempObj["attendanceID"] = t.attendanceID;
                                  tempObj["studentID"] = t.studentID;
                                  tempObj["classID"] = t.classID;
                                  tempObj["rollNo"] = t.rollNo;
                                  tempObj["studentName"] = t.studentName;
                                  tempObj["userID"] = t.userID;
                                  tempObj["teacherName"] = t.teacherName;
                                  tempObj["data"] = dateArray;
                                  objectsResultArray.push(tempObj);
                                }
                                if(!res.finished)res.status(200).json({ response: objectsResultArray });

                                }
                  
                              
                              else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                              }

                              
                            }
                          }
                            
                        })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
                });

            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setFeeTypes = function (req, res) {
  const {schoolID, branchID, feeTypeID,  feeType, taxRate,discountRate, isRecurring, isActive } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({schoolID, branchID, feeTypeID,  feeType, taxRate,discountRate, isRecurring, isActive }, studentValidations.setFeeTypes,function (err, value) {
      if (err) {
        log.doErrorLog(  fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            
            let dbquery = `
            set @schoolID = ${utils.OptionalKey(schoolID)};
            set @branchID = ${utils.OptionalKey(branchID)};
            set @feeTypeID = ${utils.OptionalKey(feeTypeID)};
            set @feeType = ${utils.OptionalKeyString(feeType)};
            set @taxRate = ${utils.OptionalKey(taxRate)};
            set @discountRate = ${utils.OptionalKey(discountRate)};
            set @isRecurring = ${utils.OptionalKey(isRecurring)};
            set @isActive = ${utils.OptionalKey(isActive)};
            call ${appConfig.getDbConfig().database}.v1_set_feetypes
            (@schoolID,@branchID, @feeTypeID, @feeType,@taxRate,@discountRate,@isRecurring,@isActive);`;
            console.log(dbquery)
            try {
               db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, 'successfully updated', error)
                    if(!res.finished)res.status(200).json({ response: "success" })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              }); 
            } 
            finally {
                db.release();
                console.log('released')
            }

            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setFeeStatus = function (req, res) {
  const { voucherUUID, paymentStatus,paymentDate, paidAmount, channelType } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({voucherUUID, paymentStatus,paymentDate, paidAmount,channelType }, studentValidations.setFeeStatus,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging, req.url, "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `
            set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
            set @paymentStatus = ${utils.OptionalKey(paymentStatus)};
            set @paymentDate = ${utils.OptionalKey(paymentDate)};
            set @paidAmount = ${utils.OptionalKey(paidAmount)};
            set @channelType = ${utils.OptionalKey(channelType)};
            call ${appConfig.getDbConfig().database}.v1_set_fee_status
            (@voucherUUID, @paymentStatus,@paymentDate,@paidAmount,@channelType);`;
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if(!res.finished)res.status(200).json({ response: "success" });
                      if (paymentStatus=='P'){
                        dbquery = `select * from v1_get_fee_voucher_notifications where voucherUUID='${voucherUUID}' `;

                        console.log(dbquery)
                        db.query(dbquery,function (error, data, fields) {
                              if(!error){
                                
                                if (!_.isEmpty(data)) {
                                  let messageText;
                                  
                                    messageText = `Thank you! Payment for the month of ${data[0].periodName} has been received on ${paymentDate}`;
                                    for (let i = 0; i < data.length; i++) {
                                    var messageConfig = appConfig.messageSettings(
                                      "Fee Voucher",
                                      messageText,
                                      data[i].parentID,
                                      "FeeChallanScreen",
                                      data[i].studentID,
                                      data[i].classID,
                                      data[i].branchID,
                                      data[i].schoolID,
                                      null
                                    );
                                    console.log(messageText)  
                                    
                                    push.SendNotification(data[i].deviceToken, messageConfig);
                                    }
                                  }
                                  // else if (paymentStatus=='U'){
                                  //   // messageText = `Payment for the month of ${data[0].periodName} is still UNPAID` ;
                                  // }
                                  // console.log(messageText) 

                                  
                                }
                                // if(!res.finished)res.status(200).json({ response: "success" });
                                else{
                                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                  console.log(errorResponse)
                                  if(!res.finished)res.status(500).json({ response: errorResponse })
                              }
                              });
                              
                          

        
                      }
                    }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.setAttendanceArray = function (req, res) {
  const { array } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  Joi.validate(array, studentValidations.setAttendanceArray, function (err,value) {
    if (err) {log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } 
    else {

      db.getConnection(function(err, db) {
        if(!err){
            if(array != undefined){

            let dbquery = "";
            for (let i = 0; i < array.length; i++) {
              dbquery =
                dbquery +
                ` call ${appConfig.getDbConfig().database}.v1_set_attendance
                (${utils.OptionalKey(array[i].attendanceID)}, 
                ${utils.OptionalKey(array[i].studentID)}, 
                ${utils.OptionalKey(array[i].classID)}, 
                ${utils.OptionalKey(array[i].attendanceDate)}, 
                ${utils.OptionalKey(array[i].isPresent)},
                ${utils.OptionalKey(array[i].notes)}, 
                ${utils.OptionalKey(array[i].userID)} );  `;
            }
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                if(!error){
                  log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                  if(!res.finished)res.status(200).json({ response: "success" });
                  let attendanceAlertArray=[
                                          [],
                                          [],
                                          [],
                                          []];
                  
                  // index 0=Abset, 1=Off, 2=Leave, 3=Late
                  for (let i = 0; i < array.length; i++) {
                    
                    if(array[i].isPresent=='A'){
                      //Absent
                      // console.log('Absent Loop')
                      attendanceAlertArray[0].push(array[i].studentID)
                    }
                    else if(array[i].isPresent=='O'){
                      // console.log('Off Loop')
                      // Off Day
                      attendanceAlertArray[1].push(array[i].studentID)
                    }
                    else if(array[i].isPresent=='L'){
                      // console.log('Leave Loop')
                      // Leave
                      attendanceAlertArray[2].push(array[i].studentID)
                    }
                    else if(array[i].isPresent=='T'){
                      //Late
                      // console.log('Late Loop')
                      attendanceAlertArray[3].push(array[i].studentID)
                    }
                  }
                  
                    if (attendanceAlertArray[0].length > 0){
                    //Absent for 0 index of sub array
                    console.log('Absent Notification')
                    let dbAbsentQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[0].join(",")}) `;
                    console.log(dbAbsentQuery)

                    db.query(dbAbsentQuery,function (error, data, fields) {
                          if(!error){
                            const studentIDs = data
                            let messageConfig, messageText
                            //const regIDs = [];
                            Object.keys(studentIDs).forEach(stud => {
                              //regIDs.push(studentIDs[stud].deviceToken);
                              if (studentIDs[stud].preferredLanguage=='ar'){
                                messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                                " غیر حاضر (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                              }
                              else {
                                messageText = studentIDs[stud].studentName +
                                  " has been marked Absent for the day (" + utils.formatReadableDate(array[0].attendanceDate,'en') + ")";
                              }
                                console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                                messageConfig = appConfig.messageSettings(
                                  "Attendance",
                                  messageText,
                                  "AttendanceScreen",
                                  studentIDs[stud].parentID, 
                                  studentIDs[stud].studentID,
                                  studentIDs[stud].classID,
                                  studentIDs[stud].branchID,
                                  studentIDs[stud].schoolID,
                                  null
                                  );
                                  push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                  // if (regIDs.length > 0)
                                  //   pushNotificationArray(regIDs, messageConfig)
                                  //     .then(d => { })
                                  //     .catch(err => {
                                  //       console.log(  "Error in sending Push notification Array"  );
                                  //       //log.doFatalLog(   fileNameForLogging,  req.url,  "Error in sending Push notification Array",  err );
                                    
                                  //     });

                            });
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          }
                      });
                    }
                    if (attendanceAlertArray[1].length > 0){
                    // Day Off for 1 index of sub array
                    console.log('Day Off Notification')
                    let dbOffQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[1].join(",")}) `;                      
                    console.log(dbOffQuery)
                    db.query(dbOffQuery,function (error, data, fields) {
                          if(!error){
                          const studentIDs = data
                          let messageConfig, messageText
                          //const regIDs = [];
                          Object.keys(studentIDs).forEach(stud => {
                            //regIDs.push(studentIDs[stud].deviceToken);
                          
                            if (studentIDs[stud].preferredLanguage=='ar'){
                              messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                              " يوم عطلة (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                            }
                            else{
                              messageText = studentIDs[stud].studentName +
                              " has been given Off for " + utils.formatReadableDate(array[0].attendanceDate,'en');
                            }
                            console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                            messageConfig = appConfig.messageSettings(
                                "Attendance",
                                messageText,
                                "AttendanceScreen",
                                studentIDs[stud].parentID, 
                                studentIDs[stud].studentID,
                                studentIDs[stud].classID,
                                studentIDs[stud].branchID,
                                studentIDs[stud].schoolID,
                                null
                                );
                            push.SendNotification(studentIDs[stud].deviceToken, messageConfig);

                            });
                                // if (regIDs.length > 0)
                                  
                                
                                  // pushNotificationArray(regIDs, messageConfig)
                                  //   .then(d => { })
                                  //   .catch(err => {
                                  //     console.log(  "Error in sending Push notification Array"  );
                                  
                          // });
                         
                      }
                      else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      }
                  });
                  
                    }
                    if (attendanceAlertArray[2].length > 0){
                    // Leave for 2 index of sub array
                    console.log('Leave Notification')
                    let dbLeaveQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[2].join(",")}) `;
                    console.log(dbLeaveQuery)

                    db.query(dbLeaveQuery,function (error, data, fields) {
                            if(!error){
                            const studentIDs = data
                            let messageConfig, messageText
                            // const regIDs = [];
                            Object.keys(studentIDs).forEach(stud => {
                              // regIDs.push(studentIDs[stud].deviceToken);
                              if (studentIDs[stud].preferredLanguage=='ar'){
                                messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                                " على الرخصة (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                              }
                              else{
                                messageText = studentIDs[stud].studentName +
                                  " has been marked Leave on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                              }
                              console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                              messageConfig = appConfig.messageSettings(
                                  "Attendance",
                                  messageText,
                                  "AttendanceScreen",
                                  studentIDs[stud].parentID, 
                                  studentIDs[stud].studentID,
                                  studentIDs[stud].classID,
                                  studentIDs[stud].branchID,
                                  studentIDs[stud].schoolID,
                                  null
                                  );
                                  push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                  // if (regIDs.length > 0)
                                  //   pushNotificationArray(regIDs, messageConfig)
                                  //     .then(d => { })
                                  //     .catch(err => {
                                  //       console.log("Error in sending Push notification Array"  );
                                  //       //log.doFatalLog(fileNameForLogging,  req.url,  "Error in sending Push notification Array",  err );
                                  // });
                            });
                        }
                        else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        }
                    });
                    }
                    if (attendanceAlertArray[3].length > 0){
                    // Late for 3 index of sub array
                    console.log('Late Notification')
                    let dbLateQuery= `select * from v1_get_attendance_notifications where studentID IN (${attendanceAlertArray[3].join(",")}) `;
                    console.log(dbLateQuery)
                    db.query(dbLateQuery,function (error, data, fields) {
                          if(!error){
                          const studentIDs = data
                          let messageConfig, messageText
                          // const regIDs = [];
                          Object.keys(studentIDs).forEach(stud => {
                            // regIDs.push(studentIDs[stud].deviceToken);
                            if (studentIDs[stud].preferredLanguage=='ar'){
                              messageText = " الیوم طفلك اسمه" + studentIDs[stud].studentName +
                              " متأخر (" + utils.formatReadableDate(array[0].attendanceDate,'ar') + ")";
                            }
                            else{
                              messageText = studentIDs[stud].studentName +
                              " has been marked Late for " + utils.formatReadableDate(array[0].attendanceDate,'en');
                            }
                            // console.log(messageText + ' : ' + studentIDs[stud].deviceToken)

                            messageConfig = appConfig.messageSettings(
                                "Attendance",
                                messageText,
                                "AttendanceScreen",
                                studentIDs[stud].parentID, 
                                studentIDs[stud].studentID,
                                studentIDs[stud].classID,
                                studentIDs[stud].branchID,
                                studentIDs[stud].schoolID,
                                null
                                );
                                push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                // if (regIDs.length > 0)
                                //   pushNotificationArray(regIDs, messageConfig)
                                //     .then(d => { })
                                //     .catch(err => {
                                //       console.log(  "Error in sending Push notification Array"  );
                                //       //log.doFatalLog(fileNameForLogging,  req.url,"Error in sending Push notification Array",  err );                                    
                                // });

                          });
                      }
                      else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      }
                    });
                    }

                }
                else{
                  log.doInfoLog( fileNameForLogging, req.url,   "no absent notificaiton",    error  );
                  if(!res.finished)res.status(500).json({ response: "error in database query" })
                }
            
              });
          } 
            finally {
                db.release();
                console.log('released')
            }
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })

          }
        
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};


exports.setStudentImage = function (req, res) {
  const { studentID, picture } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(
    { studentID, picture },studentValidations.setStudentImage,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=
            `  set @studentID = ${studentID};
                              set @picture = ${utils.OptionalKey(picture)}; 
              
                              call ${appConfig.getDbConfig().database}.v1_set_student_image
                              (@studentID,@picture);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                        data.forEach(element => {
                             if(element.constructor==Array) {
                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                const errorResponse = new Error( appConfig.EMPTY_DATA_ERROR_MSG, "data not present" );
                                if(!res.finished)res.status(404).json({ response: errorResponse });
                              } else {
                                log.doInfoLog( fileNameForLogging,  req.url,  "successfully data fetched from db" );
                                if(!res.finished)res.status(200).json({ response: "success" });
                              }
                             }
                         })
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getStudentsbyParentID = function (req, res) {
  const { parentID, schoolID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ parentID, schoolID }, studentValidations.getStudentsbyParentID,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_students WHERE parentID = '${parentID}' and schoolID = ${schoolID} `;

            console.log(dbquery)
            try {
              
            } 
            finally {
                db.release();
                console.log('released')
            }
            db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if (_.isEmpty(data)) {
                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                      const errorResponse = new Error(  appConfig.EMPTY_DATA_ERROR_MSG, "data not present" );
                      if(!res.finished)res.status(200).json({ response: errorResponse });
                    } else {
                      log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                      if(!res.finished)res.status(200).json({ response: data});
                    }
      
                  }
                  else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                            if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getStudentSearch = function (req, res) {
  const { schoolID, branchID, rollNo, studentName } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ schoolID, branchID, rollNo, studentName },studentValidations.getStudentSearch,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_student_search WHERE  schoolID = ${schoolID} and  branchID = ${branchID} `;

            if (!_.isEmpty(rollNo)) {
              dbquery += ` AND rollNo = '${rollNo}' `;
            }
    
            if (!_.isEmpty(studentName)) {
              dbquery += ` AND studentName LIKE '%${studentName.replace("'","''")}%'`;
            }
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if (_.isEmpty(data)) {
                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                      if(!res.finished)res.status(200).json({ response: errorResponse });
                    } else {
                      log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                      if(!res.finished)res.status(200).json({ response: data });
                    }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};




exports.getStudentOverallResults = function (req, res) {
  const { classID, examsTermID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ classID, examsTermID },studentValidations.getStudentOverallResults,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
          let dbquery=
          `set @classID = ${classID};
            set @examsTermID = ${utils.OptionalKey(examsTermID)};
            
            call ${appConfig.getDbConfig().database}.v1_get_student_overall_exams
            (@classID,@examsTermID);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                        data.forEach(element => {
                             if(element.constructor==Array) {
                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                if(!res.finished)res.status(200).json({ response: errorResponse });
                              } else {
                                log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                                if(!res.finished)res.status(200).json({ response: element });
                              }
                             }
                         })
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            

      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setRecurringFeeArray = function (req, res) {
  const { array } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(array, studentValidations.setRecurringFeeArray, function (err,value) {
    if (err) {
      log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } 
    else {
      db.getConnection(function(err, db) {
        if(!err){
          if(array != undefined){

          let dbquery = "";
          for (let i = 0; i < array.length; i++) {
            dbquery =
              dbquery +
              ` call ${appConfig.getDbConfig().database}.v1_set_recurring_fee
                (${utils.OptionalKey(array[i].feeLinkID)}, 
                ${utils.OptionalKey(array[i].schoolID)}, 
                ${utils.OptionalKey(array[i].branchID)}, 
                ${utils.OptionalKey(array[i].studentID)},
                ${utils.OptionalKey(array[i].feeTypeID)},
                ${utils.OptionalKey(array[i].feeAmount)}); `;
          }
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
                if(!error){
                   
                  if(!res.finished)res.status(200).json({ response: "success" });

    
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })

            }

        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
          const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

exports.getRecurringFee = function (req, res) {
  const {schoolID, branchID, classID, studentID, feeTypeID, feeLinkID} = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ schoolID, branchID, classID, studentID, feeTypeID, feeLinkID }, studentValidations.getRecurringFee,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_recurring_fee WHERE schoolID = '${schoolID}' and branchID = '${branchID}' and  classID = '${classID}'`;
            if (!_.isEmpty(feeLinkID)) {
              dbquery += ` AND feeLinkID = '${feeLinkID}'`;
            }
    
            if (!_.isEmpty(studentID)) {
              dbquery += ` AND studentID = '${studentID}'`;
            }
            if (!_.isEmpty(feeTypeID)) {
              dbquery += ` AND feeTypeID = '${feeTypeID}'`;
            }
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if (_.isEmpty(data)) {
                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                      if(!res.finished)res.status(200).json({ response: errorResponse });
                    } else {
                      log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                      if(!res.finished)res.status(200).json({ response: data });
                    }
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
          }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            

      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setFeeVoucherGenerate = function (req, res) {
  const {studentID, classID,invoiceStartNo,invoiceDate,billingPeriodID,penaltyType,penaltyAmount,dueDate,
    voucherNote,endDate,voucherDetails} = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentID,classID,invoiceStartNo,invoiceDate,billingPeriodID,penaltyType,penaltyAmount,dueDate,
      voucherNote,endDate,voucherDetails}, studentValidations.setFeeVoucherGenerate,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){

            let dbquery = ""
            if (studentID){
              dbquery= `set @studentID = ${utils.OptionalKey(studentID)};`
            }

            dbquery += ` 
            set @classID = ${utils.OptionalKey(classID)};
            set @invoiceStartNo = ${utils.OptionalKey(invoiceStartNo)};
            set @invoiceDate = ${utils.OptionalKey(invoiceDate)};
            set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};
            set @penaltyType = ${utils.OptionalKey(penaltyType)};
            set @penaltyAmount = ${utils.OptionalKey(penaltyAmount)};
            set @dueDate = ${utils.OptionalKey(dueDate)};
            set @voucherNote = ${utils.OptionalKeyString(voucherNote)};
            set @endDate = ${utils.OptionalKey(endDate)};`
            if (studentID){
              dbquery+= `call ${appConfig.getDbConfig().database}.v1_set_fee_voucher_generate_student
              (@studentID,@classID,@invoiceStartNo,@invoiceDate,@billingPeriodID,@penaltyType,@penaltyAmount,
                @dueDate,@voucherNote,@endDate);`
            }
            else{
              dbquery+= `call ${appConfig.getDbConfig().database}.v1_set_fee_voucher_generate
              (@classID,@invoiceStartNo,@invoiceDate,@billingPeriodID,@penaltyType,@penaltyAmount,
                @dueDate,@voucherNote,@endDate);`
            }
            
            console.log(dbquery)
            try {
              db.query(dbquery, function (error, data, fields) {
                  if(!error){
                     
                    if (voucherDetails !== undefined) {
                      let array = voucherDetails;
                      let dbquery = "";
                      for (let i = 0; i < array.length; i++) {
                        
                        
                        if (studentID){
                          dbquery += ` call ${ appConfig.getDbConfig().database}.v1_set_fee_voucher_details_student
                                      (${utils.OptionalKey(studentID)},
                                      ${utils.OptionalKey(classID)},
                                      ${utils.OptionalKey(billingPeriodID)},
                                      ${utils.OptionalKey(array[i].feeTypeID)},
                                      ${utils.OptionalKey(array[i].feeTypeAmount)}); 
                                       `;
                        }
                        else{
                          dbquery += ` call ${ appConfig.getDbConfig().database}.v1_set_fee_voucher_details
                                      (
                                      ${utils.OptionalKey(classID)},
                                      ${utils.OptionalKey(billingPeriodID)},
                                      ${utils.OptionalKey(array[i].feeTypeID)},
                                      ${utils.OptionalKey(array[i].feeTypeAmount)}); 
                                       `;
                        }
                          
                                       
                      }
                      console.log(dbquery)
                      db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                              if(!res.finished)res.status(200).json({ response: "success" });         

                            }
                            else{
                              log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                              const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                              if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } 
                    else {
                      if(!res.finished)res.status(200).json({ response: "success" });
                      
                    }
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
            
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getFeeVoucher = function (req, res) {
  const { billingPeriodID, branchID,voucherID, studentID, classID, paymentStatus } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ billingPeriodID, branchID,voucherID, studentID, classID,paymentStatus },studentValidations.getFeeVoucher,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`  
            set @voucherID = ${utils.OptionalKey(voucherID)}; 
            set @branchID = ${utils.OptionalKey(branchID)};
            set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};
            set @studentID = ${utils.OptionalKey(studentID)};
            set @classID = ${utils.OptionalKey(classID)};
            set @paymentStatus = ${utils.OptionalKey(paymentStatus)};
            call ${appConfig.getDbConfig().database}.v1_get_fee_vouchers
            (@voucherID,@branchID,@billingPeriodID,@studentID,@classID,@paymentStatus);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    
                        data.forEach(element => {
                             if(element.constructor==Array) {
                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                if(!res.finished)res.status(404).json({ response: errorResponse });
                              } else {
                                log.doInfoLog(
                                  fileNameForLogging,req.url,"successfully data fetched from db");
                                  for (let i = 0; i < element.length; i++){
                                    if (element[i].hasOwnProperty("voucherNote")) {
                                      let var_voucherNote = element[i].voucherNote
                                      if(var_voucherNote){
                                        delete element[i].voucherNote;
                                        element[i].voucherNote = utils.EncryptBase64(helper.OptionalKeyString(var_voucherNote))
                                      }
                                      

                                    }

                                  }
                                  if(!res.finished)res.status(200).json({ response: element });
                              }
      
                             }
                         })
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getFeeVoucherDetail = function (req, res) {
  const { billingPeriodID, classID, studentID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ billingPeriodID, classID,studentID }, studentValidations.getFeeVoucherDetail, function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=`   set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};
                            set @studentID = ${utils.OptionalKey(studentID)};
                            set @classID = ${utils.OptionalKey(classID)};
            call ${appConfig.getDbConfig().database}.v1_get_fee_voucher_detail
            (@studentID,@classID,@billingPeriodID);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                        data.forEach(element => {
                             if(element.constructor==Array) {
                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                if(!res.finished)res.status(404).json({ response: errorResponse });
                              } 
                              else {
                                log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                                if(!res.finished)res.status(200).json({ response: element });
                              }
                             }
                         })
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setFeeVoucherDelete = function (req, res) {
  const { voucherUUID, feeVoucherDetailID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ voucherUUID, feeVoucherDetailID },studentValidations.setFeeVoucherDelete,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=
            `  set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
              set @feeVoucherDetailID = ${utils.OptionalKey(feeVoucherDetailID)};
              call ${appConfig.getDbConfig().database}.v1_set_fee_voucher_delete
              (@voucherID,@feeVoucherDetailID);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    
                      log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                      if(!res.finished)res.status(200).json({ response: "success" });
                    
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.setFeeVoucher = function (req, res) {
  const {voucherID,studentID,classID,invoiceNumber, invoiceDate,billingPeriodID, penaltyType,penaltyValue, dueDate,  paymentStatus,
    paidAmount,paymentDate,voucherNote, endDate, voucherDetails } = req.body;
    
    log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({voucherID,studentID,classID,invoiceNumber, invoiceDate,billingPeriodID, penaltyType,penaltyValue, dueDate,  paymentStatus,
    paidAmount,paymentDate,voucherNote, endDate, voucherDetails }, studentValidations.setFeeVoucher,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            
            let dbquery=
            `
            set @voucherID = ${utils.OptionalKey(voucherID)};
            set @studentID = ${utils.OptionalKey(studentID)};
            set @classID = ${utils.OptionalKey(classID)};
            set @invoiceNumber = ${utils.OptionalKey(invoiceNumber)};
            set @invoiceDate = ${utils.OptionalKey(invoiceDate)};
            set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};
            set @penaltyType = ${utils.OptionalKey(penaltyType)};
            set @penaltyValue = ${utils.OptionalKey(penaltyValue)};
            set @dueDate = ${utils.OptionalKey(dueDate)};
            set @paymentStatus = ${utils.OptionalKey(paymentStatus)};
            set @paidAmount = ${utils.OptionalKey(paidAmount)};
            set @paymentDate = ${utils.OptionalKey(paymentDate)};
            set @voucherNote = ${utils.OptionalKeyString(voucherNote)};
            set @endDate = ${utils.OptionalKey(endDate)};
  
              call ${appConfig.getDbConfig().database}.v1_set_fee_voucher
              (@voucherID,@studentID,@classID,@invoiceNumber, @invoiceDate, @billingPeriodID,@penaltyType,@penaltyValue,@dueDate,@paymentStatus,@paidAmount,@paymentDate,@voucherNote, @endDate );`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    
                     
                    if (voucherDetails != undefined) {
                      let array = voucherDetails;
                      let dbquery = "";
                      for (let i = 0; i < array.length; i++) {
                        
                        dbquery =
                          dbquery + ` call ${appConfig.getDbConfig().database}.v1_set_feetype_voucher_details
                                          (${utils.OptionalKey(array[i].feeVoucherDetailID )},
                                          ${utils.OptionalKey(studentID)},
                                          ${utils.OptionalKey(classID)},
                                          ${utils.OptionalKey(billingPeriodID)},
                                          ${utils.OptionalKey(array[i].feeTypeID)},
                                          ${utils.OptionalKey(array[i].feeTypeAmount)});`;
                      }
                      console.log(dbquery)
                      db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                              if(!res.finished)res.status(200).json({ response: "success" });
                
                            }
                            else{
                              log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                              const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                              if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                    } else {
                      if(!res.finished)res.status(200).json({ response: "success" });
                    }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
             
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.getFeesVoucherPrint = function (req, res) {
  const { schoolID, branchID, classID, studentID, billingPeriodID } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  Joi.validate({schoolID, branchID, classID, studentID, billingPeriodID },studentValidations.getFeesVoucherPrint,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=` set @schoolID = ${utils.OptionalKey(schoolID)};
                          set @branchID = ${utils.OptionalKey(branchID)};
                          set @classID = ${utils.OptionalKey(classID)};
                          set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};
                          set @studentID = ${utils.OptionalKey(studentID)};
                          call ${appConfig.getDbConfig().database}.v1_get_fee_voucher_print
                          (@schoolID,@branchID,@classID,@billingPeriodID, @studentID);`

            
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, feeChallanData, fields) {
                      if(!error){
                        feeChallanData.forEach(element => {  
                          if(element.constructor==Array) { 
                          if (_.isEmpty(element)) {
                            log.doErrorLog(fileNameForLogging, req.url, "data not present");
                            const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                            if(!res.finished)res.status(200).json({ response: errorResponse });
                          } 
                          else {
                            let challanId = "";
                            for (let loop = 0; loop < element.length; loop++) {
                              if (element[loop].challanID != null || element[loop].challanID != "")
                                challanId = element[loop].challanID + "," + challanId;
                            }
                            challanId = challanId.substr(0, challanId.length - 1);
                            //console.log(challanId);
              
                            if (challanId != "" || challanId != null) {
                              // let IDs = challanId.split(",");
                              // let whereClause = "";
                              // if (IDs.length != 0) {
                              //   for (let x = 0; x < IDs.length; x++) {
                              //     whereClause = whereClause + ` challanID = '${IDs[x]}' or`;
                              //   }
                              //   whereClause = whereClause.substr(0, whereClause.length - 2);
                              //   whereClause = "WHERE " + whereClause;
                              // }
                              let whereClause = ` where FIND_IN_SET(challanID,'${challanId}')`
                              dbquery = `SELECT * FROM v1_get_fee_details ${whereClause} order by challanID asc`
                              // console.log(dbquery)
                              db.query(dbquery,function (error, feeDetailsData, fields) {
                                    if(!error){
                                      
                                      if (_.isEmpty(feeDetailsData)) {
                                        log.doErrorLog( fileNameForLogging,req.url,  "data not present");
                                        const errorResponse = new Error( appConfig.EMPTY_DATA_ERROR_MSG, "data not present");
                                        if(!res.finished)res.status(200).json({ response: errorResponse });
                                      } 
                                      else {
                                        for (let x = 0; x < element.length; x++) {
                                          let challanID = element[x].challanID;
                                          let data = new Array();
                                          let looper = 0;
                                          for (let y = 0; y < feeDetailsData.length; y++) {
                                            if (challanID == feeDetailsData[y].challanID) {
                                              data[looper] = feeDetailsData[y];
                                              looper++;
                                            }
                                          }
                                          element[x].feeDetailsData = data;
                                        }
                  
                                        if(!res.finished)res.status(200).json({ response: [{ feeChallan: element } ] });
                                      }
                        
                                    }
                                    else{
                                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                        if(!res.finished)res.status(500).json({ response: errorResponse })
                                    }
                                });
                            } else {
                              log.doErrorLog(fileNameForLogging, req.url, "data not present");
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                              if(!res.finished)res.status(200).json({ response: errorResponse });
                            }
                          }
                        }
                      })
                      
                    }
                      else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                          if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                  });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
          }
          else{
                log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};



exports.setSubjectAttendanceArray = function (req, res) {
  const { array } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  Joi.validate(array, studentValidations.setSubjectAttendanceArray, function (err,value) {
    if (err) {log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
      res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } 
    else {

      db.getConnection(function(err, db) {
        if(!err){
          if(array != undefined){

            let dbquery = "";
            var var_timetableID = array[0].timetableSubjectID
            for (let i = 0; i < array.length; i++) {
              dbquery =
                dbquery +
                ` call ${appConfig.getDbConfig().database}.v1_set_subject_attendance
                (${utils.OptionalKey(array[i].attendanceID)}, 
                ${utils.OptionalKey(array[i].attendanceDate)}, 
                ${utils.OptionalKey(array[i].batchNo)}, 
                ${utils.OptionalKey(array[i].timetableSubjectID)}, 
                ${utils.OptionalKey(array[i].classID)},
                ${utils.OptionalKey(array[i].studentID)},
                ${utils.OptionalKey(array[i].isPresent)},
                ${utils.OptionalKeyString(array[i].notes)}, 
                ${utils.OptionalKey(array[i].userID)} );  `;
            }
            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                    log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
                    if(!res.finished)res.status(200).json({ response: "success" });
                    let attendanceAlertArray=[
                                            [],
                                            [],
                                            [],
                                            [],
                                            []];
                    
                    // index 0=Abset, 1=Off, 2=Leave, 3=Late , 4=Present
                    for (let i = 0; i < array.length; i++) {
                      
                      if(array[i].isPresent=='A'){
                        //Absent
                        attendanceAlertArray[0].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='O'){
                        // Off Day
                        attendanceAlertArray[1].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='L'){
                        // Leave
                        attendanceAlertArray[2].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='T'){
                        //Late
                        attendanceAlertArray[3].push(array[i].studentID)
                      }
                      else if(array[i].isPresent=='P'){
                        //Present
                        attendanceAlertArray[4].push(array[i].studentID)
                      }
                    }
                    
                      if (attendanceAlertArray[0].length > 0){
                      //Absent for 0 index of sub array
                      console.log('Absent Notification Query')
                      let dbAbsentQuery= `select * from v1_get_att_subject_notification where studentID IN (${attendanceAlertArray[0].join(",")}) and timetableSubjectID=${var_timetableID}`;
                      console.log(dbAbsentQuery)

                        db.query(dbAbsentQuery,function (error, data, fields) {
                              if(!error){
                                const studentIDs = data
                                let messageConfig, messageText
                                // const regIDs = [];
                                Object.keys(studentIDs).forEach(stud => {
                                // regIDs.push(studentIDs[stud].deviceToken);
                                if (studentIDs[stud].preferredLanguage=='ar'){
                                  messageText = " الیوم طفلك اسمه " + studentIDs[stud].studentName +
                                  "  غیر حاضر عن " + studentIDs[stud].subjectName + " ليوم " + utils.formatReadableDate(array[0].attendanceDate,'ar');
                                }
                                else {
                                  messageText = studentIDs[stud].studentName +
                                    " has been marked Absent for " + studentIDs[stud].subjectName  + " on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                                }
                                  console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                                  messageConfig = appConfig.messageSettings(
                                    "Attendance",
                                    messageText,
                                    "AttendanceScreen",
                                    studentIDs[stud].parentID, 
                                    studentIDs[stud].studentID,
                                    studentIDs[stud].classID,
                                    studentIDs[stud].branchID,
                                    studentIDs[stud].schoolID,
                                    null
                                    );
                                    push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                    // if (regIDs.length > 0)
                                    //   pushNotificationArray(regIDs, messageConfig)
                                    //     .then(d => { })
                                    //     .catch(err => {
                                    //       console.log(  "Error in sending Push notification Array"  );
                                    //       //log.doFatalLog(   fileNameForLogging,  req.url,  "Error in sending Push notification Array",  err );
                                      
                                    //     });

                              });
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          }
                        });
                      }
                      if (attendanceAlertArray[1].length > 0){
                      // Day Off for 1 index of sub array
                      console.log('DayOff Notification Query')
                      let dbOffQuery= `select * from v1_get_att_subject_notification where studentID IN (${attendanceAlertArray[1].join(",")}) and timetableSubjectID=${var_timetableID}`;                      
                      console.log(dbOffQuery)
                      db.query(dbOffQuery,function (error, data, fields) {
                            if(!error){
                            const studentIDs = data
                            let messageConfig, messageText
                            // const regIDs = [];
                            Object.keys(studentIDs).forEach(stud => {
                              // regIDs.push(studentIDs[stud].deviceToken);
                              if (studentIDs[stud].preferredLanguage=='ar'){
                                  messageText = " الیوم طفلك اسمه " + studentIDs[stud].studentName +
                                  "  يوم عطلة عن " + studentIDs[stud].subjectName + " ليوم " + utils.formatReadableDate(array[0].attendanceDate,'ar');
                              }
                              else{
                                messageText = studentIDs[stud].studentName +
                                " has been marked Off for " + studentIDs[stud].subjectName + " on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                              }
                              console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                              messageConfig = appConfig.messageSettings(
                                  "Attendance",
                                  messageText,
                                  "AttendanceScreen",
                                  studentIDs[stud].parentID, 
                                  studentIDs[stud].studentID,
                                  studentIDs[stud].classID,
                                  studentIDs[stud].branchID,
                                  studentIDs[stud].schoolID,
                                  null
                                  );
                                  push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                  // if (regIDs.length > 0)
                                  //   pushNotificationArray(regIDs, messageConfig)
                                  //     .then(d => { })
                                  //     .catch(err => {
                                  //       console.log(  "Error in sending Push notification Array"  );
                                    
                                  // });
                            });
                        }
                        else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        }
                      });
                    
                      }
                      if (attendanceAlertArray[2].length > 0){
                      // Leave for 2 index of sub array
                      console.log('Leaves Notification Query')
                      let dbLeaveQuery= `select * from v1_get_att_subject_notification where studentID IN (${attendanceAlertArray[2].join(",")}) and timetableSubjectID=${var_timetableID}`;
                      console.log(dbLeaveQuery)

                      db.query(dbLeaveQuery,function (error, data, fields) {
                              if(!error){
                              const studentIDs = data
                              let messageConfig, messageText
                              // const regIDs = [];
                              Object.keys(studentIDs).forEach(stud => {
                                // regIDs.push(studentIDs[stud].deviceToken);
                                if (studentIDs[stud].preferredLanguage=='ar'){
                                  messageText = " الیوم طفلك اسمه " + studentIDs[stud].studentName +
                                  "  على الرخصة عن " + studentIDs[stud].subjectName + " ليوم " + utils.formatReadableDate(array[0].attendanceDate,'ar');
                                }
                                else{
                                  messageText = studentIDs[stud].studentName +
                                  " has been marked Leave for " + studentIDs[stud].subjectName  + " on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                                }
                                console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
                                messageConfig = appConfig.messageSettings(
                                    "Attendance",
                                    messageText,
                                    "AttendanceScreen",
                                    studentIDs[stud].parentID, 
                                    studentIDs[stud].studentID,
                                    studentIDs[stud].classID,
                                    studentIDs[stud].branchID,
                                    studentIDs[stud].schoolID,
                                    null
                                    );
                                    push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                    // if (regIDs.length > 0)
                                    //   pushNotificationArray(regIDs, messageConfig)
                                    //     .then(d => { })
                                    //     .catch(err => {
                                    //       console.log("Error in sending Push notification Array"  );
                                    // });
                              });
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          }
                        });
                      }
                      if (attendanceAlertArray[3].length > 0){
                      // Late for 3 index of sub array
                      console.log('Late Notification Query')
                      let dbLateQuery= `select * from v1_get_att_subject_notification where studentID IN (${attendanceAlertArray[3].join(",")}) and timetableSubjectID=${var_timetableID}`;
                      console.log(dbLateQuery)
                      db.query(dbLateQuery,function (error, data, fields) {
                            if(!error){
                            const studentIDs = data
                            let messageConfig, messageText
                            // const regIDs = [];
                            Object.keys(studentIDs).forEach(stud => {
                              // regIDs.push(studentIDs[stud].deviceToken);
                              if (studentIDs[stud].preferredLanguage=='ar'){
                                messageText = " الیوم طفلك اسمه " + studentIDs[stud].studentName +
                                "  متأخر عن " + studentIDs[stud].subjectName + " ليوم " + utils.formatReadableDate(array[0].attendanceDate,'ar');
                              }
                              else{
                                messageText = studentIDs[stud].studentName +
                                " has been marked late for " + studentIDs[stud].subjectName  + " on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                              }
                              console.log(messageText + ' : ' + studentIDs[stud].deviceToken)

                              messageConfig = appConfig.messageSettings(
                                  "Attendance",
                                  messageText,
                                  "AttendanceScreen",
                                  studentIDs[stud].parentID, 
                                  studentIDs[stud].studentID,
                                  studentIDs[stud].classID,
                                  studentIDs[stud].branchID,
                                  studentIDs[stud].schoolID,
                                  null
                                  );
                                  push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                  // if (regIDs.length > 0)
                                  //   pushNotificationArray(regIDs, messageConfig)
                                  //     .then(d => { })
                                  //     .catch(err => {
                                  //       console.log(  "Error in sending Push notification Array"  );
                                  //       //log.doFatalLog(fileNameForLogging,  req.url,"Error in sending Push notification Array",  err );                                    
                                  // });

                            });
                        }
                        else{
                          log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        }
                        });
                      }
                      if (attendanceAlertArray[4].length > 0){
                        // Present for 4 index of sub array
                        console.log('Present Notification Query')
                        let dbPresentQuery= `select * from v1_get_att_subject_notification where studentID IN (${attendanceAlertArray[4].join(",")}) and timetableSubjectID=${var_timetableID}`;
                        console.log(dbPresentQuery)
                        db.query(dbPresentQuery,function (error, data, fields) {
                              if(!error){
                              const studentIDs = data
                              let messageConfig, messageText
                              // const regIDs = [];
                              Object.keys(studentIDs).forEach(stud => {
                                // regIDs.push(studentIDs[stud].deviceToken);
                                if (studentIDs[stud].preferredLanguage=='ar'){
                                  messageText = " الیوم طفلك اسمه " + studentIDs[stud].studentName +
                                  " حاضر عن " + studentIDs[stud].subjectName + " ليوم " + utils.formatReadableDate(array[0].attendanceDate,'ar');
                                }
                                else{
                                  messageText = studentIDs[stud].studentName +
                                  " has been marked Present for " + studentIDs[stud].subjectName  + " on " + utils.formatReadableDate(array[0].attendanceDate,'en');
                                }
                                console.log(messageText + ' : ' + studentIDs[stud].deviceToken)
  
                                messageConfig = appConfig.messageSettings(
                                    "Attendance",
                                    messageText,
                                    "AttendanceScreen",
                                    studentIDs[stud].parentID, 
                                    studentIDs[stud].studentID,
                                    studentIDs[stud].classID,
                                    studentIDs[stud].branchID,
                                    studentIDs[stud].schoolID,
                                    null
                                    );
                                    push.SendNotification(studentIDs[stud].deviceToken, messageConfig);
                                    // if (regIDs.length > 0)
                                    //   pushNotificationArray(regIDs, messageConfig)
                                    //     .then(d => { })
                                    //     .catch(err => {
                                    //       console.log(  "Error in sending Push notification Array"  );
                                    //       //log.doFatalLog(fileNameForLogging,  req.url,"Error in sending Push notification Array",  err );                                    
                                    // });
  
                              });
                          }
                          else{
                            log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                          }
                          });
                        }
                
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
  
  
  }
            else{
              log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
              if(!res.finished)res.status(500).json({ response: "'array not found'" })
  
              }
        
        }
        else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

// exports.setSubjectAttendance = function (req, res) {
//   const { subjectAttendanceID,attendanceDate, batchNo, timetableSubjectID,studentID, isPresent, notes,  userID } = req.body;
//   log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
//   Joi.validate({subjectAttendanceID,attendanceDate, batchNo, timetableSubjectID,studentID, isPresent, notes,  userID}, studentValidations.setSubjectAttendance, function (err,value) {
//     if (err) {log.doErrorLog( fileNameForLogging, req.url, "required params validation failed." );
//       res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
//     } 
//     else {

//       db.getConnection(function(err, db) {
//         if(!err){

//             let dbquery = "";
           
//               dbquery = ` call ${appConfig.getDbConfig().database}.v1_set_subject_attendance
//                 (${utils.OptionalKey(subjectAttendanceID)}, 
//                 ${utils.OptionalKey(attendanceDate)}, 
//                 ${utils.OptionalKey(batchNo)}, 
//                 ${utils.OptionalKey(timetableSubjectID)}, 
//                 ${utils.OptionalKey(studentID)},
//                 ${utils.OptionalKey(isPresent)},
//                 ${utils.OptionalKey(notes)}, 
//                 ${utils.OptionalKey(userID)} );  `;
            
//             console.log(dbquery)
//             try {
//               db.query(dbquery,function (error, data, fields) {
//                   if(!error){
//                     log.doInfoLog(fileNameForLogging, req.url, "db execution successful", null);
//                     res.status(200).json({ response: "success" });

//                       if (isPresent !== 'P'){
//                         let dbquery = `select  * from v1_get_attendance_notifications where studentID IN (${studentID}) `;
//                         console.log(dbquery);
                      
//                         db.query(dbquery,function (error, data, fields) {
//                             if(!error){
                               
                              
                              
                            
//                               let messageText = data[0].studentName +
//                                 " has been marked absent for the day (" +
//                                 data[0].attendanceDate +
//                                 ")";
//                               let messageConfig = appConfig.messageSettings(
//                                 "Attendance",
//                                 messageText,
//                                 "AttendanceScreen",
//                                 data[0].parentID,
//                                 data[0].studentID,
//                                 data[0].classID,
//                                 data[0].branchID,
//                                 data[0].schoolID,
//                                 null
//                               );
//                               console.log(messageText)
                            
          
                              
//                                 pushNotificationArray(data[0].deviceToken, messageConfig)
//                                   .then(d => { })
//                                   .catch(err => {
//                                     console.log(  "Error in sending Push notification Array"  );
//                                     log.doFatalLog(   fileNameForLogging,  req.url,  "error in db execution",  err );
                                    
//                                   });
                            
//                             }
//                             else{
//                               log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                      
//                             }
//                         });
//                       }
//                       else{
//                         log.doInfoLog( fileNameForLogging, req.url,   "no absent notificaiton",    error  );
  
//                       }
                
      
//                   }
//                   else{
//                     log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
//                     const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
//                     res.status(500).json({ response: errorResponse })
//                   }
//               });
//             } 
//             finally {
//                 db.release();
//                 console.log('released')
//             }
            
//         }
//         else{
//             log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
//             const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//             res.status(500).json({ response: errorResponse })
    
//         }
//       })
//     }
//   });
// };



exports.getStudentSubjectAttendance = function (req, res) {
  const { attendanceDate, batchNo, timetableSubjectID } = req.body;
  Joi.validate({ attendanceDate, batchNo, timetableSubjectID},
    studentValidations.getStudentSubjectAttendance,function (err, value) {
      if (err) {
        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
        res.status(400).json({ response: errorResponse });
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_subject_attendance  WHERE attendanceDate = '${attendanceDate}' `;
          
            if (!_.isEmpty(timetableSubjectID)) {
              dbquery += ` AND timetableSubjectID = '${timetableSubjectID}'`;
            }
            if (!_.isEmpty(batchNo)) {
              dbquery += ` AND batchNo = '${batchNo}'`;
            }


            
            dbquery += " order by  attendanceDate,studentName";
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if (_.isEmpty(data)) {
                      log.doErrorLog(fileNameForLogging, req.url, "data not present");
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present" );
                      res.status(200).json({ response: errorResponse });
                    } 
                    else {
                      log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
        
                      let responseData = data;
        
                      let result = [];
                      let map = new Map();
                      let count = 0;
                      var objectsResultArray = [];
                      var dateArray = [];
                      var index = 0;
        
                      for (let data of responseData) {
                        if (map.has(data.studentID)) {
                          let tempObj = {};
                          tempObj[data.attendanceDate] = data.isPresent;
                          tempObj["notes"] = data.notes;
                          dateArray.push(tempObj);
                          index++;
                        } else {
                          if (count != 0) {
                            let t = responseData[index - 1];
        
                            let tempObj = {};
                            tempObj["attendanceID"] = t.attendanceID;
                            tempObj["timetableSubjectID"] = t.timetableSubjectID;
                            tempObj["studentID"] = t.studentID;
                            tempObj["batchNo"] = t.batchNo;
                            tempObj["rollNo"] = t.rollNo;
                            tempObj["studentName"] = t.studentName;
                            tempObj["userID"] = t.userID;
                            tempObj["subjectName"] = t.subjectName;
        
                            tempObj["data"] = dateArray;
                            objectsResultArray.push(tempObj);
                            dateArray = [];
                          }
                          count = 0;
                          map.set(data.studentID, result);
                          let tempObj = {};
                          tempObj[data.attendanceDate] = data.isPresent;
                          tempObj["notes"] = data.notes;
                          dateArray.push(tempObj);
                          count++;
                          index++;
                        }
                      }
                      let t = responseData[index - 1];
                      let tempObj = {};
                      tempObj["attendanceID"] = t.attendanceID;
                      tempObj["timetableSubjectID"] = t.timetableSubjectID;
                      tempObj["studentID"] = t.studentID;
                      tempObj["batchNo"] = t.batchNo;
                      tempObj["rollNo"] = t.rollNo;
                      tempObj["studentName"] = t.studentName;
                      tempObj["userID"] = t.userID;
                      tempObj["subjectName"] = t.subjectName;
                      tempObj["data"] = dateArray;
                      objectsResultArray.push(tempObj);
                      return res.status(200).json({ response: objectsResultArray });
                    }
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getAttendanceReport = function (req, res) {
  const { studentID, examsTermID,classID, fromDate, toDate } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentID,examsTermID, classID, fromDate, toDate },studentValidations.getAttendanceReport,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=` set @classID = ${utils.OptionalKey(classID)};
                          set @studentID = ${utils.OptionalKey(studentID)}; 
                          set @examsTermID = ${utils.OptionalKey(examsTermID)}; 
                          set @fromDate = ${utils.OptionalKey(fromDate)}; 
                          set @toDate = ${utils.OptionalKey(toDate)};
            
            call ${appConfig.getDbConfig().database}.v1_get_attendance_report
            (@studentID,@examsTermID,@classID,@fromDate,@toDate);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                        data.forEach(element => {
                             if(element.constructor==Array) {
                              if (_.isEmpty(element)) {
                                log.doErrorLog(fileNameForLogging, req.url, "data not present");
                                const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                                res.status(404).json({ response: errorResponse });
                              } else {
                                log.doInfoLog(
                                  fileNameForLogging,req.url,"successfully data fetched from db");
                                  res.status(200).json({ response: element });
                              }
      
                             }
                         })
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.assignSubjectToStudentArray = function (req, res) {
  const { array } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(array, studentValidations.assignSubjectToStudentArray, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){
          if(array != undefined){

          let dbquery = "";
          for (let i = 0; i < array.length; i++) {
            dbquery =
              dbquery +
              ` call ${appConfig.getDbConfig().database}.v1_set_students_subject
                    (${utils.OptionalKey(array[i].studentSubjectID)}, 
                    ${utils.OptionalKey(array[i].studentID)}, 
                    ${utils.OptionalKey(array[i].subjectID)},
                    ${utils.OptionalKey(array[i].timetableSubjectID)},
                    ${utils.OptionalKey(array[i].actionFlag)}); `;
          }
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
                if(!error){
                  if(!res.finished)res.status(200).json({ response: "success" });
    
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })

            }
           
    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};



exports.getStudentSubjects = function (req, res) {
  const { studentID, subjectID, timetableSubjectID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentID, subjectID, timetableSubjectID },studentValidations.getStudentSubjects,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery =` 
                  call ${appConfig.getDbConfig().database}.v1_get_student_subjects
                  (${utils.OptionalKey(studentID)}, 
                  ${utils.OptionalKey(subjectID)}, 
                  ${utils.OptionalKey(timetableSubjectID)}); `;

            
            
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                           
                    data.forEach(element => {
                      if(element.constructor==Array) {
                       if (_.isEmpty(element)) {
                         log.doErrorLog(fileNameForLogging, req.url, "data not present");
                         const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                         res.status(404).json({ response: errorResponse });
                       } else {
                         log.doInfoLog(
                           fileNameForLogging,req.url,"successfully data fetched from db");
                           res.status(200).json({ response: element });
                       }

                      }
                  })
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};




exports.deleteSubjectToStudentArray = function (req, res) {
  const { array } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(array, studentValidations.deleteSubjectToStudentArray, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){
          if(array != undefined){

          let dbquery = "";
          for (let i = 0; i < array.length; i++) {
            dbquery =
              dbquery +
              ` call ${appConfig.getDbConfig().database}.v1_delete_students_subject
                    (${utils.OptionalKey(array[i].studentSubjectID)});  `;
          }
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
              if(!error){
                               
                  if (_.isEmpty(data)) {
                      
                      log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                      if(!res.finished)res.status(200).json({ response: errorResponse })
                  } 
                  else {
                      log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                      if(!res.finished)res.status(200).json({response: "success"})
                  }

                }
                else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })

            }

    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

exports.setStudentTimeTableLinkArray = function (req, res) {
  const { array } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate(array, studentValidations.setStudentTimeTableLinkArray, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){
          if(array != undefined){

          let dbquery = "";
          for (let i = 0; i < array.length; i++) {
            dbquery =
              dbquery +
              ` call ${appConfig.getDbConfig().database}.v1_set_student_timetable_link
                    (${utils.OptionalKey(array[i].studentTimetableID)}, 
                    ${utils.OptionalKey(array[i].studentID)}, 
                    ${utils.OptionalKey(array[i].timetableSubjectID)});  `;
          }
          console.log(dbquery)
          try {
            db.query(dbquery, function (error, data, fields) {
                if(!error){
                  if(!res.finished)res.status(200).json({ response: "success" });
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'array not found' )
            if(!res.finished)res.status(500).json({ response: "'array not found'" })

            }
           
    
        }
        else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

exports.setStudentTimeTableDelink = function (req, res) {
  const { studentTimetableID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentTimetableID}, studentValidations.setStudentTimeTableDelink, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){

          let dbquery = "";
          
            dbquery =` call ${appConfig.getDbConfig().database}.v1_set_student_timetable_delink
                    (${utils.OptionalKey(studentTimetableID)});  `;
          
          console.log(dbquery)
          try {
            db.query(dbquery, function (error, data, fields) {
                if(!error){
                   
                  if(!res.finished)res.status(200).json({ response: "success" });

    
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};

exports.getStudentTimeTableLinked = function (req, res) {
  const { studentID, timetableSubjectID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentID, timetableSubjectID },studentValidations.getStudentTimeTableLinked,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_student_timetable_link WHERE studentID = '${studentID}' `;
            if (!_.isEmpty(timetableSubjectID)) {
              dbquery += ` AND timetableSubjectID = '${timetableSubjectID}'`;
            }
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){    
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                        if(!res.finished)res.status(200).json({ response: errorResponse });
                      } else {
                        log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                        if(!res.finished)res.status(200).json({ response: data });
                      }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
    
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getActiveSubjectsInTimetable = function (req, res) {
  const { branchID, subjectID, weekdayNo, userID,attendanceDate } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({branchID, subjectID, weekdayNo, userID, attendanceDate },studentValidations.getActiveSubjectsInTimetable, function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=` set @branchID = ${utils.OptionalKey(branchID)};
                          set @subjectID = ${utils.OptionalKey(subjectID)};
                          set @userID = ${utils.OptionalKey(userID)};
                          set @weekdayNo = ${utils.OptionalKey(weekdayNo)};
                          set @attendanceDate = ${utils.OptionalKey(attendanceDate)};
                          call ${appConfig.getDbConfig().database}.v1_get_subject_timetable_active
                          (@branchID, @subjectID, @userID,@weekdayNo, @attendanceDate);`
            
            console.log(dbquery)
            try {
              db.query(dbquery, function (error, data, fields) {
                  if(!error){
                           
                    data.forEach(element => {
                      if(element.constructor==Array) {
                          if (!_.isEmpty(element)) {
                              if(!res.finished)res.status(200).json({response: element })
                          }
                          else{
                              log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                              if(!res.finished)res.status(404).json({ response: errorResponse })
                          }
                      }
                  })
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.getStudentsInTimetable = function (req, res) {
  const { subjectID, weekdayNo,timetableSubjectID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({subjectID, weekdayNo, timetableSubjectID },studentValidations.getStudentsInTimetable,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_students_in_timetable WHERE subjectID = '${subjectID}' `;
            if (!_.isEmpty(weekdayNo)) {
              dbquery += ` AND weekdayNo = '${weekdayNo}'`;
            }
            if (!_.isEmpty(timetableSubjectID)) {
              dbquery += ` AND timetableSubjectID = '${timetableSubjectID}'`;
            }

            
            console.log(dbquery)
            try {
              db.query(dbquery, function (error, data, fields) {
                  if(!error){
                           
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                        if(!res.finished)res.status(200).json({ response: errorResponse });
                      } else {
                        log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                        if(!res.finished)res.status(200).json({ response: data });
                      }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};

exports.getStudentOwnSubjects = function (req, res) {
  const { studentID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentID },studentValidations.getStudentOwnSubjects,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_student_own_subject WHERE studentID = '${studentID}' `;
            
            
            console.log(dbquery)
            try {
              db.query(dbquery, function (error, data, fields) {
                  if(!error){
                           
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                        if(!res.finished)res.status(200).json({ response: errorResponse });
                      } else {
                        log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                        if(!res.finished)res.status(200).json({ response: data });
                      }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.getStudentDailyAttendance = function (req, res) {
  const { attendanceDateFrom, attendanceDateTo, batchNo, studentID } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  Joi.validate({ attendanceDateFrom, attendanceDateTo, batchNo, studentID }, studentValidations.getStudentDailyAttendance, function (err, value) {
    if (err) {
      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err);
      res.status(400).json({ response: errorResponse });
    } else {
      db.getConnection(function (err, db) {
        if (!err) {
          let dbquery = `SELECT * FROM v1_get_student_daily_attendance  WHERE studentID = '${studentID}' `;

          if (!_.isEmpty(attendanceDateFrom) && !_.isEmpty(attendanceDateFrom)) {
            dbquery += ` AND attendanceDate between '${attendanceDateFrom}' and '${attendanceDateTo}' `;
          }


          if (!_.isEmpty(batchNo)) {
            dbquery += ` AND batchNo = '${batchNo}'`;
          }

          dbquery += " order by  attendanceDate";
          console.log(dbquery)
          try {
            db.query(dbquery, function (error, data, fields) {
              if (!error) {

                if (_.isEmpty(data)) {
                  log.doErrorLog(fileNameForLogging, req.url, "data not present");
                  const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, "data not present");
                  res.status(200).json({ response: errorResponse });
                }
                else {
                  // console.log('here');
                  let responseData = data;
                  let mainObject = {}
                  let finalArray = new Array();
                  let detailsArray = new Array();
                  let subjectData = {}

                  let detailsObject = {};
                  for (let i = 0; i < responseData.length; i++) {

                    if (!Object.values(mainObject).includes(responseData[i].attendanceDate)) {
                      mainObject = {}
                      mainObject['attendanceDate'] = responseData[i].attendanceDate;
                      mainObject['studentID'] = responseData[i].studentID;
                      mainObject['studentName'] = responseData[i].studentName;
                      mainObject['startTime'] = responseData[i].startTime;
                      mainObject['endTime'] = responseData[i].endTime;
                      mainObject['rollNo'] = responseData[i].rollNo;
                      mainObject['notes'] = responseData[i].notes;
                      mainObject['userID'] = responseData[i].userID;

                      detailsObject['subjectName'] = responseData[i].subjectName
                      detailsObject['subjectID'] = responseData[i].subjectID
                      detailsObject['startTime'] = responseData[i].startTime
                      detailsObject['endTime'] = responseData[i].endTime
                      detailsObject['status'] = responseData[i].isPresent
                      detailsObject['batchNo'] = responseData[i].batchNo


                      detailsArray.push(detailsObject)
                      detailsObject = {}
                    } else {
                      detailsObject['subjectName'] = responseData[i].subjectName
                      detailsObject['subjectID'] = responseData[i].subjectID
                      detailsObject['startTime'] = responseData[i].startTime
                      detailsObject['endTime'] = responseData[i].endTime
                      detailsObject['status'] = responseData[i].isPresent
                      detailsObject['batchNo'] = responseData[i].batchNo
                      detailsArray.push(detailsObject)
                      detailsObject = {}
                    }

                    if (responseData[i + 1] && (responseData[i].attendanceDate != responseData[i + 1].attendanceDate)) {
                      mainObject['details'] = detailsArray
                      finalArray.push(mainObject);
                      detailsArray = new Array();
                    }

                    if (!(responseData[i + 1])) {
                      mainObject['details'] = detailsArray
                      finalArray.push(mainObject)
                      detailsArray = new Array();
                    }
                  }

                  /*let subjectObject = {};
                  let subjectArray = new Array();

                  let statusArray = new Array();
                  let batchNoArray = new Array();
                  let details;
                  for (let x = 0; x < finalArray.length; x++) {
                    details = finalArray[x].details
                    for (let iterator = 0; iterator < details.length; iterator++) {
                      console.log(x)
                      if(details.length == 1){
                        statusArray.push(details[iterator].status)
                        batchNoArray.push(details[iterator].batchNo)
                        details[iterator].status = statusArray
                        details[iterator].batchNo = batchNoArray
                       statusArray = [];
                       batchNoArray = [];
                        finalArray[x].details = details
                      }
                      else if(details[iterator+1].subjectName){
                        if(details[iterator].subjectName == details[iterator+1].subjectName){
                          statusArray.push(details[iterator].status)
                        batchNoArray.push(details[iterator].batchNo)
                        if(batchNoArray.length == 0){
                        delete details[iterator].status 
                        delete details[iterator].batchNo
                        }
                        else{
                        delete details[iterator]
                        iterator --;
                        }
                      }
                      }else if(details[iterator].subjectName == details[iterator-1].subjectName){
                        statusArray.push(details[iterator].status)
                        batchNoArray.push(details[iterator].batchNo)
                        details[iterator]['status'] = statusArray
                        details[iterator]['batchNo'] = batchNoArray
                        finalArray[x].details = details
                        statusArray = [];
                        batchNoArray = [];
                      }

                    }
                  }
*/
                  return res.status(200).json({ response: finalArray });
                }

              }
              else {
                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                res.status(500).json({ response: errorResponse })
              }
            });
          }
          finally {
            db.release();
            console.log('released')
          }


        }
        else {
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          res.status(500).json({ response: errorResponse })

        }
      })
    }
  }
  );
};






exports.setFeeVoucherStatus = function (req, res) {
  const { billingPeriodID,classID, isPublished, voucherUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ billingPeriodID,classID, isPublished , voucherUUID},studentValidations.setFeeVoucherStatus,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=
            ` set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)};
              set @classID = ${utils.OptionalKey(classID)};  
              set @isPublished = ${utils.OptionalKey(isPublished)};
              set @voucherUUID = ${utils.OptionalKey(voucherUUID)};
              
              call ${appConfig.getDbConfig().database}.v1_set_fee_voucher_publish
              (@billingPeriodID,@classID,@isPublished,@voucherUUID);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    
                      log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                      if(!res.finished)res.status(200).json({ response: "success" });

                      if (isPublished==1){
                      let dbquery = `select * from v1_get_fee_voucher_notifications where billingPeriodID='${billingPeriodID}' and classID= '${classID}'`;

                      console.log(dbquery)
                      db.query(dbquery,function (error, data, fields) {
                            if(!error){
                               
                              if (!_.isEmpty(data)) {
                                let messageText 
                                


                                
                              
                                for (let i = 0; i < data.length; i++) {
                                  if(data[i].preferredLanguage=='ar'){
                                      messageText= `تم إنشاء رسوم شهر ${data[0].periodName} آخر موعد هو ${data[0].dueDate} يُطلب منك الدفع في الوقت المحدد لتجنب أي إزعاج`
                                  }
                                  else{
                                      messageText = `Fee Voucher for ${data[0].periodName} has been generated. Last Date is ${data[0].dueDate}. You are requested to pay within due date to avoid any inconvenience` ;
                                  }
                                  var messageConfig = appConfig.messageSettings(
                                    "Fee Voucher",
                                    messageText,
                                    "FeeChallanScreen",
                                    data[i].parentID,
                                    data[i].studentID,
                                    data[i].classID,
                                    data[i].branchID,
                                    data[i].schoolID,
                                    data[i].dueDate
                                  );
                                  //console.log(messageText)  
                                  
                                  push.SendNotification(data[i].deviceToken, messageConfig);
                                }
                              }
                
                            }
                            else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                console.log(errorResponse)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                            }
                        });
                      }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.getStudentAttendance = function (req, res) {
  const { schoolID,classID, dateFrom, dateTo, studentID } = req.body;
  
  Joi.validate(
    { schoolID,classID, dateFrom, dateTo, studentID },studentValidations.getStudentAttendance,
    function (err, value) {
      if (err) {
        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
        if(!res.finished)res.status(400).json({ response: errorResponse });
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = ` set @studentID = ${utils.OptionalKey(studentID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            set @schoolID = ${utils.OptionalKey(schoolID)};
                            set @dateFrom = ${utils.OptionalKey(dateFrom)};
                            set @dateTo = ${utils.OptionalKey(dateTo)};
                            call ${appConfig.getDbConfig().database}.v1_get_student_attendance
                            (@studentID,@classID,@schoolID,@dateFrom,@dateTo);`;

            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                        data.forEach(element => {
                          if(element.constructor==Array) {
                            if (_.isEmpty(element)) {
                              log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                              if(!res.finished)res.status(200).json({ response: errorResponse })
                            } 
                            else{
                              
                                  
                              if(!error){
                                var objectsResultArray = [];
                                 if (!_.isEmpty(element[0])) {
                                  log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                    
                                  let responseData = element;
                    
                                  let result = [];
                                  let map = new Map();
                                  let count = 0;
                                  
                                  var dateArray = [];
                                  var index = 0;
                    
                                  for (let data of responseData) {
                                    if (map.has(data.studentID)) {
                                      let tempObj = {};
                                      tempObj[data.attendanceDate] = data.isPresent;
                                      tempObj["notes"] = data.notes;
                                      dateArray.push(tempObj);
                                      index++;
                                    } else {
                                      if (count != 0) {
                                        let t = responseData[index - 1];
                    
                                        let tempObj = {};
                    
                                        tempObj["attendanceID"] = t.attendanceID;
                                        tempObj["studentID"] = t.studentID;
                                        tempObj["classID"] = t.classID;
                                        tempObj["rollNo"] = t.rollNo;
                                        tempObj["studentName"] = t.studentName;
                                        tempObj["userID"] = t.userID;
                                        tempObj["teacherName"] = t.teacherName;
                    
                                        tempObj["data"] = dateArray;
                                        objectsResultArray.push(tempObj);
                                        dateArray = [];
                                      }
                                      count = 0;
                                      map.set(data.studentID, result);
                                      let tempObj = {};
                                      tempObj[data.attendanceDate] = data.isPresent;
                                      tempObj["notes"] = data.notes;
                                      dateArray.push(tempObj);
                                      count++;
                                      index++;
                                    }
                                  }
                                  let t = responseData[index - 1];
                                  let tempObj = {};
                                  tempObj["attendanceID"] = t.attendanceID;
                                  tempObj["studentID"] = t.studentID;
                                  tempObj["classID"] = t.classID;
                                  tempObj["rollNo"] = t.rollNo;
                                  tempObj["studentName"] = t.studentName;
                                  tempObj["userID"] = t.userID;
                                  tempObj["teacherName"] = t.teacherName;
                                  tempObj["data"] = dateArray;
                                  objectsResultArray.push(tempObj);
                                }
                                  dbquery = `SELECT * FROM v1_get_calendar WHERE isHoliday=1 and schoolID = '${schoolID}' AND (calendarDate BETWEEN '${dateFrom}' AND '${dateTo}')`;
                                  console.log(dbquery)
                                    db.query(dbquery,function (error, calenderData, fields) {
                                            if(!error){
                                                log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                                if(!res.finished)res.status(200).json({ attendanceDetails: objectsResultArray, calendarHolidays:calenderData });
                                            }
                                            else{
                                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                                if(!res.finished)res.status(500).json({ response: errorResponse })
                                            }
                                        });

                                }
                  
                              
                              else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                              }

                              
                            }
                          }
                            
                        })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
                });

            } 
            finally {
                db.release();
                console.log('released')
            }
            
             
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.setStudentNotesTag = function (req, res) {
  const { tagUUID, branchID, tagText, userID, isActive } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({tagUUID, branchID, tagText, userID, isActive}, studentValidations.setStudentNotesTag, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){

          let dbquery = "";
          
            dbquery =` call ${appConfig.getDbConfig().database}.v1_set_student_notes_tag
                    (${utils.OptionalKey(tagUUID)},
                    ${utils.OptionalKey(branchID)},
                    ${utils.OptionalKeyString(tagText)},
                    ${utils.OptionalKey(userID)},
                    ${utils.OptionalKey(isActive)}
                    );  `;
          
          console.log(dbquery)
          try {
            db.query(dbquery,  function (error, data, fields) {
                if(!error){
                  log.doInfoLog(fileNameForLogging,req.url, "data successfully added")
                  if(!res.finished)res.status(200).json({ response: "success" });
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
            
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  })
}

exports.setStudentNotes = function (req, res) {
  const { branchID,classID, studentNotesUUID, studentUUID, tagUUID,tagText, studentNotes, userID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({branchID,classID, studentNotesUUID, studentUUID, tagUUID,tagText, studentNotes, userID}, studentValidations.setStudentNotes, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){

          let dbquery = "";
          
            dbquery =` call ${appConfig.getDbConfig().database}.v1_set_student_notes
                    (${utils.OptionalKey(branchID)},
                    ${utils.OptionalKey(classID)},
                    ${utils.OptionalKey(studentNotesUUID)},
                    ${utils.OptionalKey(studentUUID)},
                    ${utils.OptionalKey(tagUUID)},
                    ${utils.OptionalKeyString(tagText)},
                    ${utils.OptionalKeyString(studentNotes)},
                    ${utils.OptionalKey(userID)}
                    );  `;
          
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
                if(!error){
                   
                  if(!res.finished)res.status(200).json({ response: "success" });
                  log.doInfoLog(fileNameForLogging,req.url, "data successfully added")
                }
                else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            });
          } 
          finally {
              db.release();
              console.log('released')
          }
          
    
        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  })
}

exports.getActiveStudentTags = function (req, res) {
  const { branchID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({branchID },studentValidations.getActiveStudentTags, function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_student_tag_active WHERE branchID = '${branchID}' `;
            console.log(dbquery)
            try {
              db.query(dbquery, function (error, data, fields) {
                  if(!error){
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                        if(!res.finished)res.status(404).json({ response: errorResponse });
                      } else {
                        log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                        if(!res.finished)res.status(200).json({ response: data });
                      }
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  )
}

exports.getStudentTags = function (req, res) {
  const { branchID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({branchID },studentValidations.getStudentTags, function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } 
      else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_student_tag WHERE branchID = '${branchID}' `;
            console.log(dbquery)
            try {
                db.query(dbquery, function (error, data, fields) {
                    if(!error){
                            
                        if (_.isEmpty(data)) {
                          log.doErrorLog(fileNameForLogging, req.url, "data not present");
                          const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                          if(!res.finished)res.status(404).json({ response: errorResponse });
                        } else {
                          log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                          if(!res.finished)res.status(200).json({ response: data });
                        }

                    }
                    else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        res.status(500).json({ response: errorResponse })
                    }
                });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  )
}

exports.getStudentNotes = function (req, res) {
  const { branchID, studentUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({branchID,studentUUID },studentValidations.getStudentNotes, function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `SELECT * FROM v1_get_student_notes WHERE branchID = ${utils.OptionalKey(branchID)} and studentUUID= ${utils.OptionalKey(studentUUID)} `;
            console.log(dbquery)
            try {
              db.query(dbquery, function (error, data, fields) {
                  if(!error){
                           
                      if (_.isEmpty(data)) {
                        log.doErrorLog(fileNameForLogging, req.url, "data not present");
                        const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG,"data not present");
                        if(!res.finished)res.status(404).json({ response: errorResponse });
                      } else {
                        log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                        if(!res.finished)res.status(200).json({ response: data });
                      }
               
      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  )
}


exports.deleteStudentNotes = function (req, res) {
  const { studentNotesUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentNotesUUID}, studentValidations.deleteStudentNotes, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){

          let dbquery = ` call ${appConfig.getDbConfig().database}.v1_delete_student_notes
                    (${utils.OptionalKey(studentNotesUUID)});  `;
          
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
              if(!error){
                               
                  if (_.isEmpty(data)) {
                      
                      log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                      if(!res.finished)res.status(200).json({ response: errorResponse })
                  } 
                  else {
                      log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                      if(!res.finished)res.status(200).json({response: "success"})
                  }

                }
                else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            
            });
          } 
          finally {
              db.release();
              console.log('released')
          }

        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
    
        }
      })
    }
  });
};


exports.deleteStudentNotesTag = function (req, res) {
  const { tagUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({tagUUID}, studentValidations.deleteStudentNotesTag, function (err,value  ) {
    if (err) {
      log.doErrorLog(fileNameForLogging,  req.url,  "required params validation failed."  );
      if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
    } else {
      db.getConnection(function(err, db) {
        if(!err){

          let dbquery = ` call ${appConfig.getDbConfig().database}.v1_delete_student_notes_tags
                    (${utils.OptionalKey(tagUUID)});  `;
          
          console.log(dbquery)
          try {
            db.query(dbquery,function (error, data, fields) {
              if(!error){
                               
                  if (_.isEmpty(data)) {
                      
                      log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                      const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                      if(!res.finished)res.status(200).json({ response: errorResponse })
                  } 
                  else {
                      log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                      if(!res.finished)res.status(200).json({response: "success"})
                  }

                }
                else if (error.code== 'ER_ROW_IS_REFERENCED_2'){
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: "unsuccessful", code: "ER_ROW_IS_REFERENCED_2", reason : "Reference exists" })
                }
                else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                }
            
            });
          } 
          finally {
              db.release();
              console.log('released')
          }

        }
        else{
          log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
              const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              if(!res.finished)res.status(500).json({ response: errorResponse })
  
        }
      })
    }
  });
};

exports.getParentAndInsert = function (req, res) {
  const { mobileNumber, schoolID, sourceUUID } = req.body

  log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

  Joi.validate({ mobileNumber, schoolID,sourceUUID }, studentValidations.getParentAndInsert, function (err, value) {
      if (!err) {
          db.getConnection(function(err, db) {
              
              if(!err){
                  let dbquery=` set @schoolID = ${utils.OptionalKey(schoolID)};
                                set @mobileNumber = ${utils.OptionalKey(mobileNumber)};
                                set @sourceUUID = ${utils.OptionalKey(sourceUUID)};
                                call ${appConfig.getDbConfig().database}.v1_get_parents_and_insert
                                (@schoolID,@mobileNumber,@sourceUUID);`
                  console.log(dbquery)            
                  
                  try {
                          db.query(dbquery,function (error, data, fields) {
                              if(!error){
                              
                                  log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
                                      data.forEach(element => {
                                          if(element.constructor==Array) {
                                              if (!_.isEmpty(element)) {
                                                  
                                                  if(!res.finished)res.status(200).json({response: element })
                                                  
                                                  
                                              }
                                              else{
                                                  log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                                                  const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                                                  if(!res.finished)res.status(404).json({ response: errorResponse })

                                              }
                                          }
                                      })
                              }
                              else{
                                  log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                  const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                  console.log(errorResponse)
                                  if(!res.finished)res.status(500).json({ response: errorResponse })
                              }
                          });
                  } 
                  finally {
                      db.release();
                      console.log('released')
                  }  
      
              }
              else{
                  log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                  const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                  if(!res.finished)res.status(500).json({ response: errorResponse })
      
              }
          })
          
      } else {
          log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
          if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
          
      }
  })
}



exports.setStudentSelfJoin = function (req, res) {
  const { studentUUID, parentUUID , studentName, gender,rollNo, dob, classID,classLevelID,branchID,schoolID} = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({studentUUID, parentUUID , studentName, gender,rollNo, dob, classID,classLevelID,branchID,schoolID}, studentValidations.setStudentSelfJoin,
    function (err, value) {
      if (err) {
        log.doErrorLog( fileNameForLogging,  req.url,  "required params validation failed."  );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
                    
                    let dbquery=` set @studentUUID = ${utils.OptionalKey(studentUUID)};
                                  set @parentUUID = ${utils.OptionalKey(parentUUID)};
                                  set @studentName = ${utils.OptionalKeyString(studentName)};
                                  set @picture= 'iVBORw0KGgoAAAANSUhEUgAAAF4AAABeCAYAAACq0qNuAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAABARpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPHhtcE1NOkRlcml2ZWRGcm9tIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgPHN0UmVmOmluc3RhbmNlSUQ+eG1wLmlpZDo4QjQ0MUY4NTBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwvc3RSZWY6aW5zdGFuY2VJRD4KICAgICAgICAgICAgPHN0UmVmOmRvY3VtZW50SUQ+eG1wLmRpZDo4QjQ0MUY4NjBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwvc3RSZWY6ZG9jdW1lbnRJRD4KICAgICAgICAgPC94bXBNTTpEZXJpdmVkRnJvbT4KICAgICAgICAgPHhtcE1NOkRvY3VtZW50SUQ+eG1wLmRpZDo4QjQ0MUY4ODBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOkluc3RhbmNlSUQ+eG1wLmlpZDo4QjQ0MUY4NzBGREYxMUU2QUI2RkQ3NTE5QjA3REJCRDwveG1wTU06SW5zdGFuY2VJRD4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBJbWFnZVJlYWR5PC94bXA6Q3JlYXRvclRvb2w+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgqSiLPRAAAPe0lEQVR4Ae1de4xcVRm/r5nZebYVKitFq8hDKhTFinGRtBtTLGt3ZnaRKqQlgLFFAxJQEwNRHolETPgDY6xFQgVETQvdx3QXatC2GBqCpUjRWkEaRURKNWVnZl8z9+Hvd3fO7uzs7M7M7r3T2fae9O7ce+55fOd3vvOd7/vOObeS5AUPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ8BDwEPAQ+BkwABeb604e6771aWLVsmL168eFqajx07Zh06dMhCWnO+tKsh6dy2bZu6e/duzbKsacEuRzjTMx/zl3vfCHFaIxBRSgNB27t3r7lu3TpDvHvqqac+oCjK+Zosn2Mp0pmIXyhZst0h+POepFj/tvLmEUtVD8uy/Dbe68zLTtizZ4/a2tpqPzOuEUJNnOQ2wRARGi6CbbGunp6eTyqS1IGH1Yj6eCAQiGqaJqED+JqgSgJ60zQlXdel0dHRQbw5hDzPSpKyI5FI7LcT4w87tFE6oCGAL+XK3t6udbIl3ywryuXhcEjK53Upl8tJhmGY4OZp5TfKUVRVVfx+v+Tz+aShoSHmeUGR5AfXJhK/YQdQ/DTCPHDCgScQQqSkuro+DzHyo3A4cgk5eHh4mMJdB/eSxXlVQy9HCzuHv2pTU5OMzpAGBwcPSrLy3Xg8/jTiTzj3V9MQ0ulK2LJli2/Tpk35/v7+gJEf/WmgKXgjKxoZGaE8Jm1OTI626IKY0iiihoeHfikr2qb29vYhUT/rrHc4YcCLRu/YseMCTVV6o9HoOZlMRkymTgBeiqVdNupRM9nMPzHnxiH/Dwo6ShO7/XxCgBeN7erqWqUq8i5wox+TYg6N9bvdYJSfK9RnYmr+IsB/RtBTh7rHq6g78KKRO3t6WiHAf0/5C22EoqWeqq0OsaOx7rxutEPu7xR0jSPj8k1dgRfqXF9X1yd0WXoJqqECraPeoAtIDYCvUiW1JP2z8fhVLwj6RAI3f8cUYjdrKJQN/VyhDg0tZoGhyH0Y7icSdFKlQnPSaRdIlroTc837SR9V0jrAYato9ahHop+FFTUFfI9GIpEzIdPzeKyneCnXTg32QT4cDp+mKfLjTHDPPfeUS+d4XF1EjRjCqVT3teFQ+InB7GAeSrbP8dbMskBIm3wsGvFlM9mvtSeTDwt6Z1lcVdlcB55WKYIFERMMBvyvaT7fWeAyWqB1GdJVoQCDixYvpptjwVDk3NWrVw8IuqvMX3My1xtPBxWpCgYCt0SiUYCezzcY6CRPgWKVj0Zji4eGsrcxQtDNezeCqxwvuIaWqa7n/ubTfEvz+TzNedc7fBZgmdSydMN4Jzoyem7runVZQf8syqqYxVUABNcA7HgoGCLoVB1drbNii6dPQK7Xw6FQc9bv72QyQf/0WWb/xlUQuCJE0mTL+jLEy+yprFNOGe4hcDnd/F9hlYJ+N6p3DQ0xTOEWWAi3wOuYvE6HscSOcK1OBwCyYFTJ0O/TumGe29nZ+a5ohwNlTyrCNY7fvn27XTYAvxj+8dMxiinbGxl0AkPQDbiSY5D3n2KEaAfvnQyuAT+xKG1ewoUJiJppFzCcbJADZVlcRLEsYwXLmmiHAyUXFeEa8ON1mNKy8ft5csNFGKxanW+Tu3evK1S7Bvz4xCRLZ7Eh8yhQ3HAJ64OkedVdd7lCvGvAX3311TbBEOqLCsA3unwf5w3SC/1mASPcEpGuAS9aAeXM9TpEXU78UouhSgnIA1wPLpTpONPUAZQ6VOEE4lPLMLkbYWq0MzGuoQL3aoFLzNH5YDwJOEErdXk8WoNYQzA5Avgg3jv16xrwwv8Oko+ONcR54p0CoaScAvDKfxk/b/V4LCgfUeaBu6AYfDIK5qYjjJu/erxsHcR4LW7XvLiHxDnoJqGuiZpVq1bZ6iQW8/djFxeRx35TeT70gMqtf1h6/SOBH7dHHO4F14AX+i92bB0GzYfoNoB+3NDAY2Cafp+PBtTfsSZsczy2F84vA4oMwrVLm1FkKUXgIevFTjE7utH+YCoy/YEAXXn93M9ZoN8VZnGN4wmqEDcwBH8FcUMz8ETvKqjU1wr2bUqqNbbjwC0xQyJcBZ7ihtZfMpl81TKt/mAwSJ2Yq1ANF0CrDvoUQ9d3Y1/ffujvitjF7AaxrgJfTDBWde6DT55RjpvfxfXM9l64CmRVup9lQH93lU7XgSfXkOuxOfT5fC73ZCQcVtEibmZqmEB6sKlJHR0Z6Vu7NrmL9LrJ7Wy468CzEuHzUH3+2zLZ7DBOenAzkyvaAuurMfA8jw9zkK76rFuZF55VVybUYrrqAjx9HtQQ2tra3lJU+aZQKEQaGgJ4cLuOLYUUgDe3tXW8wV3DQhUuBsrp+7oAT6ILG1bV9vbkY9jCtzkWi1HD4Z74Exly0VjMl0lnfhGPJ7dQxPCESj0IcnUCKW0AJzAEexj39nb3xqKx9nQ6Xa8DCaXk5HA6xJ9OZ55NJJM4VQgXZBF9pYmdfq4bx5Nwgi6MKnBYHJzWB873FyZb1+VqATzga+VZbzYzATrpEkzhNMjlyqsb8BzG5CiKnP3799s7hdsTibUD6YFHsKeSzxx9buv4LF9esCDmS6cHft2eGON00OYv7I2XSWc5oJyOcx14gs0Ji+oZOaqvr2/pihUr8gL8RKLjq+lM+ha8k2DAaAXud9S1ABpMlsvy6fLNprPfjic6riWYBB205XByfCnpI53kftLtNNjF5blaOLlH6MNjp/vkB7A7e41pGRsSic4n2EDIWZkd0d3dfZEiS5uhT1+GPZY8UCy4nxw4Gzo5oRgAUIKfSKOvCKLlRUtWboJN8XJx3b1dXVfBkbQd1TwLF/a3aGkTpGL6i0Fz4n42Daqq3uLDXKlUz71wGXwPHCeNjoxK/oBfGhkeuTOeTN7Hwvbt2xdsaWkZ5j2O0a/HCv+dUDk/xmf6TuAtFJ1Aeu2LI0QEgovAP+LifKKxPqaDjn4YjscforMfZUIAGgRD2PWlenpu9/l9D9CqxvEgHsmXTMu8/8CBP91BNbi4HczrVJig3qESOUS5XEZOB4jLAeLj4OrlOMPKGgigLd6gOyswprpCofANPAjAIX/8+HFLqHPI24l1oBuBZCtGQYgAEhxe3H5BsHkxnhdFCM8z8WI8fOoj8ELvlRXrkQMHXnlS2BJwfJG23K5du8KwVH8eiYSvyWazosNoW3AUStls5q+6YW3o6Oh4iZzP7Sqoh+kcCY4CjwbD8zu2VQ/fI9iIbbdbsA+RR+N5GIF6u6iPDTAAqJbNDr6L/RS3xDs6trFF3EsPIHQholKpbUtw/HUlsL4cPbYcUC8FYy9C0gDqQ7E2GKN4fg+F/gvPryiW9AcsnD6HtYA3WSaBQ0fTgGM6CUf3k5aq/CQSDi1B/WSGInHG3R2mDrp9FHkYKbfC9vgx87EcQRef5xIEEHMpw85bTFRvd/fPItHIJrqC0Qo2rKw7GJXnVU3zUf4ODQ79TtGk78NXso8FgkOVSy+91CfAsivBn1QqFQLHLwSHx2QcLsFGR3ZiBsPlvS9cdx18zxOhtBP7enpWGJJ5bzAYutIePUAWmW0NayJX4Q7eShBvc79tYCWTN/BNcTun5KkhwhHgAZKGS+eElckM9MEwugKGUQknTUsVh7cFeawSjHwu/1vw3GY1EHi6GHTKWpawceNGvcDlkwok9z/00EN2BwtxxQTMt6S5+QpTtr6hKmob5TjEECddjpZKWp39EYtoLOobSGeew+HzNeD4YbaT6uckAmp8mDPwggPGZObQ7kgk+mmIitqt0TEOUzCpKgCF4ulNcGM/xEY/hOtL0ET48Z+qAuaHM5AQu33NNcC2LdjUdDZAJuAcHVRVy47AGQrPQVTR4Pqzlh28rG39+vRcwZ8T8AL0rVu3Nr1v0aIXMVFdBPFSO+iTW2zr8NgqDaYP8Hsz7IRBEPo6ePQ1DI4juI7KpjxgoYvwy38xsO4ZmHI/gnTc5XsORlCUR+appUCioEzOBVatgBdTlgNT+NF5f0lnsi3rAb5of3Giau9nDTxlMC7bw5jq6X4+FA63OAB6Md0smxe8yIrKPevUWKi9cETwEoHczIvaji2uMCninh3IRBQnlUSKKKrSbw4KgR/tfPk/7xz9DEUa6BhXKCplLn4/W+DtjZ1orAXQd8Dk74C6OFdOL6ar9J4AshMm0C5NMfHMNhHo2bZtoqTyd2NiJ5t9Jp5IXskkAB9Q1KZqzooTIN9UVgTt5Qdwq3ZgIqUr1c1PnhBEqnwUFZWuItUQqZ0Pfs5hcLKtQfsfZPF7Cmd5a6mqZuCpJXBGT6W6ksFQ8A6Azq9gzEV21kJvo6SFOzmthyPhb8JeuYZ4CK2rWgJrGo5CnvFThD5NO4xT6DHIVMxtFdWyaumZT+lMMJyCuWTEMK0LYOH+o5bJtiaOFyvvUDgehhUfMw2jEY/H16vzCLoOTadJU+WtrFSsLVdDQNUcL/RWDK0N4VDkMczs01qk1VR8sqQBgPlwBF/+GMzexOVDgVOl9lUFvJi1UWgkm0nzsHAzRQwKr2nEVCJmnr4vfPnDPI7vIPBQ8v+KVe3p2lQVcGLWhjvgTlhwzTBIqMVUlXe6ik+ieHwDwcCXPyKLsMpyF9u1cuXKithUTMDe46wNh9NZ0KJvhSpFY+VU02Jm5BPYbvCyZmk5fB2Kx9nEi7jNlGnGl8W9p+dyt6NXg7AWyO1ViaiZKj7J3vHrE3m6njVN/U4xbtO1c0YAhWyHK/Z0yzReh/a0EDM5rccZ801X2UkeP2a+Wtaw5jPOa2u76q2ZZP2MHC9kO871b4hGIgst0/S4fXrusbkeq1dBI69dz2QzyfoZZTVllV2PJV2PL1rRUTJjR9lpT+E/WChX6Q2F92YDpMV9mAuJH6UDpcSkMC2QtMKYEqv/LfAMLkeBVB/tuEkleA/jCNCCx+4IA6dKztu5s7uVL4BjWYzLRjKDOGaoqnIn/eIIBN4LlRGwP7simcqXmFTgWJqt4iSZ6u1+1e/zXzgCjj9FfTKlmFV6NiEhFD2ff+Ptd45eUFiGnCJuynI8nWEsvb+7exnWGy4cxf9WAI2mYidVougUea9wDR3/dcNHm5ubL2aby4mbssBDm7HjDVVqwQkO5uU3dz3giUR1wRjbTGV+jsnLiZuywIuyLUO6zNPYBRq1/YJRscortzAXNlFN0WrKqpNQI+0FZ+wnXD72qUgP/lpgp6imOwtoX0RJgUA8J8n5KRxfECkWNpmeBovgw1w8RpiSrhZCTrW0RLqA24eA4xK2H1bsJFH9f/UnH28cQVdOAAAAAElFTkSuQmCC';
                                  set @gender = ${utils.OptionalKey(gender)};
                                  set @rollNo = ${utils.OptionalKey(rollNo)};
                                  set @dob = ${utils.OptionalKey(dob)};
                                  set @classID = ${utils.OptionalKey(classID)};
                                  set @classLevelID = ${utils.OptionalKey(classLevelID)};
                                  set @branchID = ${utils.OptionalKey(branchID)};
                                  set @schoolID = ${utils.OptionalKey(schoolID)};
                                  call ${appConfig.getDbConfig().database}.v1_set_student_self_join
                                  (@studentUUID, @parentUUID,@studentName, @picture, @gender,@rollNo,@dob,@classID,@classLevelID,@branchID,@schoolID);`
                    console.log(dbquery)
                    try {
                      db.query(dbquery, function (error, data, fields) {
                      if(!error){
                        
                        if(!res.finished)res.status(200).json({ response: "success" });
          
                      }
                      else if (error.code== 'ER_DUP_ENTRY'){
                        log.doFatalLog(fileNameForLogging, req.url, 'duplication error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(400).json({ response: "unsuccessful", code: error.code, reason : "Record already exist, please edit and update" })
                      }
                      else{
                        log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                        if(!res.finished)res.status(500).json({ response: errorResponse })
                      }
                    });
                    } 
                    finally {
                        db.release();
                        console.log('released')
                    }
  
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
            const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.deleteStudentSelfJoin = function (req, res) {
  const { parentUUID, studentUUID } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({ parentUUID, studentUUID }, studentValidations.deleteStudentSelfJoin,function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed." );
        if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery = `
            set @studentUUID = ${utils.OptionalKey(studentUUID)};
            set @parentUUID = ${utils.OptionalKey(parentUUID)};
            call ${appConfig.getDbConfig().database}.v1_delete_student_self_join
            (@studentUUID,@parentUUID);`;
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                    if(!res.finished)res.status(200).json({ response: "success" });
      
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};
exports.getAttendanceMobile = function (req, res) {
  const { classID, dateFrom, dateTo, studentID } = req.body;
  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);
  Joi.validate({ classID, dateFrom, dateTo, studentID },studentValidations.getAttendanceMobile,function (err, value) {
      if (err) {
        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG,err.message,err);
        if(!res.finished)res.status(400).json({ response: errorResponse });
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            
            let dbquery = ` set @studentID = ${utils.OptionalKey(studentID)};
                            set @classID = ${utils.OptionalKey(classID)};
                            set @dateFrom = ${utils.OptionalKey(dateFrom)};
                            set @dateTo = ${utils.OptionalKey(dateTo)};
                            call ${appConfig.getDbConfig().database}.v1_get_attendance_mobile
                            (@studentID,@classID,@dateFrom,@dateTo);`;

            console.log(dbquery)
            try {
                db.query(dbquery,function (error, data, fields) {
                  if(!error){
                        data.forEach(element => {
                          if(element.constructor==Array) {
                            if (_.isEmpty(element)) {
                              log.doErrorLog(fileNameForLogging, req.url, 'data not present')
                              const errorResponse = new Error(appConfig.EMPTY_DATA_ERROR_MSG, 'data not present')
                              if(!res.finished)res.status(200).json({ response: errorResponse })
                            } 
                            else{
                              
                                  
                              if(!error){
                                var objectsResultArray = [];
                                 if (!_.isEmpty(element[0])) {
                                  log.doInfoLog(fileNameForLogging,req.url,"successfully data fetched from db");
                    
                                  let responseData = element;
                    
                                  let result = [];
                                  let map = new Map();
                                  let count = 0;
                                  
                                  var dateArray = [];
                                  var index = 0;
                    
                                  for (let data of responseData) {
                                    if (map.has(data.studentID)) {
                                      let tempObj = {};
                                      tempObj[data.attendanceDate] = data.isPresent;
                                      tempObj["notes"] = data.notes;
                                      dateArray.push(tempObj);
                                      index++;
                                    } else {
                                      if (count != 0) {
                                        let t = responseData[index - 1];
                    
                                        let tempObj = {};
                    
                                        tempObj["attendanceID"] = t.attendanceID;
                                        tempObj["studentID"] = t.studentID;
                                        tempObj["classID"] = t.classID;
                                        tempObj["rollNo"] = t.rollNo;
                                        tempObj["studentName"] = t.studentName;
                                        tempObj["userID"] = t.userID;
                                        tempObj["teacherName"] = t.teacherName;
                    
                                        tempObj["data"] = dateArray;
                                        objectsResultArray.push(tempObj);
                                        dateArray = [];
                                      }
                                      count = 0;
                                      map.set(data.studentID, result);
                                      let tempObj = {};
                                      tempObj[data.attendanceDate] = data.isPresent;
                                      tempObj["notes"] = data.notes;
                                      dateArray.push(tempObj);
                                      count++;
                                      index++;
                                    }
                                  }
                                  let t = responseData[index - 1];
                                  let tempObj = {};
                                  tempObj["attendanceID"] = t.attendanceID;
                                  tempObj["studentID"] = t.studentID;
                                  tempObj["classID"] = t.classID;
                                  tempObj["rollNo"] = t.rollNo;
                                  tempObj["studentName"] = t.studentName;
                                  tempObj["userID"] = t.userID;
                                  tempObj["teacherName"] = t.teacherName;
                                  tempObj["data"] = dateArray;
                                  objectsResultArray.push(tempObj);
                                }
                                if(!res.finished)res.status(200).json({ response: objectsResultArray });

                                }
                  
                              
                              else{
                                log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                                if(!res.finished)res.status(500).json({ response: errorResponse })
                              }

                              
                            }
                          }
                            
                        })
                  }
                  else{
                    log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                    if(!res.finished)res.status(500).json({ response: errorResponse })
                  }
                });

            } 
            finally {
                db.release();
                console.log('released')
            }
            
              
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                if(!res.finished)res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


exports.getFeeVoucherExport = function (req, res) {
  const { branchID, classID, studentID, billingPeriodID, paymentStatus } = req.body;

  log.doInfoLog(fileNameForLogging, req.url, "req body", req.body);

  Joi.validate({branchID, classID, studentID, billingPeriodID, paymentStatus },studentValidations.getFeeVoucherExport,
    function (err, value) {
      if (err) {
        log.doErrorLog(fileNameForLogging,req.url,"required params validation failed.");
        res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err));
      } else {
        db.getConnection(function(err, db) {
          if(!err){
            let dbquery=` set @branchID = ${utils.OptionalKey(branchID)};
                          set @classID = ${utils.OptionalKey(classID)}; 
                          set @studentID = ${utils.OptionalKey(studentID)}; 
                          set @billingPeriodID = ${utils.OptionalKey(billingPeriodID)}; 
                          set @paymentStatus = ${utils.OptionalKey(paymentStatus)}; 
                          
            
                        call ${appConfig.getDbConfig().database}.v1_get_fee_voucher_export
                        (@branchID,@classID,@studentID,@billingPeriodID,@paymentStatus);`
            console.log(dbquery)
            try {
              db.query(dbquery,function (error, data, fields) {
                  if(!error){
                     
                        
                    var wb = new xl.Workbook();
                    var ws = wb.addWorksheet('Fee Voucher Report');
                    var fileName = 'Fee Voucher Report.xlsx'
                    var columnHeader = wb.createStyle({
                      font: {
                        color: '#000000',
                        size: 12,
                        bold: true
                      }
                    });
                    
                    var headingTop = wb.createStyle({
                      font: {
                        bold: true,
                        underline: true,
                        size: 16,
                      },
                      alignment: {
                        wrapText: true,
                        horizontal: 'center',
                      },
                    });
                    ws.cell(1, 2, 1, 9, true).string('Fee Payment Report').style(headingTop);
                    // Set value of cell A1 to 100 as a number type styled with paramaters of style
                  
                    ws.cell(2, 2).string('S No.').style(columnHeader);
                    ws.cell(2, 3).string('Roll No').style(columnHeader);
                    ws.cell(2, 4).string('Student Name').style(columnHeader);
                    ws.cell(2, 5).string('Class').style(columnHeader);
                    ws.cell(2, 6).string('Invoice Date').style(columnHeader);
                    ws.cell(2, 7).string('Invoice Number').style(columnHeader);
                    ws.cell(2, 8).string('Billing Period').style(columnHeader);
                    ws.cell(2, 9).string('Due Date').style(columnHeader);
                    ws.cell(2, 10).string('Total Amount').style(columnHeader); //feeAmountAfterTax
                    ws.cell(2, 11).string('Total Amount (After Due Date)').style(columnHeader); //feeAmountAfterDueDate
                    ws.cell(2, 12).string('Payment Source').style(columnHeader);
                    ws.cell(2, 13).string('Payment Date').style(columnHeader);
                    ws.cell(2, 14).string('Paid Amount').style(columnHeader);
                    ws.cell(2, 15).string('Status').style(columnHeader);

                          data.forEach(element => {
                            if(element.constructor==Array) {
                              
                              if (!_.isEmpty(element)) {
                                                            
                              for(let i=0;i < element.length; i++){
                                ws.cell(3+i, 2).number(i+1);
                                ws.cell(3+i, 3).string(utils.Convert2String(""+element[i].rollNo));
                                ws.cell(3+i, 4).string(element[i].studentName);
                                ws.cell(3+i, 5).string(element[i].classAlias + "-" + element[i].classSection);
                                ws.cell(3+i, 6).string(utils.formatReadableDate(element[i].invoiceDate));
                                ws.cell(3+i, 7).string(""+element[i].invoiceNumber);
                                ws.cell(3+i, 8).string(element[i].periodName);
                                ws.cell(3+i, 9).string(utils.formatReadableDate(element[i].dueDate));
                                ws.cell(3+i, 10).string(element[i].currencyCode + " " + utils.Convert2Number(element[i].feeAmountAfterTax));
                                ws.cell(3+i, 11).string(element[i].currencyCode + " " + utils.Convert2Number(element[i].feeAmountAfterDueDate));
                                ws.cell(3+i, 12).string(utils.Convert2String(element[i].channelType));
                                ws.cell(3+i, 13).string(utils.formatReadableDate(utils.Convert2String(element[i].paymentDate)));
                                ws.cell(3+i, 14).string(element[i].currencyCode + " " + utils.Convert2Number(element[i].paidAmount));
                                ws.cell(3+i, 15).string(utils.Convert2String(element[i].paymentStatus));
                                
                              }
                                

                              }
                              
                            }
                          
                            
                          })
                          wb.write(fileName , res);

      
                  }
                  else{
                      log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
                      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
                      res.status(500).json({ response: errorResponse })
                  }
              });
            } 
            finally {
                db.release();
                console.log('released')
            }
            
      
          }
          else{
            log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
                const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
      
          }
        })
      }
    }
  );
};


// exports.setParentLanguage = function (req, res) {
//   const { sourceUUID, preferredLanguage } = req.body

//   log.doInfoLog(fileNameForLogging, req.url, 'req body', req.body)

//   Joi.validate({ sourceUUID, preferredLanguage }, studentValidations.setParentLanguage, function (err, value) {
//       if (!err) {
//           db.getConnection(function(err, db) {
              
//               if(!err){
//                   let dbquery=` set @sourceUUID = ${utils.OptionalKey(sourceUUID)};
//                                 set @preferredLanguage = ${utils.OptionalKey(preferredLanguage)};
                                
//                                 call ${appConfig.getDbConfig().database}.v1_set_preferred_language
//                                 (@sourceUUID,@preferredLanguage);`
//                   console.log(dbquery)            
                  
//                   try {
//                           db.query(dbquery,function (error, data, fields) {
//                               if(!error){
                              
//                                   log.doInfoLog(fileNameForLogging, req.url, 'successfully data fetched from db')
//                                   if(!res.finished)res.status(200).json({ response: "success" });
//                               }
//                               else{
//                                   log.doFatalLog(fileNameForLogging, req.url, 'error in db execution', error)
//                                   const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, error.message, error)
//                                   console.log(errorResponse)
//                                   if(!res.finished)res.status(500).json({ response: errorResponse })
//                               }
//                           });
//                   } 
//                   finally {
//                       db.release();
//                       console.log('released')
//                   }  
      
//               }
//               else{
//                   log.doFatalLog(fileNameForLogging, req.url, 'error in getting connection pool', err)
//                   const errorResponse  = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
//                   if(!res.finished)res.status(500).json({ response: errorResponse })
      
//               }
//           })
          
//       } else {
//           log.doErrorLog(fileNameForLogging, req.url, 'required params validation failed.')
//           if(!res.finished)res.status(400).json(new Error(appConfig.INVALID_PARAMETERS, err.message, err))
          
//       }
//   })
// }