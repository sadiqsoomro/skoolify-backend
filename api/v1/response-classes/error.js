export default class Error {
    constructor(userMessage, internalMessage, stackTrace) {
        this.userMessage = userMessage
        this.internalMessage = internalMessage
        this.stackTrace = stackTrace        
    }
}
