const config = require("../../app-config"),
log = require("../utils/utils-logging"),
db = require('../../db'),
utils =  require('../utils/helper-methods'),
appConfig = require("../../app-config"),
  _ = require("lodash/core");
//const FCM = require("fcm-node");
//const serverKey = config.FCM_KEY;
//const fcm = new FCM(serverKey);
import PushNotifications from 'node-pushnotifications';

const push = new PushNotifications(config.pushNotificationsettings);



export function SendNotification(regIDs,messageSettings)
{
  push.send(regIDs, messageSettings)
  .then((results) => { 
    //console.log(results)

    let dbquery=''
        //GCM Failures
        if(results[0] && results[0].failure > 0){
          var failedMessagesGCM = results[0].message.filter(function(responseFullMessages) {
            return responseFullMessages.errorMsg != null;
          });
          
          for (let badMessage=0;badMessage<failedMessagesGCM.length;badMessage++){
            //console.log(failedMessagesGCM)
             dbquery +=`set @token = ${utils.OptionalKey(failedMessagesGCM[badMessage].regId)};
            call ${appConfig.getDbConfig().database}.v1_update_bad_tokens (@token,'Y');`

         }
         //console.log(dbquery)
          
        }
        // APN Failures
        if(results[1] && results[1].failure > 0){
          var failedMessagesAPN = results[1].message.filter(function(responseFullMessages) {
            return responseFullMessages.error != null;
          });
          for (let badMessage=0;badMessage<failedMessagesAPN.length;badMessage++){
             dbquery +=`set @token = ${utils.OptionalKey(failedMessagesAPN[badMessage].regId)};
            call ${appConfig.getDbConfig().database}.v1_update_bad_tokens (@token,'Y');`
            
  
          }
          //console.log(dbquery)
        }
        
        // db.getConnection(function(err, db) {
        //   if(!err){
        //     if (dbquery){
        //       try {
        //         console.log(dbquery)
                
        //         db.query(dbquery,function (error, data, fields) {
        //             if(!error){
        //               //console.log('all good, unregistered parents updated')
        //               // do nothing
        //             }
        //             else{
  
        //                 console.log( 'error in db execution')
        //                 console.error(error)
                        
        //             }
        //         });
        //       } 
        //       finally {
        //           db.release();
        //           console.log('released')
        //       }
        //     }
            
        //   }
        //   else{
        //     console.log('error getting connection pool')
        //   }
        // })

  })
  .catch((err) => { 
    console.log("error");
    console.log(err);
  });
}