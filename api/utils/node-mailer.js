const config = require("../../app-config"),
log = require("../utils/utils-logging"),
nMailer = require('nodemailer'),
  _ = require("lodash/core");


  let transport = nMailer.createTransport({
    host: process.env.EMAIL_SMTP_HOST,
    port: process.env.EMAIL_SMTP_PORT,
    auth: {
       user: process.env.EMAIL_SMTP_USER,
       pass: process.env.EMAIL_SMTP_PASS
    }
});


export function SendEmail( emailTo,emailSubject,emailText){

    const message = {
        from: process.env.EMAIL_SMTP_USER, // Sender address
        to: emailTo,         // List of recipients
        subject: emailSubject, // Subject line
        text: emailText // Plain text body
    };
    transport.sendMail(message, function(err, info) {
        if (err) {
          console.log(err)
        } else {
          console.log('Email Sent');
        }
    });

}



