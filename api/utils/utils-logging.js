function doInfoLog(fileName, reqPathUrl, descriptionForLogData, dataToLog) {
    //const log = require('../../log-config').getLogger(fileName)
    console.log(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
    //log.info(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
}

function doErrorLog( fileName, reqPathUrl, descriptionForLogData, dataToLog) {
    //const log = require('../../log-config').getLogger(fileName)
    console.warn(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
    //log.error(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
}

function doFatalLog( fileName, reqPathUrl, descriptionForLogData, dataToLog) {
    //const log = require('../../log-config').getLogger(fileName)
    console.error(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
    //log.fatal(`${reqPathUrl} ${descriptionForLogData} : ${JSON.stringify(dataToLog)}`)
}

module.exports = {
    doInfoLog,
    doErrorLog,
    doFatalLog
}