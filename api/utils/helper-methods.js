const { padStart } = require('lodash');

const crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = require('../../app-config').ENCRYPTION_KEY;
const cryptoJS = require('crypto-js');

let EncryptAES = function (text) {
    //console.log(password);
    const cipher = crypto.createCipher(algorithm,password)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}


let DecryptAES = function (text) {
    const decipher = crypto.createDecipher(algorithm, password)
    let dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}


let DecryptBase64 = function (data) {
    if(data==null){
        return null;
    }
    else{
        let buff =  Buffer.from(data, 'base64');  
        return buff.toString('ascii');
    }
}


let EncryptBase64 = function (data) {
    if(data==null){
        return null;
    }
    else{
        let buff = Buffer.from(data);  
        return buff.toString('base64');
    }
}

let HMAC256Encrypt = function (data, key){
    return cryptoJS.HmacSHA256(data,key).toString().toUpperCase();
}


let createStringAmpersand = function (value){
    // if (value){
    //     value = value + '&'
    // }
    // else{
    //     value =''
    // }
    return value ? value + '&': ''
}

let ConvertDecimalToPaddingZeros = function (transactionAmount) {
    if(transactionAmount){
        //console.log(transactionAmount)
        if (transactionAmount.includes("."))
            return transactionAmount.replace(".","")
        else    
            return transactionAmount + "00"
    }
    else{
        return "00"
    }
}

let TxnHashVerification = function(text)
{
     
    let decodedString = DecryptBase64(text);
    let splittedString = decodedString.split(":");
    if(splittedString.length != 3)
        return "error parsing";
    else if (!CompareDates(splittedString[2]))
    {

        return "timeout";
    }
    else
        return splittedString;
}

let CompareDates = function (date)
{
    if((date- Math.floor(Date.now() / 1000)) > 0)
    return true;
    else
    return false;
}

function OptionalKey(optionalField) {
    try{

    if (optionalField)
        return `'${optionalField.trim()}'`;
    else
        return null;
    }
    catch(error){
        return null;
    }
}

function OptionalKeyString(optionalField) {
    try{
        if (optionalField)
            return `'${optionalField.replace(/'/g, "''").replace(/;/g, "\;").replace(/--/g, "\--").trim()}'`;
        else
            return null;
    }
    catch(error){
        return null;
    }
}

function Convert2String(val) {
    if ( val === null ) 
    { 
       return ""; // change null to empty string
    } else {
       return val; // return unchanged
    }
   }

   function Convert2Number(val) {
    if ( val === null ) 
    { 
       return "0"; // change null to 0
    } else {
       return val.toLocaleString(); // return unchanged
    }
   }

   function maskInstrumentString(strValue) {
    let strLength = strValue.length
    if (strLength > 5){
    let last4Digits = strValue.slice(-4);
        return  last4Digits.padStart(strValue.length, 'x');
    }
    else{
        return strValue;
    }
   }

function RandomNumGenerator()
{
    var minimum = 1000;
    var maximum = 999999;
    return (Math.floor(Math.random() * (maximum - minimum + 1)) + minimum).toString();
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function getFullDateString(increment){
    
    var d = new Date(),
   
         YYYY= '' + d.getFullYear(),
         MM = d.getMonth() + 1 + '',
         DD = d.getDate() + increment + '',
         HH = d.getHours() + '',
         MI = d.getMinutes() +  '',
         SS = d.getSeconds() + '';
         
        if (MM.length < 2) MM = '0' + MM;
        if (DD.length < 2) DD = '0' + DD;
        if (HH.length < 2) HH = '0' + HH;
        if (MI.length < 2) MI = '0' + MI;
        if (SS.length < 2) SS = '0' + SS;

        // console.log(YYYY + 
        //     MM + 
        //     DD +
        //     HH +
        //     MI +
        //     SS)
        return    YYYY + 
                    MM + 
                    DD +
                    HH +
                    MI +
                    SS;
        
}


function formatReadableDate(date,language,dateFlag) {
    //console.log(date)
    if (date){
        
        var d = new Date(date),
            month = '' + (d.getMonth()),
            day = '' + d.getDate(),
            year = d.getFullYear();
        var options = {
            year: 'numeric', month: 'long', day: 'numeric'
            };
        var monthArray=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
        //if (day.length < 2) day = '0' + day;
        
        if(dateFlag=='\'0\''){
            return [monthArray[month], year].join('-');
        }
        else if (language=='\'ar\'')
            return d.toLocaleString('ar-EG', options);
        
        else if (language=='\'en\'')
            return  d.toLocaleString('en-CA', options); //[day,monthArray[month], year  ].join('-');
        else
            return [day.padStart(2,0),monthArray[month], year ].join('-'); 
        //if(!language && (!dateFlag || dateFlag==1))
            
        
    }
    else{
        return '';
    }
}



module.exports = {
    OptionalKey,
    OptionalKeyString,
    Convert2String,
    Convert2Number,
    EncryptAES,
    DecryptAES,
    EncryptBase64,
    DecryptBase64,
    HMAC256Encrypt,
    createStringAmpersand,
    TxnHashVerification,
    RandomNumGenerator,
    formatReadableDate,
    formatDate,
    getFullDateString,
    ConvertDecimalToPaddingZeros,
    maskInstrumentString
}