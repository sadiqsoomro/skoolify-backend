const bodyParser = require('body-parser'),
    morgan = require('morgan'),
    timeout = require('express-timeout-handler'),
    shutdownHandler = require('shutdown-handler'),
    helmet = require("helmet"),
    routes = require('./api/v1/routes/routes-index'),
    
    appConfig = require('./app-config'),
    db = require('./db'),
    port = process.env.PORT || 3000,
    cors = require('cors');
    require('dotenv').config();
    import Error from './api/v1/response-classes/error'

    module.exports = function (app) {

        app.use(bodyParser.urlencoded({ limit: '10mb', extended: true, parameterLimit:1000000 }))
        app.use(bodyParser.json({ limit: '10mb', extended: true }))
        app.use(cors())
        app.use(timeout.handler(options)) //this will uncommented when gone to production. this sets timeout on request
        app.use(morgan('dev'))
        app.use(helmet())
        
        routes(app)
        configureDb(app)

        shutdownHandler.on('exit', function () {
            console.log("Shutdown...")
        })
    }

var options = {
    timeout: appConfig.APP_TIMEOUT,
    onTimeout: function (req, res) {
        if(!res.finished)if(!res.finished)res.status(500).json({response: new Error(appConfig.GENERIC_ERROR_MSG, "request timeout.")
        })
    },

}





function configureDb(app) {
    db.getConnection(function(err, db) {

        if (err) {

            if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                console.error('Database connection was closed.')
            }
            if (err.code === 'ER_CON_COUNT_ERROR') {
                console.error('Database has too many connections.')
            }
            if (err.code === 'ECONNREFUSED') {
                console.error('Database connection was refused.')
            }
            else {
                console.error('Unable to connect to the database:');
                console.error(err.code)
                console.log("Shutdown...")
                process.exit()   
                
            }
            db.on('error', function(err) {      
                throw err;
                return;     
          });
        }
        else{
            console.log('Connection has been established successfully.')
            try{
                db.query(`show status like 'questions';`,
                function (error, data, fields) {
                    if(!error){
                        console.log(`Total Questions = ${data[0].Value}`)
                    }
                    else{
                        console.log(error)
                    }
                })       
                app.listen(port)
            }
            finally{
                db.release();
                console.log('released')
            }
            
        }

            
            
      });
      
    
    
}


